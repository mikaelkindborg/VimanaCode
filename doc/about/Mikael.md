# About Me (Mikael Kindborg)

## Computer Programming

I have worked with computer programming and software development for over 40 years, coding in more th 30 programming languages. The languages I hjave liked the most are the dynamic high-level ones, such as Lisp, Smalltalk, PHP, and Javascript.

My current professional work centers on development of embedded systems in enterprise settings. This means I get to do low-level C programming, which I find fascinating. This has inspired my to implement my own programming language, which I call VimanaCode.

Interestingly, the programming languages that counts for me are quite old by now (perhaps because I am also old). C is around 55 years old (1970). Forth is even older; it evolved with Chuck Moore during the 1960s and was made public in 1970, the same year as C appeared.

The we have Lisp, the crown jewel of programming languages. The origins of Lisp goes back to 1960 (two years before I was born). That makes Lisp around 65 years old. I should also mention Smalltalk, with the first version in 1971/1972.

## Steampunk

I have found that I enjoy the intersection of high-level and low-level. Steampunk is an example of this, such as featurimg steam engines in a SciFi setting. Forth is a true Steampunk programming language, being high-level and low-level at the same time. The same goes for my own language.

Developing my own Steampunk programming language has became a hobby of mine. The name "Vimana" is from ancient Vedic technology. I have implemented many versions of this language. The most recent implementation is Loop Machine, a small interpreter implemented in C.

## Programming the Human Mind

I also practice programming of the human mind. I am finding out how to refocus my thinking and reprogram myself. What I have found is that others have been programming me during my entire life. This is now changing. Recent 10 years I have developed a higher degree of awareness and control of my thinking mind, thanks to various meditation techniques. I have been greatly helped by mindfulness, and practices like Yoga Nidra. I believe that the techniques I use are very old, and were used in ancient times as a part of the daily life.

## Ancient Mantras

Ancient Mantras is a meditation game I have developed. You can read about it at my website:
[kindborg.com](https://kindborg.com)
