# DietPi Setup

My notes about installing DietPi on Raspberry Pi Zero.

Date: 2024-08-02

# Documentation

Software documentation:

    https://dietpi.com/docs/software/
    https://dietpi.com/docs/software/cloud/#__tabbed_14_1
    https://dietpi.com/docs/software/file_servers/#__tabbed_2_1

## Install DietPi

Use v6 image:

    DietPi_RPi-ARMv6-Bookworm.img.xz

## Install and Config

    dietpi-launcher

    dietpi-config

    dietpi-explorer

    dietpi-drive_manager

    dietpi-autostart

    dietpi-cron

    dietpi-update

    cpu

https://dietpi.com/docs/dietpi_tools/

## Setup WiFi

Use the installer menu to make network settings (SSDI and pass).
Accept default settings where applicable.

## Install Software

    dietpi-software

Select Browser software in the installer.

Install the following:

    Xfce (also installs python3)
    (Git)
    (Gogs)
    File Browser
    Samba Server
    CUPS
    (Samba Client)
    (Midnight Commander)

Alternatively: LXDE/OpenBox (LightDM)

## Running the System

Reboot after installation is done (startx may fail otherwise):

    reboot

After reboot, start X desktop with:

    startx

## Users

Create user:

    sudo adduser username
    sudo usermod -aG sudo username  // add admin rights
    cat /etc/passwd                 // list users
    cat /etc/passwd | grep home     // list human users
    sudo dietpi-launcher            // launch as admin

Reboot before starting xwindows:

    reboot
    ...
    startx

https://dietpi.com/forum/t/default-users-on-dietpi/1373

https://www.reddit.com/r/debian/comments/l3kjhf/xauthority_file_does_not_exist_how_do_i_startx/

It's a group problem... here's the recipe for a user "mac". As "root" do:

    usermod -aG cdrom,floppy,sudo,audio,dip,video,plugdev,netdev,bluetooth <username>

Delete user:

    sudo deluser username
    sudo deluser --remove-home username
    cat /etc/passwd | grep username
    sudo pkill -u username

## Commands

Use DietPi-Drive_Manager to mount USB drives, and Samba/NFS drives

Set boot screen terminal font:

    dpkg-reconfigure console-setup (select Terminus Bold)

Install packages:

    apt install build-essential
    apt install netsurf-gtk
    apt install lua (specify version number if needed)
    apt install geany
    (apt install luakit)
    (apt install dillo)
    (apt install falkon)
    (apt install gpg) (file encryption)

Light editors:

    featherpad
    leafpad
    mousepad
    gedit
    greny

Clear terminal:

    clear
    clear -x
    reset
    CTRL+L
    CTRL+SHIFT+K

Various commands:

    cpu
    free -m
    df -h
    uname -a
    top
    swapon -s

    shutdown -h now
    poweroff
    halt
    reboot

Set swapfile in the dietpi-config tool, or possibly on the command line:

    /boot/dietpi/func/dietpi-set_swapfile 1024

Various apt commands:

    apt update
    apt upgrade
    apt full-upgrade
    apt dist-upgrade

    apt list --installed
    dpkg-query -l
    dpkg -l

    apt autoremove
    apt autoclean
    apt clean

Manage services:

    systemctl stop apache2


sudo apt-get install libsdl2-dev

File Browser access:

    http://192.168.121.145:8084/files/

Samba access:

    smb://192.168.121.145

Find IP-address:

    hostname -I

Install/config software for Pi OS Lite:

    sudo raspi-config
    sudo tasksel
    sudo setupcon

Manual install:

    sudo apt install xserver-org (--no-installer-recommends)
    sudo apt install raspberrypi-ui-mods
    sudo apt install xinit (for startx)

Installation links:

    https://opensource.com/article/20/6/custom-raspberry-pi
    https://www.paulligocki.com/add-gui-to-raspberry-pi-os-lite/
    https://www.taillieu.info/index.php/internet-of-things/raspberrypi/389-raspbian-lite-with-rpd-lxde-xfce-mate-i3-openbox-x11-gui
    https://www.geeksmint.com/web-browsers-for-raspberry-pi/
    https://wiki.debian.org/DesktopEnvironment#How_it_works

    https://wiki.libsdl.org/SDL2/Installation
    https://stackoverflow.com/questions/57672568/sdl2-on-raspberry-pi-without-x
    https://forums.raspberrypi.com/viewtopic.php?t=58180
    https://raspberrypi.stackexchange.com/questions/42628/has-anyone-managed-to-get-sdl-2-3-working-without-x-in-raspbian

Install SDL:

    https://wiki.libsdl.org/SDL2/Installation

Install everything necessary to build programs that use SDL:

    sudo apt install build-essential
    sudo apt install libsdl2-dev
    gcc `sdl-config --cflags` `sdl-config --libs` -o sdltest main.cpp
    ./sdltest

    sudo apt update
    sudo apt upgrade
    sudo apt full-upgrade
    sudo apt autoremove

    sudo apt install build-essential git

    sudo apt update
    sudo apt install libsdl2-2.0-0 libsdl2-dev
    sudo apt install libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev libsdl2-net-dev
