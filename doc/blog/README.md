# Vimana Blog

VimanaCode, or Vimana for short, is a dynamic programming language influenced by Forth and Lisp. I view the language as an experimental art project.

(Work in progress - lots of notes commented out)

<!--
DIY (Do It Yourself)

Steampunk Programming

Two schools of computation, compile and runtime.

Compute as much as possible beforehand (static, early bound)
Push as much as possible to runtime (dynamic, late bound)

booleanValue ifTrue (...)    // evaluate precompute code
booleanValue (...) ifTrue    // evaluate the result of a runtime computation

Also level of abstraction with respect to hardware, and runtime error checking

The computer processor is made for executing code and computing things (dynamic behaviour)


def double (2 *)
fun double (2 *)

: double (2 *) ;
fun double (2 *) def
name double (2 *) def
name double (2 *) fun
(double) (2 *) def


(sandwich) (
  bread slice
  cheese slice
  putontop
  ham cut
  potontop
  serve
) recepie


Name of the procedure goes first
Then you put stuff on the table, then do things with them

Prefix assigment function

: sandwich (
  bread slice
  cheese slice
  putontop
  ham cut
  putontop
  serve
  me
  eat
)

Consuming elements on the stack, functions consume elements


: Letters (a b c)

: Foo 42

42 -> Foo

(
  bread slice
  cheese slice
  putontop
  ham cut
  putontop
  serve
)
-> sandwich

fun double (
  2 *
)

name double (
  2 *
) def

name Foo 42 set
name double get

42 : Foo
42 set Foo
42 -> Foo
42 (Foo) set

(42) list with one element
(Foo) quoted symbol Foo

OP_QUOTED_SYMBOL
OP_LIST
OP_NUMBER
OP_SYMBOL
OP_FUNCALL
OP_PRIMFUN


(2 *) : double


ifzero
isbigger
a b isgreater
isless

x ifzero () else ()
x () () ifZeroElse
ifTrueElse
ifFalseElse
ifGreaterElse
ifLessElse

Frequent OP_CODES goes in main switch in 8 bit tag.
Custom or less frequent codes goes in OP_EXTRA or something.

Parser can use string table for symonyms with mapping to primfun id.

A-Z = global variable (except for primfuns) - Color Forth

DYNAMIC ProgMem/CodeBlock
FIXED StringMem
FIXED ListMem
FIXED SymbolTable (index -> value, name)
InstrIndex (index in ProgMem)
FIXED CallStack
FIXED DataStack (swap rotate A B C _) grows right to left
FIXED LocalStack
  $push $pop
  $A $B $C

resetListMem symbol - clear memory from this list or string

Parse - write to progmem
  Alloc block of memory
  Parse text and write to progmem
  Write lists to ListMem

: twice (2 *)

OP_LISTBEGIN
OP_INTNUM (2)
OP_TIMES
OP_LISTEND

: Foo (a (b) c)

## List with child references

OP_LISTBEGIN
OP_SYMBOL (a)
OP_LIST (index)
OP_SYMBOL (c)
OP_LISTEND

OP_LISTBEGIN
OP_SYMBOL (b)
OP_LISTEND

MemAddr ParseList()
{
  MemBuf data;

  // Parse tokens in list
  while (not list end)
  {
    // read token

    if (token is list)
    {
      addr = ParseList();
      // add addr of list to data
    }
    else
    {
      // add parsed token to data
    }
  }

  // Write data to ProgMem
  addr = writedata (data)

  // Return list address
  return addr;
}

---

## List with children embedded

OP_LISTBEGIN (endindex)
OP_SYMBOL (a)
OP_LISTBEGIN (endindex)
OP_SYMBOL (b)
OP_LISTEND
OP_SYMBOL (c)
OP_LISTEND

index ParseList()
{
  int listhead;

  // Write OPLIST_BEGIN to ListMem

  // Parse tokens in list
  while (not list end)
  {
    // read token

    if (token is list)
    {
      // Write list to ListMem
      index = ParseList();
    }
    else
    {
      // Write parsed token to ListMem
    }
  }

  // Write OP_LISTEND tyo ListMem

  // Write listend index to OP_LISTBEGIN value
  listhead = index of listend
}

Parse()
{
  while (input)
  {
    read next token, skip space
    if ("(" == token)
    {
      ParseList(); // Write to ListMem
    }
    else
    {
      WriteTokenToTemporaryProgMem(); // number, primfun, symbol, funbol
    }
  }

  return codeBlock; // Progmem, executable code block
}


---

Callstack contains address of where to return execution
OP_LISTEND calls that address
Address zero exits the interpreter loop OP_EXIT
ifTrueElse writes the next instruction to the callstack??? (tailcall?)


SymTable
  symbolIndex value stringIndex

Strings are written to string memory


1 2 +

2 A
1 B

BA swap
CAB rotate
CBA swapover, swaap
A dup
B over
C oover

Loop Machine - high level virtual machine inspired by Forth (and a little Lisp)

Vimana Code - interpreter inspired by Forth and Lisp in the spirit of Lisp, interpreter for a concatenative language inspired by Lisp

(fact) (
  ifZeroElse
    (_ 1)
    (A 1- fact *)
) def

: fact
  (ifZeroElse
    (_ 1)
    (A 1- fact *))

Try/explore different styles/formatting (blogpost):

[ num fact -> num ]
: fact
  (ifZero (_ 1 ^)
   A 1- fact *)

[ num fact -> num ]
: fact
  (ifZero (_ 1 return)
   A sub1 fact mult)

[ list num times -> ]
: times
  (ifZero (_ _ return)
   B eval sub1 tail times)

[ list num times -> ]
: times
  (ifZero (_ _ ^)
   B eval 1- tail times)




(fact) (
    (_ 1)
    (A 1- fact *)
  ifZeroElse
) def

42 : Foo

tailcall
taileval

(foo) (
  isZero
  ifTrueElse
) def

(fact) (
    (drop then 1)
    (dup 1- fact *)
  ifZeroElse
) def

(fact) (
  ifZero
    (drop then 1)
  else
    (dup 1- fact *)
) def

def fact (
  ifZero
    (drop then 1)
  else
    (dup 1- fact *)
)

/-- list n times -> --/
((drop drop)
 (over eval 1- times)
    ifZeroElse)
: times

: times
  (ifZeroElse
   (_ _)
   (B eval 1- times))



(fact)
  (dup iszero (drop 1) (dup 1- fact *) ifelse) def

(times)
  (dup iszero
    (drop drop)
    (over eval 1- times)
  ifelse) def

(times) (
  isZero ifTrueElse
    (_ _)
    (B eval sub1 times)
) def


## Language characteristics

- Minimalistic syntax - the only hardcoded syntactic elements are whitespace, list parens and string parens
- The language consists of symbols, numbers, strings, and lists
- The syntax uses postfix notation (as in Forth, which is opposite of the Lisp prefix notation)
- Items enclosed in a list are not evaluated - this is like quote in Lisp
- The postfix order is very strict (Forth sometimes has prefix functions, in Vimana all functions are postfix)
- There is no loop construct, instead tail recursion is used for iteration (similar to e.g. Erlang)
- Like in Forth there is a data stack (parameter stack) that is used to pass arguments and return values from functions
- There are global variables, but no local variables - the data stack is used for local data
- Stack operations are used to manipulate items on the data stack (dup, drop, swap, etc.)
- A form of local variables (registers) is implemeted in Vimana itself (can also be implemented by the interpreter for better performance)
- There are list functions similar to those in Lisp (cons, first, rest, etc.)
- Memory management is based on Lisp lists (cons cells) with garbage collection

## Design goals

The Vimana project has several design goals:

- Create a dynamic language where you code directly in the abstract syntax tree (like Lisp)
- Code and data are identical (homoiconic)
- Minimalistic and consistent syntax
- Easy to implement interpreter (postfix syntax, interpret the abstract syntax tree)
- Easy to understand the implementation
- Small source code footprint for the interpreter
- Make the interpreter as fast possible given the above criteria

My motivation for a minimalistic and dynamic language is that this is what I find attractive in a programming language. I am very fond of the idea that the syntax directly mirrors what is executed by the interpreter.

An important goal is to make it easy to understand the implementation, both for myself and for others. Vimana is intended to be a DIY (Do It Yourself) programming language. Like a dynamic piece of art that you can alter and experiment with to create new forms.

Some of the goals are in conflict with each other. To make the interpreter fast, for example, might require some tricks that add complexity to the implementation. Execution speed was not at all an initial priority, but I became obsessed with making the C implementation fast when I discovered that it was possible to reach good performance.

-->
