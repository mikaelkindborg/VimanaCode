# Languages I have programmed in

    HP RPL (HP-25 calculator) (1979)
    Asta (1982)
    COBOL (1982)
    Simula (1983)
    CS4 (1983)
    SQL (1983)
    MacLisp (1983)
    Prolog (1983)
    Logo (1984)
    muLisp (1985)
    Pascal (1985)
    C (Windows 1.0) (1985)
    Interlisp-D (1986)
    Loops (1987)
    HyperTalk (HyperCard) (1988)
    Smalltalk-80 (1988)
    Scheme (1988)
    Lingo (Director) (1989)
    C++ (1994)
    PowerBuilder (1994)
    Visual Basic
    Delphi
    Java (1996)
    Awk
    JavaScript (1996)
    PHP (1998)
    ToonTalk (1999)
    Squeak (Smalltalk)
    GameMaker
    CommonLisp
    Erlang
    Oz
    Python
    Perl
    Bash
    C# (2008)
    Ruby (2010)
    Lua (2010)
    Objective C (2014)
    AppleScript
    PostScript
    VimanaCode (2021-2024)
    Plus a bunch of concatenative languages
    that I have read about but not used