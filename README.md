# VimanaCode - a minimalistic programming language

__Updated January 22, 2024__

## Introduction

VimanaCode, or Vimana for short, is an interpreted dynamic programming language influenced by Forth and Lisp. It is a DIY (Do It Yourself) language, intended to the modified and experimented with. I view VimanaCode as an experimental art project.

I am currently working on a Javascript implementation called __VimanaJS__, which is available in the __dev__ folder. My goal is to have this version ready for testing during February 2025.

## Blog

I have a blog about VimanaCode where I post articles about programming of mind and machines: [vimanacode.substack.com](https://vimanacode.substack.com/)

## Implementations

I am working on the following implementations of VimanaCode:

* VimanaJS - Javascript implementation (active development)
* VimanaPy - Python implementation (forthcoming)
* VimanaC - C implementation (forthcoming)
* VimanaSteam - List-based high-performance implementation (forthcoming)
* LoopMachine - Array-based high-performance implementation (forthcoming)

## A Taste of VimanaCode

This is a code example that illustrates the nature of the programming language:

    [fib]
      [:a 1 isSmaller ifTrueTail:
        [:a 1 - fib ^b 2 - fib +]] !def

    37 fib print

The syntax is postfix (as in Forth). The **!def** function takes two parameters off the data stack, a quoted name (symbol) and a value, and binds the name to the value. In this example the symbol **fib** is bound to a list that contains code. Symbols inside a list are not evaluated, and are thus "quoted".

When called, the **fib** function takes one parameter off the data stack and puts back the result on the stack, which is then printed. The function **:a** makes a copy of the topmost element on the stack (same as "dup" in Forth), and the function **^b** moves the second element to the top (same as "swap" in Forth).

Rounded parens can also be used, giving the syntax a more Lisp-like flavour:

    (fib)
      (:a 1 isSmaller ifTrueTail:
        (:a 1 - fib ^b 2 - fib +)) !def

    37 fib print

## Syntactic Styles

Originally used the Lisp-like syntax with rounded parens, but with __LoopMachine__ I began using square brackets. This was intended to a give the language a "steampunk" look and feel. Postfix notation is also a steampunk syntax in my view.

I have not really been able to make up my mind about which style I like best. Therefore, both paren styles are now supported in VimanaJS, and it is possible to mix them if desired. My plan is to use this freedom of paren syntax in the other implementatations as well.

Here is a blog article about [Steampunk proggramming languages](https://vimanacode.substack.com/p/steampunk-programming-languages).

Steampunk = future old tech (such as a SciFi world based on steam engine technology)

Vimanas = Vedic flying machines. VimanaCode is a programming language for Steampunk and ancient technologies.

## History

2025-01-22: The repository structure has been updated for improved clarity. The intension is to make Vimana avaiable for others to test and use during 2025. Work on VimanaJS is ongoing, and my goal is to have it ready for testing during February 2025.

2024-05-06: This repository contains ongoing work and code frequently changes. I have commented out much of the text in this file since it is outdated.

2024-05-05: Lots of work recent weeks on improving the LoopMachine interpreter and making it faster. LoopMachine is a Steampunk flavoured interpreter that use fixed length lists (arrays) to represent code and data. This is not as flexible as linked Lisp lists, but performance is very good. My plan is to complete three versions and make them accessible for use:

* LoopMachine (uses arrays)
* VimanaSteam (uses linked lists)
* VimanaC (rewrite of the original C interpreter that uses linked lists).

These versions are going to have different characteristics and performance.

2024-04-06: Next goal to rewrite the VimanaC interpreter based on the experiments with Loop Machine. Planning to do two versions, one with fixed lists (arrays) and one with Lisp lists (cons cells). Both of these will likely be more influenced by Forth than previous versions.

2024-04-04: Created a first version of the **Loop Machine** interpreter to test performance. Discovered that performance on the Apple M1 processor differs a lot from that of Intel i3. VimanaC is not faster on the M1, but GForth and Lua are much faster on this processor. Started investigating, looks like dispatch via switch is twice as fast compared to function calls (0.60s vs 1.12s).

2024-03-30: Copy-Eval for interactive programming of running code. Very interesting concept. Will develop further!

2024-03-12: Working on bindings to the SDL2 library for multimedia programming.

2023-09-23: Moving repository to GitLab.

2023-02-08: A simplified C implementation has been added. This version uses array indexes instead of full pointers for the next field of an item. This slows down performance a bit, but reduces the complexity of the code. The interpreter should be easier to port to 16 and 32 bit platforms (should run out-of-the-box on 32 bit systems).

2023-02-04: The C implementation has been added. It is optimized for speed, and is the fastest implementation I have came up with so far (help me improve). Alternative, simplified versions will follow, but will be slower.

2022-01-26: I am in the process of updating this repository. I have cleaned up the master branch and have moved experimental code and personal notes to a working branch. Currently, only the Python implementation of Vimana is in the master branch. I plan to add the C and the JavaScript implementation as well, when I have cleaned them up.

2021-04-14: The first commit. A very simple interpreter in PHP.

## Background - Lisp and Forth

I did a lot of Lisp programming in the 1980s. I really enjoyed Lisp. The InterLisp-D system was like magic. Lots of things happened in the 80s. PCs became available and Windows was introduced. The big thing was Macintosh computer, and the Amiga was also great fun.Interactive programming environments became popular. Apple started shipping HyperCard with every Mac they sold, and Smalltalk became available on Macs and PCs. It was a truly wonderful time to be a programmer.

Lisp and Smalltalk have always been my favourite programming languages. Over the years, I have worked in well over 35 different programming languages. I have had some ideas for a programming language, but it was not until Spring 2021 that I made a serious effort to implement my own language.

For some reason, I started looking at Forth when beginning to design my language. Back in school, I had a Hewlett & Packard calculator that used postfix notation (Reverse Polish Notation). I really liked that calculator. It had a solid, heavy feel to it, with distinct buttons, and I recall it had a certain smell to it. Furthermore, it was a programmable calculator. At the time, I did not really understand what a programming language was. Now I see the similarities with languages like Forth and PostScript.

When looking into Forth, I discovered that there are lots of languages that use postfix notation, so called concatenative languages. I have not programmed anything serious in Forth, but I can clearly see that Forth is a very special language. Importantly, it is a minimalistic language. I really like taking a minimalistic design and build on that.

## Retro computing and the Vimanas

In several ways, VimanaCode is a retro project. Lisp (1960) and Forth (developed during the 60ies, first public version 1970) are among the first programming languages created. Importantly, they are both interactive languages. I never programmed much Basic, but it too used to be a highly interactive language, on the Commodore C64 for example.

The name "Vimana" originates in the descriptions of the flying machines mentioned in the ancient Vedic texts. I am fascinated by ancient history, and when coming up with a name for my language, I went with "VimanaCode" to make it distinctly searchable. But I often just call the language "Vimana".

Were the Vimanas programmable? If so, what programming language did they use? We don't know, but I would envision a highly dynamic minimalistic programming language, that allowed the crew to reprogram the craft during the flight.

## Design goals

Vimana has several design goals:

- Enjoyable hobby project for the coming years - like a model railroad with a steam train, but instead it is a programmable machine running on computers
- Create a dynamic language where you code directly in the abstract syntax tree (like Lisp)
- Code and data are identical (homoiconic)
- The executed code is the exact same code as is represented in the syntax tree (isomorphic)
- Minimalistic and consistent syntax
- Easy to implement interpreter (postfix syntax, interpret the syntax tree directly)
- Easy to understand implementation (clear code style, comments)
- Small source code footprint for the interpreter
- Make the interpreter as fast possible given the above criteria

My motivation for a minimalistic and dynamic language is that this is what I find attractive in a programming language. I am very fond of the idea that the syntax directly mirrors what is executed by the interpreter.

An important goal is to make it easy to understand the implementation, both for myself and for others. Vimana is intended to be a DIY (Do It Yourself) programming language. Like a dynamic piece of art that you can alter and experiment with to create new forms.

Some of the goals are in conflict with each other. To make the interpreter fast, for example, might require some tricks that add complexity to the implementation. Execution speed was not at all an initial priority, but I became obsessed with making the C implementation fast when I discovered that it was possible to reach good performance.

## License

License: Creative Commons: Attribution-ShareAlike - CC BY-SA

I view VimanaCode as an art project.

<!--

## Get started

The Python implementation of Vimana is the most recent, and this is the recommended one try out. The C implementation is also available

### Explore VimanaPy

Explore the content of the folder [vimanapy](vimanapy/) to try out the Python implementation.

This screenshot shows an example of using the REPL for VimanaCode implemented Python:

![Vimana REPL](doc/images/vimana_repl.png)

Here are examples from a basic IDE implemented in tkinter:

![Vimana Workbench 1](doc/images/vimana_workbench_1.png)

![Vimana Workbench 2](doc/images/vimana_workbench_2.png)

In the IDE you can select any text and evaluate it as code (using CTRL-E or CMD-E).

### Explore VimanaC

Explore the content of the folder [vimanac](vimanac/) to try out the C implementations.

## Implementation languages

- Python (see folder vimanapy)
- C (high performance interpreter, see folders [vimanac/vimanac_fast](vimanac/vimanac_fast/) and [vimanac/vimanac_index](vimanac/vimanac_index/))
- JavaScript (to be added)
- PHP (very early version, now obsolete, needs a rewrite to be up-to-date)

There is a branch named "work" that contains the code I have been working on. That branch is my personal working copy that is separate from the master branch. You can look at it if you are curious, but the code is experimental and not written for others to read.

## Language characteristics

- Minimalistic syntax - the only hardcoded syntactic elements are whitespace, list parens and string parens
- The language consists of symbols, numbers, strings, and lists
- The syntax uses postfix notation (as in Forth, which is opposite of the Lisp prefix notation)
- Items enclosed in a list are not evaluated - this is like quote in Lisp
- The postfix order is very strict (Forth sometimes has prefix functions, in Vimana all functions are postfix)
- There is no loop construct, instead tail recursion is used for iteration (similar to e.g. Erlang)
- Like in Forth there is a data stack (parameter stack) that is used to pass arguments and return values from functions
- There are global variables, but no local variables - the data stack is used for local data
- Stack operations are used to manipulate items on the data stack (dup, drop, swap, etc.)
- A form of local variables (registers) is implemeted in Vimana itself (can also be implemented by the interpreter for better performance)
- There are list functions similar to those in Lisp (cons, first, rest, etc.)
- Memory management is based on Lisp lists (cons cells) with garbage collection

## Code examples

Print a string:

    {Hi World} print

Add two numbers:

    1 2 + print

Define and call a function:

    (square) (dup *) def

    21 square print

Recursive factorial function:

    (fact)
      (dup iszero (drop 1) (dup 1 - fact *) ifelse) def

    20 fact print

## Self-modifying code

Like Lisp, Vimana represents both code and data as lists. In Vimana, the code being interpreted is the exact same code as is reflected by the source code. Code can be generated and modified at runtime.

As an example of this, we will look at how to implement something like a closure with a counter in Vimana. There are no lexically scoped local variables in Vimana, but something like a closure can be created using self-modifying code.

Here is a code example:

    {This example demonstrates something similar to a closure
    coded using self-modifying code.} print

    (makecounter)
      (0 () cons (dup dup first 1 + setfirst first) cons) def

    (counter) makecounter def

    {Here is the newly created counter function:} print

    (counter) getglobal print

    {Call the counter function three times:} print

    counter print
    counter print
    counter print

    {Now the counter function looks like this:} print

    (counter) getglobal print

This is what it looks like when the above code is evaluated:

![Vimana REPL](doc/images/vimana_closure.png)

In this example, the first instruction in the function is a list that is used to hold the value of the counter. When the function is called, it increments the number that is the single of the list and pushes that number to the data stack.

Note that the dup (duplicate) function calls do not copy the list, they copy a reference to the list. So it is the same list that is manipulated in the code - the list that is the first instruction in the code of the function. It would not have worked to simply put a number as the first item in the function. Numbers are copied when pushed onto the data stack and when duplicated, and that would have made it impossible to update the actual number element in the function. A list with a number is therefore used to get a reference that can be shared and updated.

I should also clarify that when the interpreter sees a list (or a number or string), it simply pushes it onto the data stack, whithout evaluating it. When the interpreter sees a global variable, it evaluates that variable, and if it is a function, it gets called (pushed onto the call stack). This is how the Vimana interpreter operates.

The above example shows that it is possible to do things differently when stepping outside of the box of mainstream programming languages.

## Background - Lisp and Forth

I did a lot of Lisp programming in the 1980s. I really enjoyed Lisp. The InterLisp-D system was like magic. Lots of things happened in the 80s. PCs became available and Windows was introduced. The big thing was Macintosh computer, and the Amiga was also great fun.Interactive programming environments became popular. Apple started shipping HyperCard with every Mac they sold, and Smalltalk became available on Macs and PCs. It was a truly wonderful time to be a programmer.

Lisp and Smalltalk have always been my favourite programming languages. Over the years, I have worked in well over 30 different programming languages. I have had some ideas for a programming language, but it was not until Spring 2021 that I made a serious effort to implement my own language.

For some reason, I started looking at Forth when beginning to design my language. Back in school, I had a Hewlett & Packard calculator that used postfix notation (Reverse Polish Notation). I really liked that calculator. It had a solid, heavy feel to it, with distinct buttons, and I recall it had a certain smell to it. Furthermore, it was a programmable calculator. At the time, I did not really understand what a programming language was. Now I see the similarities with languages like Forth and PostScript.

When looking into Forth, I discovered that there are lots of languages that use postfix notation, so called concatenative languages. I have not programmed anything serious in Forth, but I can clearly see that Forth is a very special language. Importantly, it is a minimalistic language. I really like taking a minimalistic design and build on that.

## Retro computing and the Vimanas

In several ways, VimanaCode is a retro project. Lisp and Forth are among the first programming languages created. Importantly, they are both interactive languages. I never programmed much Basic, but it too used to be a highly interactive language, on the Commodore C64 for example.

The name "Vimana" originates in the descriptions of the flying machines mentioned in the ancient Vedic texts. I am fascinated by ancient history, and when coming up with a name for my language, I went with "VimanaCode" to make it distinctly searchable. But I most often just call the language "Vimana".

Were the Vimanas programmable? If so, what programming language did they use? We don't know, but I would envision a highly dynamic minimalistic programming language, that allowed the crew to reprogram the craft during the flight.

## Design goals

The Vimana project has several design goals:

- Create a dynamic language where you code directly in the abstract syntax tree (like Lisp)
- Code and data are identical (homoiconic)
- Minimalistic and consistent syntax
- Easy to implement interpreter (postfix syntax, interpret the abstract syntax tree)
- Easy to understand the implementation
- Small source code footprint for the interpreter
- Make the interpreter as fast possible given the above criteria

My motivation for a minimalistic and dynamic language is that this is what I find attractive in a programming language. I am very fond of the idea that the syntax directly mirrors what is executed by the interpreter.

An important goal is to make it easy to understand the implementation, both for myself and for others. Vimana is intended to be a DIY (Do It Yourself) programming language. Like a dynamic piece of art that you can alter and experiment with to create new forms.

Some of the goals are in conflict with each other. To make the interpreter fast, for example, might require some tricks that add complexity to the implementation. Execution speed was not at all an initial priority, but I became obsessed with making the C implementation fast when I discovered that it was possible to reach good performance.

## License

License: Creative Commons: Attribution-ShareAlike - CC BY-SA

I view VimanaCode as an art project.

## Benchmark

Here is a benchmark for the C implementation of Vimana, compared to other interpreted languages.

The benchmark program is the recursive Fibonacci function computing the Fibionacci number for 37. I have used this particular benchmark since I wanted to test function call performance.

Vimana interprets the abstract syntax tree directly, it is not a bytecode interpreter. Still, performace is good, and I attribute this to the postfix evaluation order which is somewhat similar to how a bytecode virtual machine works. The code is fully homoiconic and can be manipulated at runtime.

### Results

Test machine: MacBook Air 2020 1 GHz Dual-Core Intel Core i3

    VimanaC Fast     2.21s (next field is full pointer)
    VimanaC Index    2.46s (next field is index address)
    VimanaC Core     4.63s (uses list memory for stacks)

    PHP 8.1.5:       1.98s
    Ruby 2.6.3:      2.11s
    gForth 0.7.3:    2.58s
    Lua 5.4.4:       2.58s
    Lox:             2.81s
    Wren 0.4.0:      3.31s
    PostScript:      3.66s (GPL Ghostscript 9.56.1)
    Python 3.11.0b3  3.73s
    Awk:             11.17s
    Perl v5.30.2:    13.03s
    AppleScript:     35.27s

The MacBook Air 2020 is tricky to use for benchmarking since it has inefficient processor cooling. This causes the processor to throttle (slow down) when under load. I have tried to run the above benchmark tests under comparable conditions (cold machine).

### Benchmark source code

See directory [vimanac/benchmark/](vimanac/benchmark/) for benchmark source files.

VimanaCode:

    (fib)
      (dup 1 > (dup 1- fib swap 2- fib +) iftrue) def

    37 fib print

PHP:

    function fib($n)
    {
      if ($n < 2) return $n;
      return fib($n - 1) + fib($n - 2);
    }

    echo fib(37) . "\n";

Ruby:

    def fib(n)
      if n < 2 then
        n
      else
        fib(n - 1) + fib(n - 2)
      end
    end

    puts fib(37)

gForth:

    : fib ( n1 -- n2 )
      dup 1 > if
        dup 1- recurse swap 2 - recurse +
      then ;

    37 fib . cr

    bye

Lua:

    function fib(n)
      return n < 2 and n or fib(n - 1) + fib(n - 2)
    end

    print(fib(37))

Lox:

    fun fib(n) {
      if (n < 2) return n;
      return fib(n - 2) + fib(n - 1);
    }

    print fib(37);

Wren:

    class Fib {
      static get(n) {
        if (n < 2) return n
        return get(n - 1) + get(n - 2)
      }
    }

    System.print(Fib.get(37))

PostScript:

    /fib { dup 1 gt { dup 1 sub fib exch 2 sub fib add } if } bind def

    37 fib =

    gs -q -dBATCH fib.ps

Python:

    def fib(n):
      if n < 2: return n
      return fib(n - 1) + fib(n - 2)

    print(fib(37))

Awk:

    awk 'func fib(n){return(n<2?n:fib(n-1)+fib(n-2))} BEGIN {print fib(37)}'

Perl:

    sub fibRec {
        my $n = shift;
        $n < 2 ? $n : fibRec($n - 1) + fibRec($n - 2);
    }

    print(fibRec(37));

AppleScript (osascript):

    on fib(n)
      if n < 1 then
        0
      else if n < 3 then
        1
      else
        fib(n - 2) + fib(n - 1)
      end if
    end fib

    fib(37)
-->
