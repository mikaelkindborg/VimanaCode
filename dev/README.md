# Development Code

__Updated January 22, 2024__

This directory contains code that is in development.

I am working on the following interpreters for VimanaCode:

* VimanaJS - Javascript implementation (active development)
* VimanaPy - Python implementation (forthcoming)
* VimanaC - C implementation (forthcoming)
* VimanaSteam - List-based high-performance implementation (forthcoming)
* LoopMachine - Array-based high-performance implementation (forthcoming)

I intend to have VimanaJS ready for others to test during Februari 2025.

My intention is to make the code in this directory useful for others, and maintain and document the code (this is however currently not the case).
