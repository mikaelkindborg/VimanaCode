/*
File: vimana.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Vimana interpreter.
*/

#include "machine.h"

// --------------------------------------------------------------
// Main
// --------------------------------------------------------------

#define PROGMEM_MAXSIZE     500
#define DATASTACK_MAXSIZE   50
#define CALLSTACK_MAXSIZE   50
#define SYMBOLTABLE_MAXSIZE 100
#define SYMBOLMEM_MAXSIZE   500

int main(int numargs, char* args[])
{
  printf("Welcome to the Wonderful World of Vimana\n");

  TypeMachine* machine = MachineAlloc(
    PROGMEM_MAXSIZE,
    DATASTACK_MAXSIZE,
    CALLSTACK_MAXSIZE,
    SYMBOLTABLE_MAXSIZE,
    SYMBOLMEM_MAXSIZE);

  MachineAddCFuns(machine);

  //char prog[] = "[][1 2 + [3 4] [] (Foo) {Hi} print] 42 Bar[][]";
  //char prog[] = "[]";
  //char prog[] = "sayHi sayMantra [42 {Hi}] print";
  //char prog[] = "1 2 + -> Foo Foo print [foo] [42] : foo print [foo] [10 +] 32 foo print";
  char prog[] = "[fib] [dup 1 isSmaller ifTrueTail [dup 1- fib swap 2- fib +]] : 37 fib print";
  //char prog[] = "0 dup 1 isSmaller dup print ifTrueTail [42 print] 43 print";
  //char prog[] = "0 ifTrueTail [printstack 42 printstack] 43 printstack";

  TypeItem head = ParseString(machine, prog);

  //DebugPrintProgMem(machine, "PROGMEM");
  //DebugPrintSymbolTable(machine, "SYMBOLTABLE");

  //DebugPrintList(machine, "HEAD", head);
  MachinePrint(machine, head);
  PrintNewLine();

  MachineEval(machine, head);

  DebugPrintDataStack(machine, "DATASTACK");

  MachineFree(machine);

  return 0;
}

