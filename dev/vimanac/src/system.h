/*
File: system.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Unix/Linux system functions.

Libc manual:
https://www.gnu.org/software/libc/manual/html_node/index.html#SEC_Contents

Various links:
https://www.reddit.com/r/C_Programming/comments/144npye/when_two_different_processes_are_accessing_the/
https://www.gnu.org/software/libc/manual/html_node/File-Locks.html
https://www.ict.griffith.edu.au/teaching/2501ICT/archive/guide/ipc/flock.html
https://stackoverflow.com/questions/7573282/how-do-i-lock-files-using-fopen
https://picolisp.com/wiki/?ArrayAbstinence

Terminal handling:
https://en.wikibooks.org/wiki/Serial_Programming/termios
https://en.wikipedia.org/wiki/ANSI_escape_code
https://viewsourcecode.org/snaptoken/kilo/02.enteringRawMode.html
https://github.com/Cubified/tuibox
https://invisible-island.net/xterm/ctlseqs/ctlseqs.html
https://man.freebsd.org/cgi/man.cgi?query=termios&sektion=4
https://github.com/antirez/linenoise
*/

#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>


// Forward declaration
void StringBufferSetAt(TypeIndex handle, TypeIndex index, char c);
void StringBufferEnsureSize(TypeIndex handle, TypeIndex index);

// Run a system command and write response to buffer.
// Return number of bytes written to buffer
// Return -1 on error
TypeIndex SystemCommand(char* command, TypeIndex handle)
{
  // Run command
  FILE* stream = popen(command, "r");
  if (NULL == stream) { return -1; }

  // Read input stream into buffer
  int c = fgetc(stream);
  int i = 0;
  while (EOF != c)
  {
    StringBufferEnsureSize(handle, i);
    StringBufferSetAt(handle, i, c);
    c = fgetc(stream);
    ++ i;
  }

  // Zero terminate string
  StringBufferEnsureSize(handle, i);
  StringBufferSetAt(handle, i, 0);

  // Close input stream
  pclose(stream);

  return i; // Bytes written
}

// Run a system command and send data as a string
TypeBool SystemCommandSendData(char* command, char* data)
{
  FILE* stream = popen(command, "w");
  if (stream)
  {
    FileStreamWrite(stream, data);
    pclose(stream);
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

unsigned long UnixMillis()
{
  struct timeval timestamp;

  gettimeofday(&timestamp, NULL);

  // Make timestamp smaller (for 32 bit systems)
  unsigned long secondsoneyear = (60 * 60 * 24 * 365);
  unsigned long millis =
    ((timestamp.tv_sec % secondsoneyear) * 1000) +
    (timestamp.tv_usec / 1000);

  return millis;
}

void UnixSleep(int millis)
{
  int seconds = millis / 1000;
  int micros = (millis % 1000) * 1000;
  sleep(seconds);
  usleep(micros);
}

// Return random integer between 0 and n (exclusive)
int UnixRandom(int n)
{
  return rand() % n;
}
