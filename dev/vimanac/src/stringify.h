/*
File: stringify.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Functions for printing lists and items.
*/

// -------------------------------------------------------------
// Functions used during stringify to write a string
// -------------------------------------------------------------

typedef struct
{
  TypeItem  Buffer;
  TypeIndex Pos;
}
TypeBufferWriter;

void BufferWriterInit(TypeBufferWriter* writer)
{
  writer->Buffer = BufferAlloc(BUFFER_CHUNK_SIZE, OpString);
  writer->Pos = 0;
}

void BufferWriterPutChar(TypeBufferWriter* writer, char c)
{
  BufferEnsureSize(&(writer->Buffer), writer->Pos);
  BufferSetByteAt(writer->Buffer, writer->Pos, c);
  ++ (writer->Pos);
}

void BufferWriterBack(TypeBufferWriter* writer)
{
  -- (writer->Pos);
}

// -------------------------------------------------------------
// Stringify functions
// -------------------------------------------------------------

void StringifyChar(TypeBufferWriter* writer, char c)
{
  //printf("%c", c);
  BufferWriterPutChar(writer, c);
}

void StringifyString(TypeBufferWriter* writer, char* string)
{
  char* p = string;
  while (0 != *p)
  {
    StringifyChar(writer, *p);
    ++ p;
  }
}

void StringifyInteger(TypeBufferWriter* writer, TypeWord n)
{
  char buf[32];
  sprintf(buf, "%ld", (long)n);
  StringifyString(writer, buf);
}

void StringifyVimanaString(TypeBufferWriter* writer, char* string)
{
  StringifyChar(writer, STRINGBEGIN);
  StringifyString(writer, string);
  StringifyChar(writer, STRINGEND);
}

// Forward declaration
void StringifyList(TypeMachine* machine, TypeBufferWriter* writer, TypeItem head);

void StringifyItem(TypeMachine* machine, TypeBufferWriter* writer, TypeItem item)
{
  switch (ItemOp(item))
  {
    case OpList:
      StringifyList(machine, writer, item);
      StringifyChar(writer, ' ');
      break;

    case OpInteger:
      StringifyInteger(writer, ItemData(item));
      StringifyChar(writer, ' ');
      break;

    case OpCFun:
      StringifyString(writer, "[CFun] ");
      break;

    case OpSymbol:
    case OpFunction:
    {
      TypeIndex index = ItemData(item);
      char* name = machine->SymbolNameTable[index];
      StringifyString(writer, name);
      StringifyChar(writer, ' ');
      break;
    }

    case OpHandle:
    {
      TypeIndex index = ItemData(item);
      TypeItem buffer = MachineProgMemRead(machine, index);
      if (OpString == ItemOp(buffer))
      {
        StringifyVimanaString(writer, BufferPtr(buffer));
        StringifyChar(writer, ' ');
      }
      else
      {
        StringifyString(writer, "[Buffer] ");
      }
      break;
    }

    default:
      GURU_MEDITATION("Unknown type (StringifyItem)");
      break;
  }
}

void StringifyList(TypeMachine* machine, TypeBufferWriter* writer, TypeItem head)
{
  StringifyChar(writer, LEFTPAREN);

  // Get index of first item in list
  TypeIndex index = ItemData(head);
  if (index) // if (!ListEmpty(head))
  {
    while (index)
    {
      TypeItem item = MachineProgMemRead(machine, index);
      StringifyItem(machine, writer, item);
      index = ItemNext(item);
    }

    // Skip the trailing space after the last item in the list
    BufferWriterBack(writer);
  }

  StringifyChar(writer, RIGHTPAREN);
}

// --------------------------------------------------------------
// Public functions
// --------------------------------------------------------------

// Remember to deallocate the returned buffer
TypeItem MachineStringify(TypeMachine* machine, TypeItem item)
{
  TypeBufferWriter writer;
  BufferWriterInit(&writer);
  StringifyItem(machine, &writer, item);
  return writer.Buffer;
}

void MachinePrint(TypeMachine* machine, TypeItem item)
{
  TypeItem buffer = MachineStringify(machine, item);
  printf("%s", (char*)BufferPtr(buffer));
  BufferFree(buffer);
}

void PrintNewLine()
{
  printf("\n");
}
