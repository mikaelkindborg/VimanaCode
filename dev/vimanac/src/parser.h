/*
File: parser.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Parser for Vimana Code.
*/

// -------------------------------------------------------------
// Functions used during parsing for reading a stream
// -------------------------------------------------------------

typedef struct
{
  FILE*     Stream;
  // Prevent last read char from being lost
  int       LastChar;
  TypeBool  UseLastChar;
}
TypeStreamReader;

void StreamReaderInit(TypeStreamReader* reader, FILE* stream)
{
  reader->Stream = stream;
  reader->UseLastChar = FALSE;
}

void StreamReaderUseLastChar(TypeStreamReader* reader)
{
  reader->UseLastChar = TRUE;
}

int StreamReaderNextChar(TypeStreamReader* reader)
{
  if (reader->UseLastChar)
    { reader->UseLastChar = FALSE; }
  else
    { reader->LastChar = fgetc(reader->Stream); }
  return reader->LastChar;
}

// --------------------------------------------------------------
// Syntactic elements
// --------------------------------------------------------------

#define IsNotEOF(c)       (EOF != (c))
#define IsLeftParen(c)    (LEFTPAREN == (c))
#define IsRightParen(c)   (RIGHTPAREN == (c))
#define IsStringBegin(c)  (STRINGBEGIN == (c))
#define IsStringEnd(c)    (STRINGEND == (c))
#define IsCommentBegin(c) (COMMENTBEGIN == (c))
#define IsCommentEnd(c)   (COMMENTEND == (c))
#define IsQuoteComment(c) (COMMENTQUOTE == (c))
#define IsWhiteSpace(c) \
  ((' ' == (c)) || ('\t' == (c)) || ('\n' == (c)) || ('\r' == (c)))
#define IsSeparator(c) \
  (IsLeftParen(c) || IsRightParen(c) || IsStringBegin(c) || IsStringEnd(c))
#define IsWhiteSpaceOrSeparator(c) (IsWhiteSpace(c) || IsSeparator(c))

// --------------------------------------------------------------
// Parsing functions
// --------------------------------------------------------------

void SkipComment(TypeStreamReader* reader)
{
  int level = 1; // Support for nested comments
  int c = StreamReaderNextChar(reader);
  while (IsNotEOF(c))
  {
    if (IsCommentBegin(c)) { ++ level; }
    if (IsCommentEnd(c))   { -- level; }
    if (level < 1)         { break; }

    c = StreamReaderNextChar(reader);
  }
}

void SkipQuoteComment(TypeStreamReader* reader)
{
  int c = StreamReaderNextChar(reader);
  while (!IsQuoteComment(c) && IsNotEOF(c))
  {
    c = StreamReaderNextChar(reader);
  }
}

// Literal strings are allocated dynamically using malloc
TypeItem ParseStringLiteral(TypeMachine* machine, TypeStreamReader* reader)
{
  int level = 1;  // Support for nested strings
  int i = 0;      // String index

  // Allocate string buffer
  TypeItem buffer = BufferAlloc(BUFFER_CHUNK_SIZE, OpString);

  // Read characters
  int c = StreamReaderNextChar(reader);
  while (IsNotEOF(c))
  {
    if (IsStringBegin(c)) { ++ level; }
    if (IsStringEnd(c))   { -- level; }
    if (level < 1)        { break; }

    // Write char to string buffer
    BufferEnsureSize(&buffer, i);
    BufferSetByteAt(buffer, i, c);
    ++ i;

    // Read next char
    c = StreamReaderNextChar(reader);
  }

  // Terminate string
  BufferEnsureSize(&buffer, i);
  BufferSetByteAt(buffer, i, 0);

  // Write buffer item to ProgMem
  TypeIndex bufferIndex = MachineProgMemAlloc(machine);
  MachineProgMemWrite(machine, bufferIndex, buffer);

  // Return handle to string buffer
  return ItemWith(bufferIndex, 0, OpHandle);
}

char* ParseReadToken(int c, TypeStreamReader* reader)
{
  // static local variable = hidden global
  static char Token[MAX_TOKEN_LENGTH];

  // Write chars to token buffer
  int i = 0;
  int end = 0;

  // Read token
  while (IsNotEOF(c) && !IsWhiteSpaceOrSeparator(c))
  {
    // Max token length includes the zero-terminator
    if (i < MAX_TOKEN_LENGTH - 1)
    {
      // Write char to token buffer
      Token[end] = c;
      ++ end;
    }
    ++ i;
    // Read next char
    c = StreamReaderNextChar(reader);
  }

  // We have consumed the last character
  // Set flag saying to use the last kept character
  StreamReaderUseLastChar(reader);

  // Zero terminate token buffer
  Token[end] = 0;

  return Token;
}

// Get the token type
// Symbols begin with an uppercase letter
// Functions begin with anything that is not an uppercase letter
int ParseOpType(char* token)
{
  if (StringIsInteger(token))
    { return OpInteger; }
  else
  if (isupper(token[0]))
    { return OpSymbol; }
  else
    { return OpFunction; }
}

TypeItem ParseInteger(char* token)
{
  TypeWord data = (TypeWord) strtol(token, NULL, 10);
  return ItemWith(data, 0, OpInteger);
}

TypeItem ParseSymbol(TypeMachine* machine, char* token, int optype)
{
  // Look up index in the symbol table
  TypeIndex index = MachineFindSymbolIndex(machine, token);
  if (-1 == index)
  {
    // Add new symbol
    index = MachineAddSymbol(machine, token);
  }

  TypeItem item = MachineGetSymbolValue(machine, index);
  if (OpCFun == ItemOp(item))
  {
    return item;
  }

  return ItemWith(index, 0, optype);
}

TypeItem ParseToken(TypeMachine* machine, char c, TypeStreamReader* reader)
{
  TypeItem item;

  char* token = ParseReadToken(c, reader);
  int optype = ParseOpType(token);
  if (OpInteger == optype)
    { item = ParseInteger(token); }
  else
    { item = ParseSymbol(machine, token, optype); }

  return item;
}

// Return list item
TypeItem ParseList(TypeMachine* machine, TypeStreamReader* reader)
{
  // Intialize head to empty list
  TypeItem  head = ItemWith(0, 0, OpList);

  // Local variables
  TypeItem  item;
  TypeIndex prevItemIndex;
  TypeBool  addItem = FALSE;
  TypeBool  setFirst = TRUE;

  int c = StreamReaderNextChar(reader);

  while (IsNotEOF(c))
  {
    addItem = FALSE;

    if (IsWhiteSpace(c))
    {
      // Skip whitespace
    }
    else
    if (IsQuoteComment(c))
    {
      SkipQuoteComment(reader);
    }
    else
    if (IsCommentBegin(c))
    {
      SkipComment(reader);
    }
    else
    if (IsStringBegin(c))
    {
      item = ParseStringLiteral(machine, reader);
      addItem = TRUE;
    }
    else
    if (IsLeftParen(c)) // List begin
    {
      // Parse child list
      item = ParseList(machine, reader);
      addItem = TRUE;
    }
    else
    if (IsRightParen(c)) // List end
    {
      // Exit parse loop
      goto ExitParseLoop;
    }
    else
    // Otherwise it is a symbol or an integer
    {
      item = ParseToken(machine, c, reader);
      addItem = TRUE;
    }

    // Add item
    if (addItem)
    {
      // Write item
      TypeIndex itemIndex = MachineProgMemAlloc(machine);
      MachineProgMemWrite(machine, itemIndex, item);

      if (setFirst)
      {
        // Set first of head (head points to first item in list)
        head = ItemWith(itemIndex, 0, OpList);
        setFirst = FALSE;
      }
      else
      {
        // Set next of previous item
        TypeItem prevItem = MachineProgMemRead(machine, prevItemIndex);
        MachineProgMemWrite(machine, prevItemIndex,
          ItemWith(ItemData(prevItem), itemIndex, ItemOp(prevItem)));
      }

      // Save index of previous item
      prevItemIndex = itemIndex;
    }

    // Read next char
    c = StreamReaderNextChar(reader);
  }
  // while

ExitParseLoop:
  return head;
}

// --------------------------------------------------------------
// Public functions
// --------------------------------------------------------------

TypeItem ParseStream(TypeMachine* machine, FILE* stream)
{
  TypeStreamReader reader;
  StreamReaderInit(&reader, stream);
  TypeItem head = ParseList(machine, &reader);
  return head;
}

// If cannot read file return item with type OpUndefined
TypeItem ParseFile(TypeMachine* machine, char* fileName)
{
  FILE* stream = FileOpen(fileName, "r");
  if (NULL == stream)
  {
    return ItemWith(0, 0, OpUndefined);
  }
  else
  {
    TypeItem head = ParseStream(machine, stream);
    FileClose(stream);
    return head;
  }
}

TypeItem ParseString(TypeMachine* machine, char* program)
{
  FILE* stream = FileOpenStringStream(program);
  TypeItem head = ParseStream(machine, stream);
  FileClose(stream);
  return head;
}
