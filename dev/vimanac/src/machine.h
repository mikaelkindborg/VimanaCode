/*
File: machine.h
Author: Mikael Kindborg (mikael@kindborg.com)

Vimana interpreter.

Example use:

  #define PROGMEM_MAXSIZE     500 // Max num items in program memory
  #define DATASTACK_MAXSIZE   50  // Max num items on the data stack
  #define CALLSTACK_MAXSIZE   50  // Max callstack depth
  #define SYMBOLTABLE_MAXSIZE 100 // Max num symbols (including CFuns)
  #define SYMBOLMEM_MAXSIZE   500 // Max bytes in symbol mem

  TypeMachine* machine = MachineAlloc(
    PROGMEM_MAXSIZE,
    DATASTACK_MAXSIZE,
    CALLSTACK_MAXSIZE,
    SYMBOLTABLE_MAXSIZE,
    SYMBOLMEM_MAXSIZE);

  MachineAddCoreCFuns(machine);

  char prog[] = "[Hi World] print 1 2 + print";
  TypeItem head = ParseString(machine, prog);

  MachinePrint(machine, head);
  PrintNewLine();

  MachineEval(machine, head);

  MachineFree(machine);
*/

// --------------------------------------------------------------
// Include system libraries
// --------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// --------------------------------------------------------------
// Guru Meditation (error handling)
// --------------------------------------------------------------

// Exit program on error
#define GURU_MEDITATION(message) \
  printf("[GURU_MEDITATION] %s (%s: %i)\n", message, __FILE__, __LINE__); \
  exit(1);

// --------------------------------------------------------------
// Syntax elements
// --------------------------------------------------------------

#define LEFTPAREN    ('[')
#define RIGHTPAREN   (']')
#define STRINGBEGIN  ('{')
#define STRINGEND    ('}')
#define COMMENTBEGIN ('(')
#define COMMENTEND   (')')
#define COMMENTQUOTE ('"')

// --------------------------------------------------------------
// Op codes
// --------------------------------------------------------------

enum OpCodes
{
  OpReturn = 0,
  OpExit,
  OpInteger,
  OpSymbol,
  OpList,
  OpFunction,
  OpCFun,
  OpHandle,
  OpString,
  OpBuffer,
  OpUndefined
};

char* OpCodeNames[] =
{
  "OpReturn",
  "OpExit",
  "OpInteger",
  "OpSymbol",
  "OpList",
  "OpFunction",
  "OpCFun",
  "OpHandle",
  "OpString",
  "OpBuffer",
  "OpUndefined"
};

// -------------------------------------------------------------
// Data types and structs
// -------------------------------------------------------------

#define TAG_BITS              4
#define TAG_MASK              15
#define MAX_TOKEN_LENGTH      32   // Max length for symbol names
                                   // (31 chars + 1 zero-terminator char)
#define BUFFER_CHUNK_SIZE     64   // Parser chunk size for literal strings

typedef long TypeWord;  // Used for a word of data
typedef int  TypeIndex; // Used for index and size
typedef int  TypeBool;

#define TRUE  1
#define FALSE 0

typedef struct __TypeMachine TypeMachine;
typedef void (*TypeCFun) (TypeMachine*);

// Linked list element (Lisp style)
typedef struct __TypeItem
{
  TypeWord Data; // Holds data, e.g an integer
  TypeWord Next; // Holds index of next item and the opcode
}
TypeItem;

// List item access
#define ItemNext(item)   (((item).Next) >> TAG_BITS)
#define ItemOp(item)     (((item).Next) & TAG_MASK)
#define ItemData(item)   ((item).Data)

// Return an item struct
// TODO: Guard max value of next
TypeItem ItemWith(TypeWord data, TypeWord next, TypeWord op)
{
  TypeItem item = { data, (next << TAG_BITS) | op };
  return item;
}

// Interpreter VimanaMachine
// TODO: Rename machine -> interp
// (a machine has (many) interpreters (engines))
typedef struct __TypeMachine
{
  // Program memory
  TypeItem*   ProgMem;
  TypeIndex   ProgMemFirstFree;
  TypeIndex   ProgMemMaxSize;

  // Data stack
  TypeItem*   DataStack;
  TypeIndex   DataStackTop;
  TypeIndex   DataStackMaxSize;

  // Call stack
  TypeIndex*  CallStack;
  TypeIndex   CallStackTop;
  TypeIndex   CallStackMaxSize;

  // Symbol table
  TypeItem*   SymbolTable;
  char**      SymbolNameTable;
  TypeIndex   SymbolTableFirstFree;
  TypeIndex   SymbolTableMaxSize;

  // String memory
  char*       SymbolMem;
  TypeIndex   SymbolMemFirstFree;
  TypeIndex   SymbolMemMaxSize;

  // Current instruction (index in ProgMem)
  TypeIndex   InstrIndex;
}
TypeMachine;

// -------------------------------------------------------------
// Program memory access
// -------------------------------------------------------------

#define MachineProgMemRead(machine, index) \
  ((machine)->ProgMem[index])

void MachineProgMemWrite(TypeMachine* machine, TypeIndex index, TypeItem item)
{
  machine->ProgMem[index] = item;
}

// --------------------------------------------------------------
// Include helper libraries
// --------------------------------------------------------------

#include "debug.h"
#include "alloc.h"
#include "file.h"
#include "string.h"
#include "buffer.h"

// -------------------------------------------------------------
// Interpreter alloc/free
// -------------------------------------------------------------

TypeMachine* MachineAlloc(
  TypeIndex ProgMemMaxSize,
  TypeIndex DataStackMaxSize,
  TypeIndex CallStackMaxSize,
  TypeIndex SymbolTableMaxSize,
  TypeIndex SymbolMemMaxSize)
{
  TypeIndex machineSize =
    sizeof(TypeMachine) +
    ProgMemMaxSize +
    DataStackMaxSize +
    CallStackMaxSize +
    SymbolTableMaxSize +
    SymbolMemMaxSize;

  TypeMachine* machine = MemAlloc(machineSize);
  if (NULL == machine)
  {
    GURU_MEDITATION("Cannot allocate Machine");
  }

  machine->ProgMemMaxSize = ProgMemMaxSize;
  machine->DataStackMaxSize = DataStackMaxSize;
  machine->CallStackMaxSize = CallStackMaxSize;
  machine->SymbolTableMaxSize = SymbolTableMaxSize;
  machine->SymbolMemMaxSize = SymbolMemMaxSize;

  machine->ProgMemFirstFree = 2;
  machine->DataStackTop = -1;
  machine->CallStackTop = 0;
  machine->SymbolTableFirstFree = 0;
  machine->SymbolMemFirstFree = 0;

  char* address = (void*)machine;
  address += sizeof(TypeMachine);

  machine->ProgMem = (void*)address;
  address += sizeof(TypeItem) * ProgMemMaxSize;

  machine->DataStack = (void*)address;
  address += sizeof(TypeItem) * DataStackMaxSize;

  machine->CallStack = (void*)address;
  address += sizeof(TypeIndex) * CallStackMaxSize;

  machine->SymbolTable = (void*)address;
  address += sizeof(TypeItem) * SymbolTableMaxSize;

  machine->SymbolNameTable = (void*)address;
  address += sizeof(char*) * SymbolTableMaxSize;

  machine->SymbolMem = (void*)address;
  address += sizeof(char) * SymbolMemMaxSize;

  return machine;
}

void MachineFree(TypeMachine* machine)
{
  // This will dealloc memory blocks pointer to by TypeBuffer items
  //ProgMemSweep(machine);

  MemFree(machine);
}

// -------------------------------------------------------------
// Program memory allocation
// -------------------------------------------------------------

// Return index to allocted element
TypeIndex MachineProgMemAlloc(TypeMachine* machine)
{
  if (machine->ProgMemFirstFree >= machine->ProgMemMaxSize)
    { GURU_MEDITATION("ProgMem out of memory"); }

  return (machine->ProgMemFirstFree) ++;
}

// -------------------------------------------------------------
// Symbol table
// -------------------------------------------------------------

#define SymbolUndefinedGuardMacro(machine, value, symbolIndex) \
  if (OpUndefined == ItemOp(value)) { \
    printf("[ERROR] SymbolUndefined: %s\n", MachineFindSymbolName(machine, symbolIndex)); \
    GURU_MEDITATION("Symbol value is undefined"); }

#define MachineGetSymbolValue(machine, index) \
  ((machine)->SymbolTable[index])

#define MachineSetSymbolValue(machine, index, value) \
  ((machine)->SymbolTable[index] = (value))

char* MachineWriteSymbol(TypeMachine* machine, char* name);

// Return symbol table index of new symbol
TypeIndex MachineAddSymbol(TypeMachine* machine, char* name)
{
  if (machine->SymbolTableFirstFree >= machine->SymbolTableMaxSize)
  {
    GURU_MEDITATION("Symbol table out of memory");
  }

  // Initialize symbol value to undefined
  machine->SymbolTable[machine->SymbolTableFirstFree] = ItemWith(0, 0, OpUndefined);
  char* symbol = MachineWriteSymbol(machine, name);
  machine->SymbolNameTable[machine->SymbolTableFirstFree] = symbol;

  return (machine->SymbolTableFirstFree) ++;
}

TypeIndex MachineFindSymbolIndex(TypeMachine* machine, char* name)
{
  for (TypeIndex index = 0; index < machine->SymbolTableFirstFree; ++ index)
  {
    if (StrEquals(name, machine->SymbolNameTable[index]))
    {
      return index;
    }
  }

  return -1; // Not found
}

char* MachineFindSymbolName(TypeMachine* machine, TypeIndex index)
{
  return machine->SymbolNameTable[index];
}

// Return pointer to string in symbol memory
char* MachineWriteSymbol(TypeMachine* machine, char* string)
{
  TypeIndex symbolIndex = machine->SymbolMemFirstFree;
  int length = strlen(string);

  if ((machine->SymbolMemMaxSize - machine->SymbolMemFirstFree) < length)
    { GURU_MEDITATION("Out of symbol memory"); }

  // Copy string to symbol memory
  for (int i = 0; i < length; ++ i)
  {
    machine->SymbolMem[machine->SymbolMemFirstFree] = string[i];
    ++ (machine->SymbolMemFirstFree);
  }

  // Zero-terminate string
  machine->SymbolMem[machine->SymbolMemFirstFree] = 0;
  ++ (machine->SymbolMemFirstFree);

  return & (machine->SymbolMem[symbolIndex]);
}

// -------------------------------------------------------------
// C Functions
// -------------------------------------------------------------

void MachineAddCFun(TypeMachine* machine, char* name, TypeCFun fun)
{
  TypeIndex index = MachineAddSymbol(machine, name);
  machine->SymbolTable[index] = ItemWith((TypeWord)fun, 0, OpCFun);
}

// -------------------------------------------------------------
// Data stack
// -------------------------------------------------------------

#define DataStackGuardMacro(machine, n) \
  if (((machine)->DataStackTop - (n)) < -1) \
    { GURU_MEDITATION("DataStack underflow guard"); }

#define DataStackIncrMacro(machine) \
  ++ ((machine)->DataStackTop); \
  if ((machine)->DataStackTop >= (machine)->DataStackMaxSize) \
    { GURU_MEDITATION("DataStack overflow"); }

#define DataStackDecrMacro(machine) \
  -- ((machine)->DataStackTop); \
  if ((machine)->DataStackTop < -1) \
    { GURU_MEDITATION("DataStack underflow"); }

#define DataStackGetTop(machine) \
  ((machine)->DataStack[(machine)->DataStackTop])

#define DataStackSetTop(machine, value) \
  ((machine)->DataStack[(machine)->DataStackTop] = (value))

// -------------------------------------------------------------
// Call stack
// -------------------------------------------------------------

#define CallStackIncrMacro(machine) \
  ++ ((machine)->CallStackTop); \
  if ((machine)->CallStackTop > (machine)->CallStackMaxSize - 1) \
    { GURU_MEDITATION("CallStack overflow"); }

#define CallStackDecrMacro(machine) \
  -- ((machine)->CallStackTop);

// -------------------------------------------------------------
// Instruction index
// -------------------------------------------------------------

#define MachineIncrInstrIndex(machine) \
  ((machine)->InstrIndex = ItemNext((machine)->ProgMem[(machine)->InstrIndex]))

// -------------------------------------------------------------
// Evaluate a list
// -------------------------------------------------------------

// Param head is the list to be evaluated
void MachineEval(TypeMachine* machine, TypeItem head)
{
  // At the end of a list, Next is zero, which will
  // execute the instruction OpReturn
  machine->ProgMem[0] = ItemWith(0, 0, OpReturn);

  // When the top-level list returns, CallStackTop is zero,
  // which will execute the instruction OpExit
  machine->ProgMem[1] = ItemWith(0, 0, OpExit);
  machine->CallStack[0] = 1;
  machine->CallStackTop = 0;

  // -1 means DataDtack is empty
  machine->DataStackTop = -1;

  // Set InstrIndex to index of first item in list
  machine->InstrIndex = ItemData(head);

  while (1)
  {
    // Slow down interpreter sleep for 100 ms
    //int micros = 100 * 1000;
    //usleep(micros);
    //PrintIntNum(instruction);
    //PrintNewLine();

    TypeItem currentItem = MachineProgMemRead(machine, machine->InstrIndex);
    TypeWord op = ItemOp(currentItem);

    switch (op)
    {
      case OpUndefined:
        DebugPrintSymbolTable(machine, "OpUndefined");
        GURU_MEDITATION("OpUndefined: Symbol is unbound");
        break;
      case OpReturn:
        machine->InstrIndex = machine->CallStack[machine->CallStackTop];
        CallStackDecrMacro(machine)
        break;
      case OpExit:
        //machine->Run = FALSE;
        goto ExitEvalLoop;
        break;
      // Faster but less flexible
      case OpCFun:
      {
        TypeWord func = ItemData(currentItem);
        ((TypeCFun)(func))(machine);
        break;
      }
      // Call CFuns and functions
      case OpFunction: // OpFunction
      {
        // Get item in symbol table
        TypeWord symbolIndex = ItemData(currentItem);
        TypeItem value = MachineGetSymbolValue(machine, symbolIndex);
        SymbolUndefinedGuardMacro(machine, value, symbolIndex)
        /*if (OpCFun == ItemOp(value))
        {
          // Call CFun
          ((TypeCFun)ItemData(value))(machine);
        }
        else*/
        if (OpList == ItemOp(value))
        {
          // Save instruction index on return stack
          CallStackIncrMacro(machine)
          machine->CallStack[machine->CallStackTop] = ItemNext(currentItem);
          // Set instruction index to first in function list
          machine->InstrIndex = ItemData(value);
        }
        else
        {
          DebugPrintItem("Non-function value", value);
          GURU_MEDITATION("Attempt to call a non-function");
        }
        break;
      }
      case OpSymbol:
      {
        DataStackIncrMacro(machine)
        TypeWord symbolIndex = ItemData(currentItem);
        TypeItem value = MachineGetSymbolValue(machine, symbolIndex);
        SymbolUndefinedGuardMacro(machine, value, symbolIndex)
        DataStackSetTop(machine, value);
        MachineIncrInstrIndex(machine);
        //machine->InstrIndex = ItemNext(currentItem);
        break;
      }
      case OpInteger:
      case OpList:
      case OpHandle:
        DataStackIncrMacro(machine)
        DataStackSetTop(machine, currentItem);
        MachineIncrInstrIndex(machine);
        //machine->InstrIndex = ItemNext(currentItem);
        break;
      default:
        DebugPrintInt("OpCode", op);
        GURU_MEDITATION("Unexpected opcode");
        break;
    }
  }
  // while

ExitEvalLoop:
  ;
}

// --------------------------------------------------------------
// Include interpreter functions
// --------------------------------------------------------------

//#include "unix/system.h"
//#include "progmem.h"
#include "parser.h"
#include "stringify.h"
#include "cfuns.h"

// -------------------------------------------------------------
// Garbage collection
// -------------------------------------------------------------
/*
// Mark items referenced by global symbols
void InterpMarkGlobalVars(VInterp* interp)
{
  VListMemory* mem = InterpListMem(interp);
  VSymbolTable* table = InterpSymbolTable(interp);
  VByte* p = table->start;
  VByte* last = table->top;
  while (p <= last)
  {
    VSymbolEntry* entry = (VSymbolEntry*)(p);
    VItem* item = &(entry->value);

    // The items in the table should not be marked since they are
    // in non-gc memory, but any child elements need to be marked
    if (IsTypeWithChild(item))
    {
      ListMemMark(mem, ItemGetFirstItem(item));
    }

    p += sizeof(VSymbolEntry);
  }
}

// Mark items referenced by the data stack
void InterpMarkDataStack(VInterp* interp)
{
  VListMemory* mem = InterpListMem(interp);
  VDataStack* dataStack = InterpDataStack(interp);
  VByte* p = dataStack->start;
  VByte* last = dataStack->top;
  while (p <= last)
  {
    VItem* item = (VItem*)(p);

    // The items in the array should not be marked since they are
    // in non-gc memory, but any child elements need to be marked
    if (IsTypeWithChild(item))
    {
      ListMemMark(mem, ItemGetFirstItem(item));
    }

    p += sizeof(VItem);
  }
}

// Mark items referenced from the callstack
void InterpMarkCallStack(VInterp* interp)
{
  VListMemory* mem = InterpListMem(interp);
  VCallStack* callStack = InterpCallStack(interp);
  VByte* p = callStack->start;
  VByte* last = callStack->top;
  while (p <= last)
  {
    VStackFrame* frame = (VStackFrame*)(p);
    VItem* item = frame->instruction;
    ListMemMark(mem, item);
    p += sizeof(VStackFrame);
  }
}

// GC is not yet fully tested and integrated
void InterpGC(VInterp* interp)
{
  // Mark data stack
  InterpMarkDataStack(interp);

  // Mark global vars
  InterpMarkGlobalVars(interp);

  // Mark callstack
  InterpMarkCallStack(interp);

  // Sweep memory
  ListMemSweep(InterpListMem(interp));

  #ifdef TRACK_MEMORY_USAGE
  //ListMemPrintAllocCounter(InterpListMem(interp));
  #endif
}
*/
