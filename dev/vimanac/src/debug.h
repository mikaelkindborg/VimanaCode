/*
File: debug.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Debug functions
*/

// --------------------------------------------------------------
// Debug functions
// --------------------------------------------------------------

#define DebugPrint(string) printf("[%s]\n", string)
#define DebugPrintInt(label, i) printf("[%s: %i]\n", label, (int)i)
#define DebugPrintNewLine() printf("\n")

void DebugPrintItem(char* label, TypeItem item)
{
  printf("[%s] OpCode: %i data: %i next: %i\n",
    label, (int)ItemOp(item), (int)ItemData(item), (int)ItemNext(item));
}

void DebugPrintList(TypeMachine* machine, char* label, TypeItem head)
{
  TypeItem item;
  char labelBuffer[8];

  printf("[%s] List Data:\n", label);
  DebugPrintItem("List Head", head);

  TypeIndex index = ItemData(head);
  while (index)
  {
    item = MachineProgMemRead(machine, index);
    sprintf(labelBuffer, "%i", (int)index);
    DebugPrintItem(labelBuffer, item);
    index = ItemNext(item);
  }
}

void DebugPrintProgMem(TypeMachine* machine, char* label)
{
  char labelBuffer[8];

  printf("[%s] Program Memory:\n", label);
  int i = 0;
  while (i < machine->ProgMemFirstFree)
  {
    sprintf(labelBuffer, "%i", (int) i);
    DebugPrintItem(labelBuffer, MachineProgMemRead(machine, i));
    ++ i;
  }
}

void DebugPrintSymbolTable(TypeMachine* machine, char* label)
{
  char labelBuffer[MAX_TOKEN_LENGTH + 8];

  printf("[%s] Symbol Table:\n", label);
  int i = 0;
  while (i < machine->SymbolTableFirstFree)
  {
    sprintf(labelBuffer, "%i %s", (int) i, machine->SymbolNameTable[i]);
    DebugPrintItem(labelBuffer, machine->SymbolTable[i]);
    ++ i;
  }
}

void DebugPrintSymbolMem(TypeMachine* machine, char* label)
{
  printf("[%s] Symbol memory:\n", label);
  int i = 0;
  while (i < machine->SymbolMemFirstFree)
  {
    if (0 == machine->SymbolMem[i])
    {
      printf("\n");
    }
    else
    {
      printf("%c", machine->SymbolMem[i]);
    }
    ++ i;
  }
}

void DebugPrintDataStack(TypeMachine* machine, char* label)
{
  char labelBuffer[8];

  printf("[%s] Data stack:\n", label);
  for (TypeIndex i = 0; i <= machine->DataStackTop; ++ i)
  {
    TypeItem item = machine->DataStack[i];
    sprintf(labelBuffer, "%i", (int)i);
    DebugPrintItem(labelBuffer, machine->DataStack[i]);
  }
}
