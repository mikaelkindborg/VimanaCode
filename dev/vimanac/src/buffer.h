/*
File: buffer.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Dynamic memory buffer functions. Used for strings.
*/

// --------------------------------------------------------------
// Memory buffer functions
// --------------------------------------------------------------

// Allocate a buffer item
TypeItem BufferAlloc(TypeIndex size, TypeWord optype)
{
  char* buf = MemAlloc(size);
  if (NULL == buf)
    { GURU_MEDITATION("Out of memory (BufferAlloc)"); }

  // Buffer size is stored in the Next field
  return ItemWith((TypeWord)buf, size, optype);
}

void BufferFree(TypeItem buffer)
{
  MemFree((void*)ItemData(buffer));
}

TypeIndex BufferSize(TypeItem buffer)
{
  return ItemNext(buffer);
}

void* BufferPtr(TypeItem buffer)
{
  return (void*)ItemData(buffer);
}

// Get byte at buffer index
char BufferGetByteAt(TypeItem buffer, TypeIndex index)
{
  TypeIndex bufferSize = BufferSize(buffer);
  if ((index) >= bufferSize)
    { GURU_MEDITATION("Index is out bounds (BufferGetAt)"); }

  char* buf = (char*) BufferPtr(buffer);
  return buf[index];
}

// Set byte at buffer index
void BufferSetByteAt(TypeItem buffer, TypeIndex index, char c)
{
  TypeIndex bufferSize = BufferSize(buffer);
  if ((index) >= bufferSize)
    { GURU_MEDITATION("Index is out bounds (BufferSetAt)"); }

  char* buf = (char*) BufferPtr(buffer);
  buf[index] = c;
}

// Update/realloc buffer if needed to ensure that index fit
// in buffer. Note that param buffer is a pointer to an item.
void BufferEnsureSize(TypeItem* buffer, TypeIndex index)
{
  TypeIndex size = ItemNext(*buffer);
  if (index + 1 > size)
  {
    size = index + BUFFER_CHUNK_SIZE;
    char* buf = (char*) BufferPtr(*buffer);
    buf = MemRealloc(buf, size);
    if (NULL == buf)
      { GURU_MEDITATION("Out of memory (BufferEnsureSize)"); }

    *buffer = ItemWith((TypeWord)buf, size, ItemOp(*buffer));
  }
}
