/*
File: listmem.h
Author: Mikael Kindborg (mikael@kindborg.com)

Memory manager for Lisp-style linked lists.

See item.h for an explanation of memory layout.
*/

// -------------------------------------------------------------
// Forward declarations
// -------------------------------------------------------------

void InterpGC(VInterp* interp);

// -------------------------------------------------------------
// Global flags
// -------------------------------------------------------------

int ListMemVerboseGC = 0;

// -------------------------------------------------------------
// VListMemory struct
// -------------------------------------------------------------

// Addresses in list memory are pointer offsets (VAddr)

typedef struct __VListMemory
{
  VInterp* interp;        // Associated interpreter
  VByte*   start;         // Start of memory block
  VByte*   end;           // End of memory
  VByte*   nextFree;      // Next free item in memory block
  VItem*   freeList;      // First item (head) in free list

  #ifdef TRACK_MEMORY_USAGE
    int  allocCounter;
  #endif
}
VListMemory;

// -------------------------------------------------------------
// Initialize
// -------------------------------------------------------------

void ListMemInit(VListMemory* mem, VByte* start, int byteSize)
{
  mem->start = start;              // Start of address space
  mem->end = start + byteSize;     // End of address space
  mem->nextFree = start;           // Address of next free item
  mem->freeList = NULL;            // Freelist is empty

  #ifdef TRACK_MEMORY_USAGE
    mem->allocCounter = 0;
  #endif
}

#ifdef TRACK_MEMORY_USAGE
  void ListMemPrintAllocCounter(VListMemory* mem)
  {
    Print("ListMemAllocCounter: ");
    PrintIntNum(mem->allocCounter);
    PrintNewLine();
  }
#endif

// -------------------------------------------------------------
// Alloc and dealloc
// -------------------------------------------------------------

VBool ListMemIsFull(VListMemory* mem)
{
  return (NULL == mem->freeList) && (mem->nextFree >= mem->end);
}

// Allocate an item
VItem* ListMemAlloc(VListMemory* mem)
{
  VItem* item;

  // Garbage collect if all memory is allocated
  if (ListMemIsFull(mem))
  {
    // Garbage collect
    PrintLine("GARBAGE_COLLECTION");
    InterpGC(mem->interp);

    // Fail if all memory is still allocated
    if (ListMemIsFull(mem))
    {
      // In debug mode we don't halt, just return NULL
      #ifdef DEBUG
        PrintLine("[GURU_MEDITATION] DEBUG_ALLOC_ITEM_OUT_OF_MEMORY");
        return NULL;
      #else
        GURU_MEDITATION(GURU_ALLOC_ITEM_OUT_OF_MEMORY);
      #endif
    }
  }

  if (NULL != mem->freeList)
  {
    // ALLOCATE FROM FREELIST

    //PrintLine("ALLOC FROM FREELIST");

    item = mem->freeList;
    mem->freeList = ListMemGetNextItem(item);

    //printf("ITEM ADDRESS : %lu\n", (VIndex)item);
    //printf("FREELIST ADDR: %lu\n", (VIndex)(mem->freeList));
  }
  else
  {
    // ALLOCATE FROM UNUSED MEMORY

    if (mem->nextFree < mem->end)
    {
      //PrintLine("ALLOC FROM UNUSED MEMORY");

      item = (VItem*)(mem->nextFree);
      mem->nextFree += sizeof(VItem);
    }
  }

  #ifdef TRACK_MEMORY_USAGE
    ++ mem->allocCounter;
  #endif

  // Init and set default type
  ItemInit(item);

  return item;
}

void ListMemDeallocItem(VListMemory* mem, VItem* item)
{
  // Item must be allocated - type must not be None
  if (!IsTypeNone(item))
  {
    if (ListMemVerboseGC)
    {
      printf("Dealloc item of type: 0x%lX\n", (unsigned long) ItemGetType(item));
    }

    #ifdef TRACK_MEMORY_USAGE
      -- mem->allocCounter;
    #endif

    if (IsTypeBuffer(item))
    {
      // Free allocated buffer
      if (ItemGetPtr(item))
      {
        SysFree(ItemGetPtr(item));
      }
    }

    // Set type None and add deallocated item to freelist
    ItemSetNone(item);
    ListMemSetNextItem(item, mem->freeList);
    mem->freeList = item;
  }
}

// -------------------------------------------------------------
// Buffer items
// -------------------------------------------------------------

// Allocate two new items: one handle that can be copied, and a
// buffer that points to the allocated memory block. The memory
// block must be allocated with malloc() or a similar function.
//
// There must be only one instance of the item that points to
// allocated memory, and this item must NOT be shared; that is,
// it must not be present on the data stack or used for variables.
// If the raw pointer would be refereced from multiple items, the
// garbage collector would not be able to tell when it is unused.
//
// Ownership of bufferPtr goes to the memory manager.
// bufferPtr must be allocated with SysAlloc (malloc)
//
VItem* ListMemAllocHandle(VListMemory* mem, void* bufferPtr, VType bufferType)
{
  //printf("DEBUG: Alloc handle\n");

  // Allocate handle
  VItem* handle = ListMemAlloc(mem);
  ItemSetType(handle, TypeHandle);

  // Allocate buffer
  VItem* buffer = ListMemAlloc(mem);
  ItemSetType(buffer, TypeBuffer);

  // Set buffer pointer to allocated memory
  buffer->ptr = bufferPtr;

  // Set buffer type (this goes in the next field)
  ItemSetBufferType(buffer, bufferType);

  // Set handle to point to buffer
  ItemSetFirst(handle, buffer);

  // Return handle
  return handle;
}

// -------------------------------------------------------------
// Garbage collection
// -------------------------------------------------------------

void ListMemMark(VListMemory* mem, VItem* item)
{
  while (item)
  {
    if (ItemGetGCMark(item))
    {
      //PrintLine("ALREADY MARKED");
      return;
    }

    // Mark item
    ItemGCMarkSet(item);

    // Mark children
    if (IsTypeWithChild(item))
    {
      ListMemMark(mem, ItemGetFirstItem(item));
    }

    if (IsTypeBuffer(item))
    {
      // Buffer items use next field for type info,
      // there is never a next element
      item = NULL;
    }
    else
    {
      item = ListMemGetNextItem(item);
    }
  }
}

/*
// UNUSED: This function was used to mark the
// Items in the freelist, to prevent them from
// being cleared during sweep. Now TypeNone is
// used to distinguish items that shold not be
// deallocated (clearing them would destroy
// the freelist link chain).
//
// Mark free list - items in the free list
// should not be deallocated!
void ListMemMarkFreeList(VListMemory* mem)
{
  VItem* item = mem->freeList;

  while (item)
  {
    if (ItemGetGCMark(item))
    {
      return; // already marked
    }

    ItemGCMarkSet(item);

    // Mark next
    item = ListMemGetNextItem(item);
  }
}
*/

void ListMemSweep(VListMemory* mem)
{
  VByte* p = mem->start;
  VByte* end = mem->nextFree;

  while (p < end)
  {
    VItem* item = (VItem*)(p);

    if (ItemGetGCMark(item))
    {
      //PrintLine("MemSweep unmark");
      ItemGCMarkUnset(item);
    }
    else
    {
      //PrintLine("MemSweep dealloc");
      ListMemDeallocItem(mem, item);
    }

    p += sizeof(VItem);
  }
}
