/*
File: alloc.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024
*/

// --------------------------------------------------------------
// Memory allocation functions
// --------------------------------------------------------------

// Counter to keep track of allocations (useful for debugging)
int MemAllocCounter = 0;

void* MemAlloc(int numBytes)
{
  ++ MemAllocCounter;
  return malloc(numBytes);
}

void* MemRealloc(void* buffer, int numBytes)
{
  return realloc(buffer, numBytes);
}

void MemFree(void* buffer)
{
  -- MemAllocCounter;
  return free(buffer);
}
