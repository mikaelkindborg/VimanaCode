<?php

$NumLines = 0;
$NumLinesCode = 0;
$NumLinesComments = 0;
$NumLinesBlank = 0;
$NumLinesOpenCurly = 0;

/*
// Unused line count function (count all lines in a file)
function CountLines($file)
{
  global $NumLines;

  $NumLines += count(file($file));
}
*/

function StringContains($string, $substring)
{
  return false !== strpos($string, $substring);
}

function CountLines($file)
{
  global $NumLines;
  global $NumLinesCode;
  global $NumLinesComment;
  global $NumLinesBlank;
  global $NumLinesOpenCurly;

  $insideMultiLineComment = false;

  foreach (file($file) as $line):
    ++ $NumLines;
    // if inside multi-line comment
    if ($insideMultiLineComment):
      ++ $NumLinesComment;
      if (StringContains($line, "*/")):
        $insideMultiLineComment = false;
      endif;
    elseif (StringContains($line, "/*")):
      ++ $NumLinesComment;
      $insideMultiLineComment = true;
    elseif (preg_match('/^\s*\/\/./', $line)):
      ++ $NumLinesComment;
    elseif (0 == strlen(trim($line))):
      ++ $NumLinesBlank;
    //elseif (preg_match('/^\s*\{/', $line)):
    elseif ("{" === trim($line)):
      ++ $NumLinesOpenCurly;
    else:
      ++ $NumLinesCode;
    endif;
  endforeach;
}

$files = [
  "alloc.h",
  "buffer.h",
  "cfuns.h",
  "file.h",
  "machine.h",
  "parser.h",
  "string.h",
  "stringify.h",
];

foreach ($files as $file):
  CountLines($file);
endforeach;

echo "NumLinesSource:    " . ($NumLines) . "\n";
echo "NumLinesCode:      " . ($NumLinesCode) . "\n";
echo "NumLinesOpenCurly: " . ($NumLinesOpenCurly) . "\n";
echo "NumLinesComment:   " . ($NumLinesComment) . "\n";
echo "NumLinesBlank:     " . ($NumLinesBlank) . "\n";

/*
Code size 2024-05-19:
NumLinesSource:    1349
NumLinesCode:      705
NumLinesOpenCurly: 113
NumLinesComment:   333
NumLinesBlank:     198

Code size 2024-06-01:
NumLinesSource:    1720
NumLinesCode:      934
NumLinesOpenCurly: 142
NumLinesComment:   413
NumLinesBlank:     231
*/
