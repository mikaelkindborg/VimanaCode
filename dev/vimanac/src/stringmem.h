/*
File: stringmem.h
Author: Mikael Kindborg (mikael@kindborg.com)

Compact way to allocate immutable string within a block
of memory (no malloc of individual strings).

Memory layout:

    Strings are allocated in sequence
    0 (character '\0') is the string terminator

Example of memory layout with three string entries:

    One0Two0Tree0

Example use (save string):

    char* string = StringMemGetNextFree(mem);
    StringMemWriteChar(mem, 'H');
    StringMemWriteChar(mem, 'i');
    StringMemWriteFinish(mem);
    printf("%s\n", string);

Example use (temporary string):

    char* string = StringMemGetNextFree(mem);
    StringMemWriteChar(mem, 'H');
    StringMemWriteChar(mem, 'i');
    printf("%s\n", string);
    StringMemReset(mem);

*/

// -------------------------------------------------------------
// StringMem struct
// -------------------------------------------------------------

typedef struct __VStringMem
{
  char*  start;    // Start of string memory
  char*  nextFree; // Points to next free block in string memory
  char*  pos;      // Current position to write to
  VInt   size;     // Number of chars that can be stored in string memory
}
VStringMem;

// -------------------------------------------------------------
// StringMem functions
// -------------------------------------------------------------

// Initialize string memory
void StringMemInit(VStringMem* mem, VByte* start, int numChars)
{
  mem->start = (char*) start;
  mem->nextFree = mem->start;
  mem->pos = mem->nextFree;
  mem->size = numChars;
}

// Return pointer to next free block, the new string
// will be written here (current string)
char* StringMemGetNextFree(VStringMem* mem)
{
  return mem->nextFree;
}

// Finish writing to the current block
// (keep/save the current string)
void StringMemWriteFinish(VStringMem* mem)
{
  ++ mem->pos;
  mem->nextFree = mem->pos;
}

// Reset the write position to beginning of free block
// (free/discard the current string)
void StringMemReset(VStringMem* mem)
{
  mem->pos = mem->nextFree;
}

// Write a character to the current string
void StringMemWriteChar(VStringMem* mem, char c)
{
  if ((mem->start + mem->size) < (mem->pos + 2))
  {
    GURU_MEDITATION(GURU_STRING_MEMORY_OUT_OF_SPACE);
  }

  *(mem->pos) = c;
  ++ mem->pos;
  *(mem->pos) = '\0'; // Zero terminate string
}
