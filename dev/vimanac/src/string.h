/*
File: string.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

String functions
*/

// --------------------------------------------------------------
// String functions
// --------------------------------------------------------------

#define StrEquals(s1, s2) (0 == strcmp((s1), (s2)))

TypeBool StringIsInteger(char* string)
{
  TypeBool isNumber = FALSE;
  int      index = 0;

  // Check if the first character a minus sign
  if ('-' == string[index])
    { ++ index; }

  // Rest of string must have digits only
  while (0 != string[index])
  {
    isNumber = TRUE;
    if (!isdigit(string[index]))
      { isNumber = FALSE; break; }
    ++ index;
  }

  return isNumber;
}
