/*
File: cfuns.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Core C functions.
*/

// --------------------------------------------------------
// Set global symbol
// --------------------------------------------------------

// [name] value : ->
void CFun_def(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  // Get value/body
  TypeItem value = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  // Get symbol in list
  TypeItem list = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  TypeItem symbol = MachineProgMemRead(machine, ItemData(list));
  // Get symbol index
  TypeIndex symbolIndex = ItemData(symbol);
  // Set value for global symbol
  MachineSetSymbolValue(machine, symbolIndex, value);
  // Move to next instruction
  MachineIncrInstrIndex(machine);
}

// value -> symbolname ->
void CFun_set(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 1)
  // Get value on data stack
  TypeItem value = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  // Get symbol index from next instruction
  MachineIncrInstrIndex(machine);
  TypeItem symbol = MachineProgMemRead(machine, machine->InstrIndex);
  TypeIndex symbolIndex = ItemData(symbol);
  // Set value for global symbol
  MachineSetSymbolValue(machine, symbolIndex, value);
  // Move to next instruction
  MachineIncrInstrIndex(machine);
}

// --------------------------------------------------------------
// Conditional functions
// --------------------------------------------------------------

// bool ifTrueTail list ->
void CFun_ifTrueTail(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 1)
  TypeItem boolItem = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  MachineIncrInstrIndex(machine);
  if (ItemData(boolItem))
  {
    TypeItem list = MachineProgMemRead(machine, machine->InstrIndex);
    machine->InstrIndex = ItemData(list);
  }
  else
  {
    MachineIncrInstrIndex(machine);
  }
}

// -------------------------------------------------------
// Math functions
// -------------------------------------------------------

// n1 n2 + -> n
void CFun_add(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  TypeItem b = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  TypeItem a = DataStackGetTop(machine);
  ItemData(a) = ItemData(a) + ItemData(b);
  DataStackSetTop(machine, a);
  MachineIncrInstrIndex(machine);
}

// n1 n2 - -> n
void CFun_sub(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  TypeItem b = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  TypeItem a = DataStackGetTop(machine);
  ItemData(a) = ItemData(a) - ItemData(b);
  DataStackSetTop(machine, a);
  MachineIncrInstrIndex(machine);
}

// n1 n2 * -> n
void CFun_mult(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  TypeItem b = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  TypeItem a = DataStackGetTop(machine);
  ItemData(a) = ItemData(a) * ItemData(b);
  DataStackSetTop(machine, a);
  MachineIncrInstrIndex(machine);
}

// n1 n2 / -> n
void CFun_div(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  TypeItem b = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  TypeItem a = DataStackGetTop(machine);
  ItemData(a) = ItemData(a) / ItemData(b);
  DataStackSetTop(machine, a);
  MachineIncrInstrIndex(machine);
}

void CFun_sub1(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 1)
  TypeItem a = DataStackGetTop(machine);
  ItemData(a) = ItemData(a) - 1;
  DataStackSetTop(machine, a);
  MachineIncrInstrIndex(machine);
}

void CFun_sub2(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 1)
  TypeItem a = DataStackGetTop(machine);
  ItemData(a) = ItemData(a) - 2;
  DataStackSetTop(machine, a);
  MachineIncrInstrIndex(machine);
}

// -------------------------------------------------------
// Boolean functions
// -------------------------------------------------------

// n1 n2 isSmaller -> true if n2 is smaller than n1
void CFun_isSmaller(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  TypeItem b = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  TypeItem a = DataStackGetTop(machine);
  ItemData(a) = ItemData(a) > ItemData(b);
  DataStackSetTop(machine, a);
  MachineIncrInstrIndex(machine);
}

// n1 n2 isLarger -> true if n2 is larger than n1
void CFun_isLarger(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  TypeItem b = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  TypeItem a = DataStackGetTop(machine);
  ItemData(a) = ItemData(a) < ItemData(b);
  DataStackSetTop(machine, a);
  MachineIncrInstrIndex(machine);
}

// Test if value fields are equal (does not test type/opcode)
// item1 item2 eq -> true if ItemData(item1) == ItemData(item2)
void CFun_eq(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  TypeItem b = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  TypeItem a = DataStackGetTop(machine);
  ItemData(a) = ItemData(a) == ItemData(b);
  DataStackSetTop(machine, a);
  MachineIncrInstrIndex(machine);
}

// Logical not
// item not -> item
void CFun_not(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 1)
  TypeItem item = DataStackGetTop(machine);
  ItemData(item) = ! ItemData(item);
  DataStackSetTop(machine, item);
  MachineIncrInstrIndex(machine);
}

// --------------------------------------------------------
// Stack operations
// --------------------------------------------------------

// item A -> item item (dup)
void CFun_dup(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 1)
  DataStackIncrMacro(machine)
  TypeIndex top = machine->DataStackTop;
  machine->DataStack[top] = machine->DataStack[top - 1];
  MachineIncrInstrIndex(machine);
}

// item1 item2 B -> item1 item2 item1 (over)
void CFun_over(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  DataStackIncrMacro(machine)
  TypeIndex top = machine->DataStackTop;
  machine->DataStack[top] = machine->DataStack[top - 2];
  MachineIncrInstrIndex(machine);
}

// item1 item2 item3 C -> item1 item2 item3 item1
void CFun_overover(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 3)
  DataStackIncrMacro(machine)
  TypeIndex top = machine->DataStackTop;
  machine->DataStack[top] = machine->DataStack[top - 3];
  MachineIncrInstrIndex(machine);
}

// item1 item2 swap -> item2 item1
void CFun_swap(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 2)
  TypeIndex top = machine->DataStackTop;
  TypeItem item2 = machine->DataStack[top];
  machine->DataStack[top] = machine->DataStack[top - 1];
  machine->DataStack[top - 1] = item2;
  MachineIncrInstrIndex(machine);
}

// item1 item2 item3 rot -> item2 item3 item1
void CFun_rot(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 3)
  TypeIndex top = machine->DataStackTop;
  TypeItem item3 = machine->DataStack[top];
  TypeItem item2 = machine->DataStack[top - 1];
  machine->DataStack[top] = machine->DataStack[top - 2];
  machine->DataStack[top - 1] = item3;
  machine->DataStack[top - 2] = item2;
  MachineIncrInstrIndex(machine);
}

// word _ -> (drop)
void CFun_drop(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 1)
  DataStackDecrMacro(machine)
  MachineIncrInstrIndex(machine);
}

// --------------------------------------------------------------
// Printing
// --------------------------------------------------------------

void CFun_sayHi(TypeMachine* machine)
{
  DebugPrint("Hi World!");
  MachineIncrInstrIndex(machine);
}

void CFun_sayMantra(TypeMachine* machine)
{
  DebugPrint("I follow my breath");
  MachineIncrInstrIndex(machine);
}

void CFun_print(TypeMachine* machine)
{
  DataStackGuardMacro(machine, 1)
  TypeItem item = DataStackGetTop(machine);
  DataStackDecrMacro(machine)
  //DebugPrintItem("Item", item);
  MachinePrint(machine, item);
  PrintNewLine();
  MachineIncrInstrIndex(machine);
}

void CFun_printstack(TypeMachine* machine)
{
  printf("[ ");
  for (TypeIndex i = 0; i <= machine->DataStackTop; ++ i)
  {
    TypeItem item = machine->DataStack[i];
    MachinePrint(machine, item);
  }
  printf("]\n");
  MachineIncrInstrIndex(machine);
}

// -------------------------------------------------------------
// Call this function to add the functions
// -------------------------------------------------------------

void MachineAddCFuns(TypeMachine* machine)
{
  MachineAddCFun(machine, ":", CFun_def);
  MachineAddCFun(machine, "->", CFun_set);
  MachineAddCFun(machine, "ifTrueTail", CFun_ifTrueTail);
  MachineAddCFun(machine, "+", CFun_add);
  MachineAddCFun(machine, "-", CFun_sub);
  MachineAddCFun(machine, "*", CFun_mult);
  MachineAddCFun(machine, "/", CFun_div);
  MachineAddCFun(machine, "1-", CFun_sub1);
  MachineAddCFun(machine, "2-", CFun_sub2);
  MachineAddCFun(machine, "isSmaller", CFun_isSmaller);
  MachineAddCFun(machine, "isLarger", CFun_isLarger);
  MachineAddCFun(machine, "eq", CFun_eq);
  MachineAddCFun(machine, "not", CFun_not);
  MachineAddCFun(machine, "dup", CFun_dup);
  MachineAddCFun(machine, "B", CFun_over);
  MachineAddCFun(machine, "C", CFun_overover);
  MachineAddCFun(machine, "swap", CFun_swap);
  MachineAddCFun(machine, "rot", CFun_rot);
  MachineAddCFun(machine, "_", CFun_drop);
  MachineAddCFun(machine, "sayHi", CFun_sayHi);
  MachineAddCFun(machine, "sayMantra", CFun_sayMantra);
  MachineAddCFun(machine, "print", CFun_print);
  MachineAddCFun(machine, "printstack", CFun_printstack);
}
