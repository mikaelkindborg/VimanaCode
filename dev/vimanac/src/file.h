/*
File: file.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024
*/

#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>

// --------------------------------------------------------------
// File system functions
// --------------------------------------------------------------

FILE* FileOpen(char* filePath, char* mode)
{
  return fopen(filePath, mode);
}

int FileClose(FILE* stream)
{
  return fclose(stream);
}

FILE* FileOpenStringStream(char* string)
{
  return fmemopen(string, strlen(string), "r");
}

// Read file stream - returns allocated buffer.
// Deallocate returned buffer with MemFree
char* FileStreamRead(FILE* stream)
{
  char*  buffer;
  size_t size;

  // Open memory stream for writing
  FILE* memstream = open_memstream(&buffer, &size);
  if (NULL == memstream)
  {
    return NULL;
  }

  // Read file to stream
  int c = fgetc(stream);
  while (EOF != c)
  {
    fputc(c, memstream);
    c = fgetc(stream);
  }

  // Zero-terminate string
  fputc(0, memstream);

  // Close stream
  FileClose(memstream);

  // Increment MemAllocCounter
  ++ MemAllocCounter;

  // Return string buffer
  return buffer;
}

// Write string to file stream
void FileStreamWrite(FILE* stream, char* data)
{
  fprintf(stream, "%s", data);
}

// Read file - returns allocated buffer.
// Deallocate returned buffer with MemFree
char* FileRead(char* filePath)
{
  FILE* stream = FileOpen(filePath, "r");
  if (stream)
  {
    char* buffer = FileStreamRead(stream);
    FileClose(stream);
    return buffer;
  }
  else
  {
    return NULL;
  }
}

// Return pointer to the basename part of the filename
char* FileBaseName(char* fileName)
{
  char* p = strrchr(fileName, '/');
  if (NULL == p)
  {
    return fileName;
  }
  else
  {
    return p + 1;
  }
}

// Copy dirname part of the filename to resultDirName.
// Return FALSE if the filename does not include a directory
// Return TRUE if successful
int FileDirName(char* fileName, char* resultDirName)
{
  char* dirNameEnd = strrchr(fileName, '/');
  if (NULL == dirNameEnd)
  {
    return FALSE; // No directory
  }
  else
  {
    char* pSrc = fileName;
    char* pDest = resultDirName;
    while (pSrc != dirNameEnd)
    {
      *pDest = *pSrc;
      ++ pSrc;
      ++ pDest;
    }
    *pDest = '\0';

    return TRUE; // Success
  }
}

// Set the working directory.
// Return TRUE on success, FALSE on error
int SetWorkingDir(char* dirName)
{
  return 0 == chdir(dirName);
}
