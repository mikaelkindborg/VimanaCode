# Unstructures Notes

Here I keep all sorts of ideas and notes.

I would like to structure all of this one day and turn it into example programs and blogposts.

## Clipboard

Watcher logic:

    pb_old: ""
    pb_new: ""
    NOEVAL (old and new are equal)
    COPY TO CLIPBOARD "1 2 +" (user presses CMD-C)
    pb_old: ""
    pb_new: "1 2 +"
    --> EVAL "1 2 +"
    COPY TO CLIPBOARD ": 3" (program copies datastack)
    pb_old: "1 2 +"
    pb_new: ": 3"
    NOEVAL (new begins with ":")
    pb_old: ": 3"
    pb_new: ": 3"
    NOEVAL (old and new are equal)

Double copy logic:

    pb_old: ""
    pb_current: ""
    pb_new: "??"
    NOEVAL (old and new are not equal)

    USER CMD-C "1 2 +" (user presses CMD-C first time)
    pb_old: ""
    pb_new: "1 2 +"
    NOEVAL (old and new are not equal)

    PROGRAM COPY "::" (program copies sentinel)
    pb_old: "1 2 +"
    pb_new: "::"
    NOEVAL (old and new are not equal)

    USER CMD-C "1 2 +" (user presses CMD-C second time)
    pb_old: "1 2 +"
    pb_new: "1 2 +"
    --> EVAL "1 2 +" (this time they are are equal)

    PROGRAM COPY ": 3" (program copies datastack)
    pb_old: "1 2 +"
    pb_new: ": 3"
    NOEVAL (old and new are not equal)
    pb_old: ": 3"
    pb_new: ": 3"
    NOEVAL (old and new are equal)

Double copy logic adjusted (not working):

    PASTEB: "FOOBAR"
    PREV:   ""
    STATE:  0 (NOEVAL)

    PASTEB: "1 2 +" (USER COPY)
    PREV:   "1 2 +"
    STATE:  0 (NOEVAL)
    STATE:  1 (SET WHEN EQUAL)

    PASTEB: "COMMAND: EVAL WHEN EQ" (PROG COPY)
    PREV:   "1 2 +" (COMMAND NOT COPIED TO PREV)
    STATE:  1 (KEPT WHEN COMMAND)

    PASTEB: "1 2 +"
    PREV:   "1 2 +"
    STATE:  1 (EVAL WHEN EQ)

    EVAL HAPPENS

    PASTEB: ": 3" (PROG COPY DATASTACK)
    PREV:   "1 2 +"
    STATE:  0  (NOEVAL)

    PASTEB: "3"
    PREV:   "3"
    STATE:  0  (NOEVAL)

Copy EVAL logic:

    PASTEB: ""
    PREV:   ""

    PASTEB: "1 2 +" (USER COPY)
    PREV:   ""

    PASTEB: "1 2 +"
    PREV:   "1 2 +"

    PASTEB: "EVAL" (USER COPY)
    PREV:   "1 2 +"

    EVAL HAPPENS

    PASTEB: "EVAL"
    PREV:   "EVAL"

    NO EVAL BECAUSE PREV IS "EVAL"

Trailing EVAL logic:

    PASTEB: ""
    PREV:   ""

    PASTEB: "1 2 + EVAL" (USER COPY)
    PREV:   ""

    PASTEB: "1 2 + EVAL"
    PREV:   "1 2 + EVAL"

    EVAL HAPPENS
    PRINT   " [3]"

    PASTEB: " [3]" (PROGRAM COPY)
    PREV:   "1 2 + EVAL"

    PASTEB: " [3]"
    PREV:   " [3]"


pbpaste/pbcopy (mac)

xsel (brew and linux)

checkclipboard in SDL loop

To simplify things ... this is what I do: alias pbcopy='xsel --clipboard --input'; alias pbpaste='xsel --clipboard --output'  I chose the names to match the MacOS commands.

function check clipboard in SDL loop
in repl command to paste/eval clipboard

start stop commands/words

https://ss64.com

## ToonTalk

process:         process name
[box number]     preconditions
[_ 10]
[0 < number]
box[0] 10        actions target drop
box 10
box +1
number 10
number +1
number +box[0]

process: countdown
[number]
[number > 0]
number +1
printer number
sleeper 1000

--- Playfield ---
countdown [10]

--- Process ---
fact
[n res]
[n < 1]
printer res
sleep

fact
[n res]
res * n
n - 1

fact [10 1]

drawrect
[[x y w h] [r g b]]

drawrect [[0 0 100 100] [200 0 0]]


fact n
  if n < 1
    0
  else
    n * fact n - 1

factiter n res
  if n > 0
    res = res * n
    factiter n - 1 res



## Loop

Smalltalk style:

    (a b c foo -> ) doc
    (foo) (
      (a b c | x) localset
      a (x)!
      ((x): 0 >)
        ((x): print (x): 1- (x)!)
        whiletrue
      localdrop
    ) def

Association list for local vars and "closures":

    makeclosure
    closure(x)lget
    closure(x)lset

    uselocals, begin end
    (x)lget
    (x)lset

    ((symbol value)
    (symbol value)
    ...)

    (value list qsymbol lset -> ) drop
    (lset) (
      llookup rest setfirst
    ) def

    (list qsymbol lget -> ) drop
    (lget) (
      llookup rest first
    ) def

    (list qsymbol llookup -> list) drop
    (lookup) (
      over isempty
        (drop drop (() ()))
        (over first first over first eq
          (drop then first)
          (swap rest swap llookup)
          ifelse)
        ifelse
    ) def

Local stack very basic:

    (a b c foo -> ) doc
    (foo) (
      (a b c) local
      a 0 >
        (a print a 1 - b c foo)
        iftrue
    ) def

    (a b c foo -> ) doc
    (foo) (
      (a b c) local
      (a 0 >)
        (a print a 1 - (a) setl)
      whiletrue
    ) def

    (a b c foo -> ) doc
    (foo) (
      rot
      (dup 0 >)
        (dup print dup 1 -)
      loop whiletrue
      droop
    ) def

    (loop) (xpush xpush) def

    (cond body whiletrue -> )
    (whiletrue) (
      /--xpush xpush--/
      X1 eval
        (X2 eval whiletrue)
        (xdrop xdrop)
        ifelse
    ) def

    Local Stack:
    lpush ldrop L1 L2 l3

    Local X Stack:
    xpush xdrop X1 X2 X3

    (a b c foo -> ) doc
    (foo) (
      rot dup 0 >
        (dup print 1 - root foo)
        (drooop)
        ifelse
    ) def

## Vimana Code

{lib.vimana} evalfile

Implementing something like local registers in Vimana:

    () (LocalStack) setvar
    (push) (LocalStack cons (LocalStack) setvar) def
    (pop) (LocalStack rest (LocalStack) setvar) def
    (@) (LocalStack first) def
    (A) (LocalStack first) def
    (B) (LocalStack rest first) def
    (C) (LocalStack rest rest first) def

LocalStack format could use a sentinel to deallocate locals with one function call.

Example format for local stack with three local "frames":

    sentinel B A sentinel A sentinel C B A

When checking the sentinel, eq should work for checking if symbols are equal

    (sentinel) push
    pop (sentinel) first eq

Use of images for locals:

    (swap) ([🍎🍉] 🍉 🍎 []) def
    (over) ([🍎🍉] 🍎 🍉 🍎 []) def
    (rot)  ([🍎🍉🫐] 🫐 🍎 🍉 []) def
    (dup)  ([🍎] 🍎 🍎 []) def

    (swap) ([AB] B A []) def

    (then) () def

    (swap) ([🍎🫐] 🫐 🍎 []) def
    (over) ([🍎🫐] 🍎 🫐 🍎 []) def
    (rot)  ([🍎🫐🍋] 🫐 🍎 🍋 []) def
    (dup)  ([🍎] 🍎 🍎 []) def

    (incr) ([🍎] 🍎 🍎 first 1+ setfirst []) def
    (makecounter) 0 () cons ([🍎] 🍎 incr 🍎 first []) cons def

Implementation of map:

    (1 2 3) (+) map

    L F     over
    L F L   first
    L F N   over
    L F N F eval
    L F N   rot
    F N L   rest
    F N L   rot
    N L F   map
    N L     cons

    (map) (
      (over isempty (drop drop ())
        (over first over eval
          rot rest rot map cons) ifelse []) def

    (map)
      (over isempty
        (drop drop ())
        (over first over eval rot rest rot map cons)
      ifelse) [^_^]

    (map) ([🍎🫐]
      (🍎 isempty (())
        (🍎 first 🫐 eval
          🍎 rest 🫐 map cons) ifelse []) def

    (map) ([l fun]
      (l isempty (())
        (l first fun eval
          l rest fun map cons) ifelse []) def

    (defun map (l fun)
      (cond ((isempty l) ())
            (t (cons (apply fun (first l)) (map (rest l) fun)))))

    (swap) ([AB]  B A   []) def
    (over) ([AB]  A B A []) def
    (rot)  ([ABC] B C A []) def
    (dup)  ([A]   A A   []) def

Implementation of nth function (not working/tested):

    (list n nth -> list) doc
    (nth) (
      dup iszero
        (drop then first)
        (over isempty
          (drop)
          (swap rest swap 1- nth)
        ifelse)
      ifelse
    ) def

    (list n item setnth -> ) doc
    (setnth) (
      rot rot nth swap setfirst
    ) def

## SDL

This was important for something (set this mode in macOS Terminal and you don't need the compatibility of ncurses, just use VT100 control codes):

    TERM=vt100 nano

Sketch of SDL event loop:

    (loop) (
      sdlPollEvent
      sdlIsQuitEvent (handleEvent loop) iffalse
    ) def

    (handleEvent) (
      sdlIsMouseEvent (handleMouseEvent end) iftrue
      sdlIsOtherEvent (handleOtherEvent end) iftrue
    ) def

    (handleEvent) (
      sdlIsMouseEvent
        (sdlMouseX sdlMouseY handleMouseEvent end)
        iftrue
      sdlIsOtherEvent
        (handleOtherEvent end)
        iftrue
    ) def

    (end) (popStackFrame) def

    void PrimFun_popStackFrame(VInterp* interp)
    {
      InterpPopStackFrame(interp);
    }

## Virtual machines

It strikes me that it could be attractive to promote a VM as "language neutral", and provide a low-level language that does not require a complex compiler toolchain (most VMs seem focus on supporting a specific programming language, and do not promote coding "directly" on the VM).

I have seen an interesting example of this mindset in the UXN VM https://wiki.xxiivv.com/site/uxn.html, where you program the VM "directly" in a low level language called TAL https://wiki.xxiivv.com/site/uxntal.html. See also https://github.com/hundredrabbits/awesome-uxn.

I am thinking that some kind of lower-level language for a VM could be an interesting alternative to using a C or JS compiler.

## Code Notes

    [incr] [A: A A first 1+ setfirst A first] def
    [makecounter] ( 0 ) [A: A incr A first] cons def

    [incr] [dup dup first 1+ setfirst first] def
    [makecounter] ( 0 ) [dup dup incr first] cons def

    (incr) (dup dup first 1+ setfirst first) def
    (makecounter) [ 0 ] (dup dup incr first) cons def

    [box] [[] cons] def
    [boxval] [first] def
    [incr] [dup dup first 1+ setfirst boxval] def
    [makecounter] 0 box [A: A incr A boxval] cons def
    [makecounter] 0 box [dup dup incr boxval] cons def

    /-- list copy -> list --/
    (copy)
      (dup isempty not
        (dup first swap rest copy cons)
          iftrue) def

    (defun copy (l)
      (cond ((empty l) nil)
            (t (cons (car l) (copy (cdr l))))))

    (defun map (l fun)
      (cond ((empty l) nil)
            (t (cons (fun (car l)) (copy (cdr l))))))

    /-- list fun map -> list --/
    (map)
      (over isempty
        (drop)
        (over first swap eval swap rest swap map cons)
      iftrue) def

    (map) (
      begin AB!
        (A isempty
          (())
          (A first B eval A rest B map cons)
        ifelse
      end
    ) def

    (list fun map) (
      list isempty
        (())
        (list first fun eval
          list rest fun map
        cons)
      ifelse
    ) [*_*]

    (list fun map) (
      2 ARGS
      A1 isempty
        (())
        (A1 first A2 eval
          A1 rest A2 map
        cons)
      ifelse
    ) [^_^]

    /-- list fun map -> list --/
    (map) (
      [AB] ==>
        A isempty
          (())
          (A first B eval A rest B map cons)
        ifelse
      <==
    ) [^_^]

    (map) (
      AB!
        A isempty
          (())
          (A first B eval A rest B map cons)
        ifelse
      ^!^
    ) [^_^]

    (map) (
      AB!
        A isempty
          (())
          (A first B eval A rest B map cons)
        ifelse
      end
    ) [^_^]

    { Define emojis for some functions to
      liven up the code :) } drop
    ([^_^]) (def) def
    (<^_^>) (drop) [^_^]

    { Take a list and a function and map the elements
      to a new list that is returned on the stack:
        A:list B:fun map -> list
      Example:
        (1 2 3) (1 +) map -> (2 3 4)
    } <^_^>

    (map)
      (AB: A isempty
        (())
        (A first B eval A rest B map cons)
      ifelse ::) [^_^]

    8 byteArrayAlloc (buf) set!
    42 0 buf set8
    42 0 buf set16
    42 0 buf set32
    42 0 buf set64
    0 buf get8
    0 buf get16
    0 buf get32
    0 buf get64
    buf freeArray

    100 itemArrayAlloc (items) set!
    42 0 items setItem
    0 items getItem
    items freeArray

    LocalStack layout:
    items numItems, ...
    A B 2  A 1  A B C 3

    100 itemArrayAlloc (LocalStack) set!
    -1 (LocalIndex) set!

    (localStackIncrIndex)
      (LocalIndex 1+ (LocalIndex) set!) def
    (localStackPushItem)
      (localStackIncrIndex LocalStack setItem) def
    (localStackGetTopItem)
      (LocalIndex LocalStack getItem) def

    (A:)
      (localStackPush
      2 localStackPush) def
    (AB:)
      (localStackPush
      localStackPush
      3 localStackPush) def
    (::)
      (LocalIndex 0 < ({LocalStack is empty} GuruMeditation) iftrue
      LocalIndex localStackGetTopItem - (LocalIndex) set!) def

    (A) (LocalIndex 1- LocalStack getItem) def
    (B) (LocalIndex 2- LocalStack getItem) def

    (map)
      (over isempty
        (drop drop ())
        (over first over eval rot rest rot map cons)
      ifelse) [^_^]

    (defun map (l fun)
      (cond ((empty l) nil)
            (t (cons (fun (car l)) (map (cdr l) fun)))))

    (l fun map)
      (l isempty
        (())
        (l first fun eval l rest fun map cons)
      ifelse) [^_^]

    (map)
      (map-list-is-empty?
        (map-return-empty-list)
        (map-apply-function map-rest cons)
      ifelse) [^_^]

    (map-list-is-empty?) (over isempty) [^_^]
    (map-return-empty-list) (drop drop ()) [^_^]
    (map-apply-function) (over first over eval) [^_^]
    (map-rest) (swap-over rest swap map) [^_^]
    (swap-over) (swap rot) [^_^]

    (map)
      (AB: map-list-is-empty?
        (map-return-empty-list)
        (map-apply-function map-rest cons)
      ifelse ::) [^_^]

    (map-list-is-empty?) (A isempty) [^_^]
    (map-return-empty-list) (()) [^_^]
    (map-apply-function) (A first B eval) [^_^]
    (map-rest) (A rest B map) [^_^]

    (map)
      (AB: A isempty
        (())
        (map-apply-function map-rest cons)
      ifelse ::) [^_^]

    (map-apply-function) (A first B eval) [^_^]
    (map-rest) (A rest B map) [^_^]

    (times)
      (dup iszero (drop drop)
        (over eval 1- times)
        ifelse) def

    (times)
      (AB: A iszero (::)
        (A eval A B 1- :: times)
      ifelse) def

    (list n times)
      (n iszero (pop)
        (list eval list n 1- pop times)
      ifelse) def

    (defun times (fun n)
      (cond ((zerop n) nil)
            (t (fun) (times fun (- n 1)))))

VimanaMachine instructions:

    call primfun
    push int, dec, list, string, (symbol)
    eval global symbol
      push value or
      eval function

Memory areas:

    data stack
    globals
    callstack
    list memory
    string/array memory

## Random notes

    dup 10 write // write top of stack to program item 10

    (foo) (
      2 locals
      1 A!
      2 B!
      (1 2 3) (A +) map
        (B +) map
    ) def

    (foo) (
      1 locals
      1 A!
      (1 2 3) (A +) map
    ) def


    localstack

    (
      stacknum
      nargs
      A
      B
      stacknum
      nargs
      A
    )

    (map)
      (AB: A isempty
        (:: ())
        (A first B eval A rest B :: map cons)
      ifelse) [^_^]

    (map)
      (over isempty
        (drop drop ())
        (AB: A first B eval A rest B :: map cons)
      ifelse) [^_^]

    (map) (
      AB: A isempty
          (())
          (A first B eval A rest B map cons)
        ifelse
      ::
    ) [^_^]

    /-- list fun map -> list --/
    (map) (
      over isempty
        (drop drop ())
        (over first over eval spin rest swap map cons)
      ifelse
    ) [^_^]

    (spin) (swap rot) def
    A B C
    A C B
    C B A

    /-- x y z spin -> z y x --/
    (spin) (
      (R3) setglobal
      (R2) setglobal
      (R1) setglobal
      R3 R2 R1
    ) [^_^]

    A     DUP
    BA    SWAP
    BCA   ROT
    A_A   OVER
    X XX  DROP, 2DROP
    X_    NIP
    CBA   SPIN
    BA_   SWISH/UNDER-SWAP

    ABC   BCA BAC (BA_) CAB CBA ACB/BA

    BACD BA__

    1
    2 args
    3 args
    4 args
    5

    (n foo) (n 0 > (n 1- foo n print) iftrue) [^_^]
    3 foo

    (foo) (dup 0 > (dup 1- foo print) (drop) ifelse) [^_^]


    (0 fact) (1) def
    (n fact) (n n 1- fact *) def

    [map]
      [dup isempty not
        [dup first fun eval over rest map cons]
      iftrue] def


    /-- number adder -> list --/
    (adder) (() cons (first +) cons) def
    (add10) 10 adder defval
    (add10) [ 10 ] (first +) cons defval

    (1 2 3 4) add10 map -> (11 12 13 14)
    () () map -> ()
    (1) () map -> (1)
    (1 2 3 4) (drop 2) map -> (2 2 2 2)

    local variables = lots of stuff to consider; scope, allocation, lifetime, etc

    (add10) (
      (n) setl
      (+) (add) setl
      n 10 add eval
    ) def

    (add10) (
      begin
        A!
        (+) B!
        A 10 B eval
      end
    ) def

    [incr] [begin A! A A first 1+ setfirst A first end] def
    [makecounter] ( 0 ) [begin A! A incr A first end] cons def

    [fact] [
      begin A!
        A iszero
          [1 end]
          [A A 1- fact * end]
        ifelse
    ] def

    10 alloc free
    10 bytealloc free

    TypeArray
    TypeByteArray

    array index item put
    array index get
    bytearray addr bytevalue put
    bytearray addr get
    string index charcode/string put
    string index get

    {ADA} 2 {B} put -> {ADB}
    string explode
    array implode

    ((NAME AGE)
    (makefriend)

    (age) friend -> 42

    fiend (age) send -> 42

    friend(age)@ -> 42

    friend(age)(42)!

    (obj)
      ((v1 v2)
      (fun1 ((v1 v2)' ...))
      (fun2 ((v1 v2)' ...))
      (fun2 ((v1 v2)' ...)))
    defval

    obj(fun1)!
    obj(fun1) getprop eval

    (spaceanimation) (
      spaceship 0 10 movedelta
      spaceship(x)getprop screenright >
        (spaceship(x)-100 setprop)
        iftrue
    ) def

## Make a closure-like function in Vimana

    (makecounter)
      (0 () cons (dup dup first 1 + setfirst first) cons) def

    (makecounter)
      (0 () cons ([A] A first 1 + setfirst A first []) cons) def

    (makecounter)
      (0 () cons
        (A) (A first 1 + setfirst A first) let
          cons) def

    (makecounter)
      (0 (A) (A 1 + A) let) def

    (makecounter)
      (0 (A) ((A 1 + A)) fun call) def

Javascript:

    function makecounter()
    {
      return ((A) => {
        return (() => { A = A + 1; return A; })
      })(0);
    }

    function makecounter()
    {
      const fun = ((A) => {
        return (() => { A = A + 1; return A; })
      });
      return fun(0);
    }

## Old benchmark notes

    miki@mikis-MacBook-Air vimanac_fast % time ./vimana ../benchmark/fib.vimana
    ------------------------------------------------------------
    Vimana Machine Memory
    ------------------------------------------------------------
    Allocated:      30744
    Used:           22656
    Primfun table:  704
    Num primfuns:   44
    Symbol table:   800
    Symbol memory:  1032
    Interpreter:    20120
    ------------------------------------------------------------
    24157817
    ------------------------------------------------------------
    MemAllocCounter: 0
    SysAllocCounter: 0
    ------------------------------------------------------------
    ./vimana ../benchmark/fib.vimana  2.22s user 0.01s system 99% cpu 2.242 total
    miki@mikis-MacBook-Air vimanac_fast % time php ../benchmark/fib.php
    24157817
    php ../benchmark/fib.php  2.03s user 0.12s system 78% cpu 2.740 total
    miki@mikis-MacBook-Air vimanac_fast % time ruby ../benchmark/fib.rb
    24157817
    ruby ../benchmark/fib.rb  2.18s user 0.07s system 97% cpu 2.320 total
    miki@mikis-MacBook-Air vimanac_fast % time python ../benchmark/fib.py
    24157817
    python ../benchmark/fib.py  6.34s user 0.10s system 98% cpu 6.568 total
    miki@mikis-MacBook-Air vimanac_fast % time python3 ../benchmark/fib.py
    24157817
    python3 ../benchmark/fib.py  8.14s user 0.02s system 99% cpu 8.182 total
    miki@mikis-MacBook-Air vimanac_fast % time python3 ../benchmark/fib.py
    24157817
    python3 ../benchmark/fib.py  8.20s user 0.02s system 99% cpu 8.226 total
    miki@mikis-MacBook-Air vimanac_fast %

## More code notes

    VM: op value

    push
    call
    add sub mul div
    eq < >
    and or not
    quote

    [dup 10 <] [1 add dup print] while

    use handle in place of pointer

    Item types:

    1 TypeNone
    2 TypeIntNum
    3 TypeDecNum
    4 TypeSymbol
    5 TypePrimFun (or check if symbolid < PrimFunMax)
    6 TypeFun
    7 TypeList
    8 TypeHandle
    markbit

    item type is 3 bits
    mark bit is 1 bit

    Item
    [value][next|type|m]

    TypeHandle holds buffer type info string/buffer/...
    in next > NextMax
    [ptr][next|type|m]

    A handle that points to a "buffer handle" has a
    next field < NextMax
    A handle that holds the pointer has a next
    field > NextMax

    NextMax = next max value - num of buffer types

    This saves one basic item type, and works since
    a buffer handle never points to a next item.

    Also rename ItemAddr to ItemIndex (value of the next field)

    0 0 0 m
    0 0 1 m
    0 1 0 m
    0 1 1 m
    1 0 0 m
    1 0 1 m
    1 1 0 m
    1 1 1 m

    2 4 8 16 32 64 128 256
    512 1024 2048 4096 8192 16384 32768 65536

    4 bytes x 2048  items = 8192  bytes = 8KB
    4 bytes x 4096  items = 16384 bytes = 16KB
    4 bytes x 16384 items = 65536 bytes = 64KB

    bit 1 is type flag

    x x x 0 m handle/symbol index
    x x x 1 m other type
    x 0 0 1 m null
    x 0 1 1 m fun
    x 1 0 1 m
    x 1 1 1 m

    /-- Timer global variables --/

    () (TimerList) setglobal
    0  (TimerCounter) setglobal
    1  (TimerRunFlag) setglobal

    1 [TimerRunFlag] !
    1 (TimerRunFlag) !

    (setup)
      ((MyLed) 4 set) def

    (loop) (
      MyLed ledOn
      1000 millisSleep
      MyLed ledOff
      2000 millisSleep
    ) def

    (setup)
      ((MyLed ledFlip) 1000 timerAdd) def

    [setup] [
      4 [MyLed] setglobal
      [MyLed ledFlip] 1000 timerAdd
    ] defval

    (sequence)
      ((MyLed ledOn) 1000 (MyLed ledOff) 2000) setglobal
    sequence timerAddSequence

    (turnLedOn)
      (MyLed ledOn do (turnLedOff) 1000 schedule) def

    (turnLedOff)
      (MyLed ledOff do (turnLedOn) 2000 schedule) def

    (do) () def

    (Screen1) (
      1 1 1 1 1 1 1 1 1 1 1 1
      1 0 0 1 1 0 0 1 1 0 0 1
      1 0 0 1 1 0 0 1 1 0 0 1
      1 0 0 1 1 0 0 1 1 0 0 1
      1 0 0 1 1 0 0 1 1 0 0 1
      1 0 0 1 1 0 0 1 1 0 0 1
      1 0 0 1 1 0 0 1 1 0 0 1
      1 1 1 1 1 1 1 1 1 1 1 1
    ) defScreen

    (Screen1) (
      1 0 0 0 0 0 0 1 0 0 0 0
      0 1 0 0 0 0 1 0 0 0 0 0
      0 0 1 0 0 1 0 0 0 0 0 0
      0 0 0 1 1 0 0 0 0 0 0 0
      0 0 0 1 1 0 0 0 0 0 0 0
      0 0 1 0 0 1 0 0 0 0 0 0
      0 1 0 0 0 0 1 0 0 0 0 0
      1 0 0 0 0 0 0 1 0 0 0 0
    ) defScreen

    SMALL INTERPRETER

    # Primfuntable

    VPrimFunEntry
      funptr fun
      char*  name (static string)

    # Global symboltable (globals)

    VGlobalVarEntry
      VItem* value
      char*  name (symbol string)

    # Symbols

    Memory block with zero-terminated strings

    # Data stack

    Array of VItem

    # Call stack

    Array of VItem

    # List memory

    Array of VItem

    # Machine

      primfuns
      globals
      datastack
      callstack
      listmem

    # Parser

    symbols lists numbers

    # Generic array

    Use #define/#include for generic array

    #define ArrayType VItem
    #define ArrayName DataStack
    #include "array.h"
    #undef ArrayType
    #undef ArrayName

    typedef struct __VStack VStack;

    // byte pointers used for best performace
    struct __VStack
    {
      VByte* stackStart;  // bottom of stack
      VByte* stackEnd;    // end of stack (max limit)
      VByte* stackTop;    // top of stack
      size_t elementSize;
    };

    // byte pointers used for best performace
    typedef struct __VArray VArray;

    // byte pointers used for best performace
    struct __VArray
    {
      VByte* start; // start of array
      VByte* end;   // end (limit) of array
      VByte* top;   // top of stack
    };

    Array type? (instead of stack)

    void ArrayName##_Push(VStack* restrict stack, ArrayType* restrict item)
    {
      stack->stackTop += sizeof(ArrayType);

      if (! (stack->stackTop < stack->stackEnd) )
      {
        GuruMeditation(ArrayName##ArrayOverflow);
      }

      // Copy item to stack
      *((ArrayType*)stack->stackTop) = *item;
    }

    ArrayType* ArrayName##_Pop(VArray* restrict array)
    {
      if (array->top < array->start)
      {
        GuruMeditation(ArrayName##ArrayIsEmpty);
      }

      ArrayType* top = (ArrayType*) array->stackTop;
      array->stackTop -= sizeof(ArrayType);
      return top;
    }

    void StackName##_Push(VStack* restrict stack, StackType* restrict item)
    {
      stack->stackTop += sizeof(StackType);

      if (! (stack->stackTop < stack->stackEnd) )
      {
        GuruMeditation(StackName##StackOverflow);
      }

      // Copy item to stack
      *((StackType*)stack->stackTop) = *item;
    }

    StackType* StackPop(VStack* stack)
    {
      if (stack->stackTop < stack->stackStart)
      {
        GuruMeditation(StackIsEmpty##StackName);
      }

      StackType* top = (StackType*) stack->stackTop;
      stack->stackTop -= sizeof(StackType);
      return top;
    }

    #define StackTop(stack) ((StackType*)((stack)->stackTop))

    #define StackAt(stack, offsetFromTop) \
      ((StackType*)(((stack)->stackTop) - (offsetFromTop * sizeof(StackType))))

## Messages

    {message} messagePost

    messagesGet -> list of messages (10 last?)

    object messagePost -> messageId
    messageId messageGet -> object

    ((100 symbols)
    (20 callstackItems)
    (20 datastackItems)
    (1000 listItems)) interpNew -> interpRef

    configList interpNew -> interpRef
    list interpRef interpRun ->
    interpRef interpStop ->
    interpRef interpDealloc ->

    (interpCreate) (
      ((100 symbols)
      (20 callstackItems)
      (20 datastackItems)
      (1000 listItems)) interpNew
    ) def

    interpCreate (MyInterp) setglobal

    ({Hi World} print) interpRun

    {Hi World} print -> prints: Hi World

    {Hi World} stringify print -> prints: {Hi World}

    write to file system, or memory file

    messages.txt
    ({message1} {message2} {message3})

    string -> symbolid

    parse

    tostring

## Timers

    /-- Timer handling functions --/

    [timerCounterIncr]
      [TimerCounter 1+ [TimerCounter] !] :

    (timerCounterIncr)
      (TimerCounter 1+ (TimerCounter) !) :

    (timerCounterIncr)
      (TimerCounter 1+ (TimerCounter) setglobal) def

    (timerCounterNext)
      (timerCounterIncr TimerCounter) def

    /-- codelist interval timerAdd --/
    (timerAdd)
      ([AB] timerCounterIncr
      /-- (lastRun interval codelist timerId) --/
      [ millis B A TimerCounter ] TimerList cons
        (TimerList) setglobal) def

    /-- codelist interval timerAdd --/
    (timerAdd) ((action interval)
      [ millis interval action timerCounterNext ] TimerList cons
        (TimerList) setglobal) def

    (action interval timerAdd) (
      [ millis interval action timerCounterNext ] TimerList cons
        (TimerList) !
    ) def

    (timerAdd) (A! B!
      [ millis A B timerCounterNext ] TimerList cons
        (TimerList) !
    ) def

    (timerAdd) (AB!
      TimerList [ millis B A timerCounterNext ] addlast
    ) def

    (timerAdd) (
      @0! @1!
      [ millis @0 @1 timerCounterNext ] TimerList cons
        (TimerList) setglobal
    ) def

    (timerAdd)
      ([codelist interval]
      timerCounterIncr
      /-- (lastRun interval codelist timerId) --/
      [ millis interval codelist TimerCounter ] TimerList cons
        (TimerList) setglobal) def

    (timerCheckAll)
      (TimerList (timerRun) map) def

    (timerStopAll)
      (0 (TimerRunFlag) setglobal) def

    (timerClearAll)
      (() (TimerList) setglobal) def

    (timerLoop)
      (TimerRunFlag (
        timerCheckAll
        10 sleep
        timerLoop)
      iftrue) def

    /-- Timer functions --/

    (timerLastRun)     (first) def
    (timerInterval)    (rest first) def
    (timerCode)        (rest rest first) def
    (timerId)          (rest rest rest first) def

    (timerLastRunSet)  (millis setfirst drop) def

    (timerTimeToRun)
      ([A] A timerLastRun A timerInterval +
        millis <) def

    /-- timer timerRun --/
    (timerRun)
      ([A] A timerTimeToRun (
          A timerLastRunSet
          A timerCode call)
        iftrue) def

    /--
    (timerRun)
      ((timer)
        timer timerTimeToRun (
          timer timerLastRunSet
          timer timerCode call)
        iftrue) defv

    (timerRun) ([A]
      A timerTimeToRun (
        A timerLastRunSet
        A timerCode call)
      iftrue) def

    (timerRun) (
      dup timerTimeToRun
        (dup
          timerLastRunSet
          timerCode call)
        (drop)
      ifelse) def
    --/

    /-- Example --/

    (sayHi) 1000 timerAdd

    [ 0 ] (dup first 1+ setfirst first print) cons
      100 timerAdd

    [ 0 ] (dup dup first 1+ setfirst first print) cons
      100 timerAdd

    (timerStopAll) 5000 timerAdd

    timerLoop

    printstack

## Crontab

    https://www.simplified.guide/linux/automatically-run-program-on-startup

    crontab

    $ crontab -e
    @reboot vimana bootstrap.vimana

    bootstrap.vimana has the server address

    bootstrap --> server.php
    query update every 1 minutes

    1. Write server script (PHP)
    2. Write bootstrap script (Vimana)
    3. Copy vimana executable and boostrap script to device
    4. Add vimana bootstrap.vimana to crontab

## Processes

    https://askubuntu.com/questions/153900/how-can-i-start-a-process-with-a-different-name
    How can I start a process with a different name?
    bash -c "exec -a <MyProcessName> <Command>"
    Then you can kill the process with:
    pkill -f MyProcessName

    #include <unistd.h>
    #include <sys/reboot.h>

    sync();
    reboot(RB_AUTOBOOT);

    (bootstrap)
      ( {downloader.vimana} fetchAndSave spawn
        {displayer.vimana} fetchAndSave spawn ) def
    bootstrap

    (spawnSavedFile)
      (dup fileExistsAndIsGood
        (spawn then drop)
        (fetchAndSave (spawn) (fail) ifelse)
      ifelse) def

    (spawnFetchedFile)
      (dup fetchAndSave
        (spawn then drop)
        (readSafe (spawn) (fail) ifelse)
      ifelse) def

    check if update exists, if so download and respawn

    (filename (string) fetchAndSave -> content (string) or NIL) doc
    (fetchAndSave)
      (dup fetch swap over swap save) def

    filename dup
    filename filename fetch
    filename content swap
    content filename over
    content filename content swap save

    (content (string) filename (string) save -> ) doc
    (save)
      (...) def

    (downloader)
      ( {displaylist.vimana} fetch evalstring
        images fetch
        commands fetch ) def

    files:
      boostrap.vimana
      downloader.vimana
      displayer.vimana
      lib.vimana
      displaylist.vimana
      images/
        image1.jpg
        image2.jpg
        imageN.jpg

    (DisplayList) (
      {sea.jpg}
      {sky.jpg}
      {earth.jpg}
      {fire.jpg}
      {snow.jpg}
    ) defvar

    (fetchimages) (
      ({images/} swap concat dup fetch save) map
    ) def

    (displayImages) (
      dup isempty
        (drop then
        (DisplayList displayImages)
          DisplayInterval schedule)
        (dup first display then
        rest (displayImages) cons
          DisplayInterval schedule)
    ) def

    // ({images/} swap concat display) DisplayInterval  map

    DisplayList fetchImages

    DisplayList displayImages

    (DisplayInterval) 5000 defvar

    5000 (DisplayInterval) setvar

    (fetcheval) (dup fetch save evalfile) def

    {displayer.vimana} dup fetch save evalfile

## Blog posts

    Blog post about new Vimana Verision

    Syntatic sugar in a concatenative language

    In COBOL, many of the intermediate keywords are syntactic sugar that may optionally be omitted. For example, the sentence MOVE A B. and the sentence MOVE A TO B.

    (turnLedOff)
      ( MyLed ledOff
        with
          (turnLedOn)
          2000
        do schedule ) def

    (turnLedOff)
      ( MyLed ledOff
        with
          (turnLedOn)
          2000
        schedule ) def

    (fact)
      (dup iszero
        (drop then 1)
        (dup 1- fact *)
      ifelse) def

    (turnLedOff)
      ( MyLed ledOff
        then
          (turnLedOn)
          2000
        schedule ) def

    (turnLedOff)
      ( MyLed ledOff
        do
          (turnLedOn)
          2000
        schedule ) def

    (turnLedOff)
      ( MyLed ledOff
        (turnLedOn) 2000 schedule ) def

    (turnLedOff)
      ( MyLed ledOff do (turnLedOn) 2000 schedule ) def

    (turnLedOff)
      ( (MyLed ledOff) doNow
        (turnLedOn) 2000 doLater ) def

    (onloop) (
      200 200 0 setcolor
      mousex mousey 100 100 drawcircle
      (onloop) 50 schedule
    ) def

    [onloop] [
      200 200 0 setcolor
      mousex mousey 100 100 drawcircle
      [onloop] 50 schedule
    ] def

## Local stack functions (local variables steampunk style)

Use colon:

    A:
    AB:
    ABC:
    ::
    A
    B
    C

Or use square brackets:

    [A]
    [AB]
    [ABC]
    []
    A
    B
    C

Example:

    (foo) ([A] A 10 + []) def

    (foo) (A: A 10 + ::) def

    32 foo -> puts 42 on the datastack

## Steampunk Programming Languages

    Square brackets = Steampunk

    Low-level hightech
    Low-level high-level

    Forth vs Lisp
    Assembly

    Paper terminal
    Console
    knobs
    instruments

## Git

Moving from github to gitlab:

    git remote -v
    git remote set-url origin <repository.git>
    git remote set-url origin git@gitlab.com:mikaelkindborg/<repository.git>

Git global setup:

    git config --global user.name "Mikael Kindborg"
    git config --global user.email "m@k.com"

### Command line instructions

You can also upload existing files from your computer using the instructions below.

Create a new repository:

    git clone git@gitlab.com:mikaelkindborg/<repository.git>
    cd <repository>
    git switch --create main
    touch README.md
    git add README.md
    git commit -m "add README"
    git push --set-upstream origin main

Push an existing folder:

    cd existing_folder
    git init --initial-branch=main
    git remote add origin git@gitlab.com:mikaelkindborg/<repository.git>
    git add .
    git commit -m "Initial commit"
    git push --set-upstream origin main

Push an existing Git repository:

    cd existing_repo
    git remote rename origin old-origin
    git remote add origin git@gitlab.com:mikaelkindborg/<repository.git>
    git push --set-upstream origin --all
    git push --set-upstream origin --tags
