/*
File: loopmachine.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Loop Machine Interpreter.

Related links:
https://gcc.gnu.org/onlinedocs/gcc/Labels-as-Values.html
https://www.complang.tuwien.ac.at/forth/threaded-code.html
https://www.complang.tuwien.ac.at/forth/threading/
https://softwareengineering.stackexchange.com/questions/399455/is-there-a-way-to-speed-up-a-big-switch-statement
https://softwareengineering.stackexchange.com/questions/99445/is-micro-optimisation-important-when-coding
https://eli.thegreenplace.net/2012/07/12/computed-goto-for-efficient-dispatch-tables
https://news.ycombinator.com/item?id=18678920
https://news.ycombinator.com/item?id=18678699
https://gitlab.com/sifoo/snigl
https://news.ycombinator.com/item?id=25620445
(Marking the default with __builtin_unreachable() can help)
*/

// --------------------------------------------------------------
// Include system libraries
// --------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// --------------------------------------------------------------
// Syntax elements
// --------------------------------------------------------------

#define LEFTPAREN    ('[')
#define RIGHTPAREN   (']')
#define STRINGBEGIN  ('{')
#define STRINGEND    ('}')
#define COMMENTBEGIN ('(')
#define COMMENTEND   (')')
#define COMMENTQUOTE ('"')

// --------------------------------------------------------------
// Interpreter data types
// --------------------------------------------------------------

#include "optable.h"

//typedef unsigned char TypeByte;
typedef long TypeWord;
typedef int  TypeIndex; // Used for index and size
typedef int  TypeHandle;
typedef int  TypeBool;

#define TRUE  1
#define FALSE 0

// --------------------------------------------------------------
// Program memory areas
// --------------------------------------------------------------

#define PROGMEM_MAXSIZE       500 // Max number of words in ProgMem
                                  // (including list begin/end words)
#define DATASTACK_MAXSIZE     50
#define CALLSTACK_MAXSIZE     50
#define SYMBOLTABLE_MAXSIZE   50
#define PARSE_QUEUE_MAXSIZE   100  // Max length of nested literal list
#define STRINGMEM_MAXSIZE     1000 // Max number of bytes in string memory
#define HANDLETABLE_MAXSIZE   10  // Max number of handles
#define MAX_TOKEN_LENGTH      32   // Max length for symbol names
                                   // (31 chars + 1 zero-terminator char)
#define BUFFER_CHUNK_SIZE     64   // Parser chunk size for literal strings
#define TAG_BITS              6    //8          //6
#define TAG_MASK              0x3F //0xFF (255) //0x3F (63)

// On 32 bit systems, integer range is -33554432 to 33554431 with 6 opcode bits

// Program memory
TypeWord  ProgMem[PROGMEM_MAXSIZE]; // Program memory
TypeIndex ProgMemFirstFree;         // First free index

// Data stack and call stack
TypeWord  DataStack[DATASTACK_MAXSIZE]; // Data stack
// TODO: Rename CallStack to ReturnStack or RStack
TypeIndex CallStack[CALLSTACK_MAXSIZE]; // Return address/index stack
TypeIndex GlobalDataStackTop;           // For debugging (holds local
                                        // DataStackTop on exit)

// Symbol table
TypeWord  SymbolTable[SYMBOLTABLE_MAXSIZE];
char*     SymbolNameTable[SYMBOLTABLE_MAXSIZE];
TypeIndex SymbolTableFirstFree; // First free index

// String memory
char      StringMem[STRINGMEM_MAXSIZE];
TypeIndex StringMemFirstFree;   // First free index
TypeIndex StringMemStringStart; // Current string start index

// Handle table (map a handle index to a pointer)
#define HANDLE_UNDEFINED     -2
#define HANDLE_END           -1
#define HANDLE_TYPE_STRING    1
#define HANDLE_TYPE_BUFFER    2
#define HANDLE_TYPE_CUSTOM    3
typedef struct {
  void*     Pointer;  // Pointer to memory buffer
  TypeIndex Size;     // Buffer size
  TypeIndex Type;     // Object type
  TypeIndex Next;     // Next in freelist
} TypeHandleStruct;
TypeHandleStruct HandleTable[HANDLETABLE_MAXSIZE];
TypeIndex  HandleTableFirstFree;

// Word macros
#define WordValue(word)           ((word) >> TAG_BITS)
#define WordOp(word)              ((word) & TAG_MASK)
#define WordWith(value, op)       ((((TypeWord)(value)) << TAG_BITS) | (op))
#define WordSetValue(word, value) ((((TypeWord)(value)) << TAG_BITS) | ((word) & TAG_MASK))
#define ListFirstIndex(list)      (WordValue(list) + 1)
#define ListFirst(list)           (ProgMem[ListFirstIndex(list)])

// --------------------------------------------------------------
// Initialize interpreter
// --------------------------------------------------------------

void LoopMachineInitialize()
{
  ProgMemFirstFree = 1;
  SymbolTableFirstFree = 0;
  StringMemFirstFree = 0;
  StringMemStringStart = 0;
  HandleTableFirstFree = HANDLE_UNDEFINED;
}

// --------------------------------------------------------------
// Include interpreter functions
// --------------------------------------------------------------

#include "unix/system.h"
#include "debug.h"
#include "lib.h"
#include "parser.h"
#include "stringify.h"

// --------------------------------------------------------------
// Interpreter loop
// --------------------------------------------------------------

#define DataStackIncrMacro() \
  ++ DataStackTop; \
  if (DataStackTop >= DATASTACK_MAXSIZE) \
    { GURU_MEDITATION("DataStack overflow"); }
#define DataStackDecrMacro() \
  -- DataStackTop; \
  if (DataStackTop < -1) \
    { GURU_MEDITATION("DataStack underflow"); }
#define DataStackGuardMacro(n) \
  if ((DataStackTop - (n)) < -1) \
    { GURU_MEDITATION("DataStack underflow guard"); }

#define CallStackIncrMacro() \
  ++ CallStackTop; \
  if (CallStackTop > CALLSTACK_MAXSIZE - 1) \
    { DebugPrintInt("CallStackTop", CallStackTop); GURU_MEDITATION("CallStack overflow"); }
#define CallStackDecrMacro() \
  -- CallStackTop;

#define ProgRunInitializeMacro(list) \
  DataStackTop = -1; \
  CallStackTop = 0; \
  /* Set up callstack to exit at top */ \
  CallStack[CallStackTop] = 0; \
  ProgMem[0] = WordWith(0, OP_EXIT); \
  /* Set instruction index to first in list*/ \
  InstrIndex = WordValue(list) + 1;

void ProgRun(TypeWord list)
{
  // Persistent variables begin with UpperCase letter
  TypeIndex DataStackTop;
  TypeIndex CallStackTop;
  TypeIndex InstrIndex;

  ProgRunInitializeMacro(list)

  // Temporary variables
  TypeWord  currentWord; // current instruction word
  TypeWord  currentOp;   // current operation
  TypeWord  A;           // top of data stack or other value
  TypeWord  B;           // second in data stack or other value

#if defined(USE_JUMP_TABLE)

  #include "opjumps.h"

  // Jump to first op
  currentWord = ProgMem[InstrIndex];
  currentOp = WordOp(currentWord);
  goto *OpJumpTable[currentOp];

  #include "opcodes.h"
  #include "unix/opcodes_unix.h"
  #if defined(USE_SDL2)
    #include "sdl/opcodes_sdl.h"
  #endif

#else // use switch

  while (TRUE)
  {
    currentWord = ProgMem[InstrIndex];
    currentOp = WordOp(currentWord);

  Top:
    switch (currentOp)
    {
      #include "opcodes.h"
      #include "unix/opcodes_unix.h"
      #if defined(USE_SDL2)
        #include "sdl/opcodes_sdl.h"
      #endif
    }
  }

#endif

ExitProgRun:

  GlobalDataStackTop = DataStackTop;
}
