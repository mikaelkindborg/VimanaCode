/*
File: stringify.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Stringify word and lists.

The stringified data is written to the StringMem buffer.
*/

// --------------------------------------------------------------
// Stringify
// --------------------------------------------------------------

// Forward declaration
void StringifyList(TypeWord list);

void StringifyChar(char c)
{
  StringMemWriteChar(c);
}

void StringifyString(char* string)
{
  char* p = string;
  while (0 != *p)
  {
    StringMemWriteChar(*p);
    ++ p;
  }
}

void StringifyInteger(TypeWord n)
{
  char buf[32];
  sprintf(buf, "%ld", (long)n);
  StringifyString(buf);
}

void StringifyVimanaString(char* string)
{
  StringMemWriteChar(STRINGBEGIN);
  StringifyString(string);
  StringMemWriteChar(STRINGEND);
}

void StringifyWord(TypeWord word)
{
  if (OP_LIST == WordOp(word))
  {
    StringifyList(word);
  }
  else
  if (OP_LISTBEGIN == WordOp(word))
  {
    // Write opening paren
    StringifyChar(LEFTPAREN);
    // Print space for empty list (see OP_LISTEND below)
    if (0 == WordValue(word))
      { StringifyChar(' '); }
  }
  else
  if (OP_LISTEND == WordOp(word))
  {
    // Remove trailing space (for the empty list the above fix in
    // OP_LISTBEGIN is needed to not overwrite the opening paren)
    -- StringMemFirstFree;
    // Write closing paren
    StringifyChar(RIGHTPAREN);
    StringifyChar(' ');
  }
  else
  if (OP_INTEGER == WordOp(word))
  {
    StringifyInteger(WordValue(word));
    StringifyChar(' ');
  }
  else
  if (OP_STRING == WordOp(word))
  {
    char* string = (char*) HandleTable[WordValue(word)].Pointer;
    StringifyVimanaString(string);
    StringifyChar(' ');
  }
  else
  if (OP_HANDLE == WordOp(word))
  {
    StringifyString("#HANDLE");
    StringifyChar(' ');
  }
  else
  if (OP_SYMBOL == WordOp(word) || OP_FUNCTION == WordOp(word))
  {
    char* string = SymbolTableLookupNameForIndex(WordValue(word));
    StringifyString(string);
    StringifyChar(' ');
  }
  else // op code
  {
    char* string = OpTableLookupNameForCode(WordOp(word));
    if (NULL == string)
      { StringifyString("#UNKNOWN"); }
    else
      { StringifyString(string); }
    StringifyChar(' ');
  }
}

void StringifyList(TypeWord list)
{
  // Get index of list begin
  TypeIndex index = WordValue(list);

  // Get list length
  TypeIndex length = WordValue(ProgMem[index]);

  // Print elements until list end
  for (int i = index; i <= index + length + 1; ++ i)
  {
    StringifyWord(ProgMem[i]);
  }
}

//
// Stringify returns a pointer to a zero-terminated
// (temporary) string. The string memory area is used as a
// temporary buffer.
//
// The returned string must be kept or dropped before
// next call to Stringify. Keeping strings can use up
// memory quickly. Consider making a copy of the string,
// and then drop it.
//
// To keep the string call:
//   StringMemKeep();
//
// To drop the string call:
//   StringMemReset();
//
// Example:
//   char* string = Stringify(list);
//   printf("%s\n", string);
//   StringMemReset();
//
char* Stringify(TypeWord word)
{
  // Stringify word
  StringifyWord(word);

  // Terminate string
  StringMemWriteEnd();

  // Return pointer to (temporary) string buffer
  return StringMemGetPointer();
}

void ProgPrint(TypeWord word)
{
  char* string = Stringify(word);
  printf("%s", string);
  StringMemReset();
}

void ProgPrintNewLine()
{
  printf("\n");
}