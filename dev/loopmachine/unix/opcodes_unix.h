/*
File: opcodes_unix.h
Author: Mikael Kindborg (mikael@kindborg.com)

Opcodes for Unix/Linux/macOS.
*/

// command systemCommand -> stringresult
// Example: {pbpaste} systemCommand
OpCodeBegin(OP_UNIX_SYSTEMCOMMAND, "systemCommand")
{
  DataStackGuardMacro(1)

  // Get command string
  TypeIndex commandHandle = WordValue(DataStack[DataStackTop]);
  char* command = HandleTable[commandHandle].Pointer;

  // Allocate handle to hold the result string
  TypeIndex dataHandle = HandleTableAllocStringBuffer(0);

  // Perform the command
  TypeIndex bytesWritten = SystemCommand(command, dataHandle);

  if (-1 == bytesWritten)
  {
    DebugPrint("Command not found");
    DebugPrint(command);
    // Free handle?
    GURU_MEDITATION("SystemCommand error");
  }

  // Push string on data stack
  DataStack[DataStackTop] = WordWith(dataHandle, OP_STRING);

  ++ InstrIndex;
}
OpCodeEnd()

// data command systemCommandSend ->
// Example: {FOOBAR} {pbcopy} systemCommandSendData
OpCodeBegin(OP_UNIX_SYSTEMCOMMANDSENDDATA, "systemCommandSendData")
{
  DataStackGuardMacro(2)

  // Get pointer to command string
  TypeIndex handle = WordValue(DataStack[DataStackTop]);
  char* command = HandleTable[handle].Pointer;
  DataStackDecrMacro()

  // Get pointer to data string
  handle = WordValue(DataStack[DataStackTop]);
  char* data = HandleTable[handle].Pointer;
  DataStackDecrMacro()

  TypeBool success = SystemCommandSendData(command, data);
  if (!success)
    { GURU_MEDITATION("SystemCommandSendData error"); }

  ++ InstrIndex;
}
OpCodeEnd()

// millis --> timestamp
OpCodeBegin(OP_UNIX_MILLIS, "millis")
{
  DataStackIncrMacro()
  long millis = UnixMillis();
  DataStack[DataStackTop] = WordWith(millis, OP_INTEGER);
  ++ InstrIndex;
}
OpCodeEnd()

// millis sleep -->
OpCodeBegin(OP_UNIX_SLEEP, "sleep")
{
  DataStackGuardMacro(1)
  TypeWord millis = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  UnixSleep(millis);
  ++ InstrIndex;
}
OpCodeEnd()

// micros usleep -->
OpCodeBegin(OP_UNIX_USLEEP, "usleep")
{
  DataStackGuardMacro(1)
  TypeWord micros = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  usleep(micros);
  ++ InstrIndex;
}
OpCodeEnd()

// n random --> random integer between 0 and n (exclusive)
OpCodeBegin(OP_UNIX_RANDOM, "random")
{
  DataStackGuardMacro(1)
  TypeWord n = WordValue(DataStack[DataStackTop]);
  DataStack[DataStackTop] = WordWith(UnixRandom(n), OP_INTEGER);
  ++ InstrIndex;
}
OpCodeEnd()
