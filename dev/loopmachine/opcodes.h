// OP Codes
/*
File: opcodes.h
Author: Mikael Kindborg (mikael@kindborg.com)

Core opcodes.
*/

#if defined(USE_JUMP_TABLE)
  // Computed goto dispatch
  #define OpCodeBegin(opcode, name) opcode:
  #define OpCodeEnd() \
      currentWord = ProgMem[InstrIndex]; \
      currentOp = WordOp(currentWord); \
      goto *OpJumpTable[currentOp];
#else
  // Switch dispatch
  #define OpCodeBegin(opcode, name) case opcode:
  #define OpCodeEnd() break;
#endif

// ==============================================================
// INTERNAL OPCODES - NOT CALLABLE FROM CODE
// ==============================================================

// --------------------------------------------------------------
// Undefined / uncallable
// --------------------------------------------------------------

OpCodeBegin(OP_UNDEFINED, "OpUndefined")
  DebugPrintSymbolTable("OP_UNDEFINED");
  GURU_MEDITATION("OP_UNDEFINED Symbol is undefined");
OpCodeEnd()

OpCodeBegin(OP_LISTBEGIN, "OpListBegin")
  DebugPrintProgMem("OP_LISTBEGIN");
  DebugPrintInt("InstrIndex", InstrIndex);
  GURU_MEDITATION("OP_LISTBEGIN is not callable");
OpCodeEnd()

// --------------------------------------------------------------
// Push value of global symbol to data stack
// --------------------------------------------------------------

OpCodeBegin(OP_SYMBOL, "OpSymbol")
  DataStackIncrMacro()
  DataStack[DataStackTop] = SymbolTable[WordValue(currentWord)];
  ++ InstrIndex;
OpCodeEnd()

// --------------------------------------------------------------
// Push word to data stack (integer, list, string)
// --------------------------------------------------------------

OpCodeBegin(OP_INTEGER, "OpInteger")
  DataStackIncrMacro()
  DataStack[DataStackTop] = currentWord;
  ++ InstrIndex;
OpCodeEnd()

OpCodeBegin(OP_LIST, "OpList")
  DataStackIncrMacro()
  DataStack[DataStackTop] = currentWord;
  ++ InstrIndex;
OpCodeEnd()

OpCodeBegin(OP_STRING, "OpString")
  DataStackIncrMacro()
  DataStack[DataStackTop] = currentWord;
  ++ InstrIndex;
OpCodeEnd()

OpCodeBegin(OP_HANDLE, "OpHandle")
  DataStackIncrMacro()
  DataStack[DataStackTop] = currentWord;
  ++ InstrIndex;
OpCodeEnd()

// --------------------------------------------------------------
// Perform function call
// --------------------------------------------------------------

// If we want automatic tailcalls, check if the word
// is last in list (tail position)
OpCodeBegin(OP_FUNCTION, "OpFunction")
  // Save instruction index on return stack
  CallStackIncrMacro()
  CallStack[CallStackTop] = InstrIndex + 1;

  // Function words have the index to the defined word in the
  // value field - set instruction index to first element in list
  InstrIndex = ListFirstIndex(SymbolTable[WordValue(currentWord)]);
OpCodeEnd()

// --------------------------------------------------------------
// Return instruction
// --------------------------------------------------------------

OpCodeBegin(OP_LISTEND, "OpListEnd")
  // Pop instruction index from return stack
  InstrIndex = CallStack[CallStackTop];
  CallStackDecrMacro()
OpCodeEnd()

// ==============================================================
// OPCODES CALLABLE FROM CODE
// ==============================================================

// --------------------------------------------------------------
// Exit interpreter loop
// --------------------------------------------------------------

OpCodeBegin(OP_EXIT, "exit")
  goto ExitProgRun;
OpCodeEnd()

// --------------------------------------------------------------
// Set global variable
// --------------------------------------------------------------

// [name] data : ->
OpCodeBegin(OP_DEF, ":")
{
  DataStackGuardMacro(2)
  // Get value/body
  TypeWord value = DataStack[DataStackTop];
  DataStackDecrMacro()
  // Get list head with symbol
  TypeWord list = DataStack[DataStackTop];
  DataStackDecrMacro()
  // Get symbol index from first in list
  TypeIndex symbolIndex = WordValue(ListFirst(list));
  // Set value for global symbol
  SymbolTable[symbolIndex] = value;
  // Move to next instruction
  ++ InstrIndex;
}
OpCodeEnd()

// data -> globalname ->
OpCodeBegin(OP_SETARROW, "->")
  DataStackGuardMacro(1)
  // Get value on data stack
  A = DataStack[DataStackTop];
  DataStackDecrMacro()
  // Get symbol index
  ++ InstrIndex;
  currentWord = ProgMem[InstrIndex];
  // Set symbol value to stack value
  SymbolTable[WordValue(currentWord)] = A;
  ++ InstrIndex;
OpCodeEnd()

// -------------------------------------------------------
// Call / eval / parse
// -------------------------------------------------------

// tail function ->
OpCodeBegin(OP_TAILCALL, "tail")
  // The function word is the next element
  ++ InstrIndex;
  currentWord = ProgMem[InstrIndex];
  // Set instruction index to first element in list
  InstrIndex = ListFirstIndex(SymbolTable[WordValue(currentWord)]);
OpCodeEnd()

// list eval ->
OpCodeBegin(OP_EVAL, "eval")
  DataStackGuardMacro(1)
  // Save instruction index on return stack
  CallStackIncrMacro()
  CallStack[CallStackTop] = InstrIndex + 1;
  // Set instruction index to list
  InstrIndex = ListFirstIndex(DataStack[DataStackTop]);
  DataStackDecrMacro()
OpCodeEnd()

// list taileval ->
OpCodeBegin(OP_TAILEVAL, "taileval")
  // Set instruction index to start of list
  InstrIndex = ListFirstIndex(DataStack[DataStackTop]);
  DataStackDecrMacro()
OpCodeEnd()

// stringHandle parse -> list
OpCodeBegin(OP_PARSE, "parse")
  DataStackGuardMacro(1)
  // Get program string
  A = DataStack[DataStackTop];
  // Set stack to to parsed list
  DataStack[DataStackTop] = ParseString(HandleTable[WordValue(A)].Pointer);
  ++ InstrIndex;
OpCodeEnd()

// stringHandle is a filename
// stringHandle parseFile -> list
OpCodeBegin(OP_PARSEFILE, "parseFile")
  DataStackGuardMacro(1)
  // Get file name
  A = DataStack[DataStackTop];
  // Set stack to to parsed list
  DataStack[DataStackTop] = ParseFile(HandleTable[WordValue(A)].Pointer);
  ++ InstrIndex;
OpCodeEnd()

// --------------------------------------------------------------
// Conditional functions
// --------------------------------------------------------------

// bool ifTrue list ->
OpCodeBegin(OP_IFTRUE, "ifTrue")
  DataStackGuardMacro(1)
  ++ InstrIndex;
  if (WordValue(DataStack[DataStackTop]))
    { CallStackIncrMacro()
      CallStack[CallStackTop] = InstrIndex + 1;
      currentWord = ProgMem[InstrIndex];
      InstrIndex = ListFirstIndex(currentWord); }
  else
    { ++ InstrIndex; }
  DataStackDecrMacro()
OpCodeEnd()

// bool not -> bool
OpCodeBegin(OP_LOGICALNOT, "not")
  DataStackGuardMacro(1)
  DataStack[DataStackTop] = WordWith(!(WordValue(DataStack[DataStackTop])), OP_INTEGER);
OpCodeEnd()

#define IfTailMacro(condition) \
  ++ InstrIndex; \
  if (condition) { \
    currentWord = ProgMem[InstrIndex]; \
    InstrIndex = ListFirstIndex(currentWord); } \
  else { ++ InstrIndex; }

// bool ifTrueTail list ->
OpCodeBegin(OP_IFTRUETAIL, "ifTrueTail")
  DataStackGuardMacro(1)
  IfTailMacro(WordValue(DataStack[DataStackTop]))
  DataStackDecrMacro()
OpCodeEnd()

// bool ifFalseTail list ->
OpCodeBegin(OP_IFFALSETAIL, "ifFalseTail")
  DataStackGuardMacro(1)
  IfTailMacro(! WordValue(DataStack[DataStackTop]))
  DataStackDecrMacro()
OpCodeEnd()

// Wording: https://english.stackexchange.com/questions/391839/why-is-the-opposite-of-greater-than-less-than

// n1 n2 ifSmallerTail list -> (perform branch if n2 is smaller than n1)
OpCodeBegin(OP_IFSMALLERTAIL, "ifSmallerTail")
  DataStackGuardMacro(2)
  IfTailMacro(WordValue(DataStack[DataStackTop - 1]) >
              WordValue(DataStack[DataStackTop]))
  DataStackDecrMacro()
  DataStackDecrMacro()
OpCodeEnd()

// n1 n2 ifLargerTail list -> (perform branch if n2 is larger than n1)
OpCodeBegin(OP_IFLARGERTAIL, "ifLargerTail")
  DataStackGuardMacro(2)
  IfTailMacro(WordValue(DataStack[DataStackTop - 1]) <
              WordValue(DataStack[DataStackTop]))
  DataStackDecrMacro()
  DataStackDecrMacro()
OpCodeEnd()

#undef IfTailMacro

// -------------------------------------------------------
// Math functions
// -------------------------------------------------------

#define MathBinaryOpMacro(operation) \
  DataStackGuardMacro(2) \
  A = DataStack[DataStackTop]; \
  DataStackDecrMacro() \
  B = DataStack[DataStackTop]; \
  DataStack[DataStackTop] = WordSetValue(B, WordValue(B) operation WordValue(A)); \
  ++ InstrIndex;

// n1 n2 + -> n
OpCodeBegin(OP_ADD, "+")
  MathBinaryOpMacro(+)
OpCodeEnd()

// n1 n2 - -> n
OpCodeBegin(OP_SUB, "-")
  MathBinaryOpMacro(-)
OpCodeEnd()

// n1 n2 * -> n
OpCodeBegin(OP_MULT, "*")
  MathBinaryOpMacro(*)
OpCodeEnd()

// n1 n2 / -> n
OpCodeBegin(OP_DIV, "/")
  MathBinaryOpMacro(/)
OpCodeEnd()

#undef MathBinaryOpMacro

#define MathUnaryOpMacro(operation) \
  DataStackGuardMacro(1) \
  A = DataStack[DataStackTop]; \
  DataStack[DataStackTop] = WordSetValue(A, WordValue(A) operation); \
  ++ InstrIndex;

// n 1+ -> n
OpCodeBegin(OP_ADD1, "1+")
  MathUnaryOpMacro(+ 1)
OpCodeEnd()

// n 1- -> n
OpCodeBegin(OP_SUB1, "1-")
  MathUnaryOpMacro(- 1)
OpCodeEnd()

// n 2+ -> n
OpCodeBegin(OP_ADD2, "2+")
  MathUnaryOpMacro(+ 2)
OpCodeEnd()

// n 2- -> n
OpCodeBegin(OP_SUB2, "2-")
  MathUnaryOpMacro(- 2)
OpCodeEnd()

#undef MathUnaryOpMacro

// Increment global variable
// number number + -> number
OpCodeBegin(OP_INCR, "incr")
  // Get symbol index (A)
  ++ InstrIndex;
  A = WordValue(ProgMem[InstrIndex]);
  // Get current symbol value (B)
  B = SymbolTable[A];
  // Set incremented value
  SymbolTable[A] = WordWith(WordValue(B) + 1, WordOp(B));
  ++ InstrIndex;
OpCodeEnd()

// n1 n2 isSmaller -> true if n2 is smaller than n1
OpCodeBegin(OP_ISSMALLER, "isSmaller")
  DataStackGuardMacro(2)
  A = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  B = WordValue(DataStack[DataStackTop]);
  DataStack[DataStackTop] = WordWith(B > A, OP_INTEGER);
  ++ InstrIndex;
OpCodeEnd()

// n1 n2 isLarger -> true if n2 is larger than n1
OpCodeBegin(OP_ISLARGER, "isLarger")
  DataStackGuardMacro(2)
  A = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  B = WordValue(DataStack[DataStackTop]);
  DataStack[DataStackTop] = WordWith(B < A, OP_INTEGER);
  ++ InstrIndex;
OpCodeEnd()

// Test if value fields of two words are equal (does not test type/opcode)
// word1 word2 eq -> true if WordValue(word1) == WordValue(word2)
OpCodeBegin(OP_EQ, "eq")
  DataStackGuardMacro(2)
  A = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  B = WordValue(DataStack[DataStackTop]);
  DataStack[DataStackTop] = WordWith(B == A, OP_INTEGER);
  //DebugPrintWord("OPWORD", DataStack[DataStackTop]);
  //printf("OP_EQ: %i\n", (B == A));
  ++ InstrIndex;
OpCodeEnd()

// --------------------------------------------------------------
// Stack operations
// --------------------------------------------------------------

// word A -> word word (dup)
OpCodeBegin(OP_A, "A")
  DataStackGuardMacro(1)
  DataStackIncrMacro()
  DataStack[DataStackTop] = DataStack[DataStackTop - 1];
  ++ InstrIndex;
OpCodeEnd()

// word1 word2 B -> word1 word2 word1 (over)
OpCodeBegin(OP_B, "B")
  DataStackGuardMacro(2)
  DataStackIncrMacro()
  DataStack[DataStackTop] = DataStack[DataStackTop - 2];
  ++ InstrIndex;
OpCodeEnd()

// word1 word2 word3 C -> word1 word2 word3 word1
OpCodeBegin(OP_C, "C")
  DataStackGuardMacro(3)
  DataStackIncrMacro()
  DataStack[DataStackTop] = DataStack[DataStackTop - 3];
  ++ InstrIndex;
OpCodeEnd()

// word1 word2 swap -> word2 word1
OpCodeBegin(OP_SWAP, "swap")
  DataStackGuardMacro(2)
  A = DataStack[DataStackTop];
  DataStack[DataStackTop] = DataStack[DataStackTop - 1];
  DataStack[DataStackTop - 1] = A;
  ++ InstrIndex;
OpCodeEnd()

// word1 word2 word3 rot -> word2 word3 word1
OpCodeBegin(OP_ROT, "rot")
  DataStackGuardMacro(3)
  A = DataStack[DataStackTop];
  B = DataStack[DataStackTop - 1];
  DataStack[DataStackTop] = DataStack[DataStackTop - 2];
  DataStack[DataStackTop - 2] = B;
  DataStack[DataStackTop - 1] = A;
  ++ InstrIndex;
OpCodeEnd()

// word _ ->
OpCodeBegin(OP_DROP_, "_")
  DataStackGuardMacro(1)
  DataStackDecrMacro()
  ++ InstrIndex;
OpCodeEnd()

// --------------------------------------------------------------
// Call opcode defined by value field
// --------------------------------------------------------------

OpCodeBegin(OP_OPCALL, "OpCall")
  // Set currentOp to word value and goto Top if switched
  #if defined(USE_JUMP_TABLE)
    // Computed goto dispatch
    currentOp = WordValue(currentWord);
    goto *OpJumpTable[currentOp];
  #else
    // Switch dispatch
    currentOp = WordValue(currentWord);
    goto Top;
  #endif
  // DO NOT USE: OpCodeEnd()

// --------------------------------------------------------------
// ProgMem operations
// --------------------------------------------------------------

// progMemGetFirstFree -> index
OpCodeBegin(OP_PROGMEMGETFIRSTFREE, "progMemGetFirstFree")
  DataStackIncrMacro()
  DataStack[DataStackTop] = WordWith(ProgMemFirstFree, OP_INTEGER);
  ++ InstrIndex;
OpCodeEnd()

// index progMemSetFirstFree ->
OpCodeBegin(OP_PROGMEMSETFIRSTFREE, "progMemSetFirstFree")
  DataStackGuardMacro(1)
  ProgMemFirstFree = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  ++ InstrIndex;
OpCodeEnd()

// --------------------------------------------------------------
// List operations
// --------------------------------------------------------------

// Allocate a list in progmem. To deallocate, reset ProgMemFirstFree.
// numItems listAlloc -> list
OpCodeBegin(OP_LISTALLOC, "listAlloc")
{
  DataStackGuardMacro(1)

  // Write list begin
  TypeIndex numItems = WordValue(DataStack[DataStackTop]);
  TypeWord listIndex = ProgMemWrite(WordWith(numItems, OP_LISTBEGIN));

  // Initialize list with integer items
  for (int i = 0; i < numItems; ++ i)
  {
    ProgMemWrite(WordWith(0, OP_INTEGER));
  }

  // Write list end
  ProgMemWrite(WordWith(0, OP_LISTEND));

  // Push list head to stack
  DataStack[DataStackTop] = WordWith(listIndex, OP_LIST);

  ++ InstrIndex;
}
OpCodeEnd()

// list index listGetAt -> value
OpCodeBegin(OP_LISTGETAT, "listGetAt")
{
  DataStackGuardMacro(2)
  TypeIndex index = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  TypeIndex listFirstIndex = WordValue(DataStack[DataStackTop]);
  DataStack[DataStackTop] = ProgMem[listFirstIndex + 1 + index];
  ++ InstrIndex;
}
OpCodeEnd()

// list value index listSetAt ->
OpCodeBegin(OP_LISTSETAT, "listSetAt")
{
  DataStackGuardMacro(3)
  TypeIndex index = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  TypeWord value = DataStack[DataStackTop];
  DataStackDecrMacro()
  TypeIndex listFirstIndex = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  ProgMem[listFirstIndex + 1 + index] = value;
  ++ InstrIndex;
}
OpCodeEnd()

// symbol symbolValue -> value
OpCodeBegin(OP_SYMBOLVALUE, "symbolValue")
{
  DataStackGuardMacro(1)
  TypeIndex symbolIndex = WordValue(DataStack[DataStackTop]);
  DataStack[DataStackTop] = SymbolTable[symbolIndex];
  ++ InstrIndex;
}
OpCodeEnd()

// --------------------------------------------------------------
// Handle operations
// --------------------------------------------------------------

// handle handleFree ->
OpCodeBegin(OP_HANDLEFREE, "handleFree")
{
  DataStackGuardMacro(1)
  TypeWord handle = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  HandleTableFreeHandle(handle);
  ++ InstrIndex;
}
OpCodeEnd()

// handleTableFreeAll ->
OpCodeBegin(OP_HANDLETABLEFREEALL, "handleTableFreeAll")
{
  HandleTableFreeAll();
  ++ InstrIndex;
}
OpCodeEnd()

// --------------------------------------------------------------
// String buffer operations
// --------------------------------------------------------------

// Use handleFree to free the allocated string buffer
// size stringAlloc -> handle
OpCodeBegin(OP_STRINGALLOC, "stringAlloc")
{
  DataStackGuardMacro(1)
  TypeWord size = WordValue(DataStack[DataStackTop]);
  TypeIndex handle = HandleTableAllocStringBuffer(size);
  DataStack[DataStackTop] = WordWith(handle, OP_STRING);
  ++ InstrIndex;
}
OpCodeEnd()

// handle index stringAt -> char
OpCodeBegin(OP_STRINGAT, "stringAt")
{
  DataStackGuardMacro(2)
  TypeWord index = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  TypeWord handle = WordValue(DataStack[DataStackTop]);
  char c = StringBufferGetAt(handle, index);
  DataStack[DataStackTop] = WordWith(c, OP_INTEGER);
  ++ InstrIndex;
}
OpCodeEnd()

// handle char index stringSetAt ->
OpCodeBegin(OP_STRINGSETAT, "stringSetAt")
{
  DataStackGuardMacro(3)
  TypeWord index = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  TypeWord c = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  TypeWord handle = WordValue(DataStack[DataStackTop]);
  DataStackDecrMacro()
  StringBufferSetAt(handle, index, (char) c);
  ++ InstrIndex;
}
OpCodeEnd()

// --------------------------------------------------------------
// Printing
// --------------------------------------------------------------

OpCodeBegin(OP_PRINT, "print")
  DataStackGuardMacro(1)
  A = DataStack[DataStackTop];
  DataStackDecrMacro()
  ProgPrint(A);
  ProgPrintNewLine();
  ++ InstrIndex;
OpCodeEnd()

OpCodeBegin(OP_PRINTSTACK, "printstack")
{
  printf("[ ");
  for (TypeIndex i = 0; i <= DataStackTop; ++ i)
  {
    //if (i > 0) { printf(" "); }
    ProgPrint(DataStack[i]);
  }
  printf("]\n");
  ++ InstrIndex;
}
OpCodeEnd()

OpCodeBegin(OP_PRINTPROGMEM, "printProgMem")
{
  DebugPrintProgMem("PROGMEM");
  ++ InstrIndex;
}
OpCodeEnd()

OpCodeBegin(OP_PRINTSTRINGMEM, "printStringMem")
{
  DebugPrintStringMem("STRINGMEM");
  ++ InstrIndex;
}
OpCodeEnd()

OpCodeBegin(OP_PRINTSYMBOLTABLE, "printSymbolTable")
{
  DebugPrintSymbolTable("SYMBOLTABLE");
  ++ InstrIndex;
}
OpCodeEnd()

OpCodeBegin(OP_PRINTMEMORYUSE, "printMemoryUse")
{
  printf("[Memory Use]\n");
  printf("ProgMem:       %i words (%i bytes)\n",
    ProgMemFirstFree, (int)(ProgMemFirstFree * sizeof(TypeWord)));
  printf("DataStack:     %i\n", DataStackTop + 1);
  printf("CallStack:     %i\n", CallStackTop);
  printf("SymbolTable:   %i words\n", SymbolTableFirstFree);
  printf("StringMem:     %i bytes\n", StringMemFirstFree);

  ++ InstrIndex;
}
OpCodeEnd()

// --------------------------------------------------------------
// Reset and restart the interpreter
// --------------------------------------------------------------

// Clear memory and restart interpreter with the given program
// TODO: Change to list instead of string?
// stringHandle machineRestart ->
OpCodeBegin(OP_MACHINERESTART, "machineRestart")
{
  DataStackGuardMacro(1)

  // Get program string (we must copy it before it is deallocated)
  char* program = HandleTable[WordValue(DataStack[DataStackTop])].Pointer;
  char* prog = MemAlloc(strlen(program) + 1);
  if (NULL == prog)
    { GURU_MEDITATION("Out of memory (OP_MACHINERESTART)"); }
  strcpy(prog, program);

  // Reset memory areas
  HandleTableFreeAll();
  LoopMachineInitialize();

  // Parse program
  TypeWord list = ParseString(prog);

  MemFree(prog);

  // Reset interpreter
  ProgRunInitializeMacro(list)
}
OpCodeEnd()

// Clear memory and restart interpreter with the program
// in the file.
// stringHandle machineRestartWithFile ->
OpCodeBegin(OP_MACHINERESTARTWITHFILE, "machineRestartWithFile")
{
  DataStackGuardMacro(1)

  // Get filename (we must copy it before it is deallocated)
  char* filename = HandleTable[WordValue(DataStack[DataStackTop])].Pointer;
  char* file = MemAlloc(strlen(filename) + 1);
  if (NULL == file)
    { GURU_MEDITATION("Out of memory (OP_MACHINERESTARTWITHFILE)"); }
  strcpy(file, filename);
  DebugPrint(file);

  // Reset memory areas
  HandleTableFreeAll();
  LoopMachineInitialize();

  // Parse file
  TypeWord list = ParseFile(filename);

  // Reset interpreter
  ProgRunInitializeMacro(list)
}
OpCodeEnd()
