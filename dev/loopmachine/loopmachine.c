/*
File: loopmachine.c
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

LoopMachine interperter for the Vimana programming language.

How to compile:

    cc loopmachine.c -o loopmachine -O2

Or:

    gcc loopmachine.c -o loopmachine -O2

How to evaluate a Vimana source file:

    ./loopmachine FILENAME

For example:

    ./loopmachine hi.vimana

Contents of file hi.vimana:

    [Hi World] print
    40 2 + print

----------------------------------------------------------------
TODO:
* DONE read file
* DONE Makefile
* DONE eq
* DONE LIST array or list? List has head and end, can be executed as code
* DONE listAlloc, listSetAt, listAt, e.g. 3 listAlloc -> Color
* In Vimana: listSet3, listSet1, listSet2, e.g. 255 0 0 Color listSet3
* DONE Poll clipboard
* SDL2
* DebugPrintCallStack
----------------------------------------------------------------
LuaJIT

Build:
$ git clone https://github.com/LuaJIT/LuaJIT
$ cd LuaJIT
$ MACOSX_DEPLOYMENT_TARGET=$(sw_vers --productVersion) make

https://medium.com/@michael.mogenson/write-a-macos-app-with-lua-342148381e25

miki@Mikaels-MacBook-Air luajit % time src/luajit -joff ../VimanaCode/experiments/vimanac/benchmark/fib.lua
24157817
--> 1.32s
--> 1.25s

miki@Mikaels-MacBook-Air luajit % time src/luajit ../VimanaCode/experiments/vimanac/benchmark/fib.lua
24157817
--> 0.23s

miki@Mikaels-MacBook-Air loopmachine % time ./loopmachine fib.vimana
Welcome to the Wonderful LoopMachine
ProgMem size:       500 words
DataStack size:     50 words
CallStack size:     50 words
SymbolTable size:   20 words
ParseQueue size:    200 words
StringMem size:     500 bytes
Max token length:   32 bytes
Number of tag bits: 8
Number of opcodes:  43
Word size:          8 bytes
24157817
[End of Program]
[DataStack size: -1]
--> 0.62s (use optimized opcodes)
--> 0.67s (without 2-)
--> 0.72s (without ifSmallerTail)
--> 0.76s (no optimized opcodes)

With handletable (see branch handletable) add around 0.2s to the above numbers.
*/

#define USE_JUMP_TABLE

// --------------------------------------------------------------
// Include files
// --------------------------------------------------------------

#include <limits.h>
#include "loopmachine.h"

// --------------------------------------------------------------
// Main
// --------------------------------------------------------------

int main(int numargs, char* args[])
{
  char*  fileName = NULL;
  char   dirName[PATH_MAX];

  printf("Welcome to the Wonderful LoopMachine\n");

  if (2 != numargs)
  {
    printf("Specify a filename to evaluate a Vimana source file:\n");
    printf("./loopmachine FILENAME\n");
    printf("Example:\n");
    printf("./loopmachine hi.vimana\n");

    return 0;
  }

  printf("[Allocated Memory]\n");
  printf("ProgMem:          %i words\n", PROGMEM_MAXSIZE);
  printf("DataStack:        %i words\n", DATASTACK_MAXSIZE);
  printf("CallStack:        %i entries\n", CALLSTACK_MAXSIZE);
  printf("SymbolTable:      %i words\n", SYMBOLTABLE_MAXSIZE);
  printf("ParseQueue:       %i words\n", PARSE_QUEUE_MAXSIZE);
  printf("StringMem:        %i bytes\n", STRINGMEM_MAXSIZE);
  printf("Max token length: %i bytes\n", MAX_TOKEN_LENGTH);
  printf("Num op bits:      %i\n", TAG_BITS);
  printf("Num opcodes:      %i\n", OP_LAST_ENTRY);
  printf("Primary ops:      %i\n", OP_OPCALL + 1);
  printf("Word size:        %i bytes\n", (int) sizeof(TypeWord));
  printf("Total allocated:  %i bytes\n",
    (int)((PROGMEM_MAXSIZE * sizeof(TypeWord)) +
    (DATASTACK_MAXSIZE * sizeof(TypeWord)) +
    (CALLSTACK_MAXSIZE * sizeof(TypeIndex)) +
    (SYMBOLTABLE_MAXSIZE * sizeof(TypeWord)) +
    (SYMBOLTABLE_MAXSIZE * sizeof(TypeWord)) +
    (PARSE_QUEUE_MAXSIZE * sizeof(TypeWord)) +
    (STRINGMEM_MAXSIZE))
  );
  printf("------------------------------------------------------------\n");

  // Set filename
  fileName = args[1];

  // Change working directory to that of the source file
  FileDirName(fileName, dirName);
  SetWorkingDir(dirName);

  // Initialize interpreter
  LoopMachineInitialize();

  // Parse source file and run program
  char* baseName = FileBaseName(fileName);
  TypeWord list = ParseFile(baseName);
  if (OP_UNDEFINED == WordOp(list))
  {
    printf("Cannot read source file\n");
  }
  else
  {
    // Run program
    ProgRun(list);

    printf("------------------------------------------------------------\n");
    printf("End of Program - See You Soon :)\n");
    printf("DataStackTop:    %i\n", GlobalDataStackTop + 1);
    printf("MemAllocCounter: %i\n", MemAllocCounter);
    printf("[0 means empty data stack and all memory blocks deallocated]\n");
  }

  return 0;
}

/*
  <^_^>
  [^_^]
  (^_^)
   ^_^

  [fib]
    [A 1 ifSmallerTail
      [A 1- fib swap 2- fib +]] :
  37 fib print
*/
