/*
File: debug.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Debug functions
*/

// --------------------------------------------------------------
// Debug functions
// --------------------------------------------------------------

void DebugPrintWord(char* label, TypeWord word)
{
  printf("[%s] Op: %s Value: %i\n",
    label, OpNameTable[WordOp(word)], (int)WordValue(word));
}

void DebugPrintList(char* label, TypeWord list)
{
  char labelBuf[8];
  printf("[%s] List Data:\n", label);
  DebugPrintWord("List", list);

  TypeIndex index = WordValue(list);
  TypeWord word = ProgMem[index];
  TypeIndex endIndex = WordValue(word);

  for (int i = index; i <= endIndex; ++ i)
  {
    word = ProgMem[i];
    sprintf(labelBuf, "%i", (int) i);
    DebugPrintWord(labelBuf, word);
  }
}

void DebugPrintProgMem(char* label)
{
  char labelBuf[8];

  printf("[%s] Program Memory:\n", label);
  int i = 0;
  while (i < ProgMemFirstFree)
  {
    sprintf(labelBuf, "%i", (int) i);
    DebugPrintWord(labelBuf, ProgMem[i]);
    ++ i;
  }
}

void DebugPrintSymbolTable(char* label)
{
  char labelBuf[MAX_TOKEN_LENGTH + 8];

  printf("[%s] Symbol Table:\n", label);
  int i = 0;
  while (i < SymbolTableFirstFree)
  {
    sprintf(labelBuf, "%i %s", (int) i, SymbolNameTable[i]);
    DebugPrintWord(labelBuf, SymbolTable[i]);
    ++ i;
  }
}

void DebugPrintStringMem(char* label)
{
  printf("[%s] String memory:\n", label);
  int i = 0;
  while (i < StringMemFirstFree)
  {
    if (0 == StringMem[i])
    {
      printf("\n");
    }
    else
    {
      printf("%c", StringMem[i]);
    }
    ++ i;
  }
}

#define DebugPrint(string) printf("[%s]\n", string)

#define DebugPrintInt(label, i) printf("[%s: %i]\n", label, (int)i)

#define DebugPrintNewLine() printf("\n")
