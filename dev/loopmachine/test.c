/*
File: test.c
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Test programs, benchmarks.
*/

/*
# Benchmarks

## Apple M1 Tests

### Benchmark: Fibonacci

Compute Fibonacci number 37 recursively.

#### GForth 0.7.3

    time gforth ../vimanac/benchmark/fib.fs
    0.88s

    : fib ( n1 -- n2 )
      dup 1 > if
        dup 1- recurse swap 2 - recurse +
      then ;
    37 fib . cr
    bye

#### Lua 5.4.6

    time lua ../vimanac/benchmark/fib.lua
    1.87s

    function fib(n)
      return n < 2 and n or fib(n - 1) + fib(n - 2)
    end
    print(fib(37))

#### PHP 8.3.4

    time php ../vimanac/benchmark/fib.php
    1.81s

    <?php
    function fib($n)
    {
      if ($n < 2) return $n;
      return fib($n - 1) + fib($n - 2);
    }
    echo fib(37) . "\n";

#### LoopMachine

File: loopmachine.c (this file)

Uncomment/comment code below to run different tests.

    cc loopmachine.c -O2
    time ./a.out

    [fib] [A 1 isSmaller ifTrueTail [A 1 - fib swap 2- fib +]] : 37 fib print   "1.19s"
    [fib] [A 1 isSmaller ifTrueTail [A 1- fib swap 2- fib +]] : 37 fib print    "1.06s"
    [fib] [A 1 ifSmaller Tail [A 1 - fib swap 2- fib +]] : 37 fib print         "0.97s"
    [fib] [A 1 ifSmaller Tail [A 1- fib swap 2- fib +]] : 37 fib print          "0.89s"

### Benchmark: Loop and increment global counter

Increment a global variable 100000000 times using a nested loop.

#### GForth 0.7.3

    time gforth ../vimanac/benchmark/loop.fs
    0.74s

    variable loopcount
    variable counter
    10000 loopcount !
    0 counter !
    : myloop
      loopcount @ 0 DO
        loopcount @ 0 DO
    \     i . cr
          counter @ 1+ counter !
        LOOP
      LOOP ;
    myloop

#### Lua 5.4.6

    time lua ../vimanac/benchmark/loop.lua
    3.98s

    loopcount = 10000
    counter = 0
    i = 0
    while i < loopcount do
      j = 0
      while j < loopcount do
        counter = counter + 1
        j = j + 1
      end
      i = i + 1
    end
    print(counter)

#### PHP 8.3.4

    time php ../vimanac/benchmark/loop.php
    1.06s

    <?php
    $loopcount = 10000;
    $counter = 0;
    for ($i = 0; $i < $loopcount; $i++)
    {
      for ($j = 0; $j < $loopcount; $j++)
      {
        $counter = $counter + 1;
      }
    }
    echo $counter . PHP_EOL;

#### LoopMachine

File: loopmachine.c (this file)

Uncomment/comment code below to run different tests.

    cc loopmachine.c -O2
    time ./a.out

    ( 1.48s )
    [Counter] 0 :
    [LoopCount] 10000 :
    [timesworker] [A ifTrueTail [B eval 1- tail timesworker]] :
    [times] [timesworker _ _] :
    [[Counter 1+ set Counter] LoopCount times] LoopCount times Counter print

    ( 1.16s using incr )
    [Counter] 0 :
    [LoopCount] 10000 :
    [times] [timesworker _ _] :
    [timesworker] [A ifTrueTail [B eval 1- tail timesworker]] :
    [[incr Counter] LoopCount times] LoopCount times Counter print
*/

// --------------------------------------------------------------
// Include files
// --------------------------------------------------------------

#define USE_JUMP_TABLE

#include "loopmachine.h"

// --------------------------------------------------------------
// Test program
// --------------------------------------------------------------

int main()
{
  //int size = sizeof(TypeWord);
  //printf("sizeof(TypeWord): %i\n", size);

  //char prog[] = "0 ifFalseTail [42 print] 44 print";
  //char prog[] = "1 2 + print";
  //char prog[] = "42 print";
  //char prog[] = "[foo] [42] : foo print";
  //char prog[] = "42 print";
  //char prog[] = "[2 *] set double 21 double print";
  //char prog[] = "[A 1 isSmaller ifTrueTail [A 1 - fib swap 2 - fib +]] set fib 37 fib print";

  //char prog[] = "[fib] [A 1 isSmaller ifTrueTail [A 1 - fib swap 2 - fib +]] : 37 fib print"; // 1.19s
  //char prog[] = "[fib] [A 1 isSmaller ifTrueTail [A 1- fib swap 2- fib +]] : 37 fib print"; // 1.06s
  //char prog[] = "[fib] [A 1 ifSmallerTail [A 1 - fib swap 2 - fib +]] : 37 fib print"; // 0.97s
  char prog[] = "[fib] [A 1 ifSmallerTail [A 1- fib swap 2- fib +]] : 37 fib print"; // 0.89s

  //char prog[] = \
    "[LoopCount] 10 : " \
    "[times] [A ifTrueTail [B eval 1- tail times]] : " \
    "[42 print] LoopCount times";
  //char prog[] = \
    "[LoopCount] 10 : " \
    "[timesworker] [A ifTrueTail [B eval 1- tail timesworker]] : " \
    "[times] [timesworker _ _] : " \
    "[[42 print] LoopCount times] LoopCount times";

  //char prog[] = \
    "[Counter] 0 : " \
    "[LoopCount] 10000 : " \
    "[timesworker] [A ifTrueTail [B eval 1- tail timesworker]] : " \
    "[times] [timesworker _ _] : " \
    "[loop] [[[Counter 1+ -> Counter] LoopCount times] LoopCount times Counter print] : loop"; // 1.60s
  //char prog[] = \
    "[Counter] 0 : " \
    "[LoopCount] 10000 : " \
    "[times] [timesworker _ _] : " \
    "[timesworker] [A ifTrueTail [B eval 1- tail timesworker]] : " \
    "[[incr Counter] LoopCount times] LoopCount times Counter print"; // 1.26s

  FILE* stream = fmemopen(prog, strlen(prog), "r");
  //FILE* stream = fopen("fib.vimana", "r");
  TypeWord list = ParseStream(stream);
  fclose(stream);

/*
  DebugPrint("--------------------------------------------------------------");
  DebugPrintList("ParsedList", list);
  DebugPrintProgMem("ProgMem");
  DebugPrintSymbolTable("Globals");
  DebugPrintStringMem("StringMem");
  DebugPrint("--------------------------------------------------------------");
*/

  //DebugPrintProgMem("ProgMem");
  //DebugPrintWord("List", list);

  ProgPrint(list);
  ProgPrintNewLine();

  // Run
  ProgRun(list);

  return 0;
}

/*
  <^_^>
  [^_^]
  (^_^)
   ^_^

  [fib]
    [A 1 ifLessTail
      [A 1- fib swap 2- fib +]] :
  37 fib print
*/
