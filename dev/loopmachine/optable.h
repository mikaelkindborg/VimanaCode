// Table generated by op_gen.py

enum OP_CODE
{
  OP_UNDEFINED = 0,
  OP_LISTBEGIN,
  OP_SYMBOL,
  OP_INTEGER,
  OP_LIST,
  OP_STRING,
  OP_HANDLE,
  OP_FUNCTION,
  OP_LISTEND,
  OP_EXIT,
  OP_DEF,
  OP_SETARROW,
  OP_TAILCALL,
  OP_EVAL,
  OP_TAILEVAL,
  OP_PARSE,
  OP_PARSEFILE,
  OP_IFTRUE,
  OP_LOGICALNOT,
  OP_IFTRUETAIL,
  OP_IFFALSETAIL,
  OP_IFSMALLERTAIL,
  OP_IFLARGERTAIL,
  OP_ADD,
  OP_SUB,
  OP_MULT,
  OP_DIV,
  OP_ADD1,
  OP_SUB1,
  OP_ADD2,
  OP_SUB2,
  OP_INCR,
  OP_ISSMALLER,
  OP_ISLARGER,
  OP_EQ,
  OP_A,
  OP_B,
  OP_C,
  OP_SWAP,
  OP_ROT,
  OP_DROP_,
  OP_OPCALL,
  OP_PROGMEMGETFIRSTFREE,
  OP_PROGMEMSETFIRSTFREE,
  OP_LISTALLOC,
  OP_LISTGETAT,
  OP_LISTSETAT,
  OP_SYMBOLVALUE,
  OP_HANDLEFREE,
  OP_HANDLETABLEFREEALL,
  OP_STRINGALLOC,
  OP_STRINGAT,
  OP_STRINGSETAT,
  OP_PRINT,
  OP_PRINTSTACK,
  OP_PRINTPROGMEM,
  OP_PRINTSTRINGMEM,
  OP_PRINTSYMBOLTABLE,
  OP_PRINTMEMORYUSE,
  OP_MACHINERESTART,
  OP_MACHINERESTARTWITHFILE,
  OP_UNIX_SYSTEMCOMMAND,
  OP_UNIX_SYSTEMCOMMANDSENDDATA,
  OP_UNIX_MILLIS,
  OP_UNIX_SLEEP,
  OP_UNIX_USLEEP,
  OP_UNIX_RANDOM,
  OP_LAST_ENTRY
};

char* OpNameTable[] =
{
  "OpUndefined",
  "OpListBegin",
  "OpSymbol",
  "OpInteger",
  "OpList",
  "OpString",
  "OpHandle",
  "OpFunction",
  "OpListEnd",
  "exit",
  ":",
  "->",
  "tail",
  "eval",
  "taileval",
  "parse",
  "parseFile",
  "ifTrue",
  "not",
  "ifTrueTail",
  "ifFalseTail",
  "ifSmallerTail",
  "ifLargerTail",
  "+",
  "-",
  "*",
  "/",
  "1+",
  "1-",
  "2+",
  "2-",
  "incr",
  "isSmaller",
  "isLarger",
  "eq",
  "A",
  "B",
  "C",
  "swap",
  "rot",
  "_",
  "OpCall",
  "progMemGetFirstFree",
  "progMemSetFirstFree",
  "listAlloc",
  "listGetAt",
  "listSetAt",
  "symbolValue",
  "handleFree",
  "handleTableFreeAll",
  "stringAlloc",
  "stringAt",
  "stringSetAt",
  "print",
  "printstack",
  "printProgMem",
  "printStringMem",
  "printSymbolTable",
  "printMemoryUse",
  "machineRestart",
  "machineRestartWithFile",
  "systemCommand",
  "systemCommandSendData",
  "millis",
  "sleep",
  "usleep",
  "random"
};
