# File: doc.py
# Save a Vimana source file as a markdown .md file

import sys
import os.path

def main():

  # Check arguments
  #print(sys.argv)
  numArgs = len(sys.argv) - 1
  if numArgs != 1:
    print("--------------------------------------------------------------")
    print("This program saves a Vimana source file to markdown.")
    print("The output file is saved in the subdirectory 'generated',")
    print("with the extension .md")
    print("How to use:")
    print("")
    print("  python3 doc.py sourcefile.vimana")
    print("--------------------------------------------------------------")
    sys.exit(1)

  # Get arguments
  filePath = sys.argv[1]
  directory, fileName = os.path.split(filePath)
  outPath = directory + "/generated/" + fileName + ".md"
  os.makedirs(os.path.dirname(outPath), exist_ok=True)

  # Read source file
  try:
    f = open(filePath, "r", encoding="utf-8")
  except OSError as ex:
    print("Source file does not exist")
    print(ex)
    sys.exit(1)
  source = f.read()
  f.close()

  # Generated header
  data  = "__GENERATED FILE - DO NOT EDIT__\n\n"
  data += "Regenerate this file with the following command:\n\n"
  data += "    python3 doc.py " + filePath + "\n\n"

  # Append the source file
  data += source

  # Write markdown file
  f = open(outPath, "w", encoding="utf-8")
  f.write(data)
  f.close()

  print("Written file: " + outPath)

main()
