// GENERATED FILE - DO NOT EDIT
// Regenerate this file with the following command:
// python3 wrap.py ./test/test.vimana

VimanaCodeLoaderCallback('VimanaTest',
`"File: test.vimana"

# VimanaCode Test Suite

"The code below is for testing VimanaCode core functions."

## Test Helper Functions

    "Define names that read better in the test output"
    def! isTrue (ifTrue)
    def! isFalse (ifFalse)

    "Use a counter to track number of tests"
    0 -> TestCounter

    "testExpr shouldBeTrue"
    def! shouldBeTrue ((isTrue) shouldHold)

    "testExpr shouldBeFalse"
    def! shouldBeFalse ((isFalse) shouldHold)

    "testExpr condExpr shouldHold"
    def! shouldHold (
      "Increment test counter"
      TestCounter 1 + -> TestCounter
      "Print testExpr"
      :A first ^C cons :A print
      "Evaluate testExpr"
      rest eval not "evaluate testExpr"
        ({Failed} :A print error)
      ^C eval "Evaluate condExpr to check result"
    )

## Test Cases

### Basic tests

    (() isEmpty) shouldBeTrue
    ((1) isEmpty) shouldBeFalse
    (() first isEmpty) shouldBeTrue
    (() rest isEmpty) shouldBeTrue
    ((()) first isEmpty) shouldBeTrue
    ((()) rest isEmpty) shouldBeTrue
    (1) shouldBeTrue
    (0) shouldBeFalse
    (1 1 eq) shouldBeTrue
    (() () eq) shouldBeTrue
    ((1) (1) eq) shouldBeFalse
    ((a) (a) eq) shouldBeFalse
    ((1) first 1 eq) shouldBeTrue
    ((1 2) rest first 2 eq) shouldBeTrue
    ((1 2) rest rest isEmpty) shouldBeTrue
    (1 1 + 2 eq) shouldBeTrue
    (1 1 - 0 eq) shouldBeTrue
    (1 () cons first 1 eq) shouldBeTrue
    (2 1 isSmaller) shouldBeTrue
    (2 1 isBigger) shouldBeFalse
    (1 2 isSmaller) shouldBeFalse
    (1 2 isBigger) shouldBeTrue

### Set global value

    42 (FooBar1) first setval
    43 (FooBar2) set
    (FooBar3) 44 !def
    45 -> FooBar4
    def! FooBar5 46
    alt-def! FooBar6 48
    (FooBar1 42 eq) shouldBeTrue
    (FooBar2 43 eq) shouldBeTrue
    (FooBar3 44 eq) shouldBeTrue
    (FooBar4 45 eq) shouldBeTrue
    (FooBar5 46 eq) shouldBeTrue
    (FooBar6 48 eq) shouldBeTrue
    "Get global value"
    ((FooBar1) first getval 42 eq) shouldBeTrue
    ((FooBar2) get 43 eq) shouldBeTrue

### Test boolean values

    (True) shouldBeTrue
    (False) shouldBeFalse
    (Nil () eq) shouldBeTrue
    (True 1 eq) shouldBeTrue
    (False 0 eq) shouldBeTrue
    (True Nil cons first True eq) shouldBeTrue

### Test conditionals

    (True (True) ifTrue) shouldBeTrue
    (False (True) ifFalse) shouldBeTrue
    (True False (False) ifTrue) shouldBeTrue
    (True True (False) ifFalse) shouldBeTrue
    (True (True) (False) ifTrueElse) shouldBeTrue
    (False (False) (True) ifTrueElse) shouldBeTrue
    (True ifTrue: (True)) shouldBeTrue
    (True False ifTrue: (False)) shouldBeTrue
    (False ifFalse: (True)) shouldBeTrue
    (True True ifFalse: (False)) shouldBeTrue
    (True (True) ifTrueTail False) shouldBeTrue
    (False (False) ifTrueTail True) shouldBeTrue
    (True ifTrueTail: (True) False) shouldBeTrue
    (False ifTrueTail: (False) True) shouldBeTrue

### Stack operations

    (2 :A eq) shouldBeTrue
    (1 2 ^B _ 2 eq) shouldBeTrue
    (1 2 3 ^C _ _ 2 eq) shouldBeTrue
    (1 2 3 4 ^D _ _ _ 2 eq) shouldBeTrue
    (2 :A + 4 eq) shouldBeTrue
    (42 43 :B :C ^C _ ^C _ eq) shouldBeTrue
    (43 42 ^B _ 42 eq) shouldBeTrue
    (42 43 44 :C :D ^C _ ^C _ ^C _ eq) shouldBeTrue
    (42 43 :B :C ^D _ ^C _ eq) shouldBeTrue

### Test recursive function call

    "n fact -> n"
    def! fact (:A 0 eq (_ 1) (:A 1 - fact *) ifTrueElse)
    (10 fact 3628800 eq) shouldBeTrue

### Test return stack operations

"Test eval (defined above)"

    ((1 2 +) eval 3 eq) shouldBeTrue

"Test tail"

    ( (2 -> FooBar) tail
      (0) shouldBeTrue "should not happen" ) eval
    (FooBar 2 eq) shouldBeTrue

"Define and test quote using a quote-mark (not recommended)"

    def! ' (rpop :A first ^B rest rpush)
    (' QUOTE-WORKS ' QUOTE-WORKS eq) shouldBeTrue

"I do not encourage use of the quote-mark syntax, since I
find it ugly. Use lists or prefix functions to quote symbols.

By taking the first of a single element list, symbols can be
quoted in a more elegant way, using the power of the list syntax.

Quote-marks look bad in Lisp, but are needed due to the prefix
syntax where lists are evaluated. In Vimana, list are always
quoted, and no other synax is really needed (I admit prefix
functions are handy!)."

"Here is an example using @ as a short version of first.
A list-quote is just one character longer than a quote-mark."

    def! @ (first)
    ((QUOTE-WORKS)@ (QUOTE-WORKS)@ eq) shouldBeTrue

### Coroutines

    "Define basic coroutine support"
    def! coroutine (rpop rpop ^B rpush rpush)

    "Define test functions"
    def! cofun ({cofun before} print coroutine {cofun after} print)
    def! cofunTest ({cofunTest before} print cofun {cofunTest after} print)
    "Test it"
    cofunTest

### How to make a counter closure

"This test shows how to construct a kind of closure using the
first element in the function list to store a value.

This example illustrates how to generate code for functions
dynamically. The postfix '!def' function is used below, rather
than the prefix 'def!' function (which does not evaluate its
arguments)."

"Here 'def!' can be used since the function body is a literal list"

    def! makeCounter (0 () cons (:A :A first 1 + setFirst first) cons)

"Here !def (or set or ->) must be used since makeCounter is
called at runtime"

    (counter) makeCounter !def
    (counter 1 eq) shouldBeTrue
    (counter 2 eq) shouldBeTrue
    (counter 3 eq) shouldBeTrue

### Local variables

"Vimana has only global variables. Local variables can be
implemented in various ways. One way is to use global
variables, and save and restore existing values using
a name - value stack.

In the following code, such a stack is implemented using
a list to track previous variable values.

VarStack format: (value1 VarSymbol1 ...)"

    def! VarStack ()

    "value (VarSymbol) let -- save the symbol value and set new value"
    def! let (
      "Push current value and variable symbol onto the VarStack"
      first :A getval :B VarStack cons cons -> VarStack
      "Now set new value"
      setval
    )

    "endlet -- restore previous symbol value"
    def! endlet (
      "Restore previous value"
      VarStack first VarStack rest first setval
      "Remove the name - value pair from the stack"
      VarStack rest rest -> VarStack
    )

"Example function using local variables. Note that variable
names must begin with an uppercase letter (function names
begin with a non-uppercase character)."

    def! add (
      (LocalVar1) let
      (LocalVar2) let
        LocalVar1 LocalVar2 + -> LocalVar1
        LocalVar1 "put the result on data stack"
      endlet
      endlet
    )

"One thing to note in the above function, is that LocalVar1
is bound to the top of stack (A), and LocalVar2 is bound to
the second item (B). This can be counter-intuitive with
respect to the conventional reading of the parameter list
from left to right, but when using the top-of-stack view,
it could make more sense."

"A big potential problem with resuing global variables
for local variables is if/when someone calls a function
that expects a variable with the same name to be a
global one. That will not work well. A workaround could
be to name variables that are intended to be used locally
according to some convention."

### Test local variables

"Must initialize variables (workaround for interpreter behaviour)"

    1 -> LocalVar1
    1 -> LocalVar2
    (LocalVar1 1 eq) shouldBeTrue
    (LocalVar2 1 eq) shouldBeTrue
    (1 2 add 3 eq) shouldBeTrue
    (LocalVar1 1 eq) shouldBeTrue
    (LocalVar2 1 eq) shouldBeTrue

"Now introduce a version of let that uses coroutines.
The '$' function calls endlet using a coroutione call."

    "value (VarSymbol)$ -- like let but with coroutine call"
    def! $ (let coroutine endlet)

    "Test function"
    def! coadd (
      (LocalVar1)$
      (LocalVar2)$
        LocalVar1 LocalVar2 + -> LocalVar1
        LocalVar1 "put the result on data stack"
    )

    "Call test function"
    "Must initialize variables"
    1 -> LocalVar1
    1 -> LocalVar2
    (LocalVar1 1 eq) shouldBeTrue
    (LocalVar2 1 eq) shouldBeTrue
    (1 2 coadd 3 eq) shouldBeTrue
    (LocalVar1 1 eq) shouldBeTrue
    (LocalVar2 1 eq) shouldBeTrue

"Here is another test of local variables - reursive Fibonacci"

    def! LocalN 0 "Must initialize to be used as local variable"
    def! fib (
      (LocalN)$
      LocalN 2 isBigger ifTrueTail: (LocalN)
      "else" LocalN 1 - fib LocalN 2 - fib +
    )

"Test it"

    (22 fib 17711 eq) shouldBeTrue
    (LocalN 0 eq) shouldBeTrue

### Tail test

    "Test tail: function"
    (foo) (True) !def
    (tail: foo) shouldBeTrue
    "Test normal call"
    (foo) shouldBeTrue

    "Test tail: from top stack frame (should end the program)"
    (done) (
      "Print number of tests"
      (All) first TestCounter (tests passed) cons cons print
      "printstack should display empty data stack"
      {Data stack should be empty} print
      printstack
    ) !def

    "This should exit the top stack frame and end the program"
    tail: done

    {This should not happen} print
    (False) shouldBeTrue "should not happen"
`)
