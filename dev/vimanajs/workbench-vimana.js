//
// File: workbench-vimana.js
// Vimana workbench UI
// Copyright (c) 2021-2023 Mikael Kindborg
// mikael@kindborg.com
//

// Object that holds UI functions
window.VimanaUI = {}

// Create interpreter
VimanaUI.interp = new VimanaInterp()

// Evaluate a string
VimanaUI.eval = function(string)
{
  VimanaUI.interp.evalString(string)
}

// Print some object in the UI
VimanaUI.print = function(obj)
{
  console.log(obj)

  const div = document.querySelector(".vimana-frame-output")

  if (div.textContent.length > 0)
  {
     div.textContent += "\n" + obj.toString()
  }
  else
  {
    div.textContent += obj.toString()
  }

  div.scrollTop = div.scrollHeight
}

VimanaUI.clearOutput = function()
{
  const div = document.querySelector(".vimana-frame-output")
  div.innerHTML = ""
}

VimanaUI.printDataStack = function()
{
  const interp = VimanaUI.interp
  //const leftParen  = interp.useVimanaParenStyle ? LISPYLEFTPAREN  : LEFTPAREN
  //const rightParen = interp.useVimanaParenStyle ? LISPYRIGHTPAREN : RIGHTPAREN
  const leftParen  = LEFTPAREN
  const rightParen = RIGHTPAREN
  VimanaUI.print(leftParen + VimanaUI.interp.prettyPrintStack().trim() + rightParen)
}

VimanaUI.clearDataStack = function()
{
  VimanaUI.interp.dataStack = []
  VimanaUI.clearOutput()
  //VimanaUI.printDataStack()
}

// Here follows VimanaCode definitions of UI functions (using embedded JavaScript)
VimanaUI.bootstrap =
`
"Define def"
(swap set) (def) set

"Redefine print"
(print)
{
  const obj = interp.dataStackPop()
  console.log(obj)
  VimanaUI.print(obj)
}
!defprim

"Redefine printstack"
(printstack)
{
  VimanaUI.print("STACK: " + interp.prettyPrintStack())
}
!defprim

"Define eval command"
(commandEvalAsync) (
  {
    try
    {
      const selection = window.getSelection()

      if (selection.rangeCount > 0)
      {
        const code = selection.toString()
        const list = VimanaUI.interp.parse(code)

        VimanaUI.interp.evalAsync(
          list,
          function()
          {
            //console.log("commandEvalAsync FOOBAR")
            VimanaUI.printDataStack()
          })
      }
    }
    catch (exception)
    {
      console.log("EXCEPTION:")
      console.log(exception)
      VimanaUI.print(exception)
    }
  } evalJS
) def
`

VimanaUI.bootstrapX =
`
44 print
(sayhi) ({console.log("sayhi")} evalJS) def
sayhi
`
// End of VimanaUI bootstrap code

// Test for debugging
console.log('hi1')
VimanaDefinePrimFuns(VimanaUI.interp)
VimanaUI.interp.evalString("{hi2} inspect")
VimanaUI.interp.evalString("{console.log('hi3')} evalJS 42 (Foo) set Foo print")

// Run UI bootstrap code
VimanaUI.interp.evalString(VimanaUI.bootstrap)
console.log(VimanaUI.interp)
