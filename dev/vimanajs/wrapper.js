/*
File: wrapper.js
Copyright (c) 2021-2022 Mikael Kindborg
mikael@kindborg.com

Handle Vimana source code wrapped in a Javascript script
*/

window.VimanaLoadedPrograms = {}

function VimanaCodeLoaderCallback(keyName, program)
{
  VimanaLoadedPrograms[keyName] = program
}

function VimanaLoadedProgramGet(keyName)
{
  //console.log("Loading " + keyName)
  return VimanaLoadedPrograms[keyName]
}
