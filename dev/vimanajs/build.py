# File: build.py
# Script that wraps Vimana source files in Javascript
# and also generate markdown documentation files

import sys
import subprocess

def main():
  processFile("./core/core.vimana", "VimanaCoreLib")
  processFile("./core/lib.vimana", "VimanaLib")
  processFile("./test/test.vimana", "VimanaTest")
  processFile("./vimanacard/programs/vimanacard.vimana", "VimanaCardUI")
  processFile("./vimanacard/programs/home.vimana", "VimanaCardHome")
  processFile("./vimanacard/programs/welcome.vimana", "VimanaWelcomeTutorial")
  processFile("./vimanacard/programs/datastack.vimana", "VimanaDataStackTutorial")
  processFile("./vimanacard/programs/userinterface.vimana", "VimanaUITutorial")
  processFile("./vimanacard/programs/language.vimana", "VimanaLanguageOverview")
  processFile("./vimanacard/programs/functions.vimana", "VimanaFunctionReference")
  processFile("./vimanacard/programs/design.vimana", "VimanaDesign")
  processFile("./vimanacard/programs/steampunk.vimana", "VimanaSteampunk")

def processFile(fileName, keyName):
  command = "python3 wrap.py " + fileName + " " + keyName
  runCommand(command)
  command = "python3 doc.py " + fileName
  runCommand(command)

def runCommand(command):
  print(command)
  result = subprocess.run(command.split(), capture_output=True, text=True)
  print(result.stdout)

main()
