# File: wrap.py
# Wrap a Vimana source file in Javascript and generate a .js file

import sys
import os.path

def main():

  # Check arguments
  #print(sys.argv)
  numArgs = len(sys.argv) - 1
  if numArgs != 2:
    print("--------------------------------------------------------------")
    print("This program wraps a Vimana source file in Javascript.")
    print("The source code is wrapped in a Javascript callback function.")
    print("The output file is saved in the subdirectory 'generated',")
    print("with the extension .js")
    print("This is a way to load Vimana program code into the browser.")
    print("Here is how to use wrap.py:")
    print("")
    print("  python3 wrap.py sourcefile.vimana keyname")
    print("")
    print("keyname is the name for the program.")
    print("--------------------------------------------------------------")
    sys.exit(1)

  # Get arguments
  filePath = sys.argv[1]
  keyName = sys.argv[2]
  directory, fileName = os.path.split(filePath)
  outPath = directory + "/generated/" + fileName + ".js"
  os.makedirs(os.path.dirname(outPath), exist_ok=True)

  # Read source file
  try:
    f = open(filePath, "r", encoding="utf-8")
  except OSError as ex:
    print("Source file does not exist")
    print(ex)
    sys.exit(1)
  source = f.read()
  f.close()

  # Generated file header
  data  = "// GENERATED FILE - DO NOT EDIT\n"
  data += "// Regenerate this file with the following command:\n"
  data += "// python3 wrap.py " + filePath + "\n\n"

  # Wrap Vimana source code in Javascript
  data += "VimanaCodeLoaderCallback('" + keyName + "',\n`" + source + "`)\n"

  # Write JS wrapper file
  f = open(outPath, "w", encoding="utf-8")
  f.write(data)
  f.close()

  print("Written file: " + outPath)

main()
