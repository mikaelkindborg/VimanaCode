//
// File: tutorial.js
// Vimana tutorial stack
// Copyright (c) 2021-2025 Mikael Kindborg
// mikael@kindborg.com
//

// Source code for the tutorial stack
VimanaCard.CurrentStackSource =
`"Define the cards in the stack as a list"
((StackName {Vimana Tutorial})

"Home Card"
(Home (Welcome))

"Welcome Card ---------------------------------------------"
(Welcome (
{"Welcome to the Wonderful World of VimanaCode!"

"Click the icon in the lower right hand corner to display the DevPanel"

"Select this text and click the Eval button (quoted texts are comments, the rest is code)"
(255 215 0) uiSetBgColorRGB

"Then try this one"
(240 50 100) uiSetBgColorRGB

"Select this expression and click Eval (Vimana has a postfix syntax)"
1 1 +

"The resulting data stack is shown in the DevPanel (Vimana is a stack-based language)"

"Select and eval this"
1 2 3

"Then select and eval this"
+ +

"Click Clear to empty the data stack"

"Finally, select all text in this code panel and click Eval (note the plus below)"
+

"You now know how to use the UI for the Vimana programming language. Click More to explore what else you can do, or select and evaluate the word: showMeMore (code inside comments can also be selected and evaluated)"}

editorWithText)

"Syntax Card ----------------------------------------------"
(SyntaxAndNames ())

"StackOp Overview -----------------------------------------"
(StackOperations ())

)

"Set variable that holds the above list"
(CurrentStack) set

"Clear the card"
cardClear

"Show the Werlcome card"
(Welcome) cardShow`

/*
((StackName {Demo})

(Init {(Card1) cardShow})

(Card1
{{Open} ((Card2) cardShow) buttonCreate
{42 print} editorCreate})

(Card2
{{https://kindborg.com/images/vimana1.jpg} imageCreate
{Home} ((Card1) cardShow) buttonCreate})
) -> DemoStack

DemoStack stackOpen


(cardShow) (sceneClear cardFind cardGetCode evalString) !def

cardSetCode - used by the editor


(stack Demo

(card Init
{(Card1) cardShow)})

(card Card1
{sceneClear
buttonNew
  {Next} buttonText
  ((Card2) cardShow) buttonAction
  buttonShow
end})
...


Vimana.interp.evalString(Vimana.CurrentStackSource)
*/

/*

def! DemoStack
(
  (StackName {Demo Stack})
  (OnInit (Card1) cardShow)
  (OnHome (Card1) cardShow)
  (Card1
    {Goto Card 2} ((Card2) cardShow) buttonWithTextAndAction
    {42 print} editorWithText )
  (Card2
    {Goto Card 1} ((Card2) cardShow) buttonWithTextAndAction
    {44 print} editorWithText )
)

{Workspace1} editorSave
{Workspace1} editorOpen

(Card2
{{https://kindborg.com/images/vimana1.jpg} imageCreate
{Home} ((Card1) cardShow) buttonCreate})
) -> DemoStack

DemoStack stackOpen


(cardShow) (sceneClear cardFind cardGetCode evalString) !def

cardSetCode - used by the editor


(stack Demo

(card Init
{(Card1) cardShow)})

(card Card1
{sceneClear
buttonNew
  {Next} buttonText
  ((Card2) cardShow) buttonAction
  buttonShow
end})
...


Vimana.interp.evalString(Vimana.CurrentStackSource)
*/
