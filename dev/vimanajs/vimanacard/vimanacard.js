/*
File: vimanacard.js
Copyright (c) 2021-2025 Mikael Kindborg
mikael@kindborg.com

This file mainly contains functions that are called
from Javascript.

The VimanaCard UI is inspired by the HyperCard model
with a stack of cards. A stack is like a website.
Cards can link to other cards and to other stacks.

VimanaCard uses code rather that direct manipulation
to create cards.

The basic unit is the program. A stack is a program.
A card is a program within a stack. Programs can load
other programs. Programs can be stored in local storage.

The UI consists of the card, which essentially is a page
with scrollable content (this is different from the
HyperCard screen-sized cards).

The editor contains the code for the current program,
and floats on top of the card.

The development panel contains command buttons and
displays program output and the state of the data stack.
There is an icon button to show and hide panel.
*/

// --------------------------------------------------------------
// Main Object
// --------------------------------------------------------------

// Object that holds VimanaCard variables and functions
window.VimanaCard = {}

// --------------------------------------------------------------
// Create Interpreter
// --------------------------------------------------------------

VimanaCard.init = function()
{
  // Create interpreter
  VimanaCard.createInterp()

  // Load programs written in Vimana
  VimanaCard.loadSystemPrograms()
}

VimanaCard.createInterp = function()
{
  // Create interpreter
  VimanaCard.interp = VimanaInterpCreate()

  // Define primitive core functions written in Javascript
  VimanaDefinePrimFuns(VimanaCard.interp)

  // Note that additional core functions are written in Vimana
  // Call this function to define the core in Javascript for
  // improved performance (not tested)
  // NOT USED: VimanaDefineCoreNativeFuns(VimanaCard.interp)
}

// --------------------------------------------------------------
// Load Vimana Libraries and Programs
// --------------------------------------------------------------

// Load Vimana system programs (including core functions)
VimanaCard.loadSystemPrograms = function()
{
  VimanaCard.eval(VimanaCard.programLoad("VimanaCoreLib"))
  VimanaCard.eval(VimanaCard.programLoad("VimanaLib"))
  VimanaCard.eval(VimanaCard.programLoad("VimanaCardUI"))
  VimanaCard.eval(VimanaCard.programLoad("VimanaCardHome"))
  VimanaCard.editorSetText(VimanaCard.programLoad("VimanaCardHome"))
}

// Load from local storage or from loaded programs
VimanaCard.programLoad = function(name)
{
  if (null !== localStorage.getItem(name))
    { return localStorage.getItem(name) }
  else // TODO: Check if entry exists in loaded programs
    { return VimanaLoadedProgramGet(name) }
}

// --------------------------------------------------------------
// Evaluation Functions
// --------------------------------------------------------------

// Evaluate a string
VimanaCard.eval = function(string)
{
  VimanaCard.interp.evalString(string)
}

// Evaluate a list
VimanaCard.evalList = function(list)
{
  VimanaCard.interp.eval(list)
}

// --------------------------------------------------------------
// UI Functions
// --------------------------------------------------------------

// Evaluate currently selected text async (non-blocking eval)
VimanaCard.evalAsync = function()
{
  try
  {
    const selection = window.getSelection()

    if (selection.rangeCount > 0)
    {
      const code = selection.toString()
      const list = VimanaCard.interp.parse(code)

      VimanaCard.interp.evalAsync(
        list,
        function() { VimanaCard.printDataStack() } )
    }
  }
  catch (exception)
  {
    console.log("[Exception]")
    console.log(exception)
    VimanaCard.print(exception)
  }
}

VimanaCard.editorSetText = function(text)
{
  const editor = document.querySelector(".vimana-editor")
  editor.value = text
}

VimanaCard.editorGetText = function()
{
  const editor = document.querySelector(".vimana-editor")
  return editor.value
}

VimanaCard.createElement = function(className)
{
  const box = document.createElement("div")
  box.className = className
  const card = document.querySelector(".vimana-card")
  card.appendChild(box)
  return box
}

// --------------------------------------------------------------
// Print Functions
// --------------------------------------------------------------

// Print an object in the UI
VimanaCard.print = function(obj)
{
  console.log(obj)
  const div = document.querySelector(".vimana-dev-panel-editbox")
  if (div.textContent.length > 0)
    { div.textContent += "\n" + obj.toString() }
  else
    { div.textContent += obj.toString() }
  div.scrollTop = div.scrollHeight
}

VimanaCard.printDataStack = function()
{
  const interp = VimanaCard.interp
  const leftParen  = LEFTPAREN
  const rightParen = RIGHTPAREN
  VimanaCard.print(
    leftParen +
    VimanaCard.interp.printStack().trim() +
    rightParen)
}

// --------------------------------------------------------------
// Call Init Function
// --------------------------------------------------------------

VimanaCard.init()
