__GENERATED FILE - DO NOT EDIT__

Regenerate this file with the following command:

    python3 doc.py ./vimanacard/programs/language.vimana

"File: language.vimana"

{Home} ({VimanaCardHome} programOpen) uiButton

{Language Overview} uiHeading1

{Introduction} uiHeading2

{One could say that Vimana has no syntax or grammar. But it is more correct to say that the syntax is really simple.} uiText

{The only reserved characters are parens (rounded parens and square brackes) and whitespace charaters. Line breaks are treated as whitespace, and do not have any special meaning other than formatting to code to be more readable.} uiText

{A program in Vimana is a list of items. Lists can contain other lists. Items include symbols, numbers, and objects such as and strings and lists.} uiText

{Rounded parens and square brackets are used as list delimeters. They can be mixed freely. Their use is a stylistic choise made by the programmer.} uiText

{At the top level or a program, parens are not used.} uiText

{The syntax is very uniform. A program is a list. Lists are used to express program constructs, and to represent data. Conditionals and control structues use lists to represent blocks of code.} uiText

{Language Grammar} uiHeading2

{Here is a more formal definition of the grammar.} uiText

{PROGRAM   = sequence of ITEMs separated by WHITESPACE
WHITESPACE = SPACE or TAB or NEWLINE or RETURN
ITEM       = SYMBOL or INTEGER or LIST or STRING or COMMENT
INTEGER    = positive or negative integer number
SYMBOL     = GLOBALVAR or FUNCTION
GLOBALVAR  = PascalCase string (begins with an uppercase letter)
FUNCTION   = camelCase string (begins with a non uppercase letter)
LIST       = ( sequence of ITEMs separated by WHITESPACE )
ALTLIST    = [ sequence of ITEMs separated by WHITESPACE ]
STRING     = { sequence of characters }
COMMENT    = " sequence of characters "} uiEditor

"
{Execution} uiHeading2

Almost everything happens at runtime. The interpreter scans the program list and pushes elements onto a data stack. Elements are not evaluated in this step.

A function takes its parameters from the data stack.

Primitive functions can decide to evaluate parameters, to find the value of a variable, for example. They can also operate directly on the literal values on the data stack.

User-defined functions evaluate parameters when binding values to local variables, but can also operate directly on the stack without evaluating symbols, by using Forth-style stack operations.

In Lisp, lists and symbols are evaluated by default. In Vimana, lists and symbols are not evaluated until a function does so. This enables the use of unbound symbols to display text. There is no string type in the language (yet). This is also a common style in Lisp (using quoted lists and symbols).

<!--
You can however quote a symbol, for example a function symbol (preventing it from being executed), by enclosing it in a list:

Then you can pushed the value of the symbol onto the data stack (without invoking the function), by using VALUE:

    (PRINT) VALUE

You can also get the value of a value, as in this example:

    42 FOO SET
    FOO BAR SET
    BAR VALUE PRINT

(Not tested the above. VALUE is not yet in the C-version.)

There could be better ways to do this. Everything is an experiment.
-->




{The beauty of postfix notation is that there is little need to syntactically group expressions or nest functions calls. This is different from Lisp, which is a bit notorius for nested of lists that results in many parenthesis.} uiText
"