__GENERATED FILE - DO NOT EDIT__

Regenerate this file with the following command:

    python3 doc.py ./vimanacard/programs/home.vimana

"File: home.vimana"

# VimanaCard Home

"This is the source code for the home card. It creates the UI elements displayed on the screen."

"The Vimana parser supports markdown headings (they are treated as comments). Text in quote marks are also comments. The markdown style is helpful for literate programming."

"You can edit the code for the home card - it can be saved in the local storage of the web browser by evaluating the code snippet below."

"To auto-save on run, uncomment the following line (to save manually, select the text inside the quote marks and click Eval)."

    "{VimanaCardHome} programSave"

"To restore the original version, you should delete the entry in local storage using the following code snippet (select and eval text inside quote marks)."

    "{VimanaCardHome} programDelete"

## Clear the card and hide the editor

    cmdClear
    cmdEditorHide

## Define support functions

    "Function that displays the source code for a program"
    "Syntax: {program name} programLoadAndEdit"
    (programLoadAndEdit) (programLoad cmdClear cmdEditorShow cmdDevPanelShow) !def

    "Function that opens a program"
    "Syntax: {program name} programOpen"
    (programOpen) (programLoad cmdClear programRun) !def

## Create UI elements

    {Welcome Tutorial} ({VimanaWelcomeTutorial} programLoadAndEdit) uiButton
    {Data Stack Tutorial} ({VimanaDataStackTutorial} programLoadAndEdit) uiButton
    {UI Tutorial} ({VimanaUITutorial} programLoadAndEdit) uiButton
    {Language Guide} ({VimanaLanguageOverview} programOpen) uiButton
    {Function Reference} ({VimanaFunctionReference} programOpen) uiButton
    {Language Design} ({VimanaDesign} programOpen) uiButton
    {Steampunk Programming Languages} ({VimanaSteampunk} programOpen) uiButton

    {VimanaCard – WebUI for VimanaCode} uiHeading1

    {VimanaCard is a web-based user interface for the VimanaCode programming language. Importantly, VimanaCode (or Vimana for short) is a Do-It-Yourself (DIY) language. The interpreter for the language is like a steampunk machine meant to be tinkered and experimented with.} uiText

    {pictures/steampunk-vedic-programmable-machine.jpg} uiImage

    {Code Example} uiHeading2

    {VimanaCode is a stack-based, concatenative language with a postfix syntax.} uiText

    {Select the code below and click the Eval button in the Developer Panel (if this panel is hidden, click the bottom-right arrow icon to show it). You can select one line at a time and evaluate each line, or select an evaluate all of the code at once.} uiText

    {{Hi World} print
1 2 + print
(double) (2 *) !def
21 double print} uiEditor

    {VimanaCode - Lisp and Forth} uiHeading2

    {VimanaCode is a dynamic interpreted programming language inspired by Lisp and Forth. I view it as an art project - an experiment in language design. The language is designed to be minimal, consistent and clean, and to be as easy as possible to implement, while still useful.} uiText

    {Like in Lisp, the syntax is based on lists. List elements include symbols, numbers and other lists. The language uses lists for basically everything; data, functions, and control structures. Recursion is used for loops. As in Forth, the language has a postfix, concatenative syntax. Importantly, there is an explicit data stack (and a return stack), like in Forth.} uiText

    {The language is based on global variables. Each symbol is a global name. Global variable names begin with an uppercase letter. Functions are also global variables. Function names begin with any character that is not an uppercase letter. There are no local variables out-of-the box, but locals can be implemented in the language itself. In place of local variables the data stack is used.} uiText

    {Vimana is more like Lisp influenced by Forth, than the other way around. It is a high-level language compared to Forth, but more low-level than Lisp.} uiText

    {Ancient Flying Machines} uiHeading2

    {The name of the language comes from the Vimanas - the flying machines described in the Vedic literature. Were these machines programmed in an ancient programming language? Perhaps a steampunk-flavoured dialect of Lisp or Forth?} uiText

    {pictures/steampunk-vedic-flying-vimana.jpg} uiImage

    {VimanaCard UI} uiHeading2

    {VimanaCard is a web-based UI for the Vimana programming language. It is an experimental design, created for dynamic langugage enthusiasts like myself.} uiText

    {The UI has elements like text boxes, button, and images. These elements are displayed on a "card", which can be scrolled (like a web page). New widgets can be added by writing Javascript embedded in VimanaCode.} uiText

    {There is also a developer panel and a code editor, which can be shown or hidden. When the editor is visible, it floats on top of the card.} uiText

    {In the developer panel, there are buttons and an output area. Any text can be selected and evaluated as code using the Eval button. The resulting data stack is displayed in the output area. The panel can be shown or hidden with a button icon.} uiText

    {At the basic level, the UI structure is based on programs that display content on cards. One program (card) is visible at a time. Programs can call other programs, making navigation (links) between programs (cards) possible.} uiText

    {DIY Programming Languages} uiHeading2

    {VimanaCode is a DIY (Do-It-Yourself) programming language. Like a dynamic piece of art that you can alter and experiment with to create new forms.} uiText

    {The implementation used in the web-browser is written in Javascript (available on GitLab). There are also implementations in Python and C.} uiText

    {Constructing a Do-It-Yourself programming language is an enjoyable hobby project. It is like a model railroad with a steam train. Something that is done for your enjoyment. Writing an interpreter is like building a programmable machine. I view the Vimana interpreter as a steampunk engine, a retro-futuristic design meant to be tinkered and experimented with.} uiText

    {pictures/steampunk-engine.jpg} uiImage

    {HyperCard} uiHeading2

    {The name "VimanaCard" is inspired by the Apple HyperCard system from 1987. In those days it shipped with every Mac that was sold. It was an easy-to-use tool for creating interactive graphical programs.} uiText

    {The contents of a card can be scrolled (like a web page), which is different from the screen sized cards in HyperCard. Another important difference is that while HyperCard features a graphical UI editor, VimanaCard uses program code to create the user interface elements. This makes it "a HyperCard for programmers".} uiText

    {Mainly, the reference to HyperCard is made for nostalgic reasons, in an attempt to capture the spirit of a system that is meant to be an easy-to-use creatative tool, programmable by the user.} uiText

## System code

    {System Code} uiHeading2

    {You can view and also edit system code. Modifying system code in the browser is perhaps not the first you would start with - it is an advanced feature.} uiText

    {System UI Code} ({VimanaCardUI} programLoadAndEdit) uiButton

    {System Lib Code} ({VimanaLib} programLoadAndEdit) uiButton

    {System Core Code} ({VimanaCoreLib} programLoadAndEdit) uiButton

    {Changes to system code are not saved as default, but can be written to local storage with a line of code (see comment in the source code for the Home Card for details - click the Edit button and scroll down to the end of the program).} uiText

## Contact

    {Contact} uiHeading2

    {Mikael Kindborg (mikael.kindborg@gmail.com)<br/>
    GitLab: <a href="https://gitlab.com/mikaelkindborg/VimanaCode">
    gitlab.com/mikaelkindborg/VimanaCode</a><br/>
    Substack: <a href="https://vimanacode.substack.com/">
    vimanacode.substack.com</a><br/>
    } uiHtml

## Saving system code locally

"To save modified code to local storage, paste each of the following snippets into the source code for the respective program (remember to exclude the comment quotes)."

    "{VimanaCardUI} programSave"
    "{VimanaLib} programSave"
    "{VimanaCoreLib} programSave"

"To delete the locally stored versions, use select and Eval the following snippets (or remove the entries from local storage manually using the web browser tools)."

    "{VimanaCardUI} programDelete"
    "{VimanaLib} programDelete"
    "{VimanaCoreLib} programDelete"
