// GENERATED FILE - DO NOT EDIT
// Regenerate this file with the following command:
// python3 wrap.py ./vimanacard/programs/vimanacard.vimana

VimanaCodeLoaderCallback('VimanaCardUI',
`"File: vimanacard.vimana"

# VimanaCard Functions

"This file contains functions that are called from Vimana programs."

## UI Widgets

    "{{Hi} print} uiEditor"
    (uiEditor) (
      {
        const text = VimanaCard.interp.pop()
        const editBox = VimanaCard.createElement("vimana-editbox")
        editBox.contentEditable = true
        editBox.spellcheck = false
        editBox.textContent = text
      } evalJS
    ) !def

    "{Click Me} ({Hi} print) uiButton"
    (uiButton) (
      {
        const action = VimanaCard.interp.pop()
        const text = VimanaCard.interp.pop()
        const button = VimanaCard.createElement("vimana-button-small")
        button.textContent = text
        button.onclick = function() { VimanaCard.evalList(action) }
      } evalJS
    ) !def

    "{Click Me} ({Hi} print) uiButtonWide"
    (uiButtonWide) (
      {
        const action = VimanaCard.interp.pop()
        const text = VimanaCard.interp.pop()
        const button = VimanaCard.createElement("vimana-button-wide")
        button.textContent = text
        button.onclick = function() { VimanaCard.evalList(action) }
      } evalJS
    ) !def

    "{pictures/steampunk-vedic-flying-vimana.jpg} uiImage"
    (uiImage) (
      {
        const imageURL = VimanaCard.interp.pop()
        const imageBox = VimanaCard.createElement("vimana-box vimana-img")
        const imageElement = document.createElement("img")
        imageElement.src = imageURL
        imageBox.appendChild(imageElement)
      } evalJS
    ) !def

    "{pictures/steampunk-vedic-flying-vimana.jpg} uiImageWide"
    (uiImageWide) (
      {
        const imageURL = VimanaCard.interp.pop()
        const imageBox = VimanaCard.createElement("vimana-box vimana-img-wide")
        const imageElement = document.createElement("img")
        imageElement.src = imageURL
        imageBox.appendChild(imageElement)
      } evalJS
    ) !def

    "{<h1>Welcome to VimanaCode</h1>} uiHtml"
    (uiHtml) (
      {
        const html = VimanaCard.interp.pop()
        const box = VimanaCard.createElement("vimana-box")
        box.innerHTML = html
      } evalJS
    ) !def

    "{Welcome to VimanaCode} uiText"
    (uiText) (
      {
        const text = VimanaCard.interp.pop()
        const box = VimanaCard.createElement("vimana-textbox")
        box.innerHTML = text
      } evalJS
    ) !def

    "{Welcome to VimanaCode} uiHeading1"
    (uiHeading1) (
      {
        const html = VimanaCard.interp.pop()
        const box = VimanaCard.createElement("vimana-box vimana-h1")
        box.innerHTML = html
      } evalJS
    ) !def

    "{Welcome to VimanaCode} uiHeading2"
    (uiHeading2) (
      {
        const html = VimanaCard.interp.pop()
        const box = VimanaCard.createElement("vimana-box vimana-h2")
        box.innerHTML = html
      } evalJS
    ) !def

    "{Welcome to VimanaCode} uiHeading3"
    (uiHeading3) (
      {
        const html = VimanaCard.interp.pop()
        const box = VimanaCard.createElement("vimana-box vimana-h3")
        box.innerHTML = html
      } evalJS
    ) !def

    (uiSetBgColorRGB)
    {
      const list = VimanaCard.interp.pop()
      const rgb = "rgb(" + list.car + "," + list.cdr.car + "," + list.cdr.cdr.car + ")"
      const card = document.querySelector(".vimana-card")
      card.style.backgroundColor = rgb
      document.documentElement.style.backgroundColor = rgb
    }
    !defprim

## UI Commands

    (cmdHome) (
      {VimanaCardHome} programLoad
      cardScrollToTop
      cmdEditorHide
      cmdProgramRun
    ) !def

    (cmdClear) (
      cardClear
      cardScrollToTop
      dataStackClear
      outputClear
    ) !def

    (cmdEditorShow) (
      {
        const editor = document.querySelector(".vimana-editor")
        editor.style.display = "block"
      } evalJS
    ) !def

    (cmdEditorHide) (
      {
        const editor = document.querySelector(".vimana-editor")
        editor.style.display = "none"
      } evalJS
    ) !def

    (cmdDevPanelShow) (
      {
        const panel = document.querySelector(".vimana-dev-panel")
        panel.style.display = "block"
        const iconOpen = document.querySelector(".vimana-dev-panel-icon-expand")
        iconOpen.style.display = "none"
      } evalJS
    ) !def

    (cmdDevPanelHide) (
      {
        const panel = document.querySelector(".vimana-dev-panel")
        panel.style.display = "none"
        const iconOpen = document.querySelector(".vimana-dev-panel-icon-expand")
        iconOpen.style.display = "block"
      } evalJS
    ) !def

    (cmdProgramRun) (
      cmdEditorHide
      cardScrollToTop
      programRun
      { VimanaCard.printDataStack() } evalJS
    ) !def

    (cmdEval) (
      { VimanaCard.evalAsync() } evalJS
    ) !def

## UI Support Functions

    "Run the program loaded in the editor"
    (programRun) (
      { VimanaCard.eval(VimanaCard.editorGetText()) } evalJS
     ) !def

    (cardClear) (
      {
        const card = document.querySelector(".vimana-card")
        card.replaceChildren()
      } evalJS
    ) !def

    (cardScrollToTop) (
      {
        const card = document.querySelector(".vimana-card")
        card.scrollTop = 0
      } evalJS
    ) !def

    (dataStackClear) (
      {
        VimanaCard.interp.dataStack = []
      } evalJS
    ) !def

    (outputClear) (
      {
        const panel = document.querySelector(".vimana-dev-panel-editbox")
        panel.textContent = ""
      } evalJS
    ) !def

    (addEvalKeyboardShortcut) (
      {
        // Add shortcut to eval selected text with CTRL+E or CMD+E
        document.addEventListener("keydown", (event) =>
        {
          if ((event.ctrlKey || event.metaKey ) && event.key === "e")
          {
            VimanaCard.evalAsync()
            event.preventDefault()
          }
        })
      } evalJS
    ) !def

## Save/Load Functions

    "Save editor content to local storage"
    "{program name} programSave"
    (programSave) (
      {
        const name = VimanaCard.interp.pop()
        localStorage.setItem(name, VimanaCard.editorGetText())
      } evalJS
    ) !def

    "Set editor content from local storage or from loaded programs"
    "{program name} programLoad"
    (programLoad) (
      {
        const name = VimanaCard.interp.pop()
        VimanaCard.editorSetText(VimanaCard.programLoad(name))
      } evalJS
    ) !def

    "Set editor content from URL"
    "{url} programLoadFromURL"
    (programLoadFromURL) (
      "TODO"
    ) !def

    "Delete program saved in local storage"
    "{program name} programDelete"
    (programDelete) (
      {
        const name = VimanaCard.interp.pop()
        localStorage.removeItem(name)
      } evalJS
    ) !def

    "Get a list of names in local storage"
    (programNames) (
      {
        // Get keys
        const keys = Object.keys(localStorage)
        // Build list
        keys.forEach(function(key) {
          const value = localStorage.getItem(key)
          console.log(key + " : " + value) })
        // Push list
        // TODO
      } evalJS
    ) !def

## Printing Functions

"Redefine printing functions to write output to the dev panel"

    (print)
    {
      const obj = interp.pop()
      //console.log(obj)
      VimanaCard.print(obj)
    }
    !defprim

    (printstack)
    {
      VimanaCard.print("STACK: [" + interp.printStack() + "]")
    }
    !defprim

## Call Init Functions

    addEvalKeyboardShortcut
    cmdDevPanelShow
`)
