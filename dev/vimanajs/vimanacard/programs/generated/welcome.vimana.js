// GENERATED FILE - DO NOT EDIT
// Regenerate this file with the following command:
// python3 wrap.py ./vimanacard/programs/welcome.vimana

VimanaCodeLoaderCallback('VimanaWelcomeTutorial',
`# Welcome to the Wonderful World of VimanaCode!

"This is a short tutorial with basic code examples for the Vimana programming language. Quoted texts are comments, the rest is code."

"Click the arrow icon in the lower right hand part of the window to show (or hide) the Developer Panel. This panel has buttons for evaluating code and running programs. The Eval button evaluates the currently selected text, and the Run button evaluates the entire program."

## Evaluate Code

"Begin by selecting the following expression and then click the Eval button."

    1 2 +

"As you can see Vimana has a postfix syntax, where the arguments come first and the operation/function comes last."

"The resulting data stack is shown in the Developer Panel."

## The Data Stack

"Vimana is a stack-based language, which means that a stack of values is used to pass parameters and store the result of function calls. The programmer can manipulate the stack in a variety of ways, and this is detailed by the data stack tutorial."

"Click the Clear button to empty the data stack."

"Next, select and evaluate this line."

    1 2 3

"The Developer Panel should now show three elements on the data stack."

"Add the items on the stack by selecting and evaluating the two plus signs."

    + +

## More Data Stack Examples

"To remove the topmost element from the data stack, use the underscore function (_). Select and evaluate the underscore character, and notice the resulting data stack (should be empty)"

    _

"Try evaluate the following code snippet. What happens?"

    1 2 3 _ _ _

"And what happens in the following examples?"

    1 2 3 + + print

    {Hi World} print

"The print function takes the topmost element off the stack and prints it. When using print, there is no need to drop the element."

## Interactive Programming

"Remember, you can select any text anywhere, and click Eval to evaluate the selected code snippet. You can select parts of a code snippet, or all of it. This is a highly interactive programming style, inspired by the Smalltalk system."

## Functions

"Now define a function (select and eval)"

    (double) (2 *) !def

"Call the function (select and eval) and inspect the resulting data stack in the Developer Panel."

    21 double

"Note that in Vimana, function names begin with a non-uppercase character."

## Drop items from the data stack

"To remove elements from the data stack programatically, the underscore function (_) is used. It drops the topmost item from the stack. Select and eval the following underscore character, and watch what happens to the stack."

    _

## UI Objects

"Vimana programs can display UI elements on the screen. Each program has its own Card for the UI."

"Select and evaluate the following line to create an image UI object."

    {pictures/steampunk-vedic-flying-vimana.jpg} uiImage

"Click the View button to hide the editor and show the UI, then click Edit to show the editor again."

## Conclusion

"Now you know the basics of the Web UI for the Vimana programming language."

"Click the Home button to go back to the Home Card."
`)
