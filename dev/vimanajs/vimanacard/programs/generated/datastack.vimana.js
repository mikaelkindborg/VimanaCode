// GENERATED FILE - DO NOT EDIT
// Regenerate this file with the following command:
// python3 wrap.py ./vimanacard/programs/datastack.vimana

VimanaCodeLoaderCallback('VimanaDataStackTutorial',
`# Data Stack Tutorial

"Vimana is a stack-based language, which means that a stack of values is used to pass parameters and store the result of function calls. The programmer can manipulate the stack in a variety of ways, and how that works is the topic of this tutorial."

## Basic Example

"Here is an example. Select the following and and click Eval."

    1 2 3

"The resulting data stack is displayed in the Developer Panel."

"Viewed as a stack of items on a table, it looks like this:

      3
      2
      1
    -----

3 is at the top of the stack, and 1 is at the bottom of the stack."

"To remove items from the stack, the underscore function is used (in the Forth language this function is called drop). Select the underscore character and click Eval three times to remove the elements, one by one."

    _

"To quickly drop all elements from the stack while writing code, you can click the Clear button."

## Function Calls

"When a function is called, it takes zero or more arguments from the stack, and puts back zero or more arguments with the result."

"Here are some examples."

"Plus takes two arguments and puts back one result (select and Eval, then click Clear)."

    1 2 +

"The print function takes one argument and prints it (does not put anything back). One could say that print is like the underscore function, but it also prints the item it removes."

    1 2 + print

"Here is the function double. I takes a number and doubles it. It puts 2 on the stack and calls multiply. The multiply function puts back the result on the stack."

    (double) (2 *) !def
    44 double

"Here is what happens when 44 double is evaluated:

    44 is put (pushed) on the stack
    double is called
    2 is put on the stack
    stack is now:   2
                   44
                  ----
    multiply is called
    stack is now:  88
                  ----
"

"Notice that user-defined function names must begin with a non-uppercase character (a name that begins with an uppercase character is a global variable)."

## Data Stack Terminology and Notation

"
VimanaCode uses a data stack terminology where the
topmost stack element is called A, the one below is B,
next is C, and so on.

This can feel backwards when written in horizontal
left-to-right text, but makes logical sense once you
get used to it.

In this example, 4 is at the top of the stack
and 1 is at the bottom of the stack:

    1 2 3 4  <-- elements are in order D C B A

When laid out veritically (like a stack on a table)
it makes more sense:

      4 A    <-- top
      3 B
      2 C
      1 D    <-- bottom
    -------  <-- table

In place of the common stack operations used in Forth, such as dup, swap, over and rot, Vimana uses the following functions:

    :A - copy element A (dup)
    :B - copy element B (over)
    :C - copy element C
    :D - copy element D

    ^B - move element B to top (swap)
    ^C - move element C to top (rot)
    ^D - move element D to top

    _  - the underscore sign means remove top element (drop)

The intended meaning of the prefix ':' is 'copy', or 'pair',
or 'two' (two dots - the bottom dot is copied to top).

The meaning of the prefix '^' is 'move to top of the stack'.

There is no ^A function since A already is on top.

The intended meaning of the underscore character '_' is
'make the top blank' (remove the topmost element).
"

## Code Examples

"Evaluate the following code snippets, one by one, and check the result in the Developer Panel."

    1 2 3
    ^B
    ^B
    _ _ _
    1 2 3
    :C
    :C
    :C
    _ _ _ _ _ _

"Here is an example of a function that uses :A"

    (double) (:A +) !def

"Call the function"

    21 double print

## Alternative Names

"If you prefer the traditional Forth names for stack operations, you can define them yourself."

  (dup)  (:A) !def
  (swap) (^B) !def
  (over) (:B) !def
  (rot)  (^C) !def
  (drop) (_)  !def

## Debugging

"The data stack can be printed during debugging. This does not remove any elements from the stack. Here is an example."

    (double) (:A printstack +) !def
    21 double print

## Conclusion

"Now you know the basics of data stack operations in the Vimana programming language."

"Click the Home button to show the Home Card."
`)
