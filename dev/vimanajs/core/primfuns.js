/*
File: primfuns.js
Copyright (c) 2021-2025 Mikael Kindborg
mikael@kindborg.com

Vimana interpreter primitive functions.
*/

// --------------------------------------------------------------
// Primitive Functions
// --------------------------------------------------------------

function VimanaDefinePrimFuns(interp)
{
  // --------------------------------------------------
  // Javascript interface
  // --------------------------------------------------

  // string evalJS -- ?
  interp.defPrimFun("evalJS", function(interp)
  {
    const string = interp.pop()
    new Function(string)()  // eval(string)
  })

  // Define a primitive function (takes a symbol in a list and a
  // string of Javascript code)
  // (funName) {funBody} !defprim
  interp.defPrimFun("!defprim", function(interp)
  {
    const funBodyStr = interp.pop()
    const funName = interp.pop()
    const symbol = ListFirst(funName)
    const fun = new Function("interp", funBodyStr)
    interp.setGlobal(symbol, fun)
    interp.addFunSymbol(symbol)
  })

  // --------------------------------------------------
  // Return stack
  // --------------------------------------------------

  // list rpush
  interp.defPrimFun("rpush", function(interp)
  {
    const list = interp.pop()
    interp.mustBeList(list, "rpush got a non-list")
    interp.returnStackPush(list)
  })

  // rpop -- list
  interp.defPrimFun("rpop", function(interp)
  {
    const list = interp.returnStackPop()
    interp.push(list)
  })

  // rdrop
  interp.defPrimFun("rdrop", function(interp)
  {
    interp.returnStackPop()
  })

  // string parse -- list
  interp.defPrimFun("parse", function(interp)
  {
    const str = interp.pop()
    interp.mustBeString(str, "parse got a non-string")
    const list = interp.parse(str)
    interp.push(list)
  })

  // list timetorun -- seconds
  interp.defPrimFun("timeToRun", function(interp)
  {
    const list = interp.pop()
    interp.mustBeList(list, "timeToRun got a non-list")

    const t1 = performance.now()
    interp.eval(list)
    const t2 = performance.now()

    const seconds = (t2 - t1) / 1000
    interp.push(seconds)
  })

  // --------------------------------------------------
  // Global variables
  // --------------------------------------------------

  // Core functions:
  // 42 [FooBar] first setval
  // [FooBar] first getval

  // Can be implemented in Vimana:
  // 42 -> FooBar
  // 42 [FooBar] set
  // [FooBar] get

  // Set global variable
  // value name setval
  interp.defPrimFun("setval", function(interp)
  {
    const name = interp.pop()
    interp.mustBeSymbol(name, "setval name must be symbol")
    const value = interp.pop()
    interp.setGlobal(name, value)
  })

  // Get global variable
  // name getval -- value
  interp.defPrimFun("getval", function(interp)
  {
    const name = interp.pop()
    interp.mustBeSymbol(name, "getval name must be symbol")
    interp.push(interp.getGlobal(name))
  })

  // --------------------------------------------------
  // Conditionals
  // --------------------------------------------------

  // This is the conditional all others build on.
  // bool branch ifTrue
  interp.defPrimFun("ifTrue", function(interp)
  {
    const branch = interp.pop()
    const truth = interp.pop()
    interp.mustBeList(branch, "ifTrue branch is a non-list")
    if (truth)
      { interp.callInstrList(branch) }
  })

  // a not -- !a
  interp.defPrimFun("not", function(interp)
  {
    const a = interp.pop()
    interp.push(!a)
  })

  // Test if b and a are the same object
  // b a eq -- bool
  interp.defPrimFun("eq", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()

    let isEqual = (a === b)
    interp.push(isEqual)
  })

  // --------------------------------------------------
  // Math functions
  // --------------------------------------------------

  // b a isSmaller -- bool (check if a is smaller than b)
  interp.defPrimFun("isSmaller", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    interp.push(b > a)
  })

  // b a isBigger -- bool (check if a is bigger than b)
  interp.defPrimFun("isBigger", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    interp.push(b < a)
  })

  interp.defPrimFun("+", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    interp.push(b + a)
  })

  interp.defPrimFun("-", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    interp.push(b - a)
  })

  interp.defPrimFun("*", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    interp.push(b * a)
  })

  interp.defPrimFun("/", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    interp.push(b / a)
  })

  interp.defPrimFun("1+", function(interp)
  {
    const a = interp.pop()
    interp.push(a + 1)
  })

  interp.defPrimFun("1-", function(interp)
  {
    const a = interp.pop()
    interp.push(a - 1)
  })

  // Get random number integer between 0 and max 1 -
  // max random -- n
  interp.defPrimFun("random", function(interp)
  {
    const max = interp.pop()
    interp.push(Math.floor(Math.random() * max))
  })

  // --------------------------------------------------
  // List functions
  // --------------------------------------------------

  // Check if list is empty
  // list isEmpty -- true or false
  interp.defPrimFun("isEmpty", function(interp)
  {
    const list = interp.pop()
    interp.mustBeList(list, "isEmpty got a non-list")
    interp.push(ListIsEmpty(list))
  })

  // Get first item of a list
  // list first -- item
  interp.defPrimFun("first", function(interp)
  {
    const list = interp.pop()
    interp.mustBeList(list, "first got a non-list")
    interp.push(ListFirst(list))
  })

  // Get rest of a list
  // list rest -- list
  interp.defPrimFun("rest", function(interp)
  {
    const list = interp.pop()
    interp.mustBeList(list, "rest got a non-list")
    interp.push(ListRest(list))
  })

  // Cons item onto list and push new list
  // item list cons -- list
  interp.defPrimFun("cons", function(interp)
  {
    const list = interp.pop()
    const item = interp.pop()
    interp.mustBeList(list, "cons got a non-list")
    const first = ListCons(item, list)
    interp.push(first)
  })

  // Replace first element of list
  // list item setFirst
  interp.defPrimFun("setFirst", function(interp)
  {
    const item = interp.pop()
    const list = interp.pop()
    interp.mustBeList(list, "setFirst got a non-list")
    ListSetFirst(list, item)
  })

  // --------------------------------------------------
  // Printing
  // --------------------------------------------------

  // x print
  interp.defPrimFun("print", function(interp)
  {
    const x = interp.pop()
    console.log(interp.print(x))
  })

  // x prettyprint
  interp.defPrimFun("prettyprint", function(interp)
  {
    const x = interp.pop()
    console.log(interp.prettyPrint(x))
  })

  // printstack
  interp.defPrimFun("printstack", function(interp)
  {
    console.log("*** [" + interp.printStack() + "]")
  })

  // x inspect
  interp.defPrimFun("inspect", function(interp)
  {
    console.log("[Inspect]")
    const obj = interp.pop()
    console.log(obj)
  })

  // inspectglobals
  interp.defPrimFun("inspectglobals", function(interp)
  {
    console.log("[Inspect Globals]")
    console.log(interp.globalSymbols)
  })

  // printreturnstack
  interp.defPrimFun("printreturnstack", function(interp)
  {
    console.log("[Return Stack]")
    console.log(interp.returnStack)
  })

  // {message} error
  interp.defPrimFun("error", function(interp)
  {
    const message = interp.pop()
    interp.guruMeditation(message)
  })
}

// --------------------------------------------------------------
// Native Implementation of Core Functions
// --------------------------------------------------------------

// The following functions can also be implemented in Vimana itself

// Define core functions in Javascript for improved performance
function VimanaDefineCoreNativeFuns(interp)
{
  // --------------------------------------------------
  // Eval
  // --------------------------------------------------

  // list eval -- ?
  interp.defPrimFun("eval", function(interp)
  {
    const list = interp.pop()
    interp.mustBeList(list, "eval got a non-list")
    interp.callInstrList(list)
  })

  // Make a tail call to a function
  // Example:
  // tail: foobar
  // Performs a goto essentially, by jumping to the function without
  // pushing a new entry on the return stack
  interp.defPrimFun("tail:", function(interp)
  {
    // Next instruction should contain the function symbol
    const funSymbol = interp.getNextInstr()
    interp.mustBeSymbol(funSymbol, "tail: function name is not a symbol")
    interp.mustHold(interp.isFunSymbol(funSymbol), "tail: function name is not a function")

    // Get function and tail call it
    const funList = interp.getGlobal(funSymbol)
    interp.mustHold(funList !== null, "tail: function is null")
    interp.mustBeList(funList, "tail: function body is not a list")

    // Set instruction list (effectively a jump/goto)
    interp.setInstrList(funList)
  })

  // --------------------------------------------------
  // Global variables
  // --------------------------------------------------

  // Set global variable
  // value (name) set
  interp.defPrimFun("set", function(interp)
  {
    const name = interp.pop()
    interp.mustBeList(name, "set symbol must be in a list")
    const value = interp.pop()
    interp.setGlobal(ListFirst(name), value)
  })

  // Get global variable
  // (name) get -- value
  interp.defPrimFun("get", function(interp)
  {
    const name = interp.pop()
    interp.mustBeList(name, "get symbol must be in a list")
    interp.push(interp.getGlobal(ListFirst(name)))
  })

  // The arrow assignment is an infix function
  // (no need to quote the symbol inside a list)
  // value -- name
  interp.defPrimFun("->", function(interp)
  {
    // Get value from data stack
    const value = interp.pop()

    // Next instruction should be the symbol
    const name = interp.getNextInstr()
    interp.mustBeSymbol(name, "arrow assign: variable is nil")

    // Set value
    interp.setGlobal(name, value)
  })

  // --------------------------------------------------
  // Stack operations
  // --------------------------------------------------

  // Note: a is top of stack, b is second, c third, and so on
  //
  // Example:
  // c b a (may appear counterintuitive, but makes sense logically)
  //
  // a <- top
  // b <- second
  // c <- third

  // _ (underscore character) removes the top element (drop)
  // a _
  interp.defPrimFun("_", function(interp)
  {
    interp.pop()
  })

  // Copy top element (dup)
  // a :A -- a a
  interp.defPrimFun(":A", function(interp)
  {
    const a = interp.pop()
    interp.push(a)
    interp.push(a)
  })

  // Copy b to top (over)
  // b a :B -- b a b
  interp.defPrimFun(":B", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    interp.push(b)
    interp.push(a)
    interp.push(b)
  })

  // Copy c to top
  // c b a :C -- c b a c
  interp.defPrimFun(":C", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    const c = interp.pop()
    interp.push(c)
    interp.push(b)
    interp.push(a)
    interp.push(c)
  })

  // Copy d to top
  // d c b a :D -- d c b a d
  interp.defPrimFun(":D", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    const c = interp.pop()
    const d = interp.pop()
    interp.push(d)
    interp.push(c)
    interp.push(b)
    interp.push(a)
    interp.push(d)
  })

  // Move b to top (swap)
  // b a ^B -- a b
  interp.defPrimFun("^B", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    interp.push(a)
    interp.push(b)
  })

  // Move c to top (rot)
  // c b a ^C -- b a c
  interp.defPrimFun("^C", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    const c = interp.pop()
    interp.push(b)
    interp.push(a)
    interp.push(c)
  })

  // Move d to top
  // d c b a ^D -- c b a d
  interp.defPrimFun("^D", function(interp)
  {
    const a = interp.pop()
    const b = interp.pop()
    const c = interp.pop()
    const d = interp.pop()
    interp.push(c)
    interp.push(b)
    interp.push(a)
    interp.push(d)
  })

  // --------------------------------------------------
  // Conditionals
  // --------------------------------------------------

  // bool branch1 branch2 ifTrueElse
  interp.defPrimFun("ifTrueElse", function(interp)
  {
    const branch2 = interp.pop()
    const branch1 = interp.pop()
    const truth = interp.pop()
    interp.mustBeList(branch1, "ifTrueElse branch1 is a non-list")
    interp.mustBeList(branch2, "iftrueElse branch2 is a non-list")
    if (truth)
    {
      interp.callInstrList(branch1)
    }
    else
    {
      interp.callInstrList(branch2)
    }
  })

  // bool branch ifFalse
  interp.defPrimFun("ifFalse", function(interp)
  {
    const branch = interp.pop()
    const truth = interp.pop()
    interp.mustBeList(branch, "ifFalse branch is a non-list")
    if (!truth)
      { interp.callInstrList(branch) }
  })

  // bool ifTrueTail: branch
  interp.defPrimFun("ifTrueTail:", function(interp)
  {
    // Get iftrue branch
    const truth = interp.pop()

    // Next instruction contains the true block
    const branch = interp.getNextInstr()
    interp.mustBeList(branch, "ifTrueTail: branch is a non-list")
    if (truth)
    {
      // Set instruction list to branch (effectively a jump/goto)
      interp.setInstrList(branch)
    }
  })
}

/*
  // --------------------------------------------------
  // UI functions (unfinished code)
  // --------------------------------------------------

  const script = document.createElement("script")
  script.src = url
  document.body.appendChild(script)

  interp.defPrimFun("createTextBox", function(interp)
  {
    const string = interp.pop()
    const card = document.querySelector(".vimana-card-front")
    var textBox = document.createElement("div")
    textBox.style.backgroundColor = "rgb(5,100,202)"
    textBox.style.color = "rgb(255,255,255)"
    textBox.style.borderRadius = "2px"
    textBox.style.fontSize = "28px"
    textBox.style.margin = "0px 0px 10px 0px"
    textBox.style.padding = "10px 20px"
    textBox.style.fontFamily = "monospace"
    textBox.style.display = "block"
    // TODO: flex
    textBox.innerHTML = string
    card.appendChild(textBox)
  })

  // Replace rest of a list
  // list list setrest
  interp.defPrimFun("setrest", function(interp)
  {
    const rest = interp.pop()
    const list = interp.pop()
    interp.mustBeList(list, "setfirst: got a non-list")
    list.cdr = rest
  })

  // DOM FUNCTIONS ----------------------------------------

  interp.defPrimFun("createTextBox", function(interp)
  {
    const string = interp.pop()
    const card = document.querySelector(".vimana-card-front")
    var textBox = document.createElement("div")
    textBox.style.backgroundColor = "rgb(5,100,202)"
    textBox.style.color = "rgb(255,255,255)"
    textBox.style.borderRadius = "2px"
    textBox.style.fontSize = "28px"
    textBox.style.margin = "0px 0px 10px 0px"
    textBox.style.padding = "10px 20px"
    textBox.style.fontFamily = "monospace"
    textBox.style.display = "block"
    // TODO: flex
    textBox.innerHTML = string
    card.appendChild(textBox)
  })

  {<img src="http://localhost/images/cards_en_spread.jpg" style="width:500px;height:auto;">} createDiv

  {console.log("Hi World from JavaScript")} evalJS

  {TheVimanaUI.interp.evalString("{Hi World from VimanaScript} print")} evalJS

  {TheVimanaUI.interp.defPrimFun("logHi", function(interp)
    {
      console.log("Hi")
    })
  } evalJS

  interp.defPrimFun("createDiv", function(interp)
  {
    const innerHTML = interp.pop()
    const card = document.querySelector(".vimana-card-front")
    var div = document.createElement("div")
    div.innerHTML = innerHTML
    card.appendChild(div)
  })

  interp.defPrimFun("clearCard", function(interp)
  {
    const card = document.querySelector(".vimana-card-front")
    card.textContent = ""
  })

  // EXPERIMENTAL DRAWING FUNCTIONS -----------------------

  interp.defPrimFun("draw", function(interp)
  {
    const canvas = document.getElementById("vimana-canvas")
    const surface = canvas.getContext("2d")
    surface.fillStyle = "rgb(255,255,100)"
    surface.fillRect(0, 0, 200, 200)
  })

  // x y w h fillrect
  interp.defPrimFun("fillrect", function(interp)
  {
    const h = interp.pop()
    const w = interp.pop()
    const y = interp.pop()
    const x = interp.pop()
    const canvas = document.getElementById("vimana-canvas")
    const surface = canvas.getContext("2d")
    //surface.fillStyle = "rgb(0,255,255)"
    surface.fillRect(x, y, w, h)
    //surface.fillRect(0, 0, 200, 200)
  })

  // r g b setcolor
  interp.defPrimFun("setcolor", function(interp)
  {
    const b = interp.pop()
    const g = interp.pop()
    const r = interp.pop()
    const canvas = document.getElementById("vimana-canvas")
    const surface = canvas.getContext("2d")
    surface.fillStyle = "rgb(" + r + "," + g + "," + b + ")"
  })
*/
