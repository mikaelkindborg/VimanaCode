/*
File: vimanalib.js
Copyright (c) 2021-2025 Mikael Kindborg
mikael@kindborg.com

Library functions implemented in VimanaCode.
*/

// Object that holds Vimana objects
window.Vimana = {}

/*
(VarStack) () def
(let) (
  A get 'B VarStack cons cons -> VarStack) def
(endlet) (
  VarStack first VarStack rest first set
  VarStack rest rest -> VarStack) def

0 -> MyVar

(foo) (
  (MyVar) let
    MyVar 1 + -> MyVar
    MyVar print
  endlet
) def

42 foo
*/

// Define Vimana library functions
function VimanaDefineLibFuns(interp)
{
  const libSource =
  `
  "Define standard stack operation names"
  (dup)  (:A) !def
  (swap) (^B) !def
  (over) (:B) !def
  (rot)  (^C) !def
  (drop) (_)  !def
  `

  interp.evalString(libSource)
}
