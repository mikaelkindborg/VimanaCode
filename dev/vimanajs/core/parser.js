/*
File: parser.js
Copyright (c) 2021-2025 Mikael Kindborg
mikael@kindborg.com

Parser for VimanaCode.
*/

// --------------------------------------------------------------
// Syntactic elements
// --------------------------------------------------------------

const LEFTPAREN         = "["
const RIGHTPAREN        = "]"
const LISPYLEFTPAREN    = "("
const LISPYRIGHTPAREN   = ")"
const STRINGBEGIN       = "{"
const STRINGEND         = "}"
const COMMENTQUOTE      = '"'
const COMMENTHASH       = '#'

function IsNewLine(char)
  { return "\n" === char || "\r" === char }

function IsWhiteSpace(char)
  { return " " === char || "\n" === char || "\r" === char || "\t" === char }

function IsLeftParen(char)
  { return LEFTPAREN === char || LISPYLEFTPAREN === char }

function IsRightParen(char)
  { return RIGHTPAREN === char || LISPYRIGHTPAREN === char }

function IsStringBegin(char)
  { return STRINGBEGIN === char }

function IsStringEnd(char)
  { return STRINGEND === char }

function IsSeparator(char)
  { return IsLeftParen(char) || IsRightParen(char) ||
      IsStringBegin(char) || IsStringEnd(char) || IsWhiteSpace(char) }

function IsUpperCase(char)
  { return (char.toUpperCase() === char) && (char.toLowerCase() !== char) }

// --------------------------------------------------------------
// Parser
// --------------------------------------------------------------

class VimanaParser
{
  constructor(interp)
  {
    // Set instance variables
    this.interp = interp
    this.pos = 0
  }

  // Parse a string and return a list
  parse(code)
  {
    this.pos = 0
    return this.parseList(this.stripComments(code))
  }

  parseList(code)
  {
    let list = ListNil

    while (this.pos < code.length)
    {
      const char = code[this.pos]

      if (IsLeftParen(char))
      {
        // Set paren style (used by printing)
        this.interp.setParenStyle(LEFTPAREN === char)

        // Parse child list
        this.pos += 1
        list = ListConsLast(list, this.parseList(code))
      }
      else
      if (IsRightParen(char))
      {
        // Done parsing child list
        this.pos += 1
        break // Exit loop
      }
      else
      if (IsStringBegin(char))
      {
        // Parse string
        this.pos += 1
        list = ListConsLast(list, this.parseString(code))
      }
      else
      if (!IsSeparator(char))
      {
        // Got a non-separator char - parse token
        list = ListConsLast(list, this.parseToken(code))
      }
      else
      {
        // Skip whitespace
        this.pos += 1
      }
    }

    return list
  }

  // Return token string
  parseToken(code)
  {
    // Build token string
    let token = ""

    // pos is at first token character
    while (this.pos < code.length)
    {
      if (IsSeparator(code[this.pos])) { break }

      token += code[this.pos]
      this.pos += 1
    }
    // pos is at character after last token character

    // Convert token to number or symbol
    let value

    if (isFinite(token))
    {
      // Convert string to number
      value = token * 1
    }
    else
    {
      // Create symbol
      value = Symbol.for(token)

      // Functions names begin with a non-uppercase character
      if (!IsUpperCase(token[0]))
      {
        // Keep track of function symbols
        this.interp.addFunSymbol(value)
      }
    }

    return value
  }

  parseString(code)
  {
    let result = ""
    let level = 1

    // pos is at first char after the opening curly

    while (this.pos < code.length)
    {
      const char = code[this.pos]

      if (IsStringBegin(char))
        { level += 1 }
      else
      if (IsStringEnd(char))
        { level -= 1 }

      if (level <= 0) { break } // String end reached

      // Add character
      result += char
      this.pos += 1
    }

    // Move past closing curly
    this.pos += 1

    // pos is at character after closing curly

    return result
  }

  stripComments(code)
  {
    return this.stripQuoteComments(this.stripHashLineComments(code))
  }

  // Return source code stripped from "comments like this one",
  // but do not remove quoted text inside Vimana strings
  // Example: { "this text is not stripped away" }
  stripQuoteComments(code)
  {
    let skip = false
    let stringLevel = 0
    let i = 0
    let result = ""

    while (i < code.length)
    {
      const char = code[i]

      if (IsStringBegin(char))
        { stringLevel += 1}

      if (IsStringEnd(char))
        { stringLevel -= 1}

      if ((COMMENTQUOTE === char) && (stringLevel <= 0))
      {
        skip = !skip  // toogle skip mode
        i += 1        // move to next char
        result += " " // ensure whitespace before and after dropped text
      }
      else // char is not COMMENTQUOTE
      {
        if (!skip)
          { result += char } // add char
        i += 1 // move to next char
      }
    }

    return result
  }

  // Strip lines that begin with the '#' character
  stripHashLineComments(code)
  {
    let skip = false
    let i = 0
    let result = ""

    while (i < code.length)
    {
      let char = code[i]
      let skipLine = false

      // If first char after newline is a hash, skip this line
      skipLine = IsNewLine(char) && (COMMENTHASH === code[i + 1])

      // If the first char is a hash, skip the first line
      skipLine = skipLine || ((0 === i) && (COMMENTHASH === code[i]))

      if (skipLine)
      {
        // Skip until end-of-line
        while (true)
        {
          // Check for end of string
          i += 1
          if (i >= code.length) { return result }

          // Check for end of line
          char = code[i]
          if (IsNewLine(char)) { break }
        }
      }

      // Add char (will add ending newline from the above loop)
      result += char

      // Move to next char
      i += 1
    }

    return result
  }
}
