/*
File: interp.js
Copyright (c) 2021-2025 Mikael Kindborg
mikael@kindborg.com

Vimana interpreter.
*/

// --------------------------------------------------------------
// Interpreter
// --------------------------------------------------------------

// Create and initialize an interpreter instance
function VimanaInterpCreate()
{
  const interp = new VimanaInterp()
  VimanaDefinePrimFuns(interp)
  // TODO: Uncomment
  //VimanaDefineLibFuns(interp)
  return interp
}

// Interpreter class object
class VimanaInterp
{
  constructor()
  {
    this.globals = {}               // symbol table (global variables)
    this.funs = new Set()           // keeps track of function symbols
    this.dataStack = []             // items on data stack
    this.returnStack = []           // instruction return stack
    this.currInstrList = null       // current instruction list
    this.run = true                 // set to false to stop interpreter
    this.speed = 0                  // millisecond delay in async eval loop
    this.useVimanaParenStyle = false // paren style used for printing: [] or ()
    this.debugInstrList = null      // debug instruction list
    this.debugCallTrace = []        // debug function call symbol trace

    // Create printer
    this.printer = new PrettyPrinter(this)

    // Define built-in fuctions
    VimanaDefinePrimFuns(this)
  }

  getGlobal(symbol)
  {
    if (symbol in this.globals)
      { return this.globals[symbol] }
    else
      { return null }
  }

  setGlobal(symbol, value)
    { this.globals[symbol] = value }

  // Return string name for symbol
  symbolName(symbol)
    { return Symbol.keyFor(symbol) }

  // Check if a symbol is a function (symbol name begins
  // with a non-uppercase character, see parser.js)
  isFunSymbol(symbol)
    { return this.funs.has(symbol) }

  // Define a symbol to be a function
  addFunSymbol(symbol)
    { this.funs.add(symbol) }

  // Define a primitive function (fun is a Javascript
  // function object)
  defPrimFun(name, fun)
    { const symbol = Symbol.for(name)
      this.setGlobal(symbol, fun)
      this.addFunSymbol(symbol) }

  // --------------------------------------------------
  // Data stack
  // --------------------------------------------------

  // 9271-1593967

  // Push item onto data stack
  push(item)
    { this.dataStack.push(item) }

  // Pop data stack
  pop()
    { this.mustHold(0 < this.dataStack.length, "Data stack is empty")
      return this.dataStack.pop() }

  // --------------------------------------------------
  // Instruction list
  // --------------------------------------------------

  // Eval list
  setInstrList(list)
    { this.currInstrList = list
      this.debugInstrList = list }

  callInstrList(list)
    { this.returnStackPush(this.currInstrList)
      this.setInstrList(list) }

  // Return next instruction and move to next in instrList
  // primfun: pushNextInstr
  getNextInstr()
    { const instr = ListFirst(this.currInstrList)
      this.currInstrList = ListRest(this.currInstrList)
      return instr }

  // --------------------------------------------------
  // Return stack
  // --------------------------------------------------

  // Push instruction list on return stack
  returnStackPush(list)
    { this.returnStack.push(list) }

  returnStackPop()
    { this.mustHold(0 < this.returnStack.length, "Return stack is empty")
      return this.returnStack.pop() }

  returnStackIsEmpty()
    { return (0 === this.returnStack.length) }

  // --------------------------------------------------
  // Interpreter eval loop
  // --------------------------------------------------

  // Eval a string
  evalString(string)
  {
    let list = this.parse(string)
    this.eval(list)
  }

  // Eval a list
  eval(list)
  {
    this.program = list

    this.run = true

    this.returnStackPush(ListNil)
    this.setInstrList(list)

    // Eval loop
    while (this.run)
      { this.evalInstruction() }
  }

  // Eval driven by a timer, which is slower but more resource friendly
  evalAsync(list, doneFun = null)
  {
    this.program = list

    this.run = true

    this.returnStackPush(ListNil)
    this.setInstrList(list)

    // Used in runTimer
    const interp = this

    // Enter eval loop
    runTimer()

    function runTimer()
    {
      if (interp.run)
        { interp.evalInstruction()
          setTimeout(runTimer, interp.speed) }
      else
        { if (doneFun) { doneFun() } }
    }
  }

  // Evaluate next instruction
  evalInstruction()
  {
    // Check if end of instruction list
    if (IsNil(this.currInstrList))
    {
      if (this.returnStackIsEmpty())
        { this.run = false }
      else
        { this.currInstrList = this.returnStackPop() }
    }
    else
    {
      // Get next instruction and move to next
      const instr = this.getNextInstr()

      // Evaluate or push instruction
      if (IsSymbol(instr))
        { this.evalSymbol(instr) }
      else
        { this.push(instr) }
    }
  }

  // Evaluate a global variable, it can be a function, primfun,
  // or data value
  evalSymbol(symbol)
  {
    // Get value
    const value = this.getGlobal(symbol)

    // Check if unbound
    if (null === value)
      { this.guruMeditation("Unbound symbol: " + this.symbolName(symbol)) }

    // Check if it is a function symbol
    if (this.isFunSymbol(symbol))
    {
      this.callTraceAdd(symbol)
      if (IsPrimFun(value))
        { value(this) } // Call primfun
      else
      if (IsList(value))
        { this.callInstrList(value) } // Call function
      else
        { this.guruMeditation("Not a function: " + this.symbolName(symbol)) }
    }
    else
    {
      // Push symbol value onto data stack
      this.push(value)
    }
  }

  // --------------------------------------------------
  // Parser
  // --------------------------------------------------

  parse(string)
    { return new VimanaParser(this).parse(string) }

  setParenStyle(isVimanaStyle)
    { this.useVimanaParenStyle = isVimanaStyle }

  // --------------------------------------------------
  // Error handling (Guru Meditation)
  // --------------------------------------------------

  // Error handling is simple - an error aborts execution
  guruMeditation(message)
  {
    const meditation = "[Guru Meditation] " + message
    console.log(meditation)
    console.log(this.callTracePrint())
    console.log("[DataStack] " + this.printStack())
    //TODO: printDataStack, rename print to stringify?
    console.trace()
    console.log(this)
    throw meditation
  }

  mustHold(cond, message)
    { if (!cond) { this.guruMeditation(message) } }

  mustBeList(list, message)
    { if (!IsList(list))
        { console.log("*** mustBeList ***")
          console.log(list) }
      this.mustHold(IsList(list), message) }

  mustBeSymbol(symbol, message)
    { this.mustHold(IsSymbol(symbol), message) }

  mustBeString(str, message)
    { this.mustHold(IsString(str), message) }

  // --------------------------------------------------
  // Printing
  // --------------------------------------------------

  print(item)
    { return this.printer.printItem(item) }

  prettyPrint(item)
    { return this.printer.prettyPrintItem(item) }

  printStack()
    { return this.printer.printArray(this.dataStack) }

  callTraceAdd(symbol)
  {
    this.debugCallTrace.push(symbol)
    if (this.debugCallTrace.length > 200)
      { this.debugCallTrace.shift() }
  }

  callTracePrint()
  {
    let trace = "[CallTrace] "
    for (let i = 0; i < this.debugCallTrace.length; ++ i)
    {
      trace += this.symbolName(this.debugCallTrace[i])
      trace +=  " "
    }
    trace += "\n[DebugInstrList] "
    trace += this.print(this.debugInstrList)
    trace += this.print(this.currInstrList)
    return trace
  }

  //console.log(this.print(this.fullInstrList))
  //console.log(this.print(this.currInstrList)

}
// End of class VimanaInterp

// --------------------------------------------------------------
// List functions
// --------------------------------------------------------------

const ListNil = { car: null, cdr: null }

function ListCons(first, rest)
  { return { car: first, cdr: rest } }

// The first of nil is nil
function ListFirst(item)
  { return ListNil === item ? ListNil : item.car }

// The rest of nil is nil
function ListRest(item)
  { return ListNil === item ? ListNil : item.cdr }

function ListSetFirst(item, value)
  { item.car = value }

function ListSetRest(item, value)
  { item.cdr = value }

function ListLast(item)
  { while (ListNil !== ListRest(item))
      { item = ListRest(item) }
    return item }

function ListConsLast(item, data)
{
  if (ListNil === item)
    { return ListCons(data, ListNil) }
  else
    { const last = ListLast(item)
      ListSetRest(last, ListCons(data, ListNil))
      return item }
}

function ListIsEmpty(list)
  { return (ListNil === list) || null === ListFirst(list) }

// --------------------------------------------------------------
// Type check functions
// --------------------------------------------------------------

// Nil
function IsNil(x)
  { return (ListNil === x) }

// List
function IsList(x)
  { return (null !== x && typeof x === "object" && Object.hasOwn(x, "car")) }

// Primitive function defined in Javascript
function IsPrimFun(x)
  { return ("function" === typeof x) }

// String
function IsString(x)
  { return ("string" === typeof x) }

// Symbol
function IsSymbol(x)
  { return ("symbol" === typeof x) }
