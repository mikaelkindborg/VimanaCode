/*
File: prettyprint.js
Copyright (c) 2021-2025 Mikael Kindborg
mikael@kindborg.com

Pretty printer.
*/

class PrettyPrinter
{
  constructor(interp)
  {
    this.interp = interp
    this.prettyPrint = false
    this.prettyPrintIndent = 0
    this.prettyNumItemsPrinted = 0
    this.leftParen  = interp.useVimanaParenStyle ? LEFTPAREN  : LISPYLEFTPAREN
    this.rightParen = interp.useVimanaParenStyle ? RIGHTPAREN : LISPYRIGHTPAREN
  }

  printItem(item)
  {
    this.prettyPrint = false
    return this.print(item)
  }

  prettyPrintItem(item)
  {
    this.prettyPrint = true
    this.prettyPrintIndent = 0
    this.prettyNumItemsPrinted = 0
    return this.print(item)
  }

  // Returns string
  print(x)
  {
    if (IsList(x))
      { return this.printList(x) }
    else
    if (IsSymbol(x))
      { return this.interp.symbolName(x) }
    else
    if (IsString(x))
      { return STRINGBEGIN + x + STRINGEND }
    else
      { return x.toString() }
  }

  // Returns string
  printList(list)
  {
    let item = list

    // Opening paren
    let string = this.leftParen

    // Indent if many items
    if (this.prettyPrint && this.prettyNumItemsPrinted > 2)
    {
      this.prettyNumItemsPrinted = 0
      this.prettyPrintIndent += 2
      const indent = " ".repeat(this.prettyPrintIndent)
      string = "\n" + indent + this.leftParen
    }

    // Stringify items in list
    while (item !== ListNil)
    {
      if (this.prettyPrint)
        { this.prettyNumItemsPrinted ++ }
      string += this.print(ListFirst(item))
      item = ListRest(item)
      if (item !== ListNil)
        { string += " " }
    }

    // Closing paren
    string += this.rightParen

    if (this.prettyPrint && this.prettyPrintIndent > 0)
    {
      this.prettyNumItemsPrinted = 0
      this.prettyPrintIndent -= 2
    }

    return string
  }

  // Returns string
  printArray(stack)
  {
    let string = ""

    if (stack.length <= 0)
    {
      string = ""
    }
    else
    {
      for (let i = 0; i < stack.length; ++ i)
      {
        if (i > 0) { string +=  " " }
        string += this.printItem(stack[i])
      }
    }

    return string
  }
}
