__GENERATED FILE - DO NOT EDIT__

Regenerate this file with the following command:

    python3 doc.py ./core/core.vimana

"File: core.vimana"

# Vimana Core Functions

## Introduction

"This file contains definitions of VimanaCode core functions,
written in Vimana itself.

In a performance-oriented system, many of these functions
would be implemented in the native code layer.

It is important to know that in Vimana, global variables
begin with an uppercase letter, and functions begin with
anything that is a non-uppercase letter.

The interpreter calls function symbols and pushes the value
of global variable symbols onto the data stack.

Text in double quotes are comments.

Lines starting with a '#' character are also comments.
This means that the source code file can be displayed as
a markdown documentation file (literate programming).

It is a joy that the Vimana language is low level, it is
like a toolkit with building blocks can be altered at a
detailed level (like 'manual gears'). It reminds of an old
Lego kit from the 1960's that had no predefined parts."

## Data Stack Terminology and Notation

"VimanaCode uses a data stack terminology where the
topmost stack element is called A, the one below is B,
next is C, and so on.

This can feel backwards when written in horizontal
left-to-right text, but makes logical sense once you
get used to it.

In this example, 4 is at the top of the stack
and 1 is at the bottom of the stack:

    1 2 3 4 - elements are in order D C B A

When laid out veritically (like a stack on a table)
it makes more sense:

      4 A    <-- top
      3 B
      2 C
      1 D    <-- bottom
    -------  <-- table

In place of common stack operations such as dup, swap, over,
and rot (which I find hard to learn and use), the following
stack operation functions are provided (these are the ones
that I use, you can define your own functions as needed):

    :A - copy element A (dup)
    :B - copy element B (over)
    :C - copy element C
    :D - copy element D

    ^B - move element B to top (swap)
    ^C - move element C to top (rot)
    ^D - move element D to top

    _  - the underscore sign means remove top element (drop)

The intended meaning of the prefix ':' is 'copy', or 'pair',
or 'two' (two dots - the bottom dot is copied to top).

The meaning of the prefix '^' is 'move to top of the stack'.

There is no ^A function since A already is on top.

The intended meaning of the underscore character '_' is
'make the top blank' (remove the topmost element).

One could also experiment with emojis or other icons,
for example:

    💕A - make copy
    🫸B - move to top

It is easy to define alternative function names, so it is
straightforward to experiment with the syntax."

## Assignment Functions

"Define set (takes a 'quoted' symbol in a list)"

    "value (symbol) set"
    (first setval) (set) first setval

"Define get (takes a 'quoted' symbol in a list)"

    "(symbol) get"
    (first getval) (get) first setval

"Define 'internal' versions of dup and swap to avoid
global variable name collisions with the stack operations
defined below that use arrow assignment"

    ((A__) set  A__ A__)            (_DUP_) set
    ((A__) set (B__) set  A__ B__) (_SWAP_) set

"Define !def for clearer function definition syntax with the
function name first (works like set with swapped parameters)."

    "(symbol) value !def"
    (_SWAP_ first setval) (!def) set

"Examples:

    (Foo) 42 !def
    (square) (* 2) !def

I like !def because it is postfix, and the function name
is clearly visible, enclosed in parens, forming a distinct
syntactic unit.

The postfix style is also useful when dynamically generating
the function body (compare this to the the prefix version 'def!',
which is defined below, that has literal a literal syntax that
does not evaluate the arguments). Here is an example that will
print 42 as the result:

    (makeAdder) ((+) cons) !def
    (add10) 10 makeAdder !def
    32 add10 print

The function 'def!' is the prefix version of '!def'. It takes
two literals as parameters (the arguments are not evaluated).

Here is the definition of def!"

    "def! symbol value"
    ( rpop _DUP_ first _SWAP_ rest rpush "symbol is on the stack"
      rpop _DUP_ first _SWAP_ rest rpush "value is on the stack"
      _SWAP_                             "swap symbol and value"
      setval                             "set symbol value"
    ) (def!) set

"Here is an alternative version that uses fewer operations,
but the code is not as uniform."

    ( rpop _DUP_ first                   "symbol is on the stack"
      _SWAP_ rest _DUP_ first            "value is on the stack"
      _SWAP_ rest rpush                  "push rest to rstack"
      _SWAP_                             "swap symbol and value"
      setval                             "set symbol value"
    ) (alt-def!) set

"The definition of def! takes its arguments from the code
being evaluated by accessing the return stack (the 'rstack').
It pops values off the returns stack and puts back the rest
of the code list (the return stack holds pointers to code
lists).

The prefix function def! is more memory efficient than !def,
and is used by the LoopMachine interpreter, which does not
have a garbage collector.

The def! function should be implemented natively to save memory,
and is useful in LoopMachine to save memory (postfix !def uses
an extra list with one element, which is no problem when there
is garbage collection, but LoopMachine does not have that).

Forth-inspired syntax using (':') could also be defined.

The postfix function !def is the original Vimana style,
but interpreters that have garbage collection can also
use the prefix def! function."

"Define arrow set (auto-quotes the symbol). This function
should be implemented natively (especially in LoopMachine)
for improved performace."

    "value -> symbol"
    (rpop _DUP_ first _SWAP_ rest rpush setval) (->) set

## Stack Operations

"Define stack operations (use arrow assignment for clarity)"

    "a _ -- remove top of stack (drop)"
    def! _ (-> A_)

    "a :A -- a a (dup)"
    def! :A (-> A_  A_ A_)

    "b a :B -- b a b (over)"
    def! :B (-> A_ -> B_  B_ A_ B_)

    "c b a :C -- c b a c"
    def! :C (-> A_ -> B_ -> C_  C_ B_ A_ C_)

    "d c b a :D -- d c b a d"
    def! :D (-> A_ -> B_ -> C_ -> D_  D_ C_ B_ A_ D_)

    "b a ^B -- a b (swap)"
    def! ^B (-> A_ -> B_  A_ B_)

    "c b a ^C -- b a c (rot)"
    def! ^C (-> A_ -> B_ -> C_  B_ A_ C_)

    "d c b a ^D -- c b a d"
    def! ^D (-> A_ -> B_ -> C_ -> D_  C_ B_ A_ D_)

## Evaluation Functions

"The eval function can also be defined within the language itself."

    "(expr) eval -- ?"
    def! eval (rpush)

"Here is a function that exits the current level, but it will not
exit a function if called from a nested paren level.
TODO: Make primfun rdrop that can be used instead of exit"

    "Exit current level (might not exit the function!)"
    def! exit (rpop _)

"Define tail (postfix)"

    "(expr) tail -- tail calls the list"
    def! tail (rpop _ rpush)

"Define tail: (prefix)"

    "tail: funSymbol -- tails calls the function"
    def! tail: (
      rpop first "get function symbol"
      getval     "get function list"
      rpush      "push function list to rstack"
    )

## Conditional Functions

"Overview of conditional functions

The basic primitive is the non-tailing postfix ifTrue function:

    bool (expr) ifTrue

Based on this function, other forms can be implemented.

Below are definitions for:

    bool (expr) ifFalse
    bool (true-expr) (false-expr) ifTrueElse
    bool ifTrue: (expr)
    bool ifFalse: (expr)
    bool ifTrueElse: (true-expr) (false-expr)

Tailing forms ('tails out' of (exits) the current level):

    bool (expr) ifTrueTail
    bool ifTrueTail: (expr)
"

    "bool (expr) ifTrue -- non-tailing postfix"
    "ifTrue is implemented as a primive"

    "bool (expr) ifFalse -- non-tailing postfix"
    def! ifFalse (^B not ^B ifTrue)

    "bool (true-expr) (false-expr) ifTrueElse -- non-tailing postfix"
    def! ifTrueElse (
      "if true drop (false-expr) and evaluate (true-expr)"
      ^C (_ eval rpop _) ifTrue
      "otherwise drop (true-expr) and evaluate (false-expr)"
      ^B _ eval
    )

    "bool ifTrue: (expr) -- non-tailing infix"
    def! ifTrue: (
      rpop :A first "push (expr) onto the stack"
      ^B rest rpush "push rest of list to the rstack"
      ifTrue        "call ifTrue"
    )

    "bool ifFalse: (expr) -- non-tailing infix"
    def! ifFalse: (
      not           "negate bool"
      rpop :A first "push (expr) onto the stack"
      ^B rest rpush "push rest of list to the rstack"
      ifTrue        "call ifTrue"
    )

    "bool ifTrueElse: (true-expr) (false-expr) -- non-tailing infix"
    def! ifTrueElse: (
      rpop :A first "push (true-expr) onto the stack"
      ^B rest first "push (false-expr) onto the stack"
      ^B rest rpush "push rest of list onto rstack"
      ifTrue        "call ifTrue"
    )

    "bool (expr) ifTrueTail -- tailing postfix"
    def! ifTrueTail (
      "evaluate (expr) if true and tail out"
      ^B (rpop _ rpop _ eval) ifTrue
      "otherwise drop (expr)"
      _
    )

    "bool ifTrueTail: (expr) -- tailing infix"
    def! ifTrueTail: (
      "push (expr) onto the stack"
      rpop :A first ^B rest rpush
      "evaluate (expr) if true and tail out"
      ^B (rpop _ rpop _ eval) ifTrue
      "otherwise drop (expr)"
      _
    )

"Define Nil, True, and False (for readability)"

    def! Nil ()
    def! True 1
    def! False 0
