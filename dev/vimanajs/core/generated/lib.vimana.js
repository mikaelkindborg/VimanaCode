// GENERATED FILE - DO NOT EDIT
// Regenerate this file with the following command:
// python3 wrap.py ./core/lib.vimana

VimanaCodeLoaderCallback('VimanaLib',
`"File: lib.vimana"

# Library functions

"This file implements various library functions"

## Stack Operations

"Define standard stack operation names"

    (dup)  (:A) !def
    (swap) (^B) !def
    (over) (:B) !def
    (rot)  (^C) !def
    (drop) (_)  !def

## TODO

"Local variables and coroutines"
`)
