__GENERATED FILE - DO NOT EDIT__

Regenerate this file with the following command:

    python3 doc.py ./core/lib.vimana

"File: lib.vimana"

# Library functions

"This file implements various library functions"

## Stack Operations

"Define standard stack operation names"

    (dup)  (:A) !def
    (swap) (^B) !def
    (over) (:B) !def
    (rot)  (^C) !def
    (drop) (_)  !def

## TODO

"Local variables and coroutines"
