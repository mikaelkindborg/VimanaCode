# Interpreter design

This document contains pseudocode for the Vimana interpreter and parser.

# Interpreter

Interpreter global variables:

    instrList   // list with next instruction as first item
    dataStack   // parameter stack
    returnStack // instruction return stack
    symbolTable // global symbol table

Evaluation:

    eval(list):
      instrList = list
      while (returnStack is not empty):
        while (instrList is not empty):
          instr = instrList first // get instruction
          instrList = instrList next // move to next instruction in advance
          if (instr is primfun):
            call primfun (note that primsfuns can set instrList)
          else if (instr is symbol):
            value = symbolTable(instr) // lookup symbol value
            if (value is unbound):
              error
            else if (value is list)
              returnStack push instrList // save return instruction
              instrList = value // call function body
            else:
              dataStack push value
          else:
            dataStack push data
        endwhile
        // Jump to return instruction
        instrList = returnStack pop
      endwhile

## Parser

    parseCode:
      code = stripComments code
      pos = 0
      return parseList code

    parseList(code):
      list = nil
      while (pos < code length):
        c = code[pos]
        if (c is left paren):
          pos += 1
          l = parseList code
          list = addlast list l
        else if (c is right paren):
          pos += 1
          return list
        else if (c is string begin):
          pos += 1
          item = parseString code
          list = addlast list item
        else if (c is not separator):
          item = parseToken code
          list = addlast list item
        else:
          pos += 1 (skip separator char)
      return list
