# File: vimana.py
# Evaluate a VimanaCode file
# Copyright (c) 2022-2023 Mikael Kindborg
# mikael@kindborg.com
#
# Example: This command will run the code in file hi.vimana:
#
#  python3 vimana.py hi.vimana
#

import sys

from interpreter import *

numargs = len(sys.argv)

if (2 == numargs):
  f = open(sys.argv[1], "r")
  vimana(f.read())
else:
  print("Usage:")
  print("python3 vimana.py myprogram.vimana")
