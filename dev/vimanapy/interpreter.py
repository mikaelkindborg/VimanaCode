# File: interpreter.py
# VimanaCode interpreter in Python
# Copyright (c) 2022-2023 Mikael Kindborg
# mikael@kindborg.com
#
# VimanaCode is an experimental minimalistic programming language
# based on Lisp and Forth.
#

import traceback
import sys

VimanaNilType = None
VimanaListType = list
VimanaStringType = str
VimanaSeparators = ("(", ")", " ", "\n", "\r", "\t")
VimanaPrintFunction = print

# Stack frame (object on the call stack)

# The reason for having a stack frame object is to keep a
# reference to the code list in order to print a stack trace
# on error.

class VimanaStackFrame(object):

  def __init__(self, l):
    # TODO: rename to list, instrList
    self.code = l          # Code list
    self.currentInstr = l  # Current instruction (a list item)
    self.prevInstr = l     # The instruction becore the current one

  def getCode(self):
    return self.code

  def hasNextInstr(self):
    return VimanaNilType != self.currentInstr

  def moveToNextInstr(self):
    self.prevInstr = self.currentInstr
    self.currentInstr = Vimana_rest(self.currentInstr)

  def getInstr(self):
    return Vimana_first(self.currentInstr)

# Interpreter

class VimanaInterp(object):

  def __init__(self):
    self.globals = {}
    self.primFuns = {}
    self.callStack = []
    self.dataStack = []
    self.parser = VimanaParser()
    self.run = True
    Vimana_createPrimFuns(self)

  def eval(self, string):
    try:
      self.evalList(self.parser.parse(string))
      return True
    except Exception as error:
      self.pythonError(error)
      return False

  def evalList(self, item):
    if (type(item) is VimanaListType):
      self.run = True
      self.callStack = []
      # Push root stack frame (sets current instruction)
      self.callStackPush(item)
      # Call stack loop (outer loop)
      while (self.run and len(self.callStack) > 0):
        # Instruction list loop (inner loop)
        stackFrame = self.callStackTop()
        while (self.run and stackFrame.hasNextInstr()):
          # Get current instruction
          instr = stackFrame.getInstr()
          # Move to next instruction in advance
          stackFrame.moveToNextInstr()
          # Dispatch on current instruction
          if (type(instr) is VimanaStringType):
            if (instr in self.primFuns):
              # Call primfun
              self.primFuns[instr](self)
            elif (instr in self.globals):
              value = self.globals[instr]
              if (self.isFun(value)):
                # Push stack frame with function code list
                self.callStackPush(value)
              else:
                # Push global variable value
                self.push(value)
            else:
              # Push item
              self.push(instr)
          else:
            # Push item
            self.push(instr)
          # Get current stack frame (may have changed)
          stackFrame = self.callStackTop()
        # End of instruction list loop
        # Pop stack frame
        self.callStackPop()
      # End of call stack loop

  def callStackPush(self, item):
    if (type(item) is VimanaListType):
      if (0 == len(self.callStack)):
        # Push first stack frame
        stackFrame = VimanaStackFrame(item)
        self.callStack.append(stackFrame)
      else:
        topStackFrame = self.callStackTop()
        if (not topStackFrame.hasNextInstr()):
          # Tail call - reuse stack frame
          topStackFrame.code = item
          topStackFrame.currentInstr = item
        else:
          # Push new stack frame
          stackFrame = VimanaStackFrame(item)
          self.callStack.append(stackFrame)

  def callStackPop(self):
    return self.callStack.pop()

  def callStackTop(self):
    return self.callStack[-1]

  def callStackSetTop(self, item):
    self.callStack[-1] = item

  def isFun(self, item):
    return (type(item) is VimanaListType) and (3 == len(item))

  def push(self, item):
    return self.dataStack.append(item)

  def pop(self):
    if (len(self.dataStack) < 1):
      self.error("Guru Meditation #1: Data stack is empty")
    else:
      return self.dataStack.pop()

  def setglobal(self, name, value):
    self.globals[name] = value

  def getglobal(self, name):
    return self.globals[name]

  def print(self, item):
    Vimana_print(Vimana_stringify(item))

  def error(self, message, printPythonTrace=False):
    string =  "----------------------------------------------------------------\n"
    string += message + "\n"
    string += "----------------------------------------------------------------\n"
    string += self.callStackAsString() + "\n\n"
    string += self.dataStackAsString() + "\n"
    if (printPythonTrace):
      string += "\n" + traceback.format_exc()
    Vimana_print(string)
    self.run = False

  def pythonError(self, ex):
    self.error("Guru Meditation #0: Python exception occurred", True)

  def dataStackAsString(interp):
    string = "[ "
    if (len(interp.dataStack) < 1):
      string += ""
    else:
      for index in range(len(interp.dataStack)):
        string += Vimana_stringify(interp.dataStack[index]) + " "
    return string + "]"

  def callStackAsString(interp):
    string = "CALLSTACK:"
    indent = ""
    for index in range(len(interp.callStack)):
      stackFrame = interp.callStack[index]
      string += "\n" + str(index) + ": " + indent + Vimana_stringify(stackFrame.getCode(), stackFrame.prevInstr)
      #string += "\n" + str(index) + ": " + indent + Vimana_stringify(stackFrame.prevInstr)
      indent += "  "
    return string

# Parser

class VimanaParser(object):

  def parse(self, code):
    self.length = len(code)
    self.pos = 0
    return self.parseList(code)

  def parseList(self, code):
    l = VimanaNilType
    while (self.pos < self.length):
      c = code[self.pos]
      if ("(" == c):
        self.pos += 1
        l = Vimana_consLast(l, self.parseList(code))
      elif (")" == c):
        self.pos += 1
        break
      elif ("{" == c):
        self.pos += 1
        l = Vimana_consLast(l, self.parseString(code))
      elif (not c in VimanaSeparators):
        l = Vimana_consLast(l, self.parseToken(code))
      else:
        self.pos += 1
    return l

  def parseString(self, code):
    string = ""
    level = 1
    while (self.pos < self.length):
      c = code[self.pos]
      # Handle nested string delimiters
      if ("{" == c):
        level += 1;
      elif ("}" == c):
        level -= 1;
      # Check end of string
      if (level < 1):
        self.pos += 1
        break
      else:
        string += c
        self.pos += 1
    return string

  def parseToken(self, code):
    token = ""
    while (self.pos < self.length):
      c = code[self.pos]
      if (c in VimanaSeparators):
        break
      else:
        token += c
        self.pos += 1
    return self.tokenConvert(token)

  def tokenConvert(self, token):
    try:
      return int(token)
    except ValueError:
      try:
        return float(token)
      except ValueError:
        return str(token)

# Primitive functions

def Vimana_createPrimFuns(interp):
  interp.primFuns["print"] = VimanaPrim_print
  interp.primFuns["printraw"] = VimanaPrim_printraw
  interp.primFuns["printstack"] = VimanaPrim_printstack
  interp.primFuns["printcallstack"] = VimanaPrim_printcallstack
  interp.primFuns["drop"] = VimanaPrim_drop
  interp.primFuns["dup"] = VimanaPrim_dup
  interp.primFuns["swap"] = VimanaPrim_swap
  interp.primFuns["over"] = VimanaPrim_over
  interp.primFuns["first"] = VimanaPrim_first
  interp.primFuns["rest"] = VimanaPrim_rest
  interp.primFuns["cons"] = VimanaPrim_cons
  interp.primFuns["setfirst"] = VimanaPrim_setfirst
  interp.primFuns["setglobal"] = VimanaPrim_setglobal
  interp.primFuns["getglobal"] = VimanaPrim_getglobal
  interp.primFuns["+"] = VimanaPrim_add
  interp.primFuns["-"] = VimanaPrim_sub
  interp.primFuns["*"] = VimanaPrim_mul
  interp.primFuns["/"] = VimanaPrim_div
  interp.primFuns["<"] = VimanaPrim_lessthan
  interp.primFuns[">"] = VimanaPrim_greaterthan
  interp.primFuns["eq"] = VimanaPrim_eq
  interp.primFuns["not"] = VimanaPrim_not
  interp.primFuns["ifelse"] = VimanaPrim_ifelse
  interp.primFuns["eval"] = VimanaPrim_eval
  interp.primFuns["funify"] = VimanaPrim_funify
  interp.primFuns["execpy"] = VimanaPrim_execpy

def VimanaPrim_print(interp):
  item = interp.pop()
  interp.print(item)

def VimanaPrim_printraw(interp):
  item = interp.pop()
  print(item)

def VimanaPrim_printstack(interp):
  Vimana_print(interp.dataStackAsString())

def VimanaPrim_printcallstack(interp):
  Vimana_print(interp.callStackAsString())

def VimanaPrim_drop(interp):
  interp.pop()

def VimanaPrim_dup(interp):
  item = interp.pop()
  interp.push(item)
  interp.push(item)

def VimanaPrim_swap(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(b)
  interp.push(a)

def VimanaPrim_over(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a)
  interp.push(b)
  interp.push(a)

def VimanaPrim_first(interp):
  l = interp.pop()
  first = Vimana_first(l)
  interp.push(first)

def VimanaPrim_rest(interp):
  l = interp.pop()
  rest = Vimana_rest(l)
  interp.push(rest)

def VimanaPrim_cons(interp):
  b = interp.pop()
  a = interp.pop()
  l = Vimana_cons(a, b)
  interp.push(l)

def VimanaPrim_setfirst(interp):
  value = interp.pop()
  l = interp.pop()
  l[0] = value

def VimanaPrim_setglobal(interp):
  namelist = interp.pop()
  value = interp.pop()
  name = Vimana_first(namelist)
  interp.setglobal(name, value)

def VimanaPrim_getglobal(interp):
  namelist = interp.pop()
  name = Vimana_first(namelist)
  interp.push(interp.getglobal(name))

def VimanaPrim_add(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a + b)

def VimanaPrim_sub(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a - b)

def VimanaPrim_mul(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a * b)

def VimanaPrim_div(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a / b)

def VimanaPrim_lessthan(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a < b)

def VimanaPrim_greaterthan(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a > b)

def VimanaPrim_eq(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a == b)

def VimanaPrim_not(interp):
  boolValue = interp.pop()
  interp.push(not boolValue)

def VimanaPrim_ifelse(interp):
  elseBranch = interp.pop()
  trueBranch = interp.pop()
  boolValue = interp.pop()
  if (boolValue):
    interp.callStackPush(trueBranch)
  else:
    interp.callStackPush(elseBranch)

def VimanaPrim_eval(interp):
  l = interp.pop()
  interp.callStackPush(l)

def VimanaPrim_funify(interp):
  l = interp.pop()
  l.append(True)
  interp.push(l)

# Example: {vimana("{Hi Vimana} print")} execpy
def VimanaPrim_execpy(interp):
  code = interp.pop()
  exec(code)

# List functions

def Vimana_cons(a, b):
  return [a, b]

def Vimana_first(l):
  return l[0]

def Vimana_setFirst(l, a):
  l[0] = a

def Vimana_rest(l):
  return l[1]

def Vimana_setRest(l, a):
  l[1] = a

def Vimana_last(l):
  while (VimanaNilType != Vimana_rest(l)):
    l = Vimana_rest(l)
  return l

def Vimana_consLast(a, b):
  if (VimanaNilType == a):
    return Vimana_cons(b, VimanaNilType)
  else:
    last = Vimana_last(a)
    Vimana_setRest(last, Vimana_cons(b, VimanaNilType))
    return a

# Print functions

def Vimana_stringify(item, markItem=None):
  string = ""
  if (type(item) is VimanaListType):
    string += "("
    printSpace = False
    while (item != VimanaNilType):
      if (printSpace):
        string += " "
      if (item is markItem):
        string += "--> "
      string += Vimana_stringify(Vimana_first(item), markItem)
      printSpace = True
      item = Vimana_rest(item)
    string += ")"
  elif (VimanaNilType == item):
    string = "()"
  else:
    string = str(item)
  return string

def Vimana_print(obj):
  VimanaPrintFunction(obj)

def Vimana_printstack():
  VimanaPrim_printstack(Vimana_interp)

def Vimana_setprintfun(fun):
  global VimanaPrintFunction
  VimanaPrintFunction = fun

# Create interpreter
Vimana_interp = VimanaInterp()

# Evaluate string
# TODO: Rename to Vimana_eval
def vimana(string):
  return Vimana_interp.eval(string)

# Library functions
vimana("""
(funify swap setglobal) funify (def) setglobal
(iftrue) (() ifelse) def
(iffalse) (() swap ifelse) def
(iszero) (0 eq) def
(isempty) (() eq) def
(doc) (drop) def
""")

# Simple implementation of local variables (local "registers" named A B C D)
#
# Note that you must drop locals using the [] function
#
# Example:
#
#   (myswap) ([AB] B A []) def
#
vimana("""
() (LocalStack) setglobal

( x localStackPush -> ) doc
(localStackPush)
  (LocalStack cons (LocalStack) setglobal) def

( localStackPop -> ) doc
(localStackPop)
  (LocalStack rest (LocalStack) setglobal) def

( n localStackPushPopFun -> ) doc
(localStackPushPopFun)
  ((localStackPop) cons localStackPush) def

( localStackPopContext -> ) doc
(localStackPopContext)
  (LocalStack first eval) def

( x1 .. xn n localStackPushVars -> ) doc
(localStackPushVars)
  (dup iszero
    (drop)
    (swap localStackPush 1 - localStackPushVars)
  ifelse) def

( n localStackPopVars -> ) doc
(localStackPopVars)
  (dup iszero
    (drop)
    (localStackPop 1 - localStackPopVars)
  ifelse) def



(A) (LocalStack rest first) def
(B) (LocalStack rest rest first) def
(C) (LocalStack rest rest rest first) def
(D) (LocalStack rest rest rest rest first) def

([A]) (localStackPushContext localStackPushVar) def
([AB]) (localStackPushContext localStackPushVar localStackPushVar) def
([ABC]) (localStackPushContext localStackPushVar localStackPushVar localStackPushVar) def
([ABCD]) (localStackPushContext localStackPushVar localStackPushVar localStackPushVar localStackPushVar) def

([]) (localStackPopContext) def
""")

'''
vimana("""
() (LocalStack) setglobal

(localStackPushContext)
  (() LocalStack cons (LocalStack) setglobal) def

(localStackPopContext)
  (LocalStack rest (LocalStack) setglobal) def

(localStackPushVar)
  (LocalStack first cons
      LocalStack swap setfirst) def

(A) (LocalStack first first) def
(B) (LocalStack first rest first) def
(C) (LocalStack first rest rest first) def
(D) (LocalStack first rest rest rest first) def

([A]) (localStackPushContext localStackPushVar) def
([AB]) (localStackPushContext localStackPushVar localStackPushVar) def
([ABC]) (localStackPushContext localStackPushVar localStackPushVar localStackPushVar) def
([ABCD]) (localStackPushContext localStackPushVar localStackPushVar localStackPushVar localStackPushVar) def

([]) (localStackPopContext) def
""")
'''

