<?php
/*
This file is a PHP script that generates the source code for vimanapico.py

Run this script from the command line:

    php vimanapico.php

The resulting output file is:

    vimanapico.py
*/

// Capture PHP output
ob_start();
?>
# File: vimanapico.py
# Vimana for MicroPython on Raspberry Pi Pico

###########################################################
# Vimana interpreter
###########################################################

<?php
// Include interpreter Python source
require "vimana.py";
?>

###########################################################
# Primitive functions for MicroPython
###########################################################

from machine import Pin
import utime

def Vimana_createMicroPythonPrimFuns(interp):
  interp.primFuns["pinNew"] = VimanaPrim_pinNew
  interp.primFuns["pinLow"] = VimanaPrim_pinLow
  interp.primFuns["pinHigh"] = VimanaPrim_pinHigh
  interp.primFuns["pinToggle"] = VimanaPrim_pinToggle
  interp.primFuns["sleep"] = VimanaPrim_sleep

def VimanaPrim_pinNew(interp):
  mode = interp.pop()
  id = interp.pop()
  interp.push(Pin(id, mode))

def VimanaPrim_pinLow(interp):
  led = interp.pop()
  led.low()

def VimanaPrim_pinHigh(interp):
  led = interp.pop()
  led.high()

def VimanaPrim_pinToggle(interp):
  led = interp.pop()
  led.toggle()

def VimanaPrim_sleep(interp):
  t = interp.pop()
  utime.sleep(t)

Vimana_createMicroPythonPrimFuns(Vimana_interp)

# MicroPython library functions
vimana("""
1 (OUT) setglobal
""")
<?php
// Save PHP output
$content = ob_get_clean();
file_put_contents("vimanapico.py", $content);
?>