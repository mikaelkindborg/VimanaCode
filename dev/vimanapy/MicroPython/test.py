# Code below is kept as notes

# MicroPython blink

'''
vimana("""
(blink) (
  dup toggle
  1.0 sleep
  blink) def

(makePin) (
  25 OUT Pin) def

makePin
  dup low
  blink
""")
'''

'''
from machine import Pin
import utime
led = Pin(25, Pin.OUT)
led.low()
while True:
    led.toggle()
    print("BLINKY!!!")
    utime.sleep(0.2)
'''

# Tests

'''
42 () cons () cons
printstack
dup first 44 setfirst
printstack
'''

'''
vimana("""
42 [A]
44 46 [AB]
A print
B print
[]
A print
[]
LocalStack printraw
LocalStack print
printstack
44 46 [AB]
LocalStack print
[]
""")
'''

'''
# Compute Fibonacci with stack operations
vimana("""
(20 fib with stack ops) print
(fib)
  (dup 1 > (dup 1 - fib swap 2 - fib +) iftrue) def
20 fib print
""")

# Compute Fibonacci with local variable, no stack operations
vimana("""
(20 fib with local var) print
(fib)
  ([A] A A 1 > (1 - fib A 2 - fib +) iftrue []) def
20 fib print
""")

# Compute Fibonacci with stack operation and local variable
vimana("""
(20 fib with stack op and local var) print
(fib)
  (dup 1 > ([A] A 1 - fib A 2 - fib + []) iftrue) def
20 fib print
""")
'''

'''
# Compute Factorial
vimana("""
(fact)
  (dup iszero (drop 1) (dup 1 - fact *) ifelse) def

(times)
  (dup iszero
    (drop drop)
    (over eval 1 - times)
  ifelse) def

(50 fact print) 100 times

10000 fact print
""")
'''

'''
# Test ifelse
vimana("""
(foo) (10 +) def

(checkMeaningOfLife)
  (42 eq
    ((it is the meaning of life))
    ((it is something else))
      ifelse) def

12 foo foo foo
  dup print
  dup checkMeaningOfLife

printstack
""")
'''

'''
# Test parser
l = interp.parser.parse("1 2 printstack + print (a b c) print")
Vimana_interp.print(l)
Vimana_interp.evalList(l)

vimana("""
(foo) (fun (10 +)) defvar

(foo) (10 +) def

[10, ["+", None], True]

[[10, None],
 ["dup", ["dup", ["first", ["1+", ["setfirst", ["first/print", None]]]]]],
 True]

printstack

drop drop drop drop drop drop drop drop drop drop
drop drop drop drop drop drop drop drop drop drop
printstack
""")

# Test lift functions
vimana("""
() print
( ) print
2 3 cons printraw
2 (3) cons print
1 2 () cons cons print
(1 2 3)
  dup 42 setfirst
  dup print
  dup first print
  dup rest print
printstack

drop
printstack
""")

# Test setglobal
vimana("""
42 (foo) setglobal
foo print
""")
'''

'''
def Vimana_conslast(a, b):
  if (None == a):
    return Vimana_cons(b, None)
  else:
    return Vimana_cons(Vimana_first(a), Vimana_conslast(Vimana_rest(a), b))
'''

