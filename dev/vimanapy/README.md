# Vimana Py

## Introduction

This folder contains the Vimana programming language implemented in Python. It is the most recent implementation of Vimana I have made. My intension is to make it reasonably easy to understand, and to experiment with.

Initially, I had no plans for making an implementation in Python, but when I got a Raspberry Pi Pico Maker Calendar, I started coding it using MicroPython. Then I did a quick Python implementation of Vimana as an experiment.

The Vimana Python implementation is slow, but it is very dynamic. Since the Python is a dynamic language, you can call Python code directly from Vimana code, and calling Vimana from Python is of course also easy to do. The Vimana data stack is used as to pass parameters between the two languages.

## How to get started

Make sure to have Python 3 installed on your laptop or desktop machine.

You run Vimana programs from the command line.

Here is how to run a Vimana file:

    python3 vimana.py hi.vimana

A tutorial-like example with comments in the source code:

    python3 vimana.py introduction.vimana

There is also a simplistic REPL (Read Eval Print Loop):

    python3 repl.py

You can for example try this code in the REPL:

    1 2 + print
    exit

But the Workbench UI is much nicer (try this):

    python3 workbench.py

# Raspberry Pi Pico

On Raspberry Pico you need to have MicroPython installed.
I have not yet made the instructions for running Vimana in MicroPython.
