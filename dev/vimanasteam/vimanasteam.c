/*
File: vimanasteam.c
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Test programs, benchmarks
*/

/*
Benchmarks for recursive fib 37 on Mac M1 (MacBook Air 2020)

Benchmark VimanaSteam 2024-04-19

// 64 bit data (128 bit items)
miki@Mikaels-MacBook-Air vimanasteam % cc vimanasteam.c -O3
miki@Mikaels-MacBook-Air vimanasteam % time ./a.out
((A 1 > ifTrueTail (A 1 - fib swap 2 - fib +)) set fib 37 fib print)
./a.out  1.59s user 0.00s system 89% cpu 1.785 total

// 32 bit data (64 bit items)
miki@Mikaels-MacBook-Air vimanasteam % cc vimanasteam.c -O3
miki@Mikaels-MacBook-Air vimanasteam % time ./a.out
((A 1 > ifTrueTail (A 1 - fib swap 2 - fib +)) set fib 37 fib print)
./a.out  1.64s user 0.00s system 82% cpu 1.993 total

Benchmark vimanac/vimana_2024

./vimana ../benchmark/fib.vimana  2.57s user 0.00s system 99% cpu 2.588 total
miki@Mikaels-MacBook-Air vimanac_2024 % time php ../benchmark/fib.php
24157817

Benchmark PHP (PHP 8.3.4 (cli) (built: Mar 12 2024 23:42:26) (NTS) Zend Engine v4.3.4)

miki@Mikaels-MacBook-Air vimanac_2024 % time php ../benchmark/fib.php
24157817
php ../benchmark/fib.php  1.79s user 0.03s system 95% cpu 1.910 total

Benchmark Lua (Lua 5.4.6)

miki@Mikaels-MacBook-Air vimanac_2024 % time lua ../benchmark/fib.lua
24157817
lua ../benchmark/fib.lua  2.06s user 0.01s system 99% cpu 2.080 total

Benchmark GForth (gforth 0.7.3)

miki@Mikaels-MacBook-Air vimanac_2024 % time gforth ../benchmark/fib.fs
24157817
gforth ../benchmark/fib.fs  0.88s user 0.00s system 98% cpu 0.896 total

Benchmark Ruby (ruby 2.6.10p210 (2022-04-12 revision 67958) [universal.arm64e-darwin23])

miki@Mikaels-MacBook-Air vimanac_2024 % time ruby ../benchmark/fib.rb
24157817
ruby ../benchmark/fib.rb  2.35s user 0.04s system 98% cpu 2.430 total
*/

// --------------------------------------------------------------
// Include files
// --------------------------------------------------------------

#include "vimanasteam.h"

// --------------------------------------------------------------
// Test program
// --------------------------------------------------------------

int main()
{
  //char prog[] = "0 ifTrueTail (42 print) 2 (42 +) eval + print";
  //char prog[] = "1 2 + print";
  //char prog[] = "42 print";
  //char prog[] = "foo 42 + foo";
  //char prog[] = "42 print 42 set Foo Foo print";
  //char prog[] = "(2 *) set double 21 double print";
  //char prog[] = "(A 1 isLess ifTrueTail (A 1 - fib swap 2 - fib +)) set fib 37 fib print";

  //char prog[] = ":fib (A 1 isLess ifTrueTail (A 1 - fib swap 2 - fib +)) 37 fib print"; // 1.58s
  //char prog[] = ":fib (A 1 isLess ifTrueTail (A 1- fib swap 2- fib +)) 37 fib print"; // 1.40s
  //char prog[] = ":fib (A 1 ifLessTail (A 1 - fib swap 2 - fib +)) 37 fib print"; // 1.45s
  //char prog[] = ":fib (A 1 ifLessTail (A 1- fib swap 2- fib +)) 37 fib print"; // 1.17s

  //char prog[] = \
    ":LoopCount 3 " \
    ":loop (printstack A ifTrueTail (1 - tail loop)) " \
    "LoopCount loop _";

  //char prog[] = \
    ":Counter 0 " \
    ":LoopCount 10000 " \
    ":timesworker (A ifTrueTail (B eval 1- tail timesworker)) " \
    ":times (timesworker _ _) " \
    "((Counter 1+ set Counter) LoopCount times) LoopCount times Counter print"; // 2.22s
  //char prog[] = \
    ":Counter 0 " \
    ":LoopCount 10000 " \
    ":times (timesworker _ _) " \
    ":timesworker (A ifTrueTail (B eval 1- tail timesworker)) " \
    "((incr Counter) LoopCount times) LoopCount times Counter print"; // 2.22s !! (incr should be faster)

  FILE* stream = fmemopen(prog, strlen(prog), "r");
  //FILE* stream = fopen("foo.bar", "r");
  TypeItem head = ParseProgram(stream);
  fclose(stream);
/*
  DebugPrintNewLine();
  DebugPrintList("ParsedList", head);
  DebugPrintStringMem("StringMem");
  DebugPrintProgMem("ProgMem");
  DebugPrintSymbolTable("Globals");
*/

  ProgPrint(head);

  ProgEval(head);

  return 0;
}

/*
TODO: Remove commented out code

int ProgMemAddress = 0;

// Write linear list
void ProgMemWriteItem(TypeData value, TypeData op)
{
  ProgMem[ProgMemAddress].value = value;
  ProgMem[ProgMemAddress].next = NextWith(ProgMemAddress + 1, op);
  ++ ProgMemAddress;
}

int main()
{
  int size = sizeof(TypeData);
  printf("sizeof(TypeData): %i\n", size);

  ProgMemAddress = 1;

  // Print number
  ProgMemWriteItem(42, OP_INTNUM);
  ProgMemWriteItem(0, OP_PRINT);

  // Add number
  ProgMemWriteItem(42, OP_INTNUM);
  ProgMemWriteItem(2, OP_INTNUM);
  ProgMemWriteItem(0, OP_ADD);
  ProgMemWriteItem(0, OP_PRINT);
  ProgMem[ProgMemAddress - 1].next = NextWithIndex(ProgMem[ProgMemAddress - 1].next, 0);

  //ProgMemWriteItem(0, OP_EXIT);

  //ProgMemListEnd();

  // Eval
  ProgEval(1);

  return 0;
}
*/