# Syntax for Vimana

Below, the syntax for Vimana is defined in a more formal, detailed way. Examples are also included, but this document is not a tutorial. To get an introduction to Vimana, see the tutorial document.

## Grammar

    PROGRAM    = sequence of ITEMs separated by WHITESPACE
    WHITESPACE = SPACE or TAB or NEWLINE or RETURN
    ITEM       = WORD or INTEGER or LIST or STRING or COMMENT
    INTEGER    = positive or negative decimal integer number
    WORD       = GLOBAL or FUNCTION or DEFINECHAR
    GLOBAL     = PascalCase string (initial char is an uppercase letter)
    FUNCTION   = camelCase string (initial char is a non-uppercase letter)
    DEFINECHAR = :
    DEFINITION = DEFINECHAR (optional WHITESPACE) WORD WHITESPACE ITEM
    LIST       = ( sequence of ITEMs separated by WHITESPACE )
    STRING     = { sequence of characters }
    COMMENT    = [ sequence of characters ]

"The sloppy/minimalistic programmer" - "I am incredibly lazy"

## Details and Examples

### PROGRAM

A program is a list of items, but at the top-level of a program the left and right parens are not explicitly stated.

Example:

    1 2 + print

In the above example, the following happens:

    1 is pushed onto the data stack
    2 is pushed onto the data stack
    Function + is called (1 and 2 is popped off the stack,
      3 is pushed onto the stack)
    Function print pops the top item off the stack and prints it

### INTEGER

Positive integer:

    42

Negative integer:

    -42

## WORD

A word is symbol that may not contain any whitespace characters.

Words name functions and global variables:

* Global variable names begin with an uppercase letter
* Function names begin with a lowercase letter (or with a non-whitespace character, except the DEFINECHAR character)

Function words are also global variables - the difference is that when a function is seen by the interpreter, the body of the function is evaluated. When a global variable is seen by the interpreter, the value of the variable is pushed onto the data stack.

Examples:

    :Foo 21              [set global variable]
    :foo (2 *)           [set function body]

    Foo print            [prints 21]
    Foo foo print        [prints 42]
    21 foo print         [prints 42]
    foo print            [stack underflow error]

Another example:

    :Foo 21              [set global variable]
    :Bar (21)            [set global variable]
    :foo (21)            [set function body]

    Foo print            [prints 21]
    foo print            [prints 21]
    Bar print            [printf (21)]
    Foo foo + print      [prints 42]
    Bar eval Foo + print [prints 42]

When the list (21) gets evaluated, the integer 21 is pushed onto to the data stack. Function **eval** evaluates a list.

### DEFINECHAR

The define character (:) is a runtime function that sets a global value. It is like any word, except it is special in that it does not need to be separated by a trailing whitespace (the parser handles this case). However, there must be a whitespace before the define character.

Examples:

    :Bar 21
    :foo (Bar 2 * print)

    : Bar 21
    : foo (Bar 2 * print)

<!--
The following is an error/undefined behaviour:

    :Foo 42:Bar 21

The define character may be used inside words (but this is not recommended). The following names would be allowed (but would be confusing):

    :Foo42:Bar21 {Now I am confused}
    Foo42:Bar21 print

    :Foo42:Bar21: {Now I am confused again}
    Foo42:Bar21: print

    :!::::::::: {Oh no!}
    !::::::::: print
-->

### LIST

Good to know:

* A list can contain zero or more items
* Lists can be nested
* Lists are used to represent both data and programs
* Items in a list are not evaluated until the list is called as a function
* Right and left parens do not need to be separated by white space

#### Examples

Function call:

    :foo (1 2 + print)
    foo

Evaluate a list:

    (1 2 + print) eval

Evaluate the value of a global variable:

    :Foo (1 2 + print)
    Foo eval

Function **eval** evaluates a list, as if it would be a function. It would be possible to not have function words, just variables, and use eval to explicitly call each function. But this would make the code harder to read, which is why "auto evaluating" functions are used.

#### Stack Example

A list can also be empty:

    ()

The following example uses an empty list to implement a stack:

    :Stack ()
    :stackPush (Stack cons set Stack)
    :stackPop  (Stack first Stack rest set Stack)

    42 stackPush
    stackPop print

Words **set**, **cons**, **first** and **rest**, are built-in functions.

**Note that the data stack is the built-in stack used by the interpreter to store parameters, and the Stack variable in the example is a stack implemented in the Vimana language using a list.**

This is what happens when stackPop gets called:

    Stack       [ the Stack list is pushed onto the data stack ]
    first       [ the first element (item) of the list is
                  pushed onto the data stack ]
    Stack       [ the Stack list is pushed onto the data stack ]
    rest        [ the rest of the list is pushed onto the data stack ]
    set Stack   [ variable Stack is set to the top of the data stack ]

Example data stack values:

    (42)        [ Stack ]
    42          [ first ]
    42 (42)     [ Stack ]
    42 ()       [ rest ]
    42 ()       [ set Stack ]
    42

#### Syntactic Sugar

A function with an empty list does nothing. This can be used to define "syntactic sugar" words. For examle a word that separates a line of code into logical stack actions.

Here is an example of a sugar word that could make the function **stackPop** easier to read:

    :then ()

Now, compare the following two versions of stackPop:

    :stackPop (Stack first Stack rest set Stack)
    :stackPop (Stack first then Stack rest set Stack)

Note how the word **then** separates the logical actions in the function body - get the top of Stack (first), and set Stack to the rest of the list.

### STRING

A string can contain any character, whitespace, and also nested string.

Examples:

    {Hi World!} print

    {This is {a {nested} string}} print

    {This is an
      indented multiline string

    with blank lines} print

### COMMENT

A comment is skipped by the parser, and is not represented in the abstract syntax tree.

A comment can contain any character, but unlike strings, comments cannot be nested. That is, a right square bracket (]) always ends the entire comment.

Examples:

    [This is a comment]

    [This is a
     multiline comment]

<!--
This is an error/undefined behaviour:

    [[This] is a comment]

Very bad example that however would work:

    :bad ({Oh no!} print)
    :example] ({Do not do this} print)

    [[This is a comment] bad example]
-->

## Word Order

As an interesting note, the Vimana language uses postfix notation, as in Forth. In linguistics, this is called the SOV word order (Subject-Object-Verb).

### Subject-Object-Verb (SOV)

In the world, 564 natural languages out of 1376 use SOV word order. This includes: Ancient Greek, Indo-Aryan languages, Bengali, Hindi/Urdu, Burmese, Japanese, Korean, Latin, Oromo, Persian, Sanskrit, Tamil, Telugu, Turkish. These are very old languages.

### Subject-Verb-Object (SVO)

SVO word order (Subject-Verb-Object) is used by 488 natural languages. This includes many of the Western languages, and notably Chinese. Note how this word order resembles Smalltalk and object-oriented programming.

### Verb-Subject-Object (VSO and VOS)

VSO (Verb-Subject-Object) and VOS (Verb-Object-Subject) word order are used by 120 natural languages. This is the word order of prefix notation, used by Lisp, and other functional and procedural programming languages.

### No Dominant Order

Interestingly, 189 languages does not have a dominant word order. These appears to be mainly languages with an ancient origin. Sanskrit is considered SOV but has a flexible word order.

Here we can also see a connection with Forth, where the word order sometimes is prefix or infix, depending on how the programmer defines words.

### Summary

    Word Order            Nat Langs   Prog Langs
    Subject-Object-Verb   564         Concatenative (Forth, Vimana)
    Subject-Verb-Object   488         Object Oriented (Smalltalk)
    Verb-Subject-Object   95          Procedural, Functional (Lisp)
    Verb-Object-Subject   25          Procedural, Functional (Lisp)
    No Dominant Order     189         ? (Forth sometimes)

### Conclusion

As a speaker of Western languages, the postfix grammar of Forth and other languages may feel awkward. But, when looking globally, this is the most common word order among the analysed languages.

Many of the SOV natural languages have ancient origins, which is a good fit with the Vimana programming language. The inspiration from the Ancient Vedic culture can also be seen in Sanskrit, where the preferred word order of written prose is Subject-Object-Verb (SOV), even though the word order is flexible.

### Reference

The World Atlas of Language Structures (WALS) [https://wals.info/chapter/81](https://wals.info/chapter/81)

### Notes

The SOV order would also work well for object-oriented programming, including multiple dispatch.

SVO order (Smalltalk):

    sprite moveTo: point

SOV order (Vimana):

    sprite point moveTo

VSO order (Lisp):

    (moveTo sprite point)

VOS order (Lisp):

    (moveTo point sprite)
