/*
File: parser.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Vimana parser
*/

// --------------------------------------------------------------
// Parser
// --------------------------------------------------------------

char StreamLastChar;
TypeBool StreamUseLastChar = FALSE;

char StreamReadNextChar(FILE* stream)
{
  if (StreamUseLastChar)
  {
    StreamUseLastChar = FALSE;
    return StreamLastChar;
  }
  else
  {
    StreamLastChar = fgetc(stream);
    //printf("%c", StreamLastChar);
    return StreamLastChar;
  }
}

#define IsEOF(c)          (EOF == (c))
#define IsDefine(c)       (':' == (c))
#define IsLeftParen(c)    ('(' == (c))
#define IsRightParen(c)   (')' == (c))
#define IsStringBegin(c)  ('{' == (c))
#define IsStringEnd(c)    ('}' == (c))
#define IsCommentBegin(c) ('[' == (c))
#define IsCommentEnd(c)   (']' == (c))
#define IsWhiteSpace(c) \
  ((' ' == (c)) || ('\t' == (c)) || ('\n' == (c)) || ('\r' == (c)))
#define IsSeparator(c) \
  (IsLeftParen(c) || IsRightParen(c) || IsStringBegin(c) || IsStringEnd(c))
#define IsWhiteSpaceOrSeparator(c) (IsWhiteSpace(c) || IsSeparator(c))

void SkipComment(FILE* stream)
{
  int c = StreamReadNextChar(stream);
  while (!IsCommentEnd(c) && !IsEOF(c))
  {
    c = StreamReadNextChar(stream);
  }
}

char* ParseReadToken(int c, FILE* stream)
{
  // static local variable = hidden global
  static char Token[MAX_TOKEN_LENGTH];

  // Write chars to token buffer
  int i = 0;

  // Check define char (:)
  if (IsDefine(c))
  {
    // The define character may be used without trailing
    // whitespace, so we handle it here in this special case
    // It becomes a separate word in the parsed syntax tree
    Token[i] = c;
    ++ i;
  }
  else
  {
    // Read token
    while (!IsEOF(c) && !IsWhiteSpaceOrSeparator(c))
    {
      // Max token length includes the zero-terminator
      if (i < MAX_TOKEN_LENGTH - 1)
      {
        Token[i] = c;
        ++ i;
      }
      c = StreamReadNextChar(stream);
    }

    // Set flag telling that we have consumed the last car
    StreamUseLastChar = TRUE;
  }

  // Zero terminate token string
  Token[i] = 0;

  return Token;
}

// Get the token type
// Global variables begin with an uppercase letter
// Functions and opcodes/primfuns begin with anything
// that is not an uppercase letter
int ParseType(char* token)
{
  if (StringIsInteger(token))
    { return OP_INTEGER; }
  else
  if (isupper(token[0]))
    { return OP_GLOBAL; }
  else
    { return OP_FUNCTION; }
}

TypeItem ParseInteger(char* token)
{
  TypeItem item;
  item.value = (TypeData) strtol(token, NULL, 10);
  item.next = ItemNextWith(0, OP_INTEGER);
  return item;
}

TypeItem ParseOpCode(int opcode)
{
  TypeItem item;

  // Opcodes have the code in the "next" field
  item.value = 0;
  item.next = ItemNextWith(0, opcode);

  return item;
}

TypeItem ParseSymbol(char* token, int optype)
{
  TypeItem item;

  // Look up the name in the symbol table
  TypeIndex index = SymbolTableLookupIndexForName(token);
  if (-1 == index)
  {
    // Add new symbol
    index = SymbolTableAdd(token);
  }

  if (OP_FUNCTION == optype)
  {
    item.value = index;
    item.next = ItemNextWith(0, OP_FUNCTION);
  }
  else
  if (OP_GLOBAL == optype)
  {
    item.value = index;
    item.next = ItemNextWith(0, OP_GLOBAL);
  }

  return item;
}

TypeItem ParseToken(char c, FILE* stream)
{
  TypeItem item;

  char* token = ParseReadToken(c, stream);

  int optype = ParseType(token);

  if (OP_INTEGER == optype)
  {
    item = ParseInteger(token);
  }
  else
  {
    // Is it an opcode/primfun?
    int opcode = OpTableLookupCodeForName(token);
    if (opcode > -1)
    {
      item = ParseOpCode(opcode);
    }
    // Otherwise it is a symbol
    else
    {
      item = ParseSymbol(token, optype);
    }
  }

  return item;
}

TypeItem ParseList(FILE* stream)
{
  TypeItem  head;
  TypeItem  item;
  TypeIndex prevItemIndex;
  TypeBool  addItem;
  TypeBool  setFirst = TRUE;

  // Set head
  head.value = 0; // Empty list
  head.next = ItemNextWith(0, OP_LIST);

  int c = StreamReadNextChar(stream);

  while (!IsEOF(c))
  {
    addItem = FALSE;

    if (IsWhiteSpace(c))
    {
      // Skip whitespace
    }
    else
    if (IsCommentBegin(c))
    {
      SkipComment(stream);
    }
    else
    if (IsLeftParen(c))
    {
      // Returns list head
      item = ParseList(stream);
      addItem = TRUE;
    }
    else
    if (IsRightParen(c))
    {
      // TODO If single item list, change type and return item ??
      break;
      //return head;
    }
    else
    // it is a symbol or number
    {
      item = ParseToken(c, stream);
      addItem = TRUE;
    }

    // Add item
    if (addItem)
    {
      TypeIndex itemIndex = ProgMemWrite(item);
      if (setFirst)
      {
        // Set first of head (head points to first item in list)
        head.value = itemIndex;
        setFirst = FALSE;
      }
      else
      {
        // Set next of previous item
        ProgMemSetItemNext(prevItemIndex, itemIndex);
      }
      // Save previous address
      prevItemIndex = itemIndex;
    }

    c = StreamReadNextChar(stream);
  }
  // while

  return head;
}

TypeItem ParseProgram(FILE* stream)
{
  return ParseList(stream);
}

/*
This is unused code for separation of opcodes and primfuns.
Good to use if we want to keep the opcode field short and
have many primfuns.

TypeItem ParseOpCode(int opcode)
{
  TypeItem item;

  if (opcode <= OP_PRIMFUN)
  {
    // Opcodes have the code in the "next" field
    item.value = 0;
    item.next = ItemNextWith(0, opcode);
  }
  else
  {
    // Primfuns have the opcode in the value field, as the number
    // of opcodes is limited (number of primfuns is unlimited)
    item.value = opcode;
    item.next = ItemNextWith(0, OP_PRIMFUN);
  }

  return item;
}
*/
