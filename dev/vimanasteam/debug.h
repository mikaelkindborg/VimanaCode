/*
File: debug.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Debug functions
*/

// --------------------------------------------------------------
// Debug functions
// --------------------------------------------------------------

void DebugPrintItem(char* label, TypeItem item)
{
  printf("[%s] Op: %s value: %i next: %i\n",
    label, OpNameTable[ItemOp(item)], (int)ItemValue(item), (int)ItemNext(item));
}

void DebugPrintList(char* label, TypeItem head)
{
  char labelBuf[8];
  printf("[%s] List Elements (head first):\n", label);
  DebugPrintItem("Head", head);
  TypeIndex index = ListFirst(head);
  while (index)
  {
    TypeItem item = ProgMem[index];
    sprintf(labelBuf, "%i", (int) index);
    DebugPrintItem(labelBuf, item);
    index = ItemNext(item);
  }
}

void DebugPrintProgMem(char* label)
{
  char labelBuf[8];

  printf("[%s] Program Memory:\n", label);
  int i = 0;
  while (i < ProgMemFirstFree)
  {
    sprintf(labelBuf, "%i", (int) i);
    DebugPrintItem(labelBuf, ProgMem[i]);
    ++ i;
  }
}

void DebugPrintSymbolTable(char* label)
{
  char labelBuf[MAX_TOKEN_LENGTH + 8];

  printf("[%s] Symbol Table:\n", label);
  int i = 0;
  while (i < SymbolTableFirstFree)
  {
    sprintf(labelBuf, "%i %s", (int) i, SymbolNameTable[i]);
    DebugPrintItem(labelBuf, SymbolTable[i]);
    ++ i;
  }
}

void DebugPrintStringMem(char* label)
{
  printf("[%s] String memory:\n", label);
  int i = 0;
  while (i < StringMemFirstFree)
  {
    if (0 == StringMem[i])
    {
      printf("\n");
    }
    else
    {
      printf("%c", StringMem[i]);
    }
    ++ i;
  }
}

#define DebugPrint(string) printf("[%s]\n", string)

#define DebugPrintInt(label, i) printf("[%s] %i\n", label, (int)i)

#define DebugPrintNewLine() printf("\n")
