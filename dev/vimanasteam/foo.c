#include <stdio.h>
#include <stdlib.h>

int main()
{
  // Circular index 0..31
  unsigned char n = 0;
  for (int i = 0; i < 34; ++ i)
  {
    printf("%i\n", (int)(n >> 3));
    n = n + 8;
  }
}

/*
int main()
{
  int n = 0;
  FILE* stream = fopen("foo.c", "r");
  int c = getc(stream);

  while (EOF != c)
  {
    if (60 < n)
    {
      printf("\n");
      n = 0;
    }
    if ((' '  != c) && ('\n' != c))
    {
      printf("%c", (unsigned char)c);
      ++ n;
    }
    c = getc(stream);
  }
  printf("\n");

  fclose(stream);

  return 0;
}
*/