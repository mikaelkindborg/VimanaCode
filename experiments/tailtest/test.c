/*
File: test.c

Tests tail calls in C

Compile with:

    cc test.c -foptimize-sibling-calls -O2
or:

    gcc test.c -foptimize-sibling-calls -O2

Run with:

    time ./a.out

Tested on macOS.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//#define USE_TAIL_CALLS
//#define USE_TAIL_CALLS_WITH_FUNCTION_POINTERS
//#define USE_JUMP_TABLE
#define USE_SWITCH_LOOP
//#define USE_SWITCH_LOOP_WITH_FUNCTION_POINTERS

long Random = 0;
long Counter = 0;
long CounterMax = 261518 * 2000;

#ifdef USE_TAIL_CALLS

void Fun1();
void Fun2();

void Fun1()
{
  ++ Counter;
  Random = Random + (rand() % 6);
  if (Counter < CounterMax) { Fun2(); }
}

void Fun2()
{
  ++ Counter;
  Random = Random - (rand() % 6);
  if (Counter < CounterMax) { Fun1(); }
}

int main()
{
  srand(time(0));
  printf("Tail Call test\n");
  Fun1();
  printf("Counter: %ld\n", Counter);
  printf("Random:  %ld\n", Random);
  printf("End of test\n");
}

#endif

#ifdef USE_TAIL_CALLS_WITH_FUNCTION_POINTERS

typedef void (*TailFun)();

void Fun1();
void Fun2();

TailFunType FunPtr1 = Fun1;
TailFunType FunPtr2 = Fun2;

void Fun1()
{
  ++ Counter;
  Random = Random + (rand() % 6);
  if (Counter < CounterMax) { FunPtr2(); }
}

void Fun2()
{
  ++ Counter;
  Random = Random - (rand() % 6);
  if (Counter < CounterMax) { FunPtr1(); }
}

int main()
{
  srand(time(0));
  printf("Tail Call test\n");
  Fun1();
  printf("Counter: %ld\n", Counter);
  printf("Random:  %ld\n", Random);
  printf("End of test\n");
}

#endif

#ifdef USE_JUMP_TABLE

void Fun()
{
  /*
  char* JumpTable[] =
  {
    && Fun1,
    && Fun2
  };*/

  //void* fun1 = && Fun1;
  //void* fun2 = && Fun2;

Fun1:
  ++ Counter;
  Random = Random + (rand() % 6);
  if (Counter < CounterMax)
    //{ goto *JumpTable[1]; }
    { goto *(&&Fun2); }

Fun2:
  ++ Counter;
  Random = Random - (rand() % 6);
  if (Counter < CounterMax)
    //{ goto *JumpTable[0]; }
    { goto  *(&&Fun1); }
}

int main()
{
  srand(time(0));
  printf("Jump Table test\n");
  Fun();
  printf("Counter: %ld\n", Counter);
  printf("Random:  %ld\n", Random);
  printf("End of test\n");
}

#endif

#ifdef USE_SWITCH_LOOP

void Fun()
{
  int op = 0;
  while (Counter < CounterMax)
  {
    switch (op)
    {
      case 0:
        ++ Counter;
        Random = Random + (rand() % 6);
        op = 1;
        break;
      case 1:
        ++ Counter;
        Random = Random - (rand() % 6);
        op = 0;
        break;
    }
  }
}

int main()
{
  srand(time(0));
  printf("Switch Loop test\n");
  Fun();
  printf("Counter: %ld\n", Counter);
  printf("Random:  %ld\n", Random);
  printf("End of test\n");
}

#endif

#ifdef USE_SWITCH_LOOP_WITH_FUNCTION_POINTERS

typedef void (*FunType)();

void Fun1();
void Fun2();

FunType FunPtr1 = Fun1;
FunType FunPtr2 = Fun2;

void Fun1()
{
  ++ Counter;
  Random = Random + (rand() % 6);
}

void Fun2()
{
  ++ Counter;
  Random = Random - (rand() % 6);
}

void Fun()
{
  int op = 0;
  while (Counter < CounterMax)
  {
    switch (op)
    {
      case 0:
        FunPtr1();
        break;
      case 1:
        FunPtr2();
        break;
    }
  }
}

int main()
{
  srand(time(0));
  printf("Switch Loop test\n");
  Fun();
  printf("Counter: %ld\n", Counter);
  printf("Random:  %ld\n", Random);
  printf("End of test\n");
}

#endif
