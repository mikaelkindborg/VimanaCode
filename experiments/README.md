# Experimental Code

__Updated January 22, 2024__

This directory contains old versions of Vimana and experimental code.

For example, the __tailtest__ directory contains code for learning about and testing tail calls in C, compared to switch dispatch and computed goto.

The code in this directory is not maintained.
