#include <stdio.h>
#include <stdlib.h>

int isFOO(int n)
{
  return (0 == (n % 3));
}

int isBAR(int n)
{
  return (0 == (n % 5));
}

int main()
{
  for (int n = 1; n < 101; ++ n)
  {
    if (isFOO(n) && isBAR(n))
    {
      printf("FOOBAR ");
    }
    else
    if (isFOO(n))
    {
      printf("FOO ");
    }
    else
    if (isBAR(n))
    {
      printf("BAR ");
    }
    else
    {
      printf("%i ", n);
    }
  }

  printf("\n");

  return 0;
}
