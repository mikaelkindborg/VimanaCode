/*
Loop Machine Interpreter
*/

/*
Apple M1:

// gforth 0.7.3
time gforth loop.fs
0.73s

// Plain c (not optimized)
cc loop.c
0.10s

// vimanac/benchmark/loopmachine.c (optimized)
cc loopmachine.c -O3
time ./a.out
0.60s

// loopmachine.c (this file) (optimized)
cc loopmachine.c -O3
time ./a.out
0.87s
*/

// --------------------------------------------------------------
// Include files
// --------------------------------------------------------------

#include "loopmachine.h"

// --------------------------------------------------------------
// Test program
// --------------------------------------------------------------

int ProgMemAddress = 0;

void ProgMemWrite(TypeData data, TypeData op)
{
  ProgMem[ProgMemAddress] = DataWith(data, op);
  ++ ProgMemAddress;
}

int main()
{
  int size = sizeof(TypeData);
  printf("sizeof(TypeData): %i\n", size);
/*
  // Print number
  ProgMemWrite(42, OP_INTNUM);
  ProgMemWrite(0, OP_PRINT);

  // Add number
  ProgMemWrite(42, OP_INTNUM);
  ProgMemWrite(2, OP_INTNUM);
  ProgMemWrite(0, OP_ADD);
  ProgMemWrite(0, OP_PRINT);

  // Define global value at symbol index 1
  // : GlobalValue 43
  ProgMemWrite(0, OP_DEF);
  ProgMemWrite(1, OP_INTNUM);
  ProgMemWrite(43, OP_INTNUM);

  // Print the value
  ProgMemWrite(1, OP_GLOBAL);
  ProgMemWrite(0, OP_PRINT);

  // Define function ADD1
  ProgMemWrite(0, OP_DEF);
  ProgMemWrite(2, OP_INTNUM);
  ProgMemWrite(50, OP_LIST);

  // Call function ADD1
  ProgMemWrite(21, OP_INTNUM);
  ProgMemWrite(2, OP_FUNCALL);
  ProgMemWrite(0, OP_PRINT);
*/
  // Set global Counter to zero
  ProgMemWrite(0, OP_INTNUM);
  ProgMemWrite(0, OP_SET);
  ProgMemWrite(4, OP_GLOBAL);

  ProgMemWrite(4, OP_GLOBAL);
  ProgMemWrite(4, OP_PRINT);

  ProgMemWrite(4, OP_GLOBAL);
  ProgMemWrite(1, OP_INTNUM);
  ProgMemWrite(0, OP_ADD);
  ProgMemWrite(0, OP_SET);
  ProgMemWrite(4, OP_GLOBAL);

  ProgMemWrite(4, OP_GLOBAL);
  ProgMemWrite(4, OP_PRINT);

  //ProgMemWrite(0, OP_EXIT);

  // Define function COUNTDOWN
  ProgMemWrite(0, OP_DEF);
  ProgMemWrite(3, OP_INTNUM);
  ProgMemWrite(60, OP_LIST);

  // Call function COUNTDOWN
  int loopCount = 100000000;
  ProgMemWrite(loopCount, OP_INTNUM);
  ProgMemWrite(0, OP_DUP);
  ProgMemWrite(0, OP_PRINT);
  ProgMemWrite(3, OP_FUNCALL);
  ProgMemWrite(0, OP_PRINT);

  ProgMemWrite(4, OP_GLOBAL);
  ProgMemWrite(0, OP_PRINT);

  ProgMemWrite(4042, OP_INTNUM);
  ProgMemWrite(4043, OP_INTNUM);
  ProgMemWrite(4044, OP_INTNUM);
  ProgMemWrite(0, OP_SUB);

  ProgMemWrite(0, OP_PRINTSTACK);

  // Exit program
  ProgMemWrite(0, OP_EXIT);

  // Write function ADD1
  // : add1 (1 add)
  ProgMemAddress = 50;
  ProgMemWrite(ProgMemAddress + 3, OP_LISTHEAD);
  ProgMemWrite(1, OP_INTNUM);
  ProgMemWrite(0, OP_ADD);
  ProgMemWrite(0, OP_LISTEND);

  // Write function COUNTDOWN
  // : countdown (ifZero (return) sub1 tail countdown)
  ProgMemAddress = 60;
  ProgMemWrite(ProgMemAddress + 9, OP_LISTHEAD);
  ProgMemWrite(0, OP_IFZERO);
  ProgMemWrite(ProgMemAddress + 2, OP_LISTHEAD);
  ProgMemWrite(0, OP_RETURN);
  ProgMemWrite(0, OP_LISTEND);

  //ProgMemWrite(1, OP_INTNUM);
  ProgMemWrite(0, OP_SUB1);

  // incr Counter
  ProgMemWrite(1, OP_INCR);
  ProgMemWrite(4, OP_GLOBAL);

/*
  // Counter 1+ -> Counter

  ProgMemWrite(4, OP_GLOBAL);
  ProgMemWrite(1, OP_INTNUM);
  ProgMemWrite(0, OP_ADD);
  ProgMemWrite(0, OP_SET);
  ProgMemWrite(4, OP_GLOBAL);
*/
  //ProgMemWrite(0, OP_DUP);
  //ProgMemWrite(0, OP_PRINT);
  //ProgMemWrite(3, OP_FUNCALL);
  ProgMemWrite(3, OP_TAILCALL);
  ProgMemWrite(0, OP_LISTEND);

  // Run
  ProgRun(ProgMem, 0); // 0.87s

  return 0;
}
