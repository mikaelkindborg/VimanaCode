#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine Interpreter
*/

// --------------------------------------------------------------
// Interpreter
// --------------------------------------------------------------

#include "optable.h"

typedef long TypeData;
typedef int  TypeIndex;
typedef int  TypeBool;

#define TRUE  1
#define FALSE 0

TypeData SymbolTable[20];
TypeData DataStack[20];
TypeData CallStack[20];
TypeData ProgMem[100];

#define DataVal(data)           ((data) >> 8)
#define DataOp(data)            ((data) & 0xFF)
#define DataWith(value, op)     (((value) << 8) | (op))
#define DataSetVal(data, value) (((value) << 8) | ((data) & 0xFF))

// --------------------------------------------------------------
// Interpreter loop
// --------------------------------------------------------------

void ProgRun(TypeData Program[], TypeIndex startIndex)
{
  // Persistent variables begin with UpperCase letter
  TypeBool  RunFlag = TRUE;
  TypeIndex StackTop = -1;
  TypeIndex CallStackTop = -1;
  TypeIndex InstrIndex = startIndex;

  // Temporary variables
  TypeData  data;  // current instruction data
  TypeData  op;    // current operation
  TypeData  A;     // top of data stack or other value
  TypeData  B;     // second in data stack or other value

  while (RunFlag)
  {
    data = Program[InstrIndex];
    op = DataOp(data);

    //printf("Value: %li Op: %li\n", DataVal(data), op);

    switch (op)
    {
      #include "opcodes.h"
    }
  }

  printf("End of Program\n");
}

// --------------------------------------------------------------
// Parser
// --------------------------------------------------------------
/*
// Parse numbers and symbols
void ParseToken(stream)
{
}

void ParseList(stream)
{
}

void ParseProgram(stream)
{
  while (c)
  {
    if (' ' == c) // whitespace
    {
      c = next(stream);
    }
    else
    if ('(' == c)
    {
      ParseList(stream);
    }
    else
    {
      ParseToken(stream);
    }
  }
}
*/