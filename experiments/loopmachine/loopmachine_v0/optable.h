// Tables generated by opcodes.py

enum OP_CODE
{
  OP_DEF = 0,
  OP_SET,
  OP_GLOBAL,
  OP_INTNUM,
  OP_LIST,
  OP_LISTHEAD,
  OP_LISTEND,
  OP_RETURN,
  OP_FUNCALL,
  OP_TAILCALL,
  OP_EVAL,
  OP_TAILEVAL,
  OP_ADD,
  OP_SUB,
  OP_MULT,
  OP_DIV,
  OP_ADD1,
  OP_SUB1,
  OP_INCR,
  OP_IFTRUE,
  OP_IFFALSE,
  OP_IFZERO,
  OP_DUP,
  OP_A,
  OP_B,
  OP_C,
  OP_SWAP,
  OP_ROT,
  OP_DROP,
  OP_PRINT,
  OP_PRINTSTACK,
  OP_EXIT,
  OP_PRIMFUN,
  OP_LAST_ENTRY
};

char* OpNameTable[] =
{
  ":",
  "->",
  "OP_GLOBAL",
  "OP_INTNUM",
  "OP_LIST",
  "OP_LISTHEAD",
  "OP_LISTEND",
  "return",
  "OP_FUNCALL",
  "tail",
  "eval",
  "taileval",
  "+",
  "-",
  "*",
  "/",
  "1+",
  "1-",
  "incr",
  "ifTrue",
  "ifFalse",
  "ifZero",
  "dup",
  "A",
  "B",
  "C",
  "swap",
  "rot",
  "drop",
  "print",
  "printstack",
  "exit",
  "OP_PRIMFUN"
};
