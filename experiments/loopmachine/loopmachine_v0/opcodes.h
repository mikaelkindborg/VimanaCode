
// --------------------------------------------------------
// Set global variables
// --------------------------------------------------------

case OP_DEF: // :
  // Symbol index is in the data value
  ++ InstrIndex;
  data = Program[InstrIndex];
  // Set data for symbol
  ++ InstrIndex;
  SymbolTable[DataVal(data)] = Program[InstrIndex];
  ++ InstrIndex;
  break;

case OP_SET: // ->
  // Get value on data stack
  A = DataStack[StackTop];
  -- StackTop;
  // Get symbol index
  ++ InstrIndex;
  data = Program[InstrIndex];
  // Set symbol value to stack value
  SymbolTable[DataVal(data)] = A;
  ++ InstrIndex;
  break;

// --------------------------------------------------------
// Get global variable
// --------------------------------------------------------

case OP_GLOBAL:
  // Push value of global variable to data stack
  ++ StackTop;
  DataStack[StackTop] = SymbolTable[DataVal(data)];
  ++ InstrIndex;
  break;

// --------------------------------------------------------
// Push values - data stack contains full words (value + op)
// --------------------------------------------------------

case OP_INTNUM:
  // Push number to data stack
  ++ StackTop;
  DataStack[StackTop] = data;
  ++ InstrIndex;
  break;

case OP_LIST:
  printf("Push list %li\n", (long)DataVal(Program[DataVal(data)]));
  // Push the head of referenced list to data stack
  ++ StackTop;
  DataStack[StackTop] = Program[DataVal(data)];
  ++ InstrIndex;
  break;

case OP_LISTHEAD:
  // Push list head to datastack
  ++ StackTop;
  DataStack[StackTop] = Program[DataVal(data)];
  // Jump to instruction after list end
  InstrIndex = DataVal(data) + 1;
  //printf("[GURU MEDITATION] UNEXPECTED OP_LISTHEAD\n");
  //exit(1);
  break;

// --------------------------------------------------------
// Return instructions
// --------------------------------------------------------

case OP_LISTEND:
case OP_RETURN: // return
  // Pop instruction index from return stack
  InstrIndex = CallStack[CallStackTop];
  -- CallStackTop;
  break;

// -------------------------------------------------------
// Call instructions
// -------------------------------------------------------

case OP_FUNCALL:
  // Save instruction index on return stack
  ++ CallStackTop;
  CallStack[CallStackTop] = InstrIndex + 1;
  // Set address of function list (skip list head)
  InstrIndex = DataVal(SymbolTable[DataVal(data)]) + 1;
  break;

case OP_TAILCALL: // tail
  // Get address of function (skip list head)
  InstrIndex = DataVal(SymbolTable[DataVal(data)]) + 1;
  break;

case OP_EVAL: // eval
  // Save instruction index on return stack
  ++ CallStackTop;
  CallStack[CallStackTop] = InstrIndex + 1;
  // Set instruction index to list
  InstrIndex = DataVal(DataStack[StackTop]) + 1;
  -- StackTop;
  break;

case OP_TAILEVAL: // taileval
  // Set instruction index to start of list
  InstrIndex = DataVal(DataStack[StackTop]) + 1;
  -- StackTop;
  break;

// -------------------------------------------------------
// Math functions
// -------------------------------------------------------

case OP_ADD: // +
  A = DataStack[StackTop];
  -- StackTop;
  B = DataStack[StackTop];
  DataStack[StackTop] = DataSetVal(B, DataVal(B) + DataVal(A));
  ++ InstrIndex;
  break;

case OP_SUB: // -
  A = DataStack[StackTop];
  -- StackTop;
  B = DataStack[StackTop];
  DataStack[StackTop] = DataSetVal(B, DataVal(B) - DataVal(A));
  ++ InstrIndex;
  break;

case OP_MULT: // *
  A = DataStack[StackTop];
  -- StackTop;
  B = DataStack[StackTop];
  DataStack[StackTop] = DataSetVal(B, DataVal(B) * DataVal(A));
  ++ InstrIndex;
  break;

case OP_DIV: // /
  A = DataStack[StackTop];
  -- StackTop;
  B = DataStack[StackTop];
  DataStack[StackTop] = DataSetVal(B, DataVal(B) / DataVal(A));
  ++ InstrIndex;
  break;

case OP_ADD1: // 1+
  A = DataStack[StackTop];
  DataStack[StackTop] = DataSetVal(A, DataVal(A) + 1);
  ++ InstrIndex;
  break;

case OP_SUB1: // 1-
  A = DataStack[StackTop];
  DataStack[StackTop] = DataSetVal(A, DataVal(A) - 1);
  ++ InstrIndex;
  break;

case OP_INCR: // incr
  // Increment global variable
  // Get symbol index (A)
  ++ InstrIndex;
  A = DataVal(Program[InstrIndex]);
  // Get current symbol data value (B)
  B = SymbolTable[A];
  // Set incremented value
  SymbolTable[A] = DataWith(DataVal(B) + 1, DataOp(B));
  ++ InstrIndex;
  break;

// --------------------------------------------------------
// Conditional operations (leaves top element on stack)
// --------------------------------------------------------

case OP_IFTRUE: // ifTrue
  // Move to list head item
  ++ InstrIndex;
  // Test value at top of stack
  if (DataVal(DataStack[StackTop]))
  {
    // Perform the True branch, move to item after list head
    ++ InstrIndex;
  }
  else
  {
    // Set instruction index to after end of list
    data = Program[InstrIndex];
    InstrIndex = DataVal(data) + 1;
  }
  break;

case OP_IFFALSE: // ifFalse
case OP_IFZERO: // ifZero
  // Move to list head item
  ++ InstrIndex;
  // Test value at top of stack
  if (!DataVal(DataStack[StackTop]))
  {
    // Perform the False/Zero branch, move to item after list head
    ++ InstrIndex;
  }
  else
  {
    // Set instruction index to after end of list
    data = Program[InstrIndex];
    InstrIndex = DataVal(data) + 1;
  }
  break;

// --------------------------------------------------------
// Stack operations
// --------------------------------------------------------

case OP_DUP: // dup
case OP_A: // A
  ++ StackTop;
  DataStack[StackTop] = DataStack[StackTop - 1];
  ++ InstrIndex;
  break;

case OP_B: // B
  ++ StackTop;
  DataStack[StackTop] = DataStack[StackTop - 2];
  ++ InstrIndex;
  break;

case OP_C: // C
  ++ StackTop;
  DataStack[StackTop] = DataStack[StackTop - 3];
  ++ InstrIndex;
  break;

case OP_SWAP: // swap
  A = DataStack[StackTop];
  DataStack[StackTop] = DataStack[StackTop - 1];
  DataStack[StackTop - 1] = A;
  ++ InstrIndex;
  break;

case OP_ROT: // rot
  A = DataStack[StackTop];
  B = DataStack[StackTop - 1];
  DataStack[StackTop] = DataStack[StackTop - 2];
  DataStack[StackTop - 2] = B;
  DataStack[StackTop - 1] = A;
  ++ InstrIndex;
  break;

case OP_DROP: // drop
  -- StackTop;
  ++ InstrIndex;
  break;

// --------------------------------------------------------
// Printing
// --------------------------------------------------------

case OP_PRINT: // print
  A = DataStack[StackTop];
  -- StackTop;
  printf("%li\n", (long)DataVal(A));
  ++ InstrIndex;
  break;

case OP_PRINTSTACK: // printstack
{
  printf("[");
  for (TypeIndex index = 0; index <= StackTop; ++ index)
  {
    if (index > 0) { printf(" "); }
    printf("%li", (long)DataVal(DataStack[index]));
  }
  printf("]\n");
  ++ InstrIndex;
  break;
}

// --------------------------------------------------------
// Exit interpreter loop
// --------------------------------------------------------

case OP_EXIT: // exit
  RunFlag = 0;
  break;

// --------------------------------------------------------
// Other native functions
// --------------------------------------------------------

case OP_PRIMFUN:
  //#include "prim.h"
  switch (DataVal(data))
  {
    default:
      printf("PrimFuns are not implemented\n");
      break;
  }
  break;