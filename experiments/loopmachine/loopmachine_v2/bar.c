//#include <stdio.h>

// cc -E bar.c

#define DEFOP(code, name) case code:
#define ENDOP() break;

// word1 word2 eq -> true if WordValue(word1) == WordValue(word2)
DEFOP(OP_EQ, "eq")
  A = WordValue(DataStack[DataStackTop]);
  -- DataStackTop;
  B = WordValue(DataStack[DataStackTop]);
  DataStack[DataStackTop] = WordSetValue(B, B == A);
  ++ InstrIndex;
ENDOP()

#undef DEFOP
#undef ENDOP

#define DEFOP(code, name) code:
#define ENDOP() NEXT();

// word1 word2 eq -> true if WordValue(word1) == WordValue(word2)
DEFOP(OP_EQ, "eq")
  A = WordValue(DataStack[DataStackTop]);
  -- DataStackTop;
  B = WordValue(DataStack[DataStackTop]);
  DataStack[DataStackTop] = WordSetValue(B, B == A);
  ++ InstrIndex;
ENDOP()


#define OpCodeBegin(opcode, name) opcode:

#define OpCodeEnd() continue;

OpCodeBegin(OP_FOO, "foo")
   ++i;
OpCodeEnd()

#define guard(n) asm("#" #n)

guard(1)
