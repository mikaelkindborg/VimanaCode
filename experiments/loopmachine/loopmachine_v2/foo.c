#include <stdio.h>
#include <stdlib.h>

// cc -E foo.c

typedef long TypeItem;
TypeItem* array;

typedef struct
{
  int a;
  int b;
} Foo;

int main()
{
  for (int i = 1; i < 100; ++ i)
  {
    for (int n = 1; n < 1000000; ++ n)
    {
      array = malloc(sizeof(TypeItem) * i);

      char* addr = (void*) array;

      array[0] = n;
      free(array);
    }
  }
  return 0;
}

/*
typedef struct FooBar
{
  int foo;
} FooBar;

FooBar MyArray[10];

void foo(int op)
{
Top:
  switch (op)
  {
    case 1:
      printf("One\n");
      break;
    case 2:
      printf("Two\n");
      break;
    case 3:
      printf("Tree\n");
      op = 4;
      goto Top;
      break;
    case 4:
      printf("Four\n");
      break;
  }
}

int main()
{
  foo(0);
  foo(1);
  foo(2);
  foo(3);

  return 0;
}
*/