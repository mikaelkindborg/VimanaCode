"Test of Copy-Eval interactive development"

[Welcome to the LoopMachine Copy/Eval/Print loop] print

[pbpaste] [{pbpaste} systemCommand] :
[pbcopy] [{pbcopy} systemCommandSendData] :
[$] [] : "Ignore the eval character when it is evaluated"
[quit] [handleTableFreeAll exit] :

[looper] [
  500 sleep
  pbpaste -> GlobalPasteBuffer
  GlobalPasteBuffer 0 stringAt 36 eq ifTrue [
    {FooBar} pbcopy "prevent evaluation of the same snippet in next loop"
    GlobalPasteBuffer parse eval
    printstack "print stack state"
  ]
  "Free allocated paste buffer"
  GlobalPasteBuffer handleFree
  tail looper
] :

"Here we go!"
looper

(
"Select code a snippet below and press CTRL+C to evaluate.
The first character must be $ for the code to be evaluated."

$ [Hi LoopMachine] print
$ 2021 2 * print
$ _ "drops top of stack"
$ [looper] [[Bye bye] print] :
$ printMemoryUse
$ exit
$ quit
$ [Welcome to the LoopMachine Copy/Eval/Print loop] print
$ {copyeval.vimana} machineRestartWithFile

"Alternative version with subfunctions"
$
[EvalChar] 36 :
[stringFirstChar] [0 stringAt] :
[isCodeSnippet] [stringFirstChar EvalChar eq] :
[preventEvalOnNextLoop]
  [{FooBar} pbcopy "prevent evaluation of the same snippet again"] :
[looper] [
  500 sleep
  pbpaste -> GlobalPasteBuffer
  GlobalPasteBuffer isCodeSnippet ifTrue [
    GlobalPasteBuffer parse eval
    GlobalPasteBuffer handleFree
    preventEvalOnNextLoop
    printstack
  ]
  tail looper
] :

"Test list allocation"
$ 3 listAlloc printProgMem
$ A 1000 0 listSetAt
$ A 2000 1 listSetAt
$ A 3000 2 listSetAt
$ A 2 listGetAt
$ _
$ printProgMem
$ 100 progMemSetFirstFree
)