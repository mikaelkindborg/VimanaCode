#include <stdio.h>
#include <stdlib.h>

// cc -E foo.c

/*
Test of index vs pointer for array access (follow the Next
field in a Lisp style list).

Pointer access:

miki@Mikaels-MacBook-Air loopmachine % cc lisplist.c -O0
miki@Mikaels-MacBook-Air loopmachine % time ./a.out
Counter: 1100000000
./a.out  2.94s user 0.00s system 97% cpu 3.009 total
miki@Mikaels-MacBook-Air loopmachine % cc lisplist.c -O2
miki@Mikaels-MacBook-Air loopmachine % time ./a.out
Counter: 1100000000
./a.out  0.42s user 0.00s system 87% cpu 0.480 total

Index access:

miki@Mikaels-MacBook-Air loopmachine % cc lisplist.c -O0
miki@Mikaels-MacBook-Air loopmachine % time ./a.out
Counter: 1100000000
./a.out  3.53s user 0.00s system 98% cpu 3.601 total
miki@Mikaels-MacBook-Air loopmachine % cc lisplist.c -O2
miki@Mikaels-MacBook-Air loopmachine % time ./a.out
Counter: 1100000000
./a.out  0.43s user 0.00s system 87% cpu 0.499 total

InstrIndex access (linear access):

miki@Mikaels-MacBook-Air loopmachine % cc lisplist.c -O0
miki@Mikaels-MacBook-Air loopmachine % time ./a.out
Counter: 1100000000
./a.out  2.30s user 0.00s system 97% cpu 2.372 total
miki@Mikaels-MacBook-Air loopmachine % cc lisplist.c -O2
miki@Mikaels-MacBook-Air loopmachine % time ./a.out
Counter: 1100000000
./a.out  0.42s user 0.00s system 87% cpu 0.484 total
miki@Mikaels-MacBook-Air loopmachine %

Conclusion:

Index access is as fast as pointer access for optimized code.
Linear access is also similar for optimized code.
*/

typedef struct
{
  long Data;
  long Next;
}
TypeItem;

TypeItem ProgMem[12];

//#define PointerTo(index) ((long)(& ProgMem[index]))
#define PointerTo(index) (index)

long Counter = 0;

int main()
{
  ProgMem[0].Next = PointerTo(2);
  ProgMem[1].Next = PointerTo(3);
  ProgMem[2].Next = PointerTo(1);
  ProgMem[3].Next = PointerTo(9);
  ProgMem[4].Next = PointerTo(5);
  ProgMem[5].Next = PointerTo(6);
  ProgMem[6].Next = PointerTo(8);
  ProgMem[7].Next = PointerTo(11);
  ProgMem[8].Next = PointerTo(7);
  ProgMem[9].Next = PointerTo(4);
  ProgMem[10].Next = PointerTo(1);
  ProgMem[11].Next = 0;
  //ProgMem[10].Next = 0; // For InstrIndex access

  for (int i = 0; i < 100000000; ++ i)
  {
    TypeItem item = ProgMem[0];
    //long InstrIndex = 0;
    while (1)
    {
      ++ Counter;
      if (0 == item.Next) { break; }
      //++ InstrIndex;
      //item = ProgMem[InstrIndex];
      item = ProgMem[item.Next];
      //item = *((TypeItem*)(item.Next));
    }
  }

  printf("Counter: %ld\n", Counter);

  return 0;
}
