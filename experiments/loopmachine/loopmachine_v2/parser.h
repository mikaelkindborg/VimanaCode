/*
File: parser.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

LoopMachine parser
*/

// --------------------------------------------------------------
// Parser
// --------------------------------------------------------------

int StreamLastChar;
TypeBool StreamUseLastChar = FALSE;

int StreamReadNextChar(FILE* stream)
{
  if (StreamUseLastChar)
    { StreamUseLastChar = FALSE; }
  else
    { StreamLastChar = fgetc(stream); }
  return StreamLastChar;
}

#define IsNotEOF(c)       (EOF != (c))
#define IsLeftParen(c)    (LEFTPAREN == (c))
#define IsRightParen(c)   (RIGHTPAREN == (c))
#define IsStringBegin(c)  (STRINGBEGIN == (c))
#define IsStringEnd(c)    (STRINGEND == (c))
#define IsCommentBegin(c) (COMMENTBEGIN == (c))
#define IsCommentEnd(c)   (COMMENTEND == (c))
#define IsQuoteComment(c) (COMMENTQUOTE == (c))
#define IsWhiteSpace(c) \
  ((' ' == (c)) || ('\t' == (c)) || ('\n' == (c)) || ('\r' == (c)))
#define IsSeparator(c) \
  (IsLeftParen(c) || IsRightParen(c) || IsStringBegin(c) || IsStringEnd(c))
#define IsWhiteSpaceOrSeparator(c) (IsWhiteSpace(c) || IsSeparator(c))

void SkipComment(FILE* stream)
{
  int level = 1; // Support for nested comments
  int c = StreamReadNextChar(stream);
  while (IsNotEOF(c))
  {
    if (IsCommentBegin(c)) { ++ level; }
    if (IsCommentEnd(c))   { -- level; }
    if (level < 1)         { break; }

    c = StreamReadNextChar(stream);
  }
}

void SkipQuoteComment(FILE* stream)
{
  int c = StreamReadNextChar(stream);
  while (!IsQuoteComment(c) && IsNotEOF(c))
  {
    c = StreamReadNextChar(stream);
  }
}

// Strings are allocated in dynamically using malloc
TypeWord ParseStringLiteral(FILE* stream)
{
  int level = 1;  // Support for nested strings
  int i = 0; // String index

  // Allocate string buffer
  TypeIndex handle = HandleTableAllocStringBuffer(0);

  // Read characters
  int c = StreamReadNextChar(stream);
  while (IsNotEOF(c))
  {
    if (IsStringBegin(c)) { ++ level; }
    if (IsStringEnd(c))   { -- level; }
    if (level < 1)        { break; }

    // Write char to string buffer
    StringBufferEnsureSize(handle, i);
    StringBufferSetAt(handle, i, c);
    ++ i;

    // Read next char
    c = StreamReadNextChar(stream);
  }

  // Terminate string
  StringBufferEnsureSize(handle, i);
  StringBufferSetAt(handle, i, 0);

  // Return handle to string buffer
  return WordWith(handle, OP_STRING);
}

char* ParseReadToken(int c, FILE* stream)
{
  // static local variable = hidden global
  static char Token[MAX_TOKEN_LENGTH];

  // Write chars to token buffer
  int i = 0;

  // Read token
  while (IsNotEOF(c) && !IsWhiteSpaceOrSeparator(c))
  {
    // Write char to token buffer
    // Max token length includes the zero-terminator
    if (i < MAX_TOKEN_LENGTH - 1)
    {
      Token[i] = c;
      ++ i;
    }

    // Read next
    c = StreamReadNextChar(stream);
  }

  // Set flag saying that we consumed the last character
  StreamUseLastChar = TRUE;

  // Zero terminate token string
  Token[i] = 0;

  return Token;
}

// Get the token type
// Global symbols begin with an uppercase letter
// Functions begin with anything that is not an uppercase letter
int ParseType(char* token)
{
  if (StringIsInteger(token))
    { return OP_INTEGER; }
  else
  if (isupper(token[0]))
    { return OP_SYMBOL; }
  else
    { return OP_FUNCTION; }
}

TypeWord ParseInteger(char* token)
{
  return WordWith(
    (TypeWord) strtol(token, NULL, 10),
    OP_INTEGER);
}

TypeWord ParseSymbol(char* token, int optype)
{
  TypeWord word = 0;

  // Look up the name in the symbol table
  TypeIndex index = SymbolTableLookupIndexForName(token);
  if (-1 == index)
  {
    // Add new symbol
    index = SymbolTableAdd(token);
  }

  if (OP_FUNCTION == optype)
  {
    word = WordWith(index, OP_FUNCTION);
  }
  else
  if (OP_SYMBOL == optype)
  {
    word = WordWith(index, OP_SYMBOL);
  }

  return word;
}

TypeWord ParseToken(char c, FILE* stream)
{
  TypeWord word;

  char* token = ParseReadToken(c, stream);

  int optype = ParseType(token);

  if (OP_INTEGER == optype)
  {
    word = ParseInteger(token);
  }
  else
  {
    // Is it an opcode?
    int opcode = OpTableLookupCodeForName(token);
    if (opcode > -1)
    {
      if (opcode < OP_OPCALL)
      {
        // Put the opcode in the opcode field (core opcodes)
        word = WordWith(0, opcode);
      }
      else
      {
        // Put the opcode in the value field (additional opcodes)
        // (dispatches two times, which is slower, but saves opcode bits)
        word = WordWith(opcode, OP_OPCALL);
      }
    }
    else
    {
      // Otherwise it is a symbol
      word = ParseSymbol(token, optype);
    }
  }

  return word;
}

// Parse queue
TypeWord  ParseQueue[PARSE_QUEUE_MAXSIZE];
TypeIndex ParseQueueEnd = -1;

void ParseQueueAddWord(TypeWord word)
{
  ++ ParseQueueEnd;
  if (ParseQueueEnd >= PARSE_QUEUE_MAXSIZE)
    { GURU_MEDITATION("ParseQueue overflow"); }
  ParseQueue[ParseQueueEnd] = word;
}

// Return list word (OP_LIST)
TypeWord ParseList(FILE* stream)
{
  TypeIndex parseQueueStart = ParseQueueEnd;
  TypeIndex listLength = 0;
  TypeWord  word;

  int c = StreamReadNextChar(stream);

  while (IsNotEOF(c))
  {
    if (IsWhiteSpace(c))
    {
      // Skip whitespace
    }
    else
    if (IsQuoteComment(c))
    {
      SkipQuoteComment(stream);
    }
    else
    if (IsCommentBegin(c))
    {
      SkipComment(stream);
    }
    else
    if (IsStringBegin(c))
    {
      word = ParseStringLiteral(stream);
      ParseQueueAddWord(word);
      ++ listLength;
    }
    else
    if (IsLeftParen(c)) // List begin
    {
      // Parse child list
      TypeIndex parseQueueIndex = ParseQueueEnd;
      word = ParseList(stream);
      ParseQueueEnd = parseQueueIndex;
      ParseQueueAddWord(word);
      ++ listLength;
    }
    else
    if (IsRightParen(c)) // List end
    {
      // Exit parse loop
      break;
    }
    else
    // Otherwise it is a symbol or an integer
    {
      word = ParseToken(c, stream);
      ParseQueueAddWord(word);
      ++ listLength;
    }

    c = StreamReadNextChar(stream);
  }
  // while

  // Write list begin and set list length
  word = WordWith(listLength, OP_LISTBEGIN);
  TypeIndex startIndex = ProgMemWrite(word);

  // Write words in queue to ProgMem
  for (int i = 0; i < listLength; ++ i)
  {
    ProgMemWrite(ParseQueue[i + parseQueueStart + 1]);
  }

  // Restore ParseQueueEnd
  ParseQueueEnd = parseQueueStart;

  // Write list end
  word = WordWith(0, OP_LISTEND);
  ProgMemWrite(word);

  // Return word with address to list head in ProgMem
  return WordWith(startIndex, OP_LIST);
}

TypeWord ParseStream(FILE* stream)
{
  TypeWord list = ParseList(stream);
  return list;
}

TypeWord ParseFile(char* fileName)
{
  FILE* stream = FileOpen(fileName, "r");
  if (NULL == stream)
  {
    return WordWith(0, OP_UNDEFINED);
  }
  else
  {
    TypeWord list = ParseStream(stream);
    FileClose(stream);
    return list;
  }
}

TypeWord ParseString(char* program)
{
  FILE* stream = FileOpenStringStream(program);
  TypeWord list = ParseStream(stream);
  FileClose(stream);
  return list;
}
