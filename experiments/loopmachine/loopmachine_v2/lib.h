/*
File: lib.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Library functions
*/

// --------------------------------------------------------------
// Guru Meditation
// --------------------------------------------------------------

#define GURU_MEDITATION(message) \
  printf("[GURU_MEDITATION] %s (%s: %i)\n", message, __FILE__, __LINE__); \
  exit(1);

// --------------------------------------------------------------
// String functions
// --------------------------------------------------------------

#define StrEquals(s1, s2) (0 == strcmp((s1), (s2)))

TypeBool StringIsInteger(char* string)
{
  TypeBool isNumber = FALSE;
  int      index = 0;

  // Check if the first character a minus sign
  if ('-' == string[index])
    { ++ index; }

  // Rest of string must have digits only
  while (0 != string[index])
  {
    isNumber = TRUE;
    if (!isdigit(string[index]))
      { isNumber = FALSE; break; }
    ++ index;
  }

  return isNumber;
}

// --------------------------------------------------------------
// Program memory functions
// --------------------------------------------------------------

TypeIndex ProgMemWrite(TypeWord word)
{
  if (ProgMemFirstFree >= PROGMEM_MAXSIZE)
    { GURU_MEDITATION("Out of program memory"); }

  ProgMem[ProgMemFirstFree] = word;

  return ProgMemFirstFree ++;
}

// --------------------------------------------------------------
// String memory functions
// --------------------------------------------------------------

void StringMemWriteChar(char c)
{
  // Make sure there is room for the zero terminator
  if ((STRINGMEM_MAXSIZE - StringMemFirstFree) < 2)
  {
    DebugPrintInt("STRINGMEM OVERFLOW StringMemFirstFree", StringMemFirstFree);
    DebugPrintStringMem("STRINGMEM");
    GURU_MEDITATION("Out of string memory (write char)");
  }

  // Write char
  StringMem[StringMemFirstFree] = c;
  ++ StringMemFirstFree;
}

void StringMemWriteEnd()
{
  // Zero terminate string
  StringMem[StringMemFirstFree] = 0;
  ++ StringMemFirstFree;
}

// Return pointer to the current string
// Must be called before StringMemKeep()
char* StringMemGetPointer()
{
  return & StringMem[StringMemStringStart];
}

void StringMemKeep()
{
  StringMemStringStart = StringMemFirstFree;;
}

void StringMemReset()
{
  StringMemFirstFree = StringMemStringStart;
}

// Write string to StringMem.
// To keep the string call StringMemKeep()
// To release the string call StringMemReset()
void StringMemWrite(char* string)
{
  int length = strlen(string);

  if ((STRINGMEM_MAXSIZE - StringMemFirstFree) < length)
    { GURU_MEDITATION("Out of string memory (write string)"); }

  // Write string
  for (int i = 0; i < length; ++ i)
  {
    StringMemWriteChar(string[i]);
  }

  // Zero-terminate string
  StringMemWriteEnd();
}

// --------------------------------------------------------------
// Op table functions
// --------------------------------------------------------------

TypeIndex OpTableLookupCodeForName(char* name)
{
  // Begin search at OP_EXIT
  for (int index = OP_EXIT; index < OP_LAST_ENTRY; ++ index)
  {
    if (StrEquals(name, OpNameTable[index]))
      { return index; }
  }

  return -1; // Not found
}

char* OpTableLookupNameForCode(int index)
{
  return (index < OP_LAST_ENTRY) ? OpNameTable[index] : NULL;
}

// --------------------------------------------------------------
// Symbol table functions
// --------------------------------------------------------------

TypeIndex SymbolTableLookupIndexForName(char* name)
{
  for (int index = 0; index < SymbolTableFirstFree; ++ index)
  {
    if (StrEquals(name, SymbolNameTable[index]))
      { return index; }
  }

  return -1; // Not found
}

char* SymbolTableLookupNameForIndex(int index)
{
  return SymbolNameTable[index];
}

// Add name and return index of new entry
TypeIndex SymbolTableAdd(char* name)
{
  if (SymbolTableFirstFree >= SYMBOLTABLE_MAXSIZE)
    { GURU_MEDITATION("Out of symbol memory"); }

  // Store string for symbol
  StringMemWrite(name);
  SymbolNameTable[SymbolTableFirstFree] = StringMemGetPointer();
  StringMemKeep();

  // Set default value
  SymbolTable[SymbolTableFirstFree] = WordWith(0, OP_UNDEFINED);

  // Return index of symbol
  return SymbolTableFirstFree ++;
}

// --------------------------------------------------------------
// Handle table functions
// --------------------------------------------------------------

// Set up linked freelist
void HandleTableInit()
{
  TypeIndex i;
  for (i = 0; i < HANDLETABLE_MAXSIZE - 1; ++ i)
  {
    HandleTable[i].Next = i + 1;
  }
  HandleTable[i].Next = HANDLE_END; // End of freelist
  HandleTableFirstFree = 0;
}

// Allocate handle that points to a memory buffer
TypeIndex HandleTableAlloc(void* bufferPointer, TypeIndex bufferSize, TypeIndex bufferType)
{
  // Intialize handle table if needed
  if (HANDLE_UNDEFINED == HandleTableFirstFree)
    { HandleTableInit(); }

  // Use first handle in freelist
  TypeIndex index = HandleTableFirstFree;
  if (HANDLE_END == index)
    { GURU_MEDITATION("Out of handle memory (HandleTableAlloc)"); }
  HandleTableFirstFree = HandleTable[index].Next;
  HandleTable[index].Next = HANDLE_END;
  HandleTable[index].Pointer = bufferPointer;
  HandleTable[index].Size = bufferSize;
  HandleTable[index].Type = bufferType;

  return index;
}

void HandleTableFreeHandleOnly(TypeIndex index)
{
  HandleTable[index].Next = HandleTableFirstFree;
  HandleTable[index].Type = HANDLE_UNDEFINED;
  HandleTableFirstFree = index;
}

// Free the handle and the referenced buffer
// (HANDLE_TYPE_CUSTOM is not deallocated here)
void HandleTableFreeHandle(TypeIndex index)
{
  MemFree(HandleTable[index].Pointer);
  HandleTableFreeHandleOnly(index);
}

// Free all handles and dealloc buffers
// (HANDLE_TYPE_CUSTOM is not deallocated here)
void HandleTableFreeAll()
{
  for (TypeIndex i = 0; i < HANDLETABLE_MAXSIZE - 1; ++ i)
  {
    if ((HANDLE_TYPE_STRING == HandleTable[i].Type) ||
        (HANDLE_TYPE_BUFFER == HandleTable[i].Type))
    {
      MemFree(HandleTable[i].Pointer);
    }
  }

  // Trigger initialization in HandleTableAllocHandle()
  HandleTableFirstFree = HANDLE_UNDEFINED;
}

// --------------------------------------------------------------
// String buffer functions
// --------------------------------------------------------------

TypeIndex HandleTableAllocStringBuffer(TypeIndex size)
{
  // Alloc string buffer
  if (0 == size)
    { size = BUFFER_CHUNK_SIZE; }
  char* stringBuffer = MemAlloc(size);
  if (NULL == stringBuffer)
    { GURU_MEDITATION("Out of memory (HandleTableAllocStringBuffer)"); }

  // Alloc handle
  TypeIndex handle = HandleTableAlloc(stringBuffer, size, HANDLE_TYPE_STRING);

  return handle;
}

char StringBufferGetAt(TypeIndex handle, TypeIndex index)
{
  TypeIndex size = HandleTable[handle].Size;
  if ((index + 1) > size)
    { GURU_MEDITATION("Index is out bounds (StringBufferGetAt)"); }

  return ((char*)(HandleTable[handle].Pointer))[index];
}

void StringBufferSetAt(TypeIndex handle, TypeIndex index, char c)
{
  TypeIndex size = HandleTable[handle].Size;
  if ((index + 1) > size)
    { GURU_MEDITATION("Index is out bounds (StringBufferSetAt)"); }

  ((char*)(HandleTable[handle].Pointer))[index] = c;
}

void StringBufferEnsureSize(TypeIndex handle, TypeIndex index)
{
  TypeIndex size = HandleTable[handle].Size;
  if (index + 1 > size)
  {
    size += index + BUFFER_CHUNK_SIZE;
    void* stringBuffer = MemRealloc(HandleTable[handle].Pointer, size);
    if (NULL == stringBuffer)
      { GURU_MEDITATION("Out of memory (StringBufferEnsureSize)"); }

    HandleTable[handle].Size = size;
    HandleTable[handle].Pointer = stringBuffer;
  }
}
