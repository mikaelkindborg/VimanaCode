<?php

$NumLines = 0;
$NumLinesCode = 0;
$NumLinesComments = 0;
$NumLinesBlank = 0;
$NumLinesOpenCurly = 0;

/*
// Unused line count function (count all lines in a file)
function CountLines($file)
{
  global $NumLines;
  $NumLines += count(file($file));
}
*/

function StringContains($string, $substring)
{
  return false !== strpos($string, $substring);
}

function CountLines($file)
{
  global $NumLines;
  global $NumLinesCode;
  global $NumLinesComment;
  global $NumLinesBlank;
  global $NumLinesOpenCurly;

  $insideMultiLineComment = false;

  foreach (file($file) as $line):
    ++ $NumLines;
    // if inside multi-line comment
    if ($insideMultiLineComment):
      ++ $NumLinesComment;
      if (StringContains($line, "*/")):
        $insideMultiLineComment = false;
      endif;
    elseif (StringContains($line, "/*")):
      ++ $NumLinesComment;
      $insideMultiLineComment = true;
    elseif (preg_match('/^\s*\/\/./', $line)):
      ++ $NumLinesComment;
    elseif (0 == strlen(trim($line))):
      ++ $NumLinesBlank;
    //elseif (preg_match('/^\s*\{/', $line)):
    elseif ("{" === trim($line)):
      ++ $NumLinesOpenCurly;
    else:
      ++ $NumLinesCode;
    endif;
  endforeach;
}

// Base version files (exclude generated files)
$files = [
  "loopmachine.h",
  "lib.h",
  "opcodes.h",
  //"optable.h",
  //"opjumps.h",
  "parser.h",
  "stringify.h",
  "unix/system.h",
];

foreach ($files as $file):
  CountLines($file);
endforeach;

echo "NumLinesTotal:     " . ($NumLines) . "\n";
echo "NumLinesCode:      " . ($NumLinesCode) . "\n";
echo "NumLinesOpenCurly: " . ($NumLinesOpenCurly) . "\n";
echo "NumLinesComment:   " . ($NumLinesComment) . "\n";
echo "NumLinesBlank:     " . ($NumLinesBlank) . "\n";

/*
Code size 2024-04-22:
NumLinesTotal:     1254
NumLinesCode:      734
NumLinesOpenCurly: 85
NumLinesComment:   269
NumLinesBlank:     166

Code size 2024-04-27:
NumLinesTotal:     1291
NumLinesCode:      705
NumLinesOpenCurly: 71
NumLinesComment:   326
NumLinesBlank:     189

Code size 2024-04-28:
NumLinesTotal:     1504
NumLinesCode:      868
NumLinesOpenCurly: 83
NumLinesComment:   345
NumLinesBlank:     208

Code size 2024-04-28 without generated files:
NumLinesTotal:     1360
NumLinesCode:      732
NumLinesOpenCurly: 80
NumLinesComment:   343
NumLinesBlank:     205

Code size 2024-04-29:
NumLinesTotal:     1502
NumLinesCode:      867
NumLinesOpenCurly: 83
NumLinesComment:   344
NumLinesBlank:     208

Code size 2024-05-03 (buffers, strings, handle table):
NumLinesTotal:     1669
NumLinesCode:      972
NumLinesOpenCurly: 93
NumLinesComment:   376
NumLinesBlank:     228

Code size 2024-05-03 (with system.h, without generated files):
NumLinesTotal:     1900
NumLinesCode:      1097
NumLinesOpenCurly: 124
NumLinesComment:   422
NumLinesBlank:     257

Code size 2024-05-07 (new opcodes, a bit sad that size grows)
NumLinesTotal:     2025
NumLinesCode:      1121
NumLinesOpenCurly: 142
NumLinesComment:   469
NumLinesBlank:     293
*/
