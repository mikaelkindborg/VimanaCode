/*
File: loopmachine.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Loop Machine Interpreter
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// --------------------------------------------------------------
// Interpreter data types
// --------------------------------------------------------------

#include "optable.h"

typedef long TypeData;
typedef int  TypeIndex;
typedef int  TypeBool;

#define TRUE  1
#define FALSE 0

// --------------------------------------------------------------
// Program memory areas
// --------------------------------------------------------------

#define PROGMEM_MAXSIZE       100
#define DATSTACK_MAXSIZE      50
#define CALLTACK_MAXSIZE      50
#define SYMBOLTABLE_MAXSIZE   20
#define PARSER_LIST_MAX_DEPTH 20   // Max nesting depth for lists
#define STRINGMEM_MAXSIZE     500  // Max number of chars in string memory
#define MAX_TOKEN_LENGTH      32   // Max length for symbol names is
                                   // 31 chars + 1 zero-terminator char
#define TAG_BITS              8
#define TAG_MASK              0xFF

TypeIndex ProgMemFirstFree = 1; // First free index must not be zero
TypeData  ProgMem[PROGMEM_MAXSIZE]; // Program memory

// Data stack and call stack
TypeData  DataStack[DATSTACK_MAXSIZE]; // Data stack
TypeIndex CallStack[CALLTACK_MAXSIZE]; // Return address/index stack

// Global symbols
TypeIndex SymbolTableFirstFree = 0; // First free index
TypeData  SymbolTable[SYMBOLTABLE_MAXSIZE];
char*     SymbolNameTable[SYMBOLTABLE_MAXSIZE];

// String memory
TypeIndex StringMemFirstFree = 0; // First free index
TypeIndex StringMemStringStart = 0; // Current string start index
char      StringMem[STRINGMEM_MAXSIZE];

#define DataValue(data)           ((data) >> TAG_BITS)
#define DataOp(data)              ((data) & TAG_MASK)
#define DataWith(value, op)       ((((TypeData)(value)) << TAG_BITS) | (op))
#define DataSetValue(data, value) ((((TypeData)(value)) << TAG_BITS) | ((data) & TAG_MASK))
#define GlobalValue(data)         (SymbolTable[DataValue(data)])

// --------------------------------------------------------------
// Debug functions
// --------------------------------------------------------------

#include "debug.h"

// --------------------------------------------------------------
// Library functions
// --------------------------------------------------------------

#include "lib.h"

// --------------------------------------------------------------
// Parser
// --------------------------------------------------------------

#include "parser.h"

// --------------------------------------------------------------
// Stringify
// --------------------------------------------------------------

#include "stringify.h"

// --------------------------------------------------------------
// Interpreter loop
// --------------------------------------------------------------

void ProgRun(TypeData list) //TypeData ProgMem[],
{
  // Persistent variables begin with UpperCase letter
  TypeBool  RunFlag = TRUE;
  TypeIndex StackTop = -1;
  TypeIndex CallStackTop = -1;
  TypeIndex InstrIndex = DataValue(list) + 1;

  // Temporary variables
  TypeData  data;  // current instruction data
  TypeData  op;    // current operation
  TypeData  A;     // top of data stack or other value
  TypeData  B;     // second in data stack or other value

  while (RunFlag)
  {
    data = ProgMem[InstrIndex];
    op = DataOp(data);

    //DebugPrintData("MainLoop", data);
    //DebugPrintInt("InstrIndex", InstrIndex);

    switch (op)
    {
      #include "opcodes.h"
    }
  }

  DebugPrint("End of Program");
  DebugPrintInt("Data Stack Size", StackTop);
}
