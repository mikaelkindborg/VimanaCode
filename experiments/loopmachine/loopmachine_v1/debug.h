/*
File: debug.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Debug functions
*/

// --------------------------------------------------------------
// Debug functions
// --------------------------------------------------------------

void DebugPrintData(char* label, TypeData data)
{
  printf("[%s] Op: %s value: %i\n",
    label, OpNameTable[DataOp(data)], (int)DataValue(data));
}

void DebugPrintList(char* label, TypeData list)
{
  char labelBuf[8];
  printf("[%s] List Data:\n", label);
  DebugPrintData("List", list);

  TypeIndex index = DataValue(list);
  TypeData data = ProgMem[index];
  TypeIndex endIndex = DataValue(data);

  for (int i = index; i <= endIndex; ++ i)
  {
    data = ProgMem[i];
    sprintf(labelBuf, "%i", (int) i);
    DebugPrintData(labelBuf, data);
  }
}

void DebugPrintProgMem(char* label)
{
  char labelBuf[8];

  printf("[%s] Program Memory:\n", label);
  int i = 0;
  while (i < ProgMemFirstFree)
  {
    sprintf(labelBuf, "%i", (int) i);
    DebugPrintData(labelBuf, ProgMem[i]);
    ++ i;
  }
}

void DebugPrintSymbolTable(char* label)
{
  char labelBuf[MAX_TOKEN_LENGTH + 8];

  printf("[%s] Symbol Table:\n", label);
  int i = 0;
  while (i < SymbolTableFirstFree)
  {
    sprintf(labelBuf, "%i %s", (int) i, SymbolNameTable[i]);
    DebugPrintData(labelBuf, SymbolTable[i]);
    ++ i;
  }
}

void DebugPrintStringMem(char* label)
{
  printf("[%s] String memory:\n", label);
  int i = 0;
  while (i < StringMemFirstFree)
  {
    if (0 == StringMem[i])
    {
      printf("\n");
    }
    else
    {
      printf("%c", StringMem[i]);
    }
    ++ i;
  }
}

#define DebugPrint(string) printf("[%s]\n", string)

#define DebugPrintInt(label, i) printf("[%s] %i\n", label, (int)i)

#define DebugPrintNewLine() printf("\n")
