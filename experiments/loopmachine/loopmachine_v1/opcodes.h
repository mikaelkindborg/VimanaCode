// OP Codes

// --------------------------------------------------------
// Undefined
// --------------------------------------------------------

case OP_UNDEFINED:
  DebugPrintSymbolTable("OP_UNDEFINED");
  GURU_MEDITATION("Global name is undefined");
  break;

// --------------------------------------------------------
// Set global variables
// --------------------------------------------------------

// def globalname data ->
case OP_DEF: // def
case OP_DEFCHAR: // :
  // Move to next instruction (the symbol reference)
  ++ InstrIndex;
  A = ProgMem[InstrIndex];
  // Move to next instruction (the data object)
  ++ InstrIndex;
  B = ProgMem[InstrIndex];
  // If it is a list head, set OP_LIST referece
  if (OP_LISTHEAD == DataOp(B))
  {
    // Set data to the end index in list head
    data = B;
    // Set OP_LIST object
    B = DataWith(InstrIndex, OP_LIST);
    // Move instruction index to list end
    InstrIndex = DataValue(data);
  }
  // Set data for symbol
  GlobalValue(A) = B;
  // Move to next instruction
  ++ InstrIndex;
  break;

// data set globalname ->
case OP_SET: // set
case OP_SETARROW: // ->
  // Get value on data stack
  A = DataStack[StackTop];
  -- StackTop;
  // Get symbol index
  ++ InstrIndex;
  data = ProgMem[InstrIndex];
  // Set symbol value to stack value
  GlobalValue(data) = A;
  ++ InstrIndex;
  break;

// --------------------------------------------------------
// Get global variable
// --------------------------------------------------------

case OP_GLOBAL:
  // Push value of global variable to data stack
  ++ StackTop;
  DataStack[StackTop] = GlobalValue(data);
  ++ InstrIndex;
  break;

// -------------------------------------------------------
// Call instructions
// -------------------------------------------------------

case OP_FUNCALL:
  // ??? TODO Check tailcall if next is zero - dont put zero on the callstack
  // Save instruction index on return stack
  ++ CallStackTop;
  CallStack[CallStackTop] = InstrIndex + 1;
  // Set address of function list (skip list head)
  InstrIndex = DataValue(GlobalValue(data)) + 1;
  break;

case OP_TAILCALL: // tail
  // Get address of function (skip list head)
  ++ InstrIndex;
  data = ProgMem[InstrIndex];
  InstrIndex = DataValue(GlobalValue(data)) + 1;
  break;

case OP_EVAL: // eval
  // Save instruction index on return stack
  ++ CallStackTop;
  CallStack[CallStackTop] = InstrIndex + 1;
  // Set instruction index to list
  InstrIndex = DataValue(DataStack[StackTop]) + 1;
  -- StackTop;
  break;

case OP_TAILEVAL: // taileval
  // Set instruction index to start of list
  InstrIndex = DataValue(DataStack[StackTop]) + 1;
  -- StackTop;
  break;

// --------------------------------------------------------
// Return instructions
// --------------------------------------------------------

case OP_LISTEND:
case OP_RETURN: // return
  // Pop instruction index from return stack
  InstrIndex = CallStack[CallStackTop];
  -- CallStackTop;
  break;

// --------------------------------------------------------
// Exit interpreter loop
// --------------------------------------------------------

case OP_EXIT: // exit
case OP_PROGRAMEND:
  RunFlag = 0;
  break;

// --------------------------------------------------------
// Conditional functions
// --------------------------------------------------------

// bool ifTrueTail list ->
case OP_IFTRUETAIL: // ifTrueTail
  // Move to list head
  ++ InstrIndex;

  // Test value at top of stack
  if (DataValue(DataStack[StackTop]))
  {
    // Perform the True branch, move to element after list head
    ++ InstrIndex;
    -- StackTop;
  }
  else
  {
    // Set instruction index to after end of list
    data = ProgMem[InstrIndex];
    InstrIndex = DataValue(data) + 1;
    -- StackTop;
  }
  break;

// bool ifFalseTail list ->
case OP_IFFALSETAIL: // ifFalseTail
case OP_IFZEROTAIL: // ifZeroTail
  // Move to list head
  ++ InstrIndex;
  // Test value at top of stack
  if (! DataValue(DataStack[StackTop]))
  {
    // Perform the False/Zero branch, move to element after list head
    ++ InstrIndex;
    -- StackTop;
  }
  else
  {
    // Set instruction index to after end of list
    data = ProgMem[InstrIndex];
    InstrIndex = DataValue(data) + 1;
    -- StackTop;
  }
  break;

case OP_IFLESSTAIL: // ifLessTail
  // Move to list head
  ++ InstrIndex;
  // Test values at top of stack
  if (DataValue(DataStack[StackTop - 1]) > DataValue(DataStack[StackTop]))
  {
    // Perform the branch, move to element after list head
    ++ InstrIndex;
    -- StackTop;
    -- StackTop;
  }
  else
  {
    // Set instruction index to after end of list
    data = ProgMem[InstrIndex];
    InstrIndex = DataValue(data) + 1;
    -- StackTop;
    -- StackTop;
  }
  break;

case OP_IFBIGGERTAIL: // ifBiggerTail
  // Move to list head
  ++ InstrIndex;
  // Test values at top of stack
  if (DataValue(DataStack[StackTop - 1]) < DataValue(DataStack[StackTop]))
  {
    // Perform the branch, move to element after list head
    ++ InstrIndex;
    -- StackTop;
    -- StackTop;
  }
  else
  {
    // Set instruction index to after end of list
    data = ProgMem[InstrIndex];
    InstrIndex = DataValue(data) + 1;
    -- StackTop;
    -- StackTop;
  }
  break;

case OP_IFGTZEROTAIL: // ifGreaterThanZeroTail
  // Move to list head
  ++ InstrIndex;
  // Test value at top of stack
  if (DataValue(DataStack[StackTop]) > 0)
  {
    // Perform the branch, move to element after list head
    ++ InstrIndex;
    -- StackTop;
  }
  else
  {
    // Set instruction index to after end of list
    data = ProgMem[InstrIndex];
    InstrIndex = DataValue(data) + 1;
    -- StackTop;
  }
  break;

case OP_IFGT1TAIL: // ifGreaterThanOneTail
  // Move to list head
  ++ InstrIndex;
  // Test value at top of stack
  if (DataValue(DataStack[StackTop]) > 1)
  {
    // Perform the branch, move to element after list head
    ++ InstrIndex;
    -- StackTop;
  }
  else
  {
    // Set instruction index to after end of list
    data = ProgMem[InstrIndex];
    InstrIndex = DataValue(data) + 1;
    -- StackTop;
  }
  break;

// --------------------------------------------------------
// Push values - data stack contains full words (value + op)
// --------------------------------------------------------

case OP_INTEGER:
  // Push number to data stack
  ++ StackTop;
  DataStack[StackTop] = data;
  ++ InstrIndex;
  break;

case OP_LIST:
  // Push list to datastack
  ++ StackTop;
  DataStack[StackTop] = data;
  // Jump to next instruction
  ++ InstrIndex;
  break;

case OP_LISTHEAD:
  // Push OP_LIST to datastack
  ++ StackTop;
  DataStack[StackTop] = DataWith(InstrIndex, OP_LIST);
  // Jump to instruction after list end
  InstrIndex = DataValue(data) + 1;
  break;

// -------------------------------------------------------
// Math functions
// -------------------------------------------------------

case OP_ADD: // +
  A = DataStack[StackTop];
  -- StackTop;
  B = DataStack[StackTop];
  DataStack[StackTop] = DataSetValue(B, DataValue(B) + DataValue(A));
  ++ InstrIndex;
  break;

case OP_SUB: // -
  A = DataStack[StackTop];
  -- StackTop;
  B = DataStack[StackTop];
  DataStack[StackTop] = DataSetValue(B, DataValue(B) - DataValue(A));
  ++ InstrIndex;
  break;

case OP_MULT: // *
  A = DataStack[StackTop];
  -- StackTop;
  B = DataStack[StackTop];
  DataStack[StackTop] = DataSetValue(B, DataValue(B) * DataValue(A));
  ++ InstrIndex;
  break;

case OP_DIV: // /
  A = DataStack[StackTop];
  -- StackTop;
  B = DataStack[StackTop];
  DataStack[StackTop] = DataSetValue(B, DataValue(B) / DataValue(A));
  ++ InstrIndex;
  break;

case OP_ADD1: // 1+
  A = DataStack[StackTop];
  DataStack[StackTop] = DataSetValue(A, DataValue(A) + 1);
  ++ InstrIndex;
  break;

case OP_SUB1: // 1-
  A = DataStack[StackTop];
  DataStack[StackTop] = DataSetValue(A, DataValue(A) - 1);
  ++ InstrIndex;
  break;

case OP_SUB2: // 2-
  A = DataStack[StackTop];
  DataStack[StackTop] = DataSetValue(A, DataValue(A) - 2);
  ++ InstrIndex;
  break;

// Increment global variable
case OP_INCR: // incr
  // Get symbol index (A)
  ++ InstrIndex;
  A = DataValue(ProgMem[InstrIndex]);
  // Get current symbol data value (B)
  B = SymbolTable[A];
  // Set incremented value
  SymbolTable[A] = DataWith(DataValue(B) + 1, DataOp(B));
  ++ InstrIndex;
  break;

case OP_ISLESS: // isLess
  A = DataValue(DataStack[StackTop]);
  -- StackTop;
  B = DataValue(DataStack[StackTop]);
  DataStack[StackTop] = DataSetValue(B, B > A);
  ++ InstrIndex;
  break;

case OP_ISBIGGER: // isBigger
  A = DataValue(DataStack[StackTop]);
  -- StackTop;
  B = DataValue(DataStack[StackTop]);
  DataStack[StackTop] = DataSetValue(B, B < A);
  ++ InstrIndex;
  break;

case OP_ISGT1: // isGreaterThanOne
  A = DataValue(DataStack[StackTop]);
  //++ StackTop; do not keep top of stack
  DataStack[StackTop] = DataSetValue(B, A > 1);
  ++ InstrIndex;
  break;

// --------------------------------------------------------
// Stack operations
// --------------------------------------------------------

// dup
case OP_A: // A
  ++ StackTop;
  DataStack[StackTop] = DataStack[StackTop - 1];
  ++ InstrIndex;
  break;

// over
case OP_B: // B
  ++ StackTop;
  DataStack[StackTop] = DataStack[StackTop - 2];
  ++ InstrIndex;
  break;

case OP_C: // C
  ++ StackTop;
  DataStack[StackTop] = DataStack[StackTop - 3];
  ++ InstrIndex;
  break;

case OP_SWAP: // swap
  A = DataStack[StackTop];
  DataStack[StackTop] = DataStack[StackTop - 1];
  DataStack[StackTop - 1] = A;
  ++ InstrIndex;
  break;

case OP_ROT: // rot
  A = DataStack[StackTop];
  B = DataStack[StackTop - 1];
  DataStack[StackTop] = DataStack[StackTop - 2];
  DataStack[StackTop - 2] = B;
  DataStack[StackTop - 1] = A;
  ++ InstrIndex;
  break;

case OP_DROP_: // _
  -- StackTop;
  ++ InstrIndex;
  break;

// --------------------------------------------------------
// Printing
// --------------------------------------------------------

case OP_PRINT: // print
  // TODO: Print stringified
  A = DataStack[StackTop];
  -- StackTop;
  printf("%li\n", (long)DataValue(A));
  ++ InstrIndex;
  break;

case OP_PRINTSTACK: // printstack
{
  // TODO: Print stringified
  printf("[");
  for (TypeIndex index = 0; index <= StackTop; ++ index)
  {
    if (index > 0) { printf(" "); }
    printf("%li", (long)DataValue(DataStack[index]));
  }
  printf("]\n");
  ++ InstrIndex;
  break;
}

/*
// --------------------------------------------------------
// Other native functions
// --------------------------------------------------------

// Currently unused - all primfuns are opcodes for now.

// Must be last of the opcodes, primfuns follow after this id
case OP_PRIMFUN:
  //#include "prim.h"
  switch (DataValue(data))
  {
    #include "primfuns.h"
  }
  break;
*/
