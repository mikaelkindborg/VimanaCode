/*
File: lib.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Library functions
*/

// --------------------------------------------------------------
// Guru Meditation
// --------------------------------------------------------------

void GURU_MEDITATION(char* message)
{
  printf("[GURU_MEDITATION %s]\n", message);
  exit(1);
}

// --------------------------------------------------------------
// String functions
// --------------------------------------------------------------

#define StrEquals(s1, s2) (0 == strcmp((s1), (s2)))

TypeBool StringIsInteger(char* string)
{
  TypeBool isNumber = FALSE;
  int      index = 0;

  // Check if the first character a minus sign
  if ('-' == string[index])
    { ++ index; }

  // Rest of string must have digits only
  while (0 != string[index])
  {
    isNumber = TRUE;
    if (!isdigit(string[index]))
      { isNumber = FALSE; break; }
    ++ index;
  }

  return isNumber;
}

// --------------------------------------------------------------
// Program memory functions
// --------------------------------------------------------------

TypeIndex ProgMemWrite(TypeData data)
{
  if (ProgMemFirstFree >= PROGMEM_MAXSIZE)
    { GURU_MEDITATION("Out of program memory"); }

  ProgMem[ProgMemFirstFree] = data;

  return ProgMemFirstFree ++;
}

// --------------------------------------------------------------
// String memory functions
// --------------------------------------------------------------

void StringMemWriteChar(char c)
{
  if ((STRINGMEM_MAXSIZE - StringMemFirstFree) < 2)
    { GURU_MEDITATION("Out of string memory"); }

  // Write char
  StringMem[StringMemFirstFree] = c;
  ++ StringMemFirstFree;

  //printf("%c", c);
}

char* StringMemWriteEnd()
{
  // Zero terminate string
  StringMem[StringMemFirstFree] = 0;
  ++ StringMemFirstFree;

  // Return string pointer
  return & StringMem[StringMemStringStart];
}

void StringMemKeep()
{
  StringMemStringStart = StringMemFirstFree;;
}

void StringMemReset()
{
  StringMemFirstFree = StringMemStringStart;
}

char* StringMemWrite(char* string)
{
  int length = strlen(string);

  if ((STRINGMEM_MAXSIZE - StringMemFirstFree) < length)
    { GURU_MEDITATION("Out of string memory"); }

  // Write string
  for (int i = 0; i < length; ++ i)
  {
    StringMemWriteChar(string[i]);
  }

  // Return string pointer
  return StringMemWriteEnd();
}

char* StringMemWriteAndKeep(char* string)
{
  char* s = StringMemWrite(string);
  StringMemKeep();
  return s;
}

// --------------------------------------------------------------
// Op table functions
// --------------------------------------------------------------

TypeIndex OpTableLookupCodeForName(char* name)
{
  for (int index = 0; index < OP_LAST_ENTRY; ++ index)
  {
    if (StrEquals(name, OpNameTable[index]))
      { return index; }
  }

  return -1; // Not found
}

char* OpTableLookupNameForCode(int index)
{
  return OpNameTable[index];
}

// --------------------------------------------------------------
// Symbol table function
// --------------------------------------------------------------

TypeIndex SymbolTableLookupIndexForName(char* name)
{
  for (int index = 0; index < SymbolTableFirstFree; ++ index)
  {
    if (StrEquals(name, SymbolNameTable[index]))
      { return index; }
  }

  return -1; // Not found
}

char* SymbolTableLookupNameForIndex(int index)
{
  return SymbolNameTable[index];
}

// Add name and return index of new entry
TypeIndex SymbolTableAdd(char* name)
{
  if (SymbolTableFirstFree >= SYMBOLTABLE_MAXSIZE)
    { GURU_MEDITATION("Out of symbol memory"); }

  // Set string for symbol
  SymbolNameTable[SymbolTableFirstFree] = StringMemWriteAndKeep(name);

  // Set default value
  SymbolTable[SymbolTableFirstFree] = DataWith(0, OP_UNDEFINED);

  // Return index of symbol
  return SymbolTableFirstFree ++;
}
