/*
File: stringify.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Stringify data and lists
*/

// --------------------------------------------------------------
// Stringify
// --------------------------------------------------------------

// Forward declaration
void StringifyList(TypeData list);

void StringifyChar(char c)
{
  StringMemWriteChar(c);
}

void StringifyString(char* string)
{
  char* p = string;
  while (0 != *p)
  {
    StringMemWriteChar(*p);
    ++ p;
  }
}

void StringifyInteger(TypeData n)
{
  char buf[32];
  sprintf(buf, "%ld", (long)n);
  StringifyString(buf);
}

void StringifyVimanaString(char* string)
{
  StringMemWriteChar('{');
  StringifyString(string);
  StringMemWriteChar('}');
}

void StringifyData(TypeData data)
{
  if (OP_LIST == DataOp(data))
  {
    StringifyList(data);
  }
  else
  if (OP_LISTHEAD == DataOp(data))
  {
    StringifyChar('(');
  }
  else
  if (OP_LISTEND == DataOp(data))
  {
    -- StringMemFirstFree;
    StringifyChar(')');
    StringifyChar(' ');
  }
  else
  if (OP_PROGRAMEND == DataOp(data))
  {
    -- StringMemFirstFree;
    StringifyChar(')');
  }
  else
  if (OP_INTEGER == DataOp(data))
  {
    StringifyInteger(DataValue(data));
    StringifyChar(' ');
  }
  /*else
  if (OP_STRING == DataOp(data))
  {
    char* string = ??
    StringifyVimanaString(string);
  }*/
  else
  if (OP_GLOBAL == DataOp(data) || OP_FUNCALL == DataOp(data))
  {
    char* string = SymbolTableLookupNameForIndex(DataValue(data));
    StringifyString(string);
    StringifyChar(' ');
  }
  else // op code
  {
    char* string = OpTableLookupNameForCode(DataOp(data));
    StringifyString(string);
    StringifyChar(' ');
  }
}

void StringifyList(TypeData list)
{
  // Get index of list head
  TypeIndex index = DataValue(list);

  // Get end index
  TypeIndex endIndex = DataValue(ProgMem[index]);

  // Print elements from list head until list end
  for (int i = index; i <= endIndex; ++ i)
  {
    TypeData data = ProgMem[i];
    StringifyData(data);
  }
}

//
// Stringify returns a pointer to a zero-terminated
// (temporary) string. The string memory area is used as a
// temporary buffer.
//
// The returned string must be kept or dropped before
// next call to Stringify. Keeping strings can
// use up memory quickly. To keep the string it is better
// to make a copy of it, and then drop it.
//
// To keep the string call:
//   StringMemKeep();
//
// To drop the string call:
//   StringMemReset();
//
// Example:
//   char* string = Stringify(list);
//   printf("%s\n", string);
//   StringMemReset();
//
char* Stringify(TypeData data)
{
  // Stringify data
  StringifyData(data);

  // Terminate string and return pointer to string buffer
  return StringMemWriteEnd();
}

void ProgPrint(TypeData data)
{
  char* string = Stringify(data);
  printf("%s\n", string);
  StringMemReset();
  //DebugPrintInt("StringMemFirstFree", StringMemFirstFree);
  //DebugPrintInt("StringMemStringStart", StringMemStringStart);
}
