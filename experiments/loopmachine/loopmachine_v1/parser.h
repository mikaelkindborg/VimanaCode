/*
File: parser.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

LoopMachine parser
*/

// --------------------------------------------------------------
// Parser
// --------------------------------------------------------------

char StreamLastChar;
TypeBool StreamUseLastChar = FALSE;

char StreamReadNextChar(FILE* stream)
{
  if (StreamUseLastChar)
  {
    StreamUseLastChar = FALSE;
    return StreamLastChar;
  }
  else
  {
    StreamLastChar = fgetc(stream);
    return StreamLastChar;
  }
}

#define IsEOF(c)          (EOF == (c))
#define IsDefine(c)       (':' == (c))
#define IsLeftParen(c)    ('(' == (c))
#define IsRightParen(c)   (')' == (c))
#define IsStringBegin(c)  ('{' == (c))
#define IsStringEnd(c)    ('}' == (c))
#define IsCommentBegin(c) ('[' == (c))
#define IsCommentEnd(c)   (']' == (c))
#define IsWhiteSpace(c) \
  ((' ' == (c)) || ('\t' == (c)) || ('\n' == (c)) || ('\r' == (c)))
#define IsSeparator(c) \
  (IsLeftParen(c) || IsRightParen(c) || IsStringBegin(c) || IsStringEnd(c))
#define IsWhiteSpaceOrSeparator(c) (IsWhiteSpace(c) || IsSeparator(c))

void SkipComment(FILE* stream)
{
  int c = StreamReadNextChar(stream);
  while (!IsCommentEnd(c) && !IsEOF(c))
  {
    c = StreamReadNextChar(stream);
  }
}

char* ParserReadToken(int c, FILE* stream)
{
  // static local variable = hidden global
  static char Token[MAX_TOKEN_LENGTH];

  // Write chars to token buffer
  int i = 0;

  // Check define char (:)
  if (IsDefine(c))
  {
    // The define character may be used without trailing
    // whitespace, so we handle it here in this special case
    // It becomes a separate word in the parsed syntax tree
    Token[i] = c;
    ++ i;
  }
  else
  {
    // Read token
    while (!IsEOF(c) && !IsWhiteSpaceOrSeparator(c))
    {
      // Max token length includes the zero-terminator
      if (i < MAX_TOKEN_LENGTH - 1)
      {
        Token[i] = c;
        ++ i;
      }
      c = StreamReadNextChar(stream);
    }

    // Set flag telling that we have consumed the last car
    StreamUseLastChar = TRUE;
  }

  // Zero terminate token string
  Token[i] = 0;

  return Token;
}

// Get the token type
// Global variables begin with an uppercase letter
// Functions and opcodes/primfuns begin with anything
// that is not an uppercase letter
int ParseType(char* token)
{
  if (StringIsInteger(token))
    { return OP_INTEGER; }
  else
  if (isupper(token[0]))
    { return OP_GLOBAL; }
  else
    { return OP_FUNCALL; }
}

TypeData ParseInteger(char* token)
{
  return DataWith(
    (TypeData) strtol(token, NULL, 10),
    OP_INTEGER);
}

TypeData ParseOpCode(int opcode)
{
  return DataWith(0, opcode);
}

TypeData ParseSymbol(char* token, int optype)
{
  TypeData data = 0;

  // Look up the name in the symbol table
  TypeIndex index = SymbolTableLookupIndexForName(token);
  if (-1 == index)
  {
    // Add new symbol
    index = SymbolTableAdd(token);
  }

  if (OP_FUNCALL == optype)
  {
    data = DataWith(index, OP_FUNCALL);
  }
  else
  if (OP_GLOBAL == optype)
  {
    data = DataWith(index, OP_GLOBAL);
  }

  return data;
}

TypeData ParseToken(char c, FILE* stream)
{
  TypeData data;

  char* token = ParserReadToken(c, stream);

  int optype = ParseType(token);

  if (OP_INTEGER == optype)
  {
    data = ParseInteger(token);
  }
  else
  {
    // Is it an opcode/primfun?
    int opcode = OpTableLookupCodeForName(token);
    if (opcode > -1)
    {
      data = ParseOpCode(opcode);
    }
    // Otherwise it is a symbol
    else
    {
      data = ParseSymbol(token, optype);
    }
  }

  return data;
}

// Stack that keeps track of OP_LISTHEAD indexes
TypeIndex ParserListStack[PARSER_LIST_MAX_DEPTH];
TypeIndex ParserListStackTop = -1;

// Write a FLAT list with OP_LISTHEAD and OP_LISTEND separators
// Return OP_LIST data element
TypeData ParseList(FILE* stream)
{
  TypeIndex headIndex; // Address of parsed list
  TypeData  data;
  TypeIndex index;

  // Write list head placeholder
  headIndex = ProgMemWrite(0);

  int c = StreamReadNextChar(stream);

  while (!IsEOF(c))
  {
    if (IsWhiteSpace(c))
    {
      // Skip whitespace
    }
    else
    if (IsCommentBegin(c))
    {
      SkipComment(stream);
    }
    else
    if (IsLeftParen(c))
    {
      // Write list head
      data = DataWith(0, OP_LISTHEAD);
      index = ProgMemWrite(data);

      // Add list head index to stack
      ++ ParserListStackTop;
      ParserListStack[ParserListStackTop] = index;
    }
    else
    if (IsRightParen(c))
    {
      // Write list end
      data = DataWith(0, OP_LISTEND);
      index = ProgMemWrite(data);

      // Write end index to list head
      ProgMem[ParserListStack[ParserListStackTop]] = DataWith(index, OP_LISTHEAD);
      -- ParserListStackTop;
    }
    else
    // it is a symbol or an integer
    {
      data = ParseToken(c, stream);
      ProgMemWrite(data);
    }

    c = StreamReadNextChar(stream);
  }
  // while

  // Write OP_PROGRAMEND at end of list (note that we DONT use OP_LISTEND here)
  data = DataWith(0, OP_PROGRAMEND);
  index = ProgMemWrite(data);

  // Write head with end index of parsed list
  ProgMem[headIndex] = DataWith(index, OP_LISTHEAD);

  TypeData list = DataWith(headIndex, OP_LIST);

  return list;
}

TypeIndex ParseProgram(FILE* stream)
{
  return ParseList(stream);
}
