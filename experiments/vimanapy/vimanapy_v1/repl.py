# File: repl.py
# Read Eval Print Loop (REPL) for VimanaCode
# Copyright (c) 2022-2023 Mikael Kindborg
# mikael@kindborg.com
#
# Run this command:
#
#  python3 repl.py
#

# Make line editing and arrow keys work
import readline

# Import the Vimana interpreter
from interpreter import *

print("Welcome to VimanaCode")
print("Type exit to quit")
x = ""
while (x != "exit"):
  x = input(": ")
  vimana(x)
