This tutorial was posted on Substack December 2022.

Will follow up with a tutorial on how to run Vimana in MicroPython.

# How to run MicroPython on a Raspberry Pi Pico with minimal setup

This is a step-by-step guide for how to get started with interactive programming in Python on the Raspberry Pi Pico.

The background is that I wanted to run MicroPython with minimal setup, without having to install a Python IDE.

This proved to be straightforward - you just install the MicroPython interpeter on the Pico and then use a serial terminal to interact with it.

## 1. Install MicroPython on the Pico

This step is done by connecting the Pico to your computer in a way that enables it to be used as a USB drive:

1. Get the USB cable and connect it to the Pico.

2. Press the small "BOOTSEL" button on the Pico and hold it while inserting the other end of the USB cable into your computer, and then release the button.

3. The Pico can now be used as a USB drive from on your computer.

4. Download the MicroPython firmware file at this address: https://micropython.org/download/rp2-pico/rp2-pico-latest.uf2

5. Copy the downloaded file to the Pico using drag and drop.

When the file firmware file has been copied to the Pico, it will reboot automatically and run the MicroPython interpreter, which can be interacted with over the USB serial port.

## 2. Interact with the MicroPython interpreter

Next step is to use a serial terminal to connect to the Pico and interact with the Python interpreter. On Linux and macOS you can use the "screen" utility (on Windows you can use PuTTY).

The following example shows how to use screen on Linux/macOS.

### 2.1. Find the name of the Pico USB serial port

Open a terminal window on your computer.

List available TTY ports using this command (MacOS and Linux):

    ls /dev/tty*

The port of the Pico will probably be named something like:

    /dev/tty.usbmodem142301

### 2.2. Connect using screen

Use screen to connect to the Pico:

    screen /dev/tty.usbmodem142301

We are now connected to Python REPL (Read Eval Print Loop).

### 2.3. Run Python code

The REPL enables you to type and evaluate any MicroPython code.

For example, try typing the following and press RETURN:

    1 + 2

The result is displayed in the terminal.

### 2.4. Turn on the onboard LED

To turn on the onboard LED of the Pico, type this code (press RETURN after each line):

    from machine import Pin
    led = Pin(25, Pin.OUT)
    led.value(1)

To turn the LED off, type:

    led.value(0)

We are now up and running with coding the Pico in Python - minimal setup required.

### 2.5. Quit screen

To end the screen terminal session, type:

    CTRL-A k y

(Press CTRL-A on the keyboard, release and then press the K key, and confirm by pressing the Y key.)

## 3. Write Python programs on the Pico

To develop programs on the Pico, write the code in a text editor on your computer and then copy and paste the code into the Python REPL. There are other ways as well, but this is a handy way to experiment and do interactive coding.

In the following example we will write a program that repeatedly blinks the onboard LED. For this, we will use a timer.

Here is the code:

from machine import Pin, Timer

led = Pin(25, Pin.OUT)

def tick(timer):
  global led
  led.toggle()

timer = Timer()
timer.init(period=2000, mode=Timer.PERIODIC, callback=tick)

This program will switch the onboard LED on and off every 2000 milliseconds. Note that toggle is a predefined method on the led object.

Select the code, paste it into the Python REPL, and press RETURN - this will run the program.

It is now possible to do interactive coding.

To change the blink interval, paste this line and press RETURN:

    timer.init(period=1000, mode=Timer.PERIODIC, callback=tick)

To stop the timer, use:

    timer.deinit()

This interactive appraoch to coding enables you to tinker with the program and see the result instantly.

## 4. Good to know

### 4.1. Paste mode

It might happen that indentation errors occur when pasing code into the REPL. This is caused by the auto-indent feature.

The above program works to paste because there is an empty line after the indented lines in the tick function. Without the empty line, the subsequent lines would have been indented autiomatically, causing an error.

The REPL has a special mode called "paste mode" that turns off the auto-indent feature. To use paste mode, do as follows:

    Press CTRL-E to enter paste mode
    Paste your code
    Press CTRL-D to exit paste mode

### 4.2. Soft reset

If your program hangs, you can soft-reset the board typ typing CTRL-D

## 4.3. Help

To get a help text, type:

    help()

### 4.4. Quit screen

To end the screen terminal session, type:

    CTRL-A k y

## 5. Pico boot modes

The Raspberry Pi Pico has two boot modes:

### 5.1. USB drive mode ("bootloader mode")

Pressing the "BOOTSEL" button while inserting the USB cable into the computer boots the device in USB drive mode (also called "bootloader mode").

In this mode you can copy firmware files to the device. These files have extenstion ".uf2".

The MicroPython firmware file is an example of a UF2 file.

### 5.2. Firmware mode (run installed firmware)

Connecting the USB cable without pressing "BOOTSEL" boots the firmware installed on the device.

If the MicroPython firmware is installed, the interpreter starts and can be interacted with over the serial USB port.

## 6. Links

https://www.raspberrypi.com/documentation/microcontrollers/micropython.html#drag-and-drop-micropython
https://docs.micropython.org/en/latest/library/machine.Timer.html
https://docs.micropython.org/en/latest/reference/repl.html
https://mikeesto.medium.com/uploading-to-the-raspberry-pi-pico-without-thonny-53de1a10da30
https://bigl.es/tuesday-tooling-pico-mix/
