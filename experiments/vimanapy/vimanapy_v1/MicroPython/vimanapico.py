# File: vimanapico.py
# Vimana for MicroPython on Raspberry Pi Pico

###########################################################
# Vimana interpreter
###########################################################

# File: vimana.py
# Vimana interpreter in Python
# Copyright (c) 2022-2023 Mikael Kindborg
# mikael@kindborg.com
#
# Vimana is an experimental minimalistic programming language
# based on Lisp and Forth.

VimanaListType = list
VimanaStringType = str
VimanaSeparators = ("(", ")", " ", "\n", "\r", "\t")

# Interpreter

class VimanaInterp(object):

  def __init__(self):
    self.globals = {}
    self.primFuns = {}
    self.callStack = []
    self.dataStack = []
    self.parser = VimanaParser()
    Vimana_createPrimFuns(self)

  def eval(self, string):
    try:
      self.evalList(self.parser.parse(string))
    except TypeError as error:
      self.printCallStack(error)

  def printCallStack(self, error):
    Vimana_print("ERROR: " + str(error))
    VimanaPrim_printstack(self)
    VimanaPrim_printCallStack(self)

  def evalList(self, item):
    if (type(item) is VimanaListType):
      # Push root stack frame
      self.callStackPush(item)
      # Call stack loop
      while (len(self.callStack) > 0):
        # Instruction list loop
        while (None != self.callStackTop()):
          # Get data for current instruction
          data = Vimana_first(self.callStackTop())
          # Set next list item in advance
          self.callStackSetTop(Vimana_rest(self.callStackTop()))
          # Dispatch on current instruction
          if (type(data) is VimanaStringType):
            if (data in self.primFuns):
              # Call primfun
              self.primFuns[data](self)
            elif (data in self.globals):
              value = self.globals[data]
              if (self.isFun(value)):
                # Push stack frame with function list
                self.callStackPush(value)
              else:
                # Push global variable value
                self.push(value)
            else:
              # Push data
              self.push(data)
          else:
            # Push data
            self.push(data)
        # End of instruction list - pop stack frame
        self.callStackPop()

  def callStackPush(self, item):
    if (type(item) is VimanaListType):
      if (0 == len(self.callStack)):
        # Push new stack frame
        self.callStack.append(item)
      elif (None == self.callStackTop()):
        # Tail call
        self.callStackSetTop(item)
      else:
        # Push new stack frame
        self.callStack.append(item)

  def callStackPop(self):
    return self.callStack.pop()

  def callStackTop(self):
    return self.callStack[-1]

  def callStackSetTop(self, item):
    self.callStack[-1] = item

  def isFun(self, item):
    return (type(item) is VimanaListType) and (3 == len(item))

  def push(self, item):
    return self.dataStack.append(item)

  def pop(self):
    return self.dataStack.pop()

  def setglobal(self, name, value):
    self.globals[name] = value

  def print(self, item):
    Vimana_print(Vimana_stringify(item))

# Parser

class VimanaParser(object):

  def parse(self, code):
    self.length = len(code)
    self.pos = 0
    return self.parseList(code)

  def parseList(self, code):
    l = None
    while (self.pos < self.length):
      c = code[self.pos]
      if ("(" == c):
        self.pos += 1
        l = Vimana_conslast(l, self.parseList(code))
      elif (")" == c):
        self.pos += 1
        break
      elif (not c in VimanaSeparators):
        l = Vimana_conslast(l, self.parseToken(code))
      else:
        self.pos += 1
    return l

  def parseToken(self, code):
    token = ""
    while (self.pos < self.length):
      c = code[self.pos]
      if (c in VimanaSeparators):
        break
      else:
        token += c
        self.pos += 1
    return self.tokenConvert(token)

  def tokenConvert(self, token):
    try:
      return int(token)
    except ValueError:
      try:
        return float(token)
      except ValueError:
        return str(token)

# Primitive functions

def Vimana_createPrimFuns(interp):
  interp.primFuns["print"] = VimanaPrim_print
  interp.primFuns["printraw"] = VimanaPrim_printraw
  interp.primFuns["printstack"] = VimanaPrim_printstack
  interp.primFuns["drop"] = VimanaPrim_drop
  interp.primFuns["dup"] = VimanaPrim_dup
  interp.primFuns["swap"] = VimanaPrim_swap
  interp.primFuns["over"] = VimanaPrim_over
  interp.primFuns["first"] = VimanaPrim_first
  interp.primFuns["rest"] = VimanaPrim_rest
  interp.primFuns["cons"] = VimanaPrim_cons
  interp.primFuns["setfirst"] = VimanaPrim_setfirst
  interp.primFuns["setglobal"] = VimanaPrim_setglobal
  interp.primFuns["+"] = VimanaPrim_add
  interp.primFuns["-"] = VimanaPrim_sub
  interp.primFuns["*"] = VimanaPrim_mul
  interp.primFuns["/"] = VimanaPrim_div
  interp.primFuns["<"] = VimanaPrim_lessthan
  interp.primFuns[">"] = VimanaPrim_greaterthan
  interp.primFuns["eq"] = VimanaPrim_eq
  interp.primFuns["not"] = VimanaPrim_not
  interp.primFuns["ifelse"] = VimanaPrim_ifelse
  interp.primFuns["eval"] = VimanaPrim_eval
  interp.primFuns["funify"] = VimanaPrim_funify

def VimanaPrim_print(interp):
  item = interp.pop()
  interp.print(item)

def VimanaPrim_printraw(interp):
  item = interp.pop()
  print(item)

def VimanaPrim_printstack(interp):
  string = "STACK: "
  for index in range(len(interp.dataStack)):
    string += Vimana_stringify(interp.dataStack[index]) + " "
  Vimana_print(string)

def VimanaPrim_printCallStack(interp):
  Vimana_print("CALLSTACK:")
  for index in range(len(interp.callStack)):
    string = Vimana_stringify(interp.callStack[index])
    Vimana_print("  " + string)

def VimanaPrim_drop(interp):
  interp.pop()

def VimanaPrim_dup(interp):
  item = interp.pop()
  interp.push(item)
  interp.push(item)

def VimanaPrim_swap(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(b)
  interp.push(a)

def VimanaPrim_over(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a)
  interp.push(b)
  interp.push(a)

def VimanaPrim_first(interp):
  l = interp.pop()
  first = Vimana_first(l)
  interp.push(first)

def VimanaPrim_rest(interp):
  l = interp.pop()
  rest = Vimana_rest(l)
  interp.push(rest)

def VimanaPrim_cons(interp):
  b = interp.pop()
  a = interp.pop()
  l = Vimana_cons(a, b)
  interp.push(l)

def VimanaPrim_setfirst(interp):
  value = interp.pop()
  l = interp.pop()
  l[0] = value

def VimanaPrim_setglobal(interp):
  namelist = interp.pop()
  value = interp.pop()
  name = Vimana_first(namelist)
  interp.setglobal(name, value)

def VimanaPrim_add(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a + b)

def VimanaPrim_sub(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a - b)

def VimanaPrim_mul(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a * b)

def VimanaPrim_div(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a / b)

def VimanaPrim_lessthan(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a < b)

def VimanaPrim_greaterthan(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a > b)

def VimanaPrim_eq(interp):
  b = interp.pop()
  a = interp.pop()
  interp.push(a == b)

def VimanaPrim_not(interp):
  boolValue = interp.pop()
  interp.push(not boolValue)

def VimanaPrim_ifelse(interp):
  elseBranch = interp.pop()
  trueBranch = interp.pop()
  boolValue = interp.pop()
  if (boolValue):
    interp.callStackPush(trueBranch)
  else:
    interp.callStackPush(elseBranch)

def VimanaPrim_eval(interp):
  l = interp.pop()
  interp.callStackPush(l)

def VimanaPrim_funify(interp):
  l = interp.pop()
  l.append(True)
  interp.push(l)

# List functions

def Vimana_cons(a, b):
  return [a, b]

def Vimana_first(l):
  return l[0]

def Vimana_rest(l):
  return l[1]

def Vimana_last(l):
  while (None != Vimana_rest(l)):
    l = Vimana_rest(l)
  return l

def Vimana_conslast(a, b):
  if (None == a):
    return Vimana_cons(b, None)
  else:
    last = Vimana_last(a)
    last[1] = Vimana_cons(b, None)
    return a

# Print functions

def Vimana_stringify(item):
  string = ""
  if (type(item) is VimanaListType):
    string += "("
    printSpace = False
    while (item != None):
      if (printSpace):
        string += " "
      string += Vimana_stringify(Vimana_first(item))
      printSpace = True
      item = Vimana_rest(item)
    string += ")"
  elif (None == item):
    string = "()"
  else:
    string = str(item)
  return string

def Vimana_print(obj):
  print(obj)

# Create interpreter
Vimana_interp = VimanaInterp()

# Evaluate string
def vimana(string):
  Vimana_interp.eval(string)

# Library functions
vimana("""
(funify swap setglobal) funify (def) setglobal
(iftrue) (() ifelse) def
(iffalse) (() swap ifelse) def
(iszero) (0 eq) def
(isempty) (() eq) def
""")

# Local variables
vimana("""
() (LocalStack) setglobal

(localStackPushContext)
  (() LocalStack cons (LocalStack) setglobal) def

(localStackPopContext)
  (LocalStack rest (LocalStack) setglobal) def

(localStackPushVar)
  (LocalStack first cons
      LocalStack swap setfirst) def

(A) (LocalStack first first) def
(B) (LocalStack first rest first) def
(C) (LocalStack first rest rest first) def
(D) (LocalStack first rest rest rest first) def

([A]) (localStackPushContext localStackPushVar) def
([AB]) (localStackPushContext localStackPushVar localStackPushVar) def
([ABC]) (localStackPushContext localStackPushVar localStackPushVar localStackPushVar) def
([ABCD]) (localStackPushContext localStackPushVar localStackPushVar localStackPushVar localStackPushVar) def

([]) (localStackPopContext) def
""")

###########################################################
# Primitive functions for MicroPython
###########################################################

from machine import Pin
import utime

def Vimana_createMicroPythonPrimFuns(interp):
  interp.primFuns["pinNew"] = VimanaPrim_pinNew
  interp.primFuns["pinLow"] = VimanaPrim_pinLow
  interp.primFuns["pinHigh"] = VimanaPrim_pinHigh
  interp.primFuns["pinToggle"] = VimanaPrim_pinToggle
  interp.primFuns["sleep"] = VimanaPrim_sleep

def VimanaPrim_pinNew(interp):
  mode = interp.pop()
  id = interp.pop()
  interp.push(Pin(id, mode))

def VimanaPrim_pinLow(interp):
  led = interp.pop()
  led.low()

def VimanaPrim_pinHigh(interp):
  led = interp.pop()
  led.high()

def VimanaPrim_pinToggle(interp):
  led = interp.pop()
  led.toggle()

def VimanaPrim_sleep(interp):
  t = interp.pop()
  utime.sleep(t)

Vimana_createMicroPythonPrimFuns(Vimana_interp)

# MicroPython library functions
vimana("""
1 (OUT) setglobal
""")
