# File: workbench.py
# Simple IDE for VimanaCode
# Copyright (c) 2022-2023 Mikael Kindborg
# mikael@kindborg.com

# Run this command:
#
#  python3 repl.py
#

import os
import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import filedialog
from tkinter import messagebox

# Import the Vimana interpreter

from interpreter import *

# Globals

TopWindow = None
TextBox1 = None
TextBox2 = None
CurrentFileName = None

IntroductionText = """
{VimanaCode is a minimalistic programming language inspired
by Forth and Lisp. It is meant to be a DIY (Do It Yourself)
programming language, with an simple interpreter that can
be modified and experimented with.
} print

{You can run the selected code or all code in this window.

For example, select this line of text with the mouse:

    1 2 + print

Then use the menu command Eval Selection. The result is
printed in the output panel below.

Unicode charaters can also be used. Select and evaluate this code:

    (😀) (dup iszero (drop) ((😀) first print 1 - 😀) def
    10 😀

In the above example, a unicode emoji is used as the function name.
Enclosing the emoji in a list prevents it from being called. That
makes it possible to take it from the list and print it.

You can also use the Run All menu command to evaluate all
of the code in this window.
} print

{{This is a string}
} print

{Strings can be used as code comments, like this:

  {This is a comment} drop
} print

{When Vimana sees a value (string, list, or number) it is
pushed onto the data stack.
} print

{Let's push a number and print the stack.} print
42
printstack

{
Now let's push a list.} print
(1 2 +)
printstack

{
Now drop these items.} print

drop drop
printstack

{
When Vimana sees a function, it calls the function.
The function uses the parameters on the data stack.

Let's add some numbers and print the result.} print

1 2 + print

{
Use the Open menu command to explore more code examples
and tutorials.
} print

{Thanks for trying VimanaCode! 😀} print
"""

# Menu Commands

def CommandEval(e=None):
  selection = GetSelection()
  if (None != selection):
    success = vimana(selection)
    if (success):
      Vimana_printstack()

def CommandRun(e=None):
  selection = TextBox1.get("1.0", tk.END)
  vimana(selection)

def CommandPrintStack(e=None):
  Vimana_printstack()

def CommandClearOutput(e=None):
  ClearOutput()

def CommandAbout(e=None):
  ShowOutput("VimanaCode is a minimalistic programming language inspired by Forth and Lisp");

def CommandHelp(e=None):
  ShowOutput("Select a block of code and use Eval Selection to evaluate the selection");

def CommandOpen(e=None):
  global CurrentFileName
  filetypes = (("Vimana files", "*.vimana"), ("All files", "*.*"))
  filename = filedialog.askopenfilename(
    title="Open file",
    parent=TopWindow,
    initialdir=os.getcwd(),
    filetypes=filetypes)
  print(str(filename))
  if (None != filename):
    f = open(filename, "r")
    content = f.read()
    SetCode(content)
    CurrentFileName = filename

def CommandSaveAs(e=None):
  global CurrentFileName
  filetypes = (("Vimana files", "*.vimana"), ("All files", "*.*"))
  filename = filedialog.asksaveasfilename(
    title="Save file",
    parent=TopWindow,
    initialdir=os.getcwd(),
    filetypes=filetypes)
  #messagebox.showinfo(
  #  title="Selected File",
  #  message=filename)
  if (None != filename):
    f = open(filename, "w")
    f.write(GetCode())
    CurrentFileName = filename
    ShowOutput("File saved: " + CurrentFileName)

def CommandSave(e=None):
  if (None != CurrentFileName):
    f = open(CurrentFileName, "w")
    f.write(GetCode())
    ShowOutput("File saved: " + CurrentFileName)

def GetSelection():
  try:
    start = TextBox1.index(tk.SEL_FIRST)
    end = TextBox1.index(tk.SEL_LAST)
    selection = TextBox1.get(start, end)
    return selection
  except:
    return None

def ShowOutput(text):
  TextBox2.insert(tk.END, text + "\n")
  TextBox2.see(tk.END)

def ClearOutput():
  TextBox2.delete("1.0", tk.END)

def SetCode(text):
  TextBox1.delete("1.0", tk.END)
  TextBox1.insert("1.0", text)

def GetCode():
  return TextBox1.get("1.0", tk.END)

# Create UI

def CreateUI():
  global TextBox1
  global TextBox2

  window = tk.Tk()

  window.title("Welcome to the Wonderful World of VimanaCode")
  #window.geometry("600x400")

  frame = tk.Frame(window)
  frame.pack(fill="both", expand=True)
  #frame.grid()

  #Label(frame, text="Hi World!").grid(column=0, row=0)
  #Button(frame, text="Quit", command=window.destroy).grid(column=1, row=0)

  TextBox1 = scrolledtext.ScrolledText(frame)
  TextBox1.config(font=("Courier", 24), undo=True, height=18)
  TextBox1.pack(fill="both", expand=True)
  #TextBox1.grid(column=0, row=0)

  TextBox2 = scrolledtext.ScrolledText(frame)
  TextBox2.config(font=("Courier", 24), undo=True, height=15)
  TextBox2.pack(fill="both", expand=True)
  #TextBox2.grid(column=0, row=1)

  return window

def CreateMenuBar(window):
  menubar = tk.Menu(window)

  menuFile = tk.Menu(menubar)
  menuFile.add_command(label="Open...", command=CommandOpen, accelerator="Cmd+O")
  menuFile.add_separator()
  menuFile.add_command(label="Save", command=CommandSave, accelerator="Cmd+S")
  menuFile.add_separator()
  menuFile.add_command(label="Save As...", command=CommandSaveAs)

  menuRun = tk.Menu(menubar)
  menuRun.add_command(label="Eval Selection", command=CommandEval, accelerator="Cmd+E")
  menuRun.add_separator()
  menuRun.add_command(label="Run All", command=CommandRun, accelerator="Cmd+R")
  menuRun.add_separator()
  menuRun.add_command(label="Show Data Stack", command=CommandPrintStack, accelerator="Cmd+D")
  menuRun.add_separator()
  menuRun.add_command(label="Clear Output", command=CommandClearOutput)

  #menuRun.add_command(label="Vimana Hi", command=lambda:vimana("{Hi World} print"))

  menuHelp = tk.Menu(menubar)
  menuHelp.add_command(label="About Vimana", command=CommandAbout)
  menuHelp.add_separator()
  menuHelp.add_command(label="Show Help", command=CommandHelp)

  menubar.add_cascade(label="File", menu=menuFile)
  menubar.add_cascade(label="Run", menu=menuRun)
  menubar.add_cascade(label="Help", menu=menuHelp)

  window.config(menu=menubar)

  window.bind("<Control-o>", CommandOpen)
  window.bind("<Command-o>", CommandOpen)
  window.bind("<Control-s>", CommandSave)
  window.bind("<Command-s>", CommandSave)

  window.bind("<Control-e>", CommandEval)
  window.bind("<Command-e>", CommandEval)
  window.bind("<Control-r>", CommandRun)
  window.bind("<Command-r>", CommandRun)
  window.bind("<Control-d>", CommandPrintStack)
  window.bind("<Command-d>", CommandPrintStack)

def Main():
  global TopWindow
  Vimana_setprintfun(ShowOutput)
  TopWindow = CreateUI()
  SetCode(IntroductionText)
  TextBox1.focus_set()
  CreateMenuBar(TopWindow)
  TopWindow.mainloop()

Main()
