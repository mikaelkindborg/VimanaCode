/*
File: test.c

Tests for 64-bit version of Vimana.
*/

#define TRACK_MEMORY_USAGE
#define DEBUG

#include "../src/vimana.h"

// -------------------------------------------------------------
// Global variables
// -------------------------------------------------------------

static int GlobalNumFailedTests = 0;

// -------------------------------------------------------------
// Function declarations
// -------------------------------------------------------------

VInterp* CreateInterpNoPrimFuns();
VInterp* CreateInterpWithPrimFuns();
VInterp* CreateInterpBig();

VItem*   InterpAllocMaxItems(VInterp* interp, int start);
void     InterpPrintItems(VInterp* interp, VItem* first);
int      InterpCountItems(VInterp* interp, VItem* first);

void     ShouldHold(char* description, int condition);
void     PrintTestResult();
void     PrintSeparator();
void     LogTest(char* testName);

void     PrintBinaryULong(unsigned long n);

// -------------------------------------------------------------
// Tests
// -------------------------------------------------------------

void TestItemAttributes()
{
  LogTest("TestItemAttributes");

  VItem theItem;
  VItem* item = &theItem;

  PrintLine("Integer 1:");
  PrintBinaryULong(1);
/*
  PrintLine("TypeIntNum,TypeList,TypeHandle:");
  PrintBinaryULong(TypeIntNum);
  PrintBinaryULong(TypeList);
  PrintBinaryULong(TypeHandle);

  PrintLine("MarkBit,TypeMask,TagMask:");
  PrintBinaryULong(MarkBit);
  PrintBinaryULong(TypeMask);
  PrintBinaryULong(TagMask);
*/

  ItemInit(item);
  PrintLine("Initialized item:");
  PrintBinaryULong(item->next);

  PrintLine("GC mark set:");
  ItemGCMarkSet(item);
  PrintBinaryULong(item->next);

  PrintLine("TypeList set:");
  ItemSetType(item, TypeList);
  PrintBinaryULong(item->next);

  ShouldHold("TestItemAttributes: Item type should equal TypeList",
    TypeList == ItemGetType(item));

  PrintLine("next set to 3:");
  ItemSetNext(item, 3);
  PrintBinaryULong(item->next);

  ShouldHold("TestItemAttributes: Item next should equal", 3 == ItemGetNext(item));

  ShouldHold("TestItemAttributes: Mark bit should be set", 0 != ItemGetGCMark(item));

  ItemSetIntNum(item, 16);
  PrintLine("TypeIntNum set:");
  PrintBinaryULong(item->next);

  ItemGCMarkUnset(item);
  PrintLine("GC mark unset:");
  PrintBinaryULong(item->next);

  ShouldHold("TestItemAttributes: Mark bit should be unset", 0 == ItemGetGCMark(item));

  ShouldHold("TestItemAttributes: Item type should equal TypeIntNum", TypeIntNum == ItemGetType(item));

  ShouldHold("TestItemAttributes: Item value should equal 16", 16 == ItemGetIntNum(item));

  printf("Type:  %lu\n", ItemGetType(item));
  PrintBinaryULong(ItemGetType(item));
  printf("Mark:  %lu\n", ItemGetGCMark(item));
  printf("Next:  %lu\n", (unsigned long) ItemGetNext(item));
  printf("First: %lu\n", (unsigned long) ItemGetFirstItem(item));
}

// These tests are especially interesting for 32/16 bit platforms.
void TestItemNext()
{
  LogTest("TestItemNext");

  // Variable "interp" is used by macros on 32/16 bit platforms
  VInterp* interp = CreateInterpNoPrimFuns();

  // Variable "mem" is used by macros on 32/16 bit platforms
  VListMemory* mem = InterpListMem(interp);

  // Test first and next
  VItem* first = InterpAllocItem(interp);
  VItem* next = InterpAllocItem(interp);
  ItemSetNextItem(first, next);

  ShouldHold("TestItemNext: next of first should be next",
    ItemGetNextItem(first) == next);
  ShouldHold("TestItemNext: next of first should be next (ListMem)",
    ListMemGetNextItem(first) == next);

  ShouldHold("TestItemNext: next of next should be NULL",
    NULL == ItemGetNextItem(next));
  ShouldHold("TestItemNext: next of next should be NULL (ListMem)",
    NULL == ListMemGetNextItem(next));

  // Test empty item
  VItem item;
  ItemInit(&item);

  ShouldHold("TestItemNext: next of empty item should be NULL",
    NULL == ItemGetNextItem(&item));
  ShouldHold("TestItemNext: next of empty item should be NULL (ListMem)",
    NULL == ListMemGetNextItem(&item));

  InterpFree(interp);

  ShouldHold("TestItemNext: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestAlloc()
{
  LogTest("TestAlloc");

  VInterp* interp = CreateInterpNoPrimFuns();

  VItem* item;

  item = InterpAllocItem(interp);
  ItemGCMarkSet(item);
  ItemSetType(item, TypeIntNum);
  ItemSetNext(item, 0);

  item = InterpAllocItem(interp);
  ItemGCMarkSet(item);
  ItemSetType(item, TypeIntNum);
  ItemSetNext(item, 0);

  ListMemPrintAllocCounter(InterpListMem(interp));

  ListMemSweep(InterpListMem(interp));

  ListMemPrintAllocCounter(InterpListMem(interp));

  ShouldHold("TestInterpAlloc: allocCounter should equal 2",
    2 == InterpListMem(interp)->allocCounter);

  ListMemSweep(InterpListMem(interp));

  ListMemPrintAllocCounter(InterpListMem(interp));

  ShouldHold("TestInterpAlloc: allocCounter should be 0",
    0 == InterpListMem(interp)->allocCounter);

  InterpFree(interp);

  ShouldHold("TestInterpAlloc: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestAllocDealloc()
{
  LogTest("TestAllocDealloc");

  VItem* first;

  VInterp* interp = CreateInterpNoPrimFuns();

  PrintLine("Alloc max");
  first = InterpAllocMaxItems(interp, 1);
  int numItems = InterpCountItems(interp, first);
  printf("Num items: %i\n", numItems);
  ListMemPrintAllocCounter(InterpListMem(interp));
  InterpPrintItems(interp, first);

  ListMemSweep(InterpListMem(interp));

  PrintLine("Alloc max again");
  first = InterpAllocMaxItems(interp, 21);
  int numItems2 = InterpCountItems(interp, first);
  printf("Num items: %i\n", numItems2);
  ListMemPrintAllocCounter(InterpListMem(interp));
  InterpPrintItems(interp, first);

  ListMemSweep(InterpListMem(interp));

  PrintLine("Alloc max yet again");
  first = InterpAllocMaxItems(interp, 41);
  int numItems3 = InterpCountItems(interp, first);
  printf("Num items: %i\n", numItems3);
  ListMemPrintAllocCounter(InterpListMem(interp));
  InterpPrintItems(interp, first);

  ShouldHold("TestAllocDealloc: Allocated items should be equal",
    (numItems == numItems2) && (numItems == numItems3));

  ListMemSweep(InterpListMem(interp));

  ListMemPrintAllocCounter(InterpListMem(interp));

  InterpFree(interp);

  ShouldHold("TestAllocDealloc: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestSetFirst()
{
  LogTest("TestSetFirst");

  VItem* first;
  VItem* item;
  VItem* next;

  VInterp* interp = CreateInterpNoPrimFuns();

  // Link items by first pointer

  first = InterpAllocItem(interp);
  item = first;
  ItemSetNext(item, 1); // Use next for the item value in this test

  next = InterpAllocItem(interp);
  ItemSetNext(next, 2); // Use next for the item value in this test
  ItemSetFirst(item, next);
  item = next;

  next = InterpAllocItem(interp);
  ItemSetNext(next, 3); // Use next for the item value in this test
  ItemSetFirst(item, next);
  item = next;

  // Last item
  ItemSetFirst(item, NULL);

  item = first;
  while (item)
  {
    printf("Item value: %lu\n", (unsigned long) ItemGetNext(item));
    item = ItemGetFirstItem(item);
  }

  ListMemSweep(InterpListMem(interp));

  ShouldHold("TestSetFirst: allocCounter should be zero",
    0 == InterpListMem(interp)->allocCounter);

  ListMemPrintAllocCounter(InterpListMem(interp));

  InterpFree(interp);

  ShouldHold("TestSetFirst: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

// Create a string item and get the string value
void TestGetString()
{
  LogTest("TestGetString");

  VInterp* interp = CreateInterpNoPrimFuns();

  char* str1 = "Hi World";
  VItem* handle = InterpAllocHandle(interp, StrCopy(str1), BufferTypeString);

  char* str2 = ItemGetString(handle);
  printf("String: %s\n", str2);

  ShouldHold("TestGetString: Strings should equal", StrEquals(str1, str2));

  ListMemSweep(InterpListMem(interp));

  ListMemPrintAllocCounter(InterpListMem(interp));

  InterpFree(interp);
}

// Test a string referenced by two items
void TestStringReference()
{
  LogTest("TestStringReference");

  VInterp* interp = CreateInterpNoPrimFuns();

  // Alloc string buffer
  char* str1 = "Hi World";
  VItem* handle = InterpAllocHandle(interp, StrCopy(str1), BufferTypeString);

  // Alloc item that points to the same string buffer
  VItem* item = InterpAllocItem(interp);
  *item = *handle; // copy handle

  char* str2 = ItemGetString(handle);
  char* str3 = ItemGetString(item);
  printf("String: %s\n", str3);

  ShouldHold("TestStringReference: Strings should equal", StrEquals(str2, str3));

  ListMemSweep(InterpListMem(interp));

  ShouldHold("TestStringReference: allocCounter should be zero",
    0 == InterpListMem(interp)->allocCounter);

  ListMemPrintAllocCounter(InterpListMem(interp));

  InterpFree(interp);

  ShouldHold("TestStringReference: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestStringMem()
{
  LogTest("TestStringMem");

  VInterp* interp = CreateInterpNoPrimFuns();

  VStringMem* stringMem = InterpStringMem(interp);

  char* s1 = StringMemGetNextFree(stringMem);
  StringMemWriteChar(stringMem, 'H');
  StringMemWriteChar(stringMem, 'I');
  StringMemWriteFinish(stringMem);

  char* s2 = StringMemGetNextFree(stringMem);
  StringMemWriteChar(stringMem, 'H');
  StringMemWriteChar(stringMem, 'I');
  StringMemReset(stringMem);
  StringMemWriteChar(stringMem, ' ');
  StringMemWriteChar(stringMem, 'W');
  StringMemWriteChar(stringMem, 'O');
  StringMemWriteChar(stringMem, 'R');
  StringMemWriteChar(stringMem, 'L');
  StringMemWriteChar(stringMem, 'D');
  StringMemWriteFinish(stringMem);

  ShouldHold("TestStringMem: s1 shold be 'HI'", StrEquals(s1, "HI"));
  ShouldHold("TestStringMem: s2 shold be ' WORLD'", StrEquals(s2, " WORLD"));

  printf("Strings: %s%s\n", s1 , s2);

  InterpFree(interp);

  ShouldHold("TestStringMem: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestSymbolTable()
{
  LogTest("TestSymbolTable");

  char* s1 = "First";
  char* s2 = "Second";
  char* s3 = "Third";

  VInterp* interp = CreateInterpNoPrimFuns();

  VSymbolTable* symbolTable = InterpSymbolTable(interp);

  VSymbolEntry* entry;

  entry = SymbolTableAddEntry(symbolTable, s1);
  ItemSetIntNum(&(entry->value), 1);

  entry = SymbolTableAddEntry(symbolTable, s2);
  ItemSetIntNum(&(entry->value), 2);

  entry = SymbolTableAddEntry(symbolTable, s3);
  ItemSetIntNum(&(entry->value), 3);

  entry = SymbolTableFindEntry(symbolTable, s1);
  ShouldHold("TestSymbolTable: entry value should be 1",
    1 == ItemGetIntNum(&(entry->value)));
  ShouldHold("TestSymbolTable: entry symbol should be First",
    StrEquals(entry->symbol, "First"));

  entry = SymbolTableFindEntry(symbolTable, s2);
  ShouldHold("TestSymbolTable: entry value should be 2",
    2 == ItemGetIntNum(&(entry->value)));
  ShouldHold("TestSymbolTable: entry symbol should be Second",
    StrEquals(entry->symbol, "Second"));

  entry = SymbolTableFindEntry(symbolTable, s3);
  ShouldHold("TestSymbolTable: entry value should be 3",
    3 == ItemGetIntNum(&(entry->value)));
  ShouldHold("TestSymbolTable: entry symbol should be Third",
    StrEquals(entry->symbol, "Third"));

  InterpFree(interp);

  ShouldHold("TestSymbolTable: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestCreatePrimFuns()
{
  LogTest("TestCreatePrimFuns");

  VInterp* interp = CreateInterpWithPrimFuns();

  VSymbolTable* symbolTable = InterpSymbolTable(interp);

  // Print number of primfuns
  int numPrimFuns = SymbolTableNumPrimFuns(symbolTable);
  printf("Number of primfuns: %i\n", numPrimFuns);
  PrintSeparator();

  // Check that some primfuns are present
  VBool plusIsPresent = FALSE;
  VBool dupIsPresent = FALSE;

  // Check and print primfuns
  VByte* p;
  for (p = symbolTable->start;
       p <= symbolTable->top;
       p += sizeof(VSymbolEntry))
  {
    VSymbolEntry* entry = (VSymbolEntry*)(p);

    if (IsTypePrimFun(&(entry->value)))
    {
      if (StrEquals(entry->symbol, "+"))   { plusIsPresent = TRUE; }
      if (StrEquals(entry->symbol, "dup")) { dupIsPresent  = TRUE; }

      printf("%s\n", entry->symbol);
    }
  }
  PrintSeparator();

  ShouldHold("TestCreatePrimFuns: + and dup shold be present",
    plusIsPresent && dupIsPresent);

  InterpFree(interp);

  ShouldHold("TestCreatePrimFuns: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestPrintItem()
{
  LogTest("TestPrintItem");

  VInterp* interp = CreateInterpWithPrimFuns();

  char* s1 = "First";
  char* s2 = "Second";
  char* s3 = "Third";

  VSymbolTable* symbolTable = InterpSymbolTable(interp);

  VSymbolEntry* entry1 = SymbolTableAddEntry(symbolTable, s1);
  VSymbolEntry* entry2 = SymbolTableAddEntry(symbolTable, s2);
  VSymbolEntry* entry3 = SymbolTableAddEntry(symbolTable, s3);
  VSymbolEntry* entry4 = SymbolTableFindEntry(symbolTable, "+");

  VItem* item1 = InterpAllocItem(interp);
  ItemSetIntNum(item1, 42);

  VItem* item2 = InterpAllocItem(interp);
  ItemSetIntNum(item2, -42);

  VItem* item3 = InterpAllocItem(interp);
  ItemSetSymbol(item3, entry1);

  VItem* item4 = InterpAllocItem(interp);
  ItemSetPrimFun(item4, entry4->value.primFunPtr);

  VItem* list = InterpAllocItem(interp);
  ItemSetType(list, TypeList);
  ItemSetFirst(list, item1);
  ItemSetNextItem(item1, item2);
  ItemSetNextItem(item2, item3);
  ItemSetNextItem(item3, item4);

  InterpPrint(interp, list);

  char* s = InterpStringify(interp, list);
  ShouldHold("TestPrintItem strings should be equal",
    StrEquals("(42 -42 First +)", s));
  StringMemReset(InterpStringMem(interp));

  InterpFree(interp);

  ShouldHold("TestPrintItem: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestParse()
{
  LogTest("TestParse");

  VInterp* interp = CreateInterpWithPrimFuns();

  VItem* list;
  char*  code;

  code = "foo bar foobar 1 foo 42";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);

  code = "1 2 (((3 4) 5))";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);

  code = "";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  ShouldHold("TestParse: List should be empty", IsEmpty(list));
  ShouldHold("TestParse: First should be 0", 0 == ItemGetFirstItem(list));

  code = "()";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  ShouldHold("TestParse: Child should be empty",
    IsEmpty(ItemGetFirstItem(list)));

  code = "   (( ( ) ))   ";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  ShouldHold("TestParse: Innermost should be empty",
    IsEmpty(ItemGetFirstItem(ItemGetFirstItem(ItemGetFirstItem(list)))));

  code = "{Hi World}";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  ShouldHold("TestParse: First should be string",
    IsTypeString(ItemGetFirstItem(list)));

  code = "{hi {bar}} foo";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  ShouldHold("TestParse: First should be string",
    IsTypeString(ItemGetFirstItem(list)));

  code = "42";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  ShouldHold("TestParse: First should be intnum (positive number)",
    IsTypeIntNum(ItemGetFirstItem(list)));

  code = "-42";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  ShouldHold("TestParse: First should be intnum (negative number)",
    IsTypeIntNum(ItemGetFirstItem(list)));

  code = "42.2";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  ShouldHold("TestParse: First should be symbol (1)",
    IsTypeSymbol(ItemGetFirstItem(list)));

  code = "42.2.3";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  ShouldHold("TestParse: First should be symbol (2)",
    IsTypeSymbol(ItemGetFirstItem(list)));

  code = "1000 2 3 /";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  //ShouldHold("TestParse: First should be symbol (2)",
   // IsTypeSymbol(ItemGetFirstItem(list)));

  InterpFree(interp);

  ShouldHold("TestParse: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestInterp()
{
  LogTest("TestInterp");

  VInterp* interp = CreateInterpWithPrimFuns();

  VItem* list;
  char*  code;

  code = "1 2 3 1 2 3 + + + + + (SUM) setglobal SUM dup print";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  InterpEval(interp, list);
  PrimFun_printstack(interp);
  ShouldHold(
    "TestInterp: top of stack should be 12",
    12 == ItemGetIntNum(InterpStackTop(interp)));

  InterpFree(interp);

  ShouldHold("TestInterp: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestFun()
{
  LogTest("TestFun");

  VInterp* interp = CreateInterpWithPrimFuns();

  VItem* list;
  char*  code;

  // First test
  code = "(foo) (2 *) def 21 foo";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  InterpEval(interp, list);
  PrimFun_printstack(interp);
  ShouldHold(
    "TestFun: top of stack should be 42",
    42 == ItemGetIntNum(InterpStackTop(interp)));

  // Second test
  code = "(foo) (bar) def (foo) ({foo}) def foo";
  list = InterpParse(interp, code);
  InterpEval(interp, list);
  PrimFun_printstack(interp);
  ShouldHold(
    "TestFun: top of stack should be {foo}",
    StrEquals("foo", ItemGetString(InterpStackTop(interp))));

  InterpFree(interp);

  ShouldHold("TestFun: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestNth()
{
  LogTest("TestNth");

  VInterp* interp = CreateInterpWithPrimFuns();

  VItem* list;
  char*  code;

  // First test
  code = "(1 2 3) 1 nth";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  InterpEval(interp, list);
  PrimFun_printstack(interp);
  ShouldHold(
    "TestNth: top of stack should be 2",
    2 == ItemGetIntNum(InterpStackTop(interp)));

  // Second test
  code = "(1 2 3) dup 1 42 setnth 1 nth";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  InterpEval(interp, list);
  PrimFun_printstack(interp);
  ShouldHold(
    "TestNth: top of stack should be 42",
    42 == ItemGetIntNum(InterpStackTop(interp)));

  InterpFree(interp);

  ShouldHold("TestFun: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestFib()
{
  LogTest("TestFib");

  VInterp* interp = CreateInterpWithPrimFuns();

  VItem* list;
  char*  code;

  code = "(fib) (dup 1 printstack > (dup 1- fib swap 2- fib +) iftrue) def 8 fib";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  InterpEval(interp, list);
  PrimFun_printstack(interp);
  ShouldHold(
    "TestFib: top of stack should be 21",
    21 == ItemGetIntNum(InterpStackTop(interp)));

  InterpFree(interp);

  ShouldHold("TestFib: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

void TestFibBig()
{
  LogTest("TestFibBig");

  VInterp* interp = CreateInterpBig();

  VItem* list;
  char*  code;

  code = "(fib) (dup 1 > (dup 1- fib swap 2- fib +) iftrue) def 37 fib";
  list = InterpParse(interp, code);
  InterpPrint(interp, list);
  InterpEval(interp, list);
  PrimFun_printstack(interp);
  /*ShouldHold(
    "TestFib: top of stack should be 21",
    21 == ItemGetIntNum(InterpStackTop(interp)));*/

  InterpFree(interp);

  ShouldHold("TestFibBig: GlobAllocCounter should be zero", 0 == SysGetAllocCounter());
}

// -------------------------------------------------------------
// Main
// -------------------------------------------------------------

int main()
{
  PrintLine("WELCOME TO VIMANACODE TESTS");

  TestItemAttributes();
  TestAlloc();
  TestItemNext();
  TestAllocDealloc();
  TestSetFirst();
  TestGetString();
  TestStringReference();
  TestStringMem();
  TestSymbolTable();
  TestCreatePrimFuns();
  TestPrintItem();
  TestParse();
  TestInterp();
  TestFun();
  TestNth();
  TestFib();
  TestFibBig();

  PrintSeparator();
  SysPrintMemStat();

  PrintSeparator();
  PrintTestResult();

  return 0;
}

// -------------------------------------------------------------
// Test helpers
// -------------------------------------------------------------

VInterp* CreateInterpNoPrimFuns()
{
  VInterp* interp = InterpAlloc(
    20, // SYMBOL_TABLE_SIZE
    10, // DATA_STACK_SIZE,
    10, // CALL_STACK_SIZE
    20  // LIST_MEMORY_SIZE
  );
  return interp;
}

VInterp* CreateInterpWithPrimFuns()
{
  VInterp* interp = InterpAlloc(
    100, // SYMBOL_TABLE_SIZE
    10,  // DATA_STACK_SIZE,
    10,  // CALL_STACK_SIZE
    100  // LIST_MEMORY_SIZE
  );
  InterpAddCorePrimFuns(interp);
  InterpPrintNumberOfPrimFuns(interp);
  return interp;
}

VInterp* CreateInterpBig()
{
  VInterp* interp = InterpAlloc(
    100, // SYMBOL_TABLE_SIZE
    100, // DATA_STACK_SIZE,
    100, // CALL_STACK_SIZE
    100  // LIST_MEMORY_SIZE
  );
  InterpAddCorePrimFuns(interp);
  InterpPrintNumberOfPrimFuns(interp);
  return interp;
}

VItem* InterpAllocMaxItems(VInterp* interp, int start)
{
  // Alloc items until out of memory

  //PrintLine("Alloc max items");

  VItem* first = NULL;
  int counter = start;

  VItem* item = InterpAllocItem(interp);
  while (item)
  {
    printf("ALLOC COUNTER: %i\n", counter);
    ItemSetIntNum(item, counter ++);

    if (first)
    {
      ItemSetNextItem(item, first);
    }

    first = item;

    item = InterpAllocItem(interp);
  }
PrintLine("RETURN");
  return first;
}

void InterpPrintItems(VInterp* interp, VItem* first)
{
  while (first)
  {
    PrintIntNum(ItemGetIntNum(first));
    Print(" ");
    first = ItemGetNextItem(first);
  }
  PrintNewLine();
}

int InterpCountItems(VInterp* interp, VItem* first)
{
  int counter = 0;
  while (first)
  {
    ++ counter;
    first = ItemGetNextItem(first);
  }
  return counter;
}

void ShouldHold(char* description, int condition)
{
  if (!condition)
  {
    ++ GlobalNumFailedTests;
    printf("[SHOULD_HOLD] %s\n", description);
  }
}

void PrintTestResult()
{
  if (GlobalNumFailedTests > 0)
    printf("FAILED TESTS: %i\n", GlobalNumFailedTests);
  else
    printf("ALL TESTS PASS\n");
}

void PrintSeparator()
{
  PrintLine("------------------------------------------------------------");
}

void LogTest(char* testName)
{
  PrintNewLine();
  PrintSeparator();
  PrintLine(testName);
  PrintSeparator();
}

// -------------------------------------------------------------
// Print functions
// -------------------------------------------------------------

void PrintBinaryUShort(unsigned short n)
{
  int numBits = sizeof(n) * 8;
  for (long i = numBits - 1 ; i >= 0; --i)
  {
    PrintChar(n & (1L << i) ? '1' : '0');
  }
  PrintNewLine();
}

void PrintBinaryUInt(unsigned int n)
{
  int numBits = sizeof(n) * 8;
  for (long i = numBits - 1 ; i >= 0; --i)
  {
    PrintChar(n & (1L << i) ? '1' : '0');
  }
  PrintNewLine();
}

void PrintBinaryULong(unsigned long n)
{
  int numBits = sizeof(n) * 8;
  for (long i = numBits - 1 ; i >= 0; --i)
  {
    PrintChar(n & (1L << i) ? '1' : '0');
  }
  PrintNewLine();
}
