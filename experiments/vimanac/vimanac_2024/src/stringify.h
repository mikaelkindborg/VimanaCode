/*
File: stringify.h
Author: Mikael Kindborg (mikael@kindborg.com)

Functions for printing lists and items.
*/

// -------------------------------------------------------------
// Print lists and items
// -------------------------------------------------------------

void StringifyList(VInterp* interp, VItem* list);

void StringifyChar(VInterp* interp, char c)
{
  StringMemWriteChar(InterpStringMem(interp), c);
}

void StringifyString(VInterp* interp, char* string)
{
  char* p = string;
  while ('\0' != *p)
  {
    StringMemWriteChar(InterpStringMem(interp), *p);
    ++ p;
  }
}

void StringifyIntNum(VInterp* interp, VInt num)
{
  char buf[32];
  sprintf(buf, "%ld", (long)num);
  StringifyString(interp, buf);
}

void StringifyVimanaString(VInterp* interp, char* string)
{
  StringMemWriteChar(InterpStringMem(interp), '{');
  char* p = string;
  while ('\0' != *p)
  {
    StringMemWriteChar(InterpStringMem(interp), *p);
    ++ p;
  }
  StringMemWriteChar(InterpStringMem(interp), '}');
}

void StringifyItem(VInterp* interp, VItem* item)
{
  if (IsList(item))
  {
    StringifyList(interp, item);
  }
  else if (IsTypeIntNum(item))
  {
    StringifyIntNum(interp, item->intNum);
  }
  else if (IsTypePrimFun(item))
  {
    char* string = SymbolTableFindPrimFunName(
      InterpSymbolTable(interp), ItemGetPtr(item));
    StringifyString(interp, string);
  }
  else if (IsTypeSymbol(item))
  {
    char* string = GlobalVarGetName(item);
    StringifyString(interp, string);
  }
  else if (IsTypeString(item))
  {
    char* string = ItemGetString(item);
    StringifyVimanaString(interp, string);
  }
  else if (IsTypeHandle(item))
  {
    StringifyString(interp, "[HANDLE]");
  }
  else
  {
    GURU_MEDITATION(GURU_PRINT_ITEM_UNKNOWN_TYPE);
  }
}

void StringifyList(VInterp* interp, VItem* list)
{
  StringifyChar(interp, '(');

  VBool printSpace = FALSE;

  VItem* item = ItemGetFirstItem(list);
  while (item)
  {
    if (printSpace) { StringifyChar(interp, ' '); }
    StringifyItem(interp, item);
    item = ItemGetNextItem(item);
    printSpace = TRUE;
  }

  StringifyChar(interp, ')');
}

//
// InterpStringify returns a pointer to a zero-terminated
// (temporary) string. The string memory area is used as a
// temporary buffer.
//
// The returned string must be kept or dropped before
// next call to InterpStringify. Keeping strings can
// use up memory quickly. To keep the string it is better
// to make a copy of it, and then drop it.
//
// To drop the string call:
//   StringMemReset(InterpStringMem(interp));
//
// To keep the string call:
//   StringMemWriteFinish(InterpStringMem(interp));
//
// Example:
//   char* string = InterpStringify(interp, list);
//   PrintLine(string);
//   StringMemReset(InterpStringMem(interp));
//
char* InterpStringify(VInterp* interp, VItem* item)
{
  char* string = StringMemGetNextFree(InterpStringMem(interp));
  StringifyItem(interp, item);
  return string;
}

// Print an item or a list
void InterpPrint(VInterp* interp, VItem* item)
{
  char* string = InterpStringify(interp, item);
  PrintLine(string);
  StringMemReset(InterpStringMem(interp));
}
