#ifndef __VIMANA_H___
#define __VIMANA_H___

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "base.h"
#include "gurumeditation.h"
#include "alloc.h"
#include "string.h"
#include "item.h"
#include "symboltable.h"
#include "stringmem.h"
#include "datastack.h"
#include "callstack.h"
#include "listmem.h"
#include "interp.h"
#include "parser.h"
#include "stringify.h"
#include "primfuns.h"

#endif