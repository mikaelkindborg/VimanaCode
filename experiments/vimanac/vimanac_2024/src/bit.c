#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//#include <openssl/ssl.h>
//#include <openssl/err.h>
//#include <curl/curl.h>

int main(int argc, char* args[])
{
  FILE* stream;
  int c;

  stream = popen("pbpaste", "r");
  if (!stream)
  {
      printf("ERROR popen\n");
      return -1;
  }

  while ((c = fgetc(stream)) != EOF)
  {
    putchar(c);
  }

  putchar('\n');

  pclose(stream);

  exit(0);
}

int xmain(int argc, char* args[])
{
  uint64_t counter;
  uint64_t bits;
  uint64_t mask;
  uint64_t result;
  uint8_t  byte;
  uint8_t* bytep;

  printf("sizeof(long) : %lu\n", sizeof(long));

  printf("NULL == 0.0  : %i\n", NULL == (int)0.0);
  printf("NULL == 0    : %i\n", NULL == 0);

  exit(0);

/*
// Test of define and include

#define FOOBAR "Foobar1"
#include "bit.h"
#undef FOOBAR
#define FOOBAR "Foobar2"
#include "bit.h"
#undef FOOBAR
#define FOOBAR "Foobar3"
#include "bit.h"
#undef FOOBAR

#define MYTYPE int
MYTYPE n;
MYTYPE* pn;
pn = &n;
*pn = 42;
printf("n is %i\n", n);

exit(0);
*/

/*
  bits = 511;
  printf("BITS : %llu\n", bits);
  byte = bits >> 8;
  printf("BYTE1: %u\n", byte);
  byte = bits;
  printf("BYTE2: %u\n", byte);
  printf("----------------\n");

  bytep = (uint8_t*) &bits;
  printf("BYTE1: %u\n", *bytep);
  ++bytep;
  printf("BYTE2: %u\n", *bytep);
  *bytep = 255;
  printf("BITS : %llu\n", bits);
*/

  srand((unsigned int)time(0));

  uint64_t  buf[1024];
  uint64_t* pbuf;
  uint64_t  value;
  //int       index = 912;
  int       index = rand() % 1024;

/*
  pbuf = buf + index;
  for (counter = 0; counter < 0x00FFFFFFFF; ++counter)
  {
    //index = time(0) % 1024;
    //index = rand() % 1024;
    //index = counter % 1024;
    pbuf = buf + index;
    value = *pbuf;
  }

  printf("VAL: %llu\n", value);
*/

// --------------------------------------------------------
// Test various operators

// cc bit.c -O0
// time ./a.out

  double dval;
  bits = 0x7FFFFFFFFFFFFFFF;
  mask = 0x8000000000000000;
  //bits = 0x0000000000000000;
  //mask = 0x0000000000000001;
  // 4294967295 iterations
  for (counter = 0; counter < 0x00FFFFFFFF; ++counter)
  {
      value = bits & mask;                    // 2.74s
    //value = bits | mask;                    // 2.74s
    //value = bits;                           // 2.74s
    //value = counter;                        // 3.08s
    //value = mask - counter;                 // 5.62s
    //dval = (double)mask - (double)counter;  // 6.10s
    //dval = dval - (double)counter;          // 12.17s
  }

  printf("VAL: %llu\n", value); // use with value
  //printf("DVAL: %f\n", dval); // use with dval

// --------------------------------------------------------

/*
  bits = 0xFFFFFFFF;
  mask = 0xFF;
  for (counter = 0; counter < 0x01FFFFFFFF; ++counter)
  {
    //result = bits & mask;
    result = bits >> 8;
  }

  printf("BITS: %llu\n", bits);
  printf("MASK: %llu\n", mask);
  printf("RES : %llu\n", result);
  printf("CNT : %llu\n", counter);
*/
/*
  bits = 0xFFFF;
  printf("NUM4: %llu\n", bits);
  bits = 0xFFFFFFFF;
  printf("NUM8: %llu\n", bits);
  bits = 0xFFFFFFFFFFFFFFFF;
  printf("NUMX: %llu\n", bits);
*/
}