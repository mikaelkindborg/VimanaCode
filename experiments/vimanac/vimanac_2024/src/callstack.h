/*
File: callstack.h
Author: Mikael Kindborg (mikael@kindborg.com)

Call stack functions.
*/

// -------------------------------------------------------------
// Call stack structs
// -------------------------------------------------------------

typedef struct __VStackFrame
{
  //VItem* code;       // List being evaluated
  VItem* instruction;  // Current instruction (a list item)
}
VStackFrame;

typedef struct __VCallStack
{
  VByte* start;        // Start of callstack
  VByte* end;          // End of callstack
  VByte* top;          // Current stackframe
}
VCallStack;

// -------------------------------------------------------------
// Call stack functions
// -------------------------------------------------------------

#define CallStackTop(callStack) \
  ((VStackFrame*)((callStack)->top))

#define CallStackIsEmpty(callStack) \
  ((callStack)->top < (callStack)->start)

void CallStackInit(VCallStack* callStack, VByte* start, int byteSize)
{
  callStack->start = start;
  callStack->end = start + byteSize;
  callStack->top = start - sizeof(VStackFrame);
}

// Push new stackframe with code list (no tailcall)
void CallStackPushNewStackFrame(VCallStack* callStack, VItem* list)
{
  // Add new stackframe
  callStack->top += sizeof(VStackFrame);
  VStackFrame* current = CallStackTop(callStack);

  // Set first instruction in the frame
  current->instruction = ItemGetFirstItem(list);

  // Set code list
  // TODO: current->code = list;
}

// Push a new stackframe if needed, reuse top stackframe on tailcall
void CallStackPush(VCallStack* callStack, VItem* list)
{
  // The current stackframe is the parent for the new stackframe
  VStackFrame* parent = CallStackTop(callStack);

  // Assume tailcall (resuse parent stackframe)
  VStackFrame* current = parent;

  // Check tailcall (are there any instructions left?)
  if (parent->instruction)
  {
    // NOT A TAILCALL - PUSH NEW STACK FRAME

    callStack->top += sizeof(VStackFrame);

    if (callStack->top >= callStack->end)
    {
      GURU_MEDITATION(GURU_CALLSTACK_OVERFLOW);
    }

    // Set new stackframe
    current = CallStackTop(callStack);
  }

  // Set instruction and code list (stackframe is reused on tailcall)
  current->instruction = ItemGetFirstItem(list);
  // TODO: current->code = list;
}

// Pop stackframe
void CallStackPop(VCallStack* callStack)
{
  if (CallStackIsEmpty(callStack))
  {
    GURU_MEDITATION(GURU_CALLSTACK_IS_EMPTY);
  }

  callStack->top -= sizeof(VStackFrame);
}
