/*
File: primfuns_sdl.h
Author: Mikael Kindborg (mikael@kindborg.com)

Primfuns for SDL2

Building/linking:
https://wiki.libsdl.org/SDL2/README/macos
https://gist.github.com/cicanci/b54715298ab55dff2fbcd0ca3829d13b
https://discourse.libsdl.org/t/building-sdl2-on-os-x-the-unix-way/27136

Load images:
https://github.com/lvandeve/lodepng
https://gigi.nullneuron.net/gigilabs/tag/sdl2/page/3/
https://github.com/serge-rgb/TinyJPEG/
https://wiki.libsdl.org/SDL2/CategoryAPI
https://wiki.libsdl.org/SDL2/FrontPage
https://lazyfoo.net/tutorials/SDL/index.php
https://github.com/nothings/stb
https://www.silbinarywolf.com/post/124379907558/loading-png-files-with-stbimage-and-sdl2

Cool articles:
https://zserge.com/posts/too-many-forths/
https://zserge.com/posts/fenster/


#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

SDL_Surface *LoadImage(char* filename)
{
	// Read data
	int32 width, height, bytesPerPixel;
	void* data = stbi_load(filename, &width, &height, &bytesPerPixel, 0);

	// Calculate pitch
	int pitch;
    pitch = width * bytesPerPixel;
    pitch = (pitch + 3) & ~3;

    // Setup relevance bitmask
	int32 Rmask, Gmask, Bmask, Amask;
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
    Rmask = 0x000000FF;
    Gmask = 0x0000FF00;
    Bmask = 0x00FF0000;
    Amask = (bytesPerPixel == 4) ? 0xFF000000 : 0;
#else
    int s = (bytesPerPixel == 4) ? 0 : 8;
    Rmask = 0xFF000000 >> s;
    Gmask = 0x00FF0000 >> s;
    Bmask = 0x0000FF00 >> s;
    Amask = 0x000000FF >> s;
#endif
	SDL_Surface* surface = SDL_CreateRGBSurfaceFrom(data, width, height, bytesPerPixel*8, pitch, Rmask, Gmask,
                             Bmask, Amask);
	if (!surface)
	{
		// NOTE: Should free stbi_load 'data' variable here
		return NULL;
	}
	return surface;
}

object
  properties

fact
  n

compute
  if n == 0
    result = 1
  else
    f = new fact
    f.n = n - 1
    f.compute
    result = n * f.result
    f.free

*/

enum GURU_SDL
{
  GURU_SDL_INIT_ERROR = 2000,
  GURU_SDL_CREATE_WINDOW_ERROR,
  GURU_SDL_ITEM_IS_NULL,
  GURU_SDL_ITEM_IS_NOT_LIST,
  GURU_SDL_CANNOT_LOAD_IMAGE_FILE,
  GURU_SDL_FUBAR
};

#define SDL_ItemMustNotBeNull(c) \
  if (NULL == c) { GURU_MEDITATION(GURU_SDL_ITEM_IS_NULL); }

// Global event data struct
static SDL_Event SDL_EventData;

// sdlInit ->
void PrimFun_sdlInit(VInterp* interp)
{
  if (SDL_Init(SDL_INIT_VIDEO) < 0)
  {
    GURU_MEDITATION(GURU_SDL_INIT_ERROR);
  }
}

// width height sdlCreateWindow -> windowRef
void PrimFun_sdlCreateWindow(VInterp* interp)
{
  SDL_Window* window = NULL;

  VItem* height = InterpStackPop(interp);
  VItem* width = InterpStackPop(interp);

  window = SDL_CreateWindow
  (
    "Vimana SDL",
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    ItemGetIntNum(width),
    ItemGetIntNum(height),
    SDL_WINDOW_SHOWN
  );

  if (NULL == window)
  {
    GURU_MEDITATION(GURU_SDL_CREATE_WINDOW_ERROR);
  }

  VItem windowRef;
  ItemInit(&windowRef);
  ItemSetType(&windowRef, TypeIntNum);
  ItemSetPtr(&windowRef, window);

  InterpStackPush(interp, &windowRef);
}

// windowRef sdlDestroyWindow ->
void PrimFun_sdlDestroyWindow(VInterp* interp)
{
  VItem* windowRef = InterpStackPop(interp);
  SDL_DestroyWindow(ItemGetPtr(windowRef));
}

// windowRef sdlWindowFullScreen ->
void PrimFun_sdlWindowFullScreen(VInterp* interp)
{
  VItem* windowRef = InterpStackPop(interp);
  SDL_SetWindowFullscreen(ItemGetPtr(windowRef), SDL_WINDOW_FULLSCREEN_DESKTOP);
}

// windowRef sdlWindowNormal ->
void PrimFun_sdlWindowNormal(VInterp* interp)
{
  VItem* windowRef = InterpStackPop(interp);
  SDL_SetWindowFullscreen(ItemGetPtr(windowRef), SDL_WINDOW_FULLSCREEN_DESKTOP);
}

// sdlScreenWidth -> width
void PrimFun_sdlScreenWidth(VInterp* interp)
{
  SDL_DisplayMode mode;
  SDL_GetDesktopDisplayMode(0, &mode);
  VItem item;
  ItemInit(&item);
  ItemSetIntNum(&item, mode.w);
  InterpStackPush(interp, &item);
}

// sdlScreenHeight -> width
void PrimFun_sdlScreenHeight(VInterp* interp)
{
  SDL_DisplayMode mode;
  SDL_GetDesktopDisplayMode(0, &mode);
  VItem item;
  ItemInit(&item);
  ItemSetIntNum(&item, mode.h);
  InterpStackPush(interp, &item);
}

// windowRef (x y width height) (red green blue) sdlFillRect ->
void PrimFun_sdlFillRect(VInterp* interp)
{
  VItem* color = InterpStackPop(interp);
  VItem* rect = InterpStackPop(interp);
  VItem* windowRef = InterpStackPop(interp);

  ItemMustBeList(color, GURU_SDL_ITEM_IS_NOT_LIST);
  ItemMustBeList(rect, GURU_SDL_ITEM_IS_NOT_LIST);

  // Get color values
  VItem* c = ItemGetFirstItem(color);
  SDL_ItemMustNotBeNull(c);
  Uint8 red = ItemGetIntNum(c);
  c = ItemGetNextItem(c);
  SDL_ItemMustNotBeNull(c);
  Uint8 green = ItemGetIntNum(c);
  c = ItemGetNextItem(c);
  SDL_ItemMustNotBeNull(c);
  Uint8 blue = ItemGetIntNum(c);

  // Set rect
  SDL_Rect sdlRect;
  VItem* r = ItemGetFirstItem(rect);
  SDL_ItemMustNotBeNull(r);
  sdlRect.x = ItemGetIntNum(r);
  r = ItemGetNextItem(r);
  SDL_ItemMustNotBeNull(r);
  sdlRect.y = ItemGetIntNum(r);
  r = ItemGetNextItem(r);
  SDL_ItemMustNotBeNull(r);
  sdlRect.w = ItemGetIntNum(r);
  r = ItemGetNextItem(r);
  SDL_ItemMustNotBeNull(r);
  sdlRect.h = ItemGetIntNum(r);

  SDL_Surface* screenSurface = SDL_GetWindowSurface(ItemGetPtr(windowRef));

  Uint32 rgb = SDL_MapRGB(screenSurface->format, red, green, blue);

  SDL_FillRect(screenSurface, &sdlRect, rgb);

  SDL_UpdateWindowSurface(ItemGetPtr(windowRef));
}

// filepath sdlLoadImage -> imageRef
void PrimFun_sdlLoadImage(VInterp* interp)
{
  VItem* filepath = InterpStackPop(interp);

  // Load image
  SDL_Surface* bitmap = SDL_LoadBMP(ItemGetString(filepath));
  if (NULL == bitmap)
  {
    GURU_MEDITATION(GURU_SDL_CANNOT_LOAD_IMAGE_FILE);
  }

  VItem imageRef;
  ItemInit(&imageRef);
  ItemSetPtr(&imageRef, bitmap);
  InterpStackPush(interp, &imageRef); // copies imageRef
}

// imageRef sdlFreeImage ->
void PrimFun_sdlFreeImage(VInterp* interp)
{
  VItem* imageRef = InterpStackPop(interp);

  SDL_FreeSurface(ItemGetPtr(imageRef));
}

// Display an image in a window scaled to destRect
// windowRef imageRef destRect sdlShowImage ->
// Example:
// windowRef imageRef (x y width height) sdlShowImage ->
// Empty destRest means stretch to window:
// windowRef imageRef () sdlBlitImage ->
void PrimFun_sdlBlitImage(VInterp* interp)
{
  VItem* destRect = InterpStackPop(interp);
  VItem* imageRef = InterpStackPop(interp);
  VItem* windowRef = InterpStackPop(interp);

  ItemMustBeList(destRect, GURU_SDL_ITEM_IS_NOT_LIST);

  SDL_Rect  sdlRect;
  SDL_Rect* sdlDestRect;

  if (IsEmpty(destRect))
  {
    sdlDestRect = NULL;
  }
  else
  {
    sdlDestRect = &sdlRect;

    // Set rect
    VItem* r = ItemGetFirstItem(destRect);
    SDL_ItemMustNotBeNull(r);
    sdlRect.x = ItemGetIntNum(r);
    r = ItemGetNextItem(r);
    SDL_ItemMustNotBeNull(r);
    sdlRect.y = ItemGetIntNum(r);
    r = ItemGetNextItem(r);
    SDL_ItemMustNotBeNull(r);
    sdlRect.w = ItemGetIntNum(r);
    r = ItemGetNextItem(r);
    SDL_ItemMustNotBeNull(r);
    sdlRect.h = ItemGetIntNum(r);
  }

  // Blit image
  SDL_Surface* screenSurface = SDL_GetWindowSurface(ItemGetPtr(windowRef));
  SDL_BlitScaled(ItemGetPtr(imageRef), NULL, screenSurface, sdlDestRect);
  SDL_UpdateWindowSurface(ItemGetPtr(windowRef));
}

// sdlPollEvent -> eventDataRef
void PrimFun_sdlPollEvent(VInterp* interp)
{
  SDL_PollEvent(&SDL_EventData);
/*
  VItem eventDataRef;
  ItemInit(&eventDataRef);
  ItemSetType(&eventDataRef, TypeIntNum);
  ItemSetPtr(&eventDataRef, &eventData);
  InterpStackPush(interp, &eventDataRef);
*/
}

// eventDataRef sdlIsQuitEvent -> boolean
void PrimFun_sdlIsQuitEvent(VInterp* interp)
{
  /*
  VItem* eventDataRef = InterpStackTop(interp);
  SDL_Event* eventData = ItemGetPtr(eventDataRef);
  eventDataRef->intNum = (SDL_QUIT == (eventData->type));
  */

  VItem item;
  ItemInit(&item);
  ItemSetIntNum(&item, SDL_QUIT == SDL_EventData.type);
  InterpStackPush(interp, &item);
}

void PrimFun_sdlIsMouseDownEvent(VInterp* interp)
{
  VItem item;
  ItemInit(&item);
  ItemSetIntNum(&item, SDL_MOUSEBUTTONDOWN == SDL_EventData.type);
  InterpStackPush(interp, &item);
}

void PrimFun_sdlIsMouseUpEvent(VInterp* interp)
{
  VItem item;
  ItemInit(&item);
  ItemSetIntNum(&item, SDL_MOUSEBUTTONUP == SDL_EventData.type);
  InterpStackPush(interp, &item);
}

void PrimFun_sdlMouseX(VInterp* interp)
{
  int x;
  int y;
  SDL_GetMouseState(&x, &y);
  VItem item;
  ItemInit(&item);
  ItemSetIntNum(&item, x);
  InterpStackPush(interp, &item);
}

void PrimFun_sdlMouseY(VInterp* interp)
{
  int x;
  int y;
  SDL_GetMouseState(&x, &y);
  VItem item;
  ItemInit(&item);
  ItemSetIntNum(&item, y);
  InterpStackPush(interp, &item);
}

// millis sdlDelay ->
void PrimFun_sdlDelay(VInterp* interp)
{
  VItem* delay = InterpStackPop(interp);

  SDL_Delay(delay->intNum);
}

// sdlQuit ->
void PrimFun_sdlQuit(VInterp* interp)
{
  SDL_Quit();
}

void InterpAddSDLPrimFuns(VInterp* interp)
{
  InterpAddPrimFun(interp, "sdlInit", PrimFun_sdlInit);
  InterpAddPrimFun(interp, "sdlCreateWindow", PrimFun_sdlCreateWindow);
  InterpAddPrimFun(interp, "sdlDestroyWindow", PrimFun_sdlDestroyWindow);
  InterpAddPrimFun(interp, "sdlWindowFullScreen", PrimFun_sdlWindowFullScreen);
  InterpAddPrimFun(interp, "sdlWindowNormal", PrimFun_sdlWindowNormal);
  InterpAddPrimFun(interp, "sdlScreenWidth", PrimFun_sdlScreenWidth);
  InterpAddPrimFun(interp, "sdlScreenHeight", PrimFun_sdlScreenHeight);
  InterpAddPrimFun(interp, "sdlFillRect", PrimFun_sdlFillRect);
  InterpAddPrimFun(interp, "sdlLoadImage", PrimFun_sdlLoadImage);
  InterpAddPrimFun(interp, "sdlFreeImage", PrimFun_sdlFreeImage);
  InterpAddPrimFun(interp, "sdlBlitImage", PrimFun_sdlBlitImage);
  InterpAddPrimFun(interp, "sdlPollEvent", PrimFun_sdlPollEvent);
  InterpAddPrimFun(interp, "sdlIsQuitEvent", PrimFun_sdlIsQuitEvent);
  InterpAddPrimFun(interp, "sdlIsMouseDownEvent", PrimFun_sdlIsMouseDownEvent);
  InterpAddPrimFun(interp, "sdlIsMouseUpEvent", PrimFun_sdlIsMouseUpEvent);
  InterpAddPrimFun(interp, "sdlMouseX", PrimFun_sdlMouseX);
  InterpAddPrimFun(interp, "sdlMouseY", PrimFun_sdlMouseY);
  InterpAddPrimFun(interp, "sdlDelay", PrimFun_sdlDelay);
  InterpAddPrimFun(interp, "sdlQuit", PrimFun_sdlQuit);
}
