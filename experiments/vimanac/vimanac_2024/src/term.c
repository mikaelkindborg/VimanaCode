#include "unix/terminal.h"

int main()
{
  //atexit(termExit);

  TermScreenSetCommodore64EditMode();

  TermScreenClear();
  TermCursorHome();

  // Interactive programming based on the clipboard COPY command key
  // A new take on dynamic programming environments
  // A new take on the REPL - the editor is the REPL
  // Commodore 64 inspired
  // Interactive programming with the clipboard
  // Unconventional retro style programming in a DIY language
  // Make primfun or Vimana fun watchclipboard - you can watch the
  // clipboard from any part of the program!

  TermWrite("Welcome to the Vimana Workbench!\r\n");
  TermWrite("Here you can evaluate Vimana code interactively.\r\n");
  TermWrite("Select some code with the mouse and press COMMAND-C.\r\n");
  TermWrite("Then select the word DOIT and press COMMAND-C.\r\n");
  TermWrite("The resulting data stack is printed to the screen.\r\n");
  TermWrite("Note that DOIT is a workbench command, it is not part\r\n");
  TermWrite("of the Vimana language. Code can be copied in any editor.\r\n");
  TermWrite("Copy QUIT to exit, or press CTRL-Q or CTRL-C.\r\n");
  TermWrite("Now copy a code snippet, then copy the word DOIT\r\n");
  TermWrite("\r\n");
  TermWrite("1 2 +\r\n");
  TermWrite("drop\r\n");
  TermWrite("(hi) ({Hi World} print) def\r\n");
  TermWrite("hi\r\n");
  TermWrite("0 (counter) setglobal\r\n");
  TermWrite("(loop) (\r\n");
  TermWrite("  counter print\r\n");
  TermWrite("  counter 1 + (counter) setglobal\r\n");
  TermWrite("  1000 sleep\r\n");
  TermWrite("  pasteboard {STOP} eq (loop) iffalse\r\n");
  TermWrite(") def\r\n");
  TermWrite("loop\r\n");

/*
0 (counter) setglobal
(loop) (
  counter print
  counter 1 + (counter) setglobal
  1000 sleep
  pasteboard {STOP} eq not (loop) iftrue
) def
loop
*/

  int c = TermReadChar();
  while (KEY_CTRL_C != c && KEY_CTRL_Q != c)
  {
    usleep(1000);
    if (c > 0)
    {
      printf("%i\r\n", c);
/*
      if (!TermMoveCursor(c))
      {
        unsigned char ch = (unsigned char)c;
        TermWriteChar(&ch);
        //printf("FOO\n");
        //printf("%c", c);
        //fflush(stdout);
      }
*/
    }
    c = TermReadChar();
  }

  TermScreenSetStandardMode();

  printf("\n");

  return 0;
}
