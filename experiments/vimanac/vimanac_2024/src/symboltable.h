/*
File: symboltable.h
Author: Mikael Kindborg (mikael@kindborg.com)

Global symbol table (global variables).
*/

// -------------------------------------------------------------
// Symbol table struct
// -------------------------------------------------------------

typedef struct __VSymbolEntry
{
  char* symbol;        // name of the symbol
  VItem value;         // item that holds the symbol value
}
VSymbolEntry;

typedef struct __VSymbolTable
{
  VByte* start;        // Start of symbol table
  VByte* end;          // End of symbol table
  VByte* top;          // Next free entry in symbol table
}
VSymbolTable;

// -------------------------------------------------------------
// Symbol table functions
// -------------------------------------------------------------

void SymbolTableInit(VSymbolTable* table, VByte* start, int byteSize)
{
  table->start = start;
  table->end = start + byteSize;
  table->top = start - sizeof(VSymbolEntry);
}

void* SymbolTableAddEntry(VSymbolTable* table, char* name)
{
  // Add symbol entry
  table->top += sizeof(VSymbolEntry);
  
  if (table->top >= table->end)
  {
    GURU_MEDITATION(GURU_SYMBOL_TABLE_OUT_OF_MEMORY);
  }

  // Set symbol name
  VSymbolEntry* entry = (VSymbolEntry*)(table->top);
  entry->symbol = name;

  // Important to set type to None (see interp.h in function 
  // InterpEval() where IsTypeNone() is called to check
  // that a symbol is not unbound)
  ItemSetNone(&(entry->value));

  return entry;
}

VSymbolEntry* SymbolTableFindEntry(VSymbolTable* table, char* name)
{
  for (VByte* p = table->start; p <= table->top; p += sizeof(VSymbolEntry))
  {
    VSymbolEntry* entry = (VSymbolEntry*)(p);
    if (StrEquals(entry->symbol, name))
    {
      return entry; // Found symbol
    }
  }

  return NULL; // Symbol not found
}

// Search for entry that has the given function pointer
char* SymbolTableFindPrimFunName(VSymbolTable* table, VPrimFunPtr primFunPtr)
{
  for (VByte* p = table->start; p <= table->top; p += sizeof(VSymbolEntry))
  {
    VSymbolEntry* entry = (VSymbolEntry*)(p);
    if (IsTypePrimFun(&(entry->value)) && ((entry->value).primFunPtr == primFunPtr))
    {
      return entry->symbol; // Found name
    }
  }

  return NULL; // Name not found
}

// Helper function that iterates over the symbol entries
// and perform some action (printing, counting)

#define SYMBOLTABLE_COUNT_PRIMFUNS 1
#define SYMBOLTABLE_COUNT_GLOBALS  2
#define SYMBOLTABLE_PRINT_PRIMFUNS 3
#define SYMBOLTABLE_PRINT_GLOBALS  4

// Perform action and return counter value
int SymbolTableEnumerate(VSymbolTable* table, int action)
{
  int numPrimFuns = 0;
  int numGlobals = 0;

  VByte* p;
  for (p = table->start;
       p <= table->top;
       p += sizeof(VSymbolEntry))
  {
    VSymbolEntry* entry = (VSymbolEntry*)(p);
    if (IsTypePrimFun(&(entry->value)))
    {
      if (SYMBOLTABLE_PRINT_PRIMFUNS == action)
      {
        PrintLine(entry->symbol);
      }
      ++ numPrimFuns;
    }
    else
    {
      if (SYMBOLTABLE_PRINT_GLOBALS == action)
      {
        PrintLine(entry->symbol);
      }
      ++ numGlobals;
    }
  }

  if (SYMBOLTABLE_PRINT_GLOBALS == action)
  {
    Print("Number of globals: ");
    PrintIntNum(numGlobals);
    PrintNewLine();
    Print("Total number of symbols: ");
    PrintIntNum(numGlobals + numPrimFuns);
    PrintNewLine();
  }
  else
  if (SYMBOLTABLE_PRINT_PRIMFUNS == action)
  {
    Print("Number of primfuns: ");
    PrintIntNum(numPrimFuns);
    PrintNewLine();
  }

  return (SYMBOLTABLE_COUNT_PRIMFUNS == action) ?
    numPrimFuns : numGlobals;
}

int SymbolTableNumPrimFuns(VSymbolTable* table)
{
  return SymbolTableEnumerate(table, SYMBOLTABLE_COUNT_PRIMFUNS);
}

int SymbolTableNumGlobals(VSymbolTable* table)
{
  return SymbolTableEnumerate(table, SYMBOLTABLE_COUNT_GLOBALS);
}

void SymbolTablePrintPrimFuns(VSymbolTable* table)
{
  SymbolTableEnumerate(table, SYMBOLTABLE_PRINT_PRIMFUNS);
}

void SymbolTablePrintGlobals(VSymbolTable* table)
{
  SymbolTableEnumerate(table, SYMBOLTABLE_PRINT_GLOBALS);
}
