/*
File: parser.h
Author: Mikael Kindborg (mikael@kindborg.com)
*/

// Incremental block size for string allocations (malloc)
#define PARSER_STRING_BLOCK_SIZE 1024

#define IsLeftParen(c)   ('(' == (c))
#define IsRightParen(c)  (')' == (c))
#define IsStringBegin(c) ('{' == (c))
#define IsStringEnd(c)   ('}' == (c))
#define IsEndOfString(c) ('\0' == (c))
#define IsWhiteSpace(c) \
  (' ' == (c) || '\t' == (c) || '\n' == (c) || '\r' == (c))
#define IsWhiteSpaceOrSeparatorOrEndOfString(c) \
  (IsEndOfString(c) || IsLeftParen(c)  || IsStringBegin(c) || \
   IsWhiteSpace(c)  || IsRightParen(c) || IsStringEnd(c))

// Allocate a string using malloc
char* ParseString(char* p, char** next)
{
  int   level = 1;  // Nested string level
  int   index = 0;  // Current string index (string length)

  // Allocate string buffer (may grow)
  int   bufferCapacity = PARSER_STRING_BLOCK_SIZE;
  char* buf = SysAlloc(PARSER_STRING_BLOCK_SIZE);

  if (NULL == buf)
  {
    GURU_MEDITATION(GURU_PARSER_CANNOT_ALLOC_STRING);
  }

  // Source code position p now is at opening curly

  // Move past opening curly
  ++ p;

  while (!IsEndOfString(*p))
  {
    // Handle nested strings
    if (IsStringBegin(*p)) ++ level;
    if (IsStringEnd(*p))   -- level;

    // Check if this was the ending closing curly
    if (level <= 0) break;

    // Copy character to string
    buf[index] = *p;

    // Move to next character
    ++ p;
    ++ index;

    // Grow buffer if needed
    if (bufferCapacity < index + 1)
    {
      buf = realloc(buf, bufferCapacity + PARSER_STRING_BLOCK_SIZE);
      if (NULL == buf)
      {
        GURU_MEDITATION(GURU_PARSER_CANNOT_REALLOC_STRING);
      }
    }
  }

  // Terminate string and shrink to fit
  buf[index] = '\0';
  buf = realloc(buf, index + 1);

  // Move past closing curly
  *next = p + 1;

  // Position p is now at character after closing curly

  // Return string
  return buf;
}

// Get the token type (symbol or integer)
VType ParseTokenType(char* token)
{
  char* p = token;

  // We check for a number, if not a number it is a symbol

  // Check if the first character a minus sign
  if ('-' == *p)
  {
    // A single minus sign is not a number
    if (1 == strlen(token))
    {
      return TypeSymbol;
    }

    // Advance pointer
    ++ p;
  }

  // Token must have digits only
  while (!IsEndOfString(*p))
  {
    if (!isdigit(*p))
    {
      return TypeSymbol; // Not a number
    }

    // Advance pointer
    ++ p;
  }

  // Passed number check, it is a number
  return TypeIntNum;
}

VItem* ParseToken(VInterp* interp, char* token)
{
  VBool keepSymbolString = FALSE;

  VType  type = ParseTokenType(token);
  VItem* item = InterpAllocItem(interp);

  if (TypeIntNum == type)
  {
    // NUMBER

    VInt intNum = strtol(token, NULL, 10);
    ItemSetIntNum(item, intNum);

    // Free token
    StringMemReset(InterpStringMem(interp));
  }
  else
  // It is a symbol
  {
    // Does the symbol exist?
    VSymbolEntry* entry = SymbolTableFindEntry(InterpSymbolTable(interp), token);
    if (entry)
    {
      // SYMBOL EXISTS

      // Get the symbol entry
      VItem* value = &(entry->value);

      // Point the symbol to the symbol entry (global variable)
      ItemSetSymbol(item, entry);

      // Free token (already in string memory)
      StringMemReset(InterpStringMem(interp));
    }
    else
    {
      // NEW SYMBOL

      // Add token to string memory
      StringMemWriteFinish(InterpStringMem(interp));

      // Add it to the symbol table
      entry = SymbolTableAddEntry(InterpSymbolTable(interp), token);

      // Point the symbol to the entry (global variable)
      ItemSetSymbol(item, entry);
    }
  }

  return item;
}

// Read next token into string memory
char* ReadNextToken(VInterp* interp, char* p, char** next)
{
  char* token = StringMemGetNextFree(InterpStringMem(interp));

  while (!IsWhiteSpaceOrSeparatorOrEndOfString(*p))
  {
    StringMemWriteChar(InterpStringMem(interp), *p);
    ++ p;
  }

  *next = p;

  return token;
}

// A comment looks like this: /-- comment --/
#define IsComment(p)    (StrStartsWith(p, "/--"))
#define IsCommentEnd(p) (StrStartsWith(p, "--/"))

char* SkipComment(char* p)
{
  if (!IsComment(p))
  {
    return p; // Not a comment
  }

  // Move past opening comment
  p = p + 3;

  // Scan for end comment
  while (0 != *p)
  {
    if (IsCommentEnd(p))
    {
      return p + 3; // End of comment
    }

    ++ p;
  }

  return NULL; // End of string reached
}

VItem* ParseList(VInterp* interp, char* code, char** next)
{
  VItem* item  = NULL;
  VItem* prev  = NULL;

  // Allocate list head
  VItem* head = InterpAllocItem(interp);
  ItemSetType(head, TypeList);

  char* p = code;

  while (!IsEndOfString(*p))
  {
    if (IsWhiteSpace(*p))
    {
      // Skip whitespace
      ++ p;
      continue;
    }
    else
    if (IsLeftParen(*p))
    {
      // Parse child list
      item = ParseList(interp, p + 1, &p);
    }
    else
    if (IsRightParen(*p))
    {
      // Done parsing list
      ++ p;
      *next = p;
      break;
    }
    else
    if (IsStringBegin(*p))
    {
      // Handle takes ownership of string
      char* string = ParseString(p, &p);
      item = InterpAllocHandle(interp, string, BufferTypeString);
    }
    else
    if (IsStringEnd(*p))
    {
      GURU_MEDITATION(GURU_PARSER_UNBALANCED_STRING_END);
    }
    else
    if (IsComment(p))
    {
      p = SkipComment(p);
      if (NULL == p) { break; } // end of code string (unclosed comment)
      continue;
    }
    else
    {
      char* token = ReadNextToken(interp, p, &p);
      item = ParseToken(interp, token);
    }

    // TODO: Possibly check if item is NULL, but should not be needed

    // If there is no previous item, this is the first element
    if (NULL == prev)
    {
      ItemSetFirst(head, item);
    }
    else
    // Add item to tail
    {
      ItemSetNextItem(prev, item);
    }

    // Remember previous element
    prev = item;
  }
  // while

  return head;
}

VItem* InterpParse(VInterp* interp, char* code)
{
  char* p;
  return ParseList(interp, code, &p);
}
