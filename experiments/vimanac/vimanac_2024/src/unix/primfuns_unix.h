/*
File: primfuns_unix.h
Author: Mikael Kindborg (mikael@kindborg.com)

Primfuns for Unix/Linux/macOS

https://www.reddit.com/r/C_Programming/comments/144npye/when_two_different_processes_are_accessing_the/
https://www.gnu.org/software/libc/manual/html_node/File-Locks.html
https://www.ict.griffith.edu.au/teaching/2501ICT/archive/guide/ipc/flock.html
https://stackoverflow.com/questions/7573282/how-do-i-lock-files-using-fopen
https://picolisp.com/wiki/?ArrayAbstinence
*/

#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>

/*
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
*/

#include "file.h"
#include "unixfuns.h"

enum GURU_UNIX
{
  GURU_UNIX_READFILE_ARG_NOT_STRING = 1000,
  GURU_UNIX_READFILE_FILE_NOT_FOUND,
  GURU_UNIX_SETWORKINGDIR_ARG_NOT_STRING,
  GURU_UNIX_SETWORKINGDIR_DIRECTORY_DOES_NOT_EXIST,
  GURU_UNIX_SYSTEMCOMMAND_ARG_NOT_STRING,
  GURU_UNIX_SYSTEMCOMMAND_NOT_FOUND,
  GURU_UNIX_SYSTEMCOMMANDSEND_ARG1_NOT_STRING,
  GURU_UNIX_SYSTEMCOMMANDSEND_ARG2_NOT_STRING,
  GURU_UNIX_SYSTEMCOMMANDSEND_FAILED,
  GURU_UNIX_SLEEP_NOT_INTNUM,
  GURU_UNIX_RANDOM_NOT_INTNUM,
  GURU_UNIX_FUBAR
};

void PrimFun_readfile(VInterp* interp)
{
  VItem* handle = InterpStackPop(interp);
  if (!IsTypeString(handle)) GURU_MEDITATION(GURU_UNIX_READFILE_ARG_NOT_STRING);

  char* fileName = ItemGetString(handle);

  char* data = FileRead(fileName);

  if (NULL == data)
  {
    GURU_MEDITATION(GURU_UNIX_READFILE_FILE_NOT_FOUND);
  }

  // Alloc new handle with file contents
  handle = InterpAllocHandle(interp, data, BufferTypeString);
  InterpStackPush(interp, handle);
}

void PrimFun_setworkingdir(VInterp* interp)
{
  VItem* handle = InterpStackPop(interp);
  if (!IsTypeString(handle)) GURU_MEDITATION(GURU_UNIX_SETWORKINGDIR_ARG_NOT_STRING);

  char* dirName = ItemGetString(handle);
  VBool result = SetWorkingDir(dirName);

  if (FALSE == result)
  {
    GURU_MEDITATION(GURU_UNIX_SETWORKINGDIR_DIRECTORY_DOES_NOT_EXIST);
  }
}

void PrimFun_systemcommand(VInterp* interp)
{
  VItem* handle = InterpStackPop(interp);
  if (!IsTypeString(handle)) GURU_MEDITATION(GURU_UNIX_SYSTEMCOMMAND_ARG_NOT_STRING);

  char* command = ItemGetString(handle);

  char* data = SystemCommand(command);

  if (NULL == data)
  {
    GURU_MEDITATION(GURU_UNIX_SYSTEMCOMMAND_NOT_FOUND);
  }

  // Alloc new handle with file contents
  handle = InterpAllocHandle(interp, data, BufferTypeString);
  InterpStackPush(interp, handle);
}

void PrimFun_systemcommandsend(VInterp* interp)
{
  VItem* handle1 = InterpStackPop(interp);
  if (!IsTypeString(handle1)) GURU_MEDITATION(GURU_UNIX_SYSTEMCOMMANDSEND_ARG2_NOT_STRING);

  VItem* handle2 = InterpStackPop(interp);
  if (!IsTypeString(handle2)) GURU_MEDITATION(GURU_UNIX_SYSTEMCOMMANDSEND_ARG1_NOT_STRING);

  char* data = ItemGetString(handle1);
  char* command = ItemGetString(handle2);

  VBool success = SystemCommandSend(command, data);

  if (!success)
  {
    GURU_MEDITATION(GURU_UNIX_SYSTEMCOMMANDSEND_FAILED);
  }
}

void PrimFun_evalfile(VInterp* interp)
{
  PrimFun_readfile(interp);
  PrimFun_parse(interp);
  PrimFun_eval(interp);
}

// millis --> millisecond time stamp
void PrimFun_millis(VInterp* interp)
{
  VItem item;
  long millis = UnixMillis();
  ItemSetIntNum(&item, millis);
  InterpStackPush(interp, &item);
}

// millis sleep -->
void PrimFun_sleep(VInterp* interp)
{
  VItem* item = InterpStackPop(interp);
  if (!IsTypeIntNum(item))
  {
    GURU_MEDITATION(GURU_UNIX_SLEEP_NOT_INTNUM);
  }

  int millis = item->intNum;
  UnixSleep(millis);
}

// micros sleep -->
void PrimFun_usleep(VInterp* interp)
{
  VItem* item = InterpStackPop(interp);
  if (!IsTypeIntNum(item))
  {
    GURU_MEDITATION(GURU_UNIX_SLEEP_NOT_INTNUM);
  }

  int micros = item->intNum;
  usleep(micros);
}

// n random --> random integer between 0 and n (exclusive)
void PrimFun_random(VInterp* interp)
{
  VItem* item = InterpStackTop(interp);
  if (!IsTypeIntNum(item))
  {
    GURU_MEDITATION(GURU_UNIX_RANDOM_NOT_INTNUM);
  }

  int n = item->intNum;
  item->intNum = UnixRandom(n);
}

void InterpAddUnixPrimFuns(VInterp* interp)
{
  // Init random number generator
  srand(time(0));

  // Add functions
  InterpAddPrimFun(interp, "readfile", PrimFun_readfile);
  InterpAddPrimFun(interp, "setworkingdir", PrimFun_setworkingdir);
  InterpAddPrimFun(interp, "systemcommand", PrimFun_systemcommand);
  InterpAddPrimFun(interp, "systemcommandsend", PrimFun_systemcommandsend);
  InterpAddPrimFun(interp, "evalfile", PrimFun_evalfile);
  InterpAddPrimFun(interp, "millis", PrimFun_millis);
  InterpAddPrimFun(interp, "sleep", PrimFun_sleep);
  InterpAddPrimFun(interp, "random", PrimFun_random);
}
