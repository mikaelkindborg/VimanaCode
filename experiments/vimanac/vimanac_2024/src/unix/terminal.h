/*
File: terminal.h
Author: Mikael Kindborg (mikael@kindborg.com)

Functions for VT100 terminal handling.
*/

#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/time.h>

/*
https://en.wikibooks.org/wiki/Serial_Programming/termios
https://en.wikipedia.org/wiki/ANSI_escape_code
https://viewsourcecode.org/snaptoken/kilo/02.enteringRawMode.html
https://github.com/Cubified/tuibox
https://invisible-island.net/xterm/ctlseqs/ctlseqs.html
https://man.freebsd.org/cgi/man.cgi?query=termios&sektion=4
*/

#define KEY_CTRL_C   3
#define KEY_CTRL_Q   17
#define KEY_ESC      27
#define KEY_LBRACKET 91
#define KEY_UP       65
#define KEY_DOWN     66
#define KEY_RIGHT    67
#define KEY_LEFT     68
#define KEY_DEL      127
#define KEY_ENTER    13

#define TRUE  1
#define FALSE 0

#define TERMINAL 0

#define TermWrite(s)       write(TERMINAL, s, strlen(s))
#define TermWriteChar(c)   write(TERMINAL, c, 1)
#define TermWriteNewLine() TermWrite("\n")

#define TermScreenClear()  TermWrite("\033[2J")
#define TermCursorHome()   TermWrite("\033[H")
#define TermCursorShow()   TermWrite("\033[?25h")
#define TermCursorUp()     TermWrite("\033[A")
#define TermCursorDown()   TermWrite("\033[B")
#define TermCursorRight()  TermWrite("\033[C")
#define TermCursorLeft()   TermWrite("\033[D")

// Struct for saving original terminal settings
static struct termios TermOrig;

int TermReadChar()
{
  unsigned char c = 0;
  int n = read(TERMINAL, &c, 1);
  if (n > 0)
  {
    return c;
  }
  else
  {
    return 0;
  }
}

int TermMoveCursor(int c)
{
  int done = FALSE;

  if (KEY_ESC == c)
  {
    TermCursorShow();

    int ch2 = TermReadChar();

    if (KEY_LBRACKET == ch2)
    {
      done = TRUE;

      int ch3 = TermReadChar();

      if (KEY_UP == ch3)
      {
        TermCursorUp();
      }
      else
      if (KEY_DOWN == ch3)
      {
        TermCursorDown();
      }
      else
      if (KEY_RIGHT == ch3)
      {
        TermCursorRight();
      }
      else
      if (KEY_LEFT == ch3)
      {
        TermCursorLeft();
      }
    }
  }
  else
  if (KEY_DEL == c)
  {
    done = TRUE;
    TermCursorLeft();
    TermWrite(" ");
    TermCursorLeft();
  }
  else
  if (KEY_ENTER == c)
  {
    done = TRUE;
    TermWriteNewLine();
  }

  return done;
}

// Set a terminal mode similar to Commodore 64 BASIC editor
void TermScreenSetCommodore64EditMode()
{
  struct termios termNew;

  tcgetattr(TERMINAL, &TermOrig);
  termNew = TermOrig;

  termNew.c_iflag &=
    ~(BRKINT | INLCR | ICRNL | IGNCR | INPCK | PARMRK | ISTRIP | IXON);
  termNew.c_iflag |= IGNBRK;
  //termNew.c_oflag &= ~(OPOST);
  termNew.c_oflag |= (OPOST | ONLCR | ONLRET);
  //termNew.c_oflag |= (ONLCR | ONLRET);
  termNew.c_cflag |= (CS8);
  termNew.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  termNew.c_cc[VTIME] = 0; // min decitime
  termNew.c_cc[VMIN]  = 0; // min num chars

  tcsetattr(TERMINAL, TCSAFLUSH, &termNew);
}

// Restore terminal mode
void TermScreenSetStandardMode()
{
  //tcsetattr(TERMINAL, TCSANOW, &TermOrig);
  tcsetattr(TERMINAL, TCSAFLUSH, &TermOrig);
}

/*
// Unused
void TermScreenSize()
{
  struct winsize ws;
  int i = ioctl(1, TIOCGWINSZ, &ws);
  printf ("ioctl: %i\n", i);
  printf ("w: %i\n", ws.ws_col);
  printf ("h: %i\n", ws.ws_row);
}
*/
