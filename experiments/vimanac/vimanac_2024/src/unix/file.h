/*
File: file.h
Author: Mikael Kindborg (mikael@kindborg.com)
*/

typedef FILE VPrintStream;

#define FILE_STAT_MODIFIED 1
#define FILE_STAT_NOT_MODIFIED 2
#define FILE_STAT_ERROR -1

// Read file stream - returns allocated buffer
// Deallocate returned buffer with SysFree
char* FileStreamRead(FILE* file)
{
  char*  buffer;
  size_t size;
  int    c;

  VPrintStream* stream = open_memstream(&buffer, &size);

  if (file && stream)
  {
    // Read file to stream
    while ((c = getc(file)) != EOF)
    {
      fputc(c, stream);
    }

    // Zero-terminate string
    fputc(0, stream);

    // Close stream
    fclose(stream);

    // Increment SysAllocCounter
    SysAllocCounterIncr();

    // Return string buffer
    return buffer;
  }
  else
  {
    return NULL;
  }
}

// Write string to file stream
void FileStreamWrite(FILE* file, char* data)
{
  fprintf(file, "%s", data);
}

// Read file - returns allocated buffer
// Deallocate returned buffer with SysFree
char* FileRead(char* filePath)
{
  FILE* file = fopen(filePath, "r");
  if (file)
  {
    char* buffer = FileStreamRead(file);
    fclose(file);
    return buffer;
  }
  else
  {
    return NULL;
  }
}

// https://www.gnu.org/software/libc/manual/html_node/index.html#SEC_Contents

// Return pointer to the basename part of the filename
char* FileBaseName(char* fileName)
{
  char* p = strrchr(fileName, '/');
  if (NULL == p)
  {
    return fileName;
  }
  else
  {
    return p + 1;
  }
}

// Copy dirname part of the filename to resultDirName
// Return FALSE if the filename does not include a directory,
// TRUE if successful
VBool FileDirName(char* fileName, char* resultDirName)
{
  char* dirNameEnd = strrchr(fileName, '/');
  if (NULL == dirNameEnd)
  {
    return FALSE; // No directory
  }
  else
  {
    char* pSrc = fileName;
    char* pDest = resultDirName;
    while (pSrc != dirNameEnd)
    {
      *pDest = *pSrc;
      ++ pSrc;
      ++ pDest;
    }
    *pDest = '\0';

    return TRUE; // Success
  }
}

// Set the working directory.
// Return TRUE on success, FALSE on error
VBool SetWorkingDir(char* dirName)
{
  return 0 == chdir(dirName);
}

// Check if file is modified
// Return FILE_STAT_* constant
// Pass lastUpdate = 0 on first call, like this:
// time_t lastUpdate = 0;
// int result = FileIsModified(fileName, &lastUpdate);
int FileIsModified(char* fileName, time_t* lastUpdate)
{
  struct stat statbuf;

  int result = stat(fileName, &statbuf);
  if (result) return FILE_STAT_ERROR;

  int status = FILE_STAT_NOT_MODIFIED;

  if ((0 != *lastUpdate) && (statbuf.st_mtime > *lastUpdate))
  {
    status = FILE_STAT_MODIFIED;
  }

  *lastUpdate = statbuf.st_mtime;

  return status;
}

// Run a system command and return output as a string
// Deallocate returned string buffer with SysFree
char* SystemCommand(char* command)
{
  FILE* file = popen(command, "r");
  if (file)
  {
    char* buffer = FileStreamRead(file);
    pclose(file);
    return buffer;
  }
  else
  {
    return NULL;
  }
}

// Run a system command and send data as a string
VBool SystemCommandSend(char* command, char* data)
{
  FILE* file = popen(command, "w");
  if (file)
  {
    FileStreamWrite(file, data);
    pclose(file);
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}
