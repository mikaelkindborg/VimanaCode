/*
File: unixfuns.h
Author: Mikael Kindborg (mikael@kindborg.com)

Various functions for Unix and macOS platforms.
*/

unsigned long UnixMillis()
{
  struct timeval timestamp;

  gettimeofday(&timestamp, NULL);

  // Make timestamp smaller (for 32 bit systems)
  unsigned long secondsoneyear = (60 * 60 * 24 * 365);
  unsigned long millis =
    ((timestamp.tv_sec % secondsoneyear) * 1000) +
    (timestamp.tv_usec / 1000);

  return millis;
}

void UnixSleep(int millis)
{
  int seconds = millis / 1000;
  int micros = (millis % 1000) * 1000;
  sleep(seconds);
  usleep(micros);
}

// Return random integer between 0 and n (exclusive)
int UnixRandom(int n)
{
  return rand() % n;
}
