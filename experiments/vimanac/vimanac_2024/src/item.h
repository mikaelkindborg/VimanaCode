/*
File: item.h
Author: Mikael Kindborg (mikael@kindborg.com)

# Items

Items are similar to conses in Lisp. They hold a value and
an address to the next item.

Items have a fixed size and are allocated from a larger block
by the memory manager in listmem.h.

## Item memory, lists, and garbage collection

Items are copied when pushed onto the data stack and
assigned to variables. The data stack and global variables
have separate memory areas (arrays), which are not handled
by the memory manager.

Items allocated by the memory manager are garbage collected.
Items in the data stack and in variables are used as the roots
for garbage collection.

Importantly, items on the stack and in variables are non-garbage
collected items. These items are not garbage collected themselves,
since they are not allocated by the memory manager. However, they
may point to items in the garbage collected space, for example
lists.

Here is an example:

    Data Stack     Managed Memory
    ----------     ------------------------
    ListHead   --> FirstItem --> SecondItem

    Example of a list that contains a list:

    Data Stack     Managed Memory
    ----------     ------------------------
    ListHead   --> ListHead   --> SecondItem
                    !
                  FirstItem  --> SecondItem

Note that a list item always is of TypeList or TypeFun.
The list item is the head of the list. The elements in the
list have types that correspond to the data they hold.

For example, the rest function will put a list item on the
data stack that points to the first item in the rest of the list.

Example program:

    (1 2 3) rest

Stack sequence:

    (1 2 3) // List pushed onto the stack
            // rest is called (note that the function
            // itself is NOT pushed onto the stack)
    (2 3)   // Resulting list on the stack

Diagram:

    Data Stack     Managed Memory
    ----------     ------------------------
    ListHead   --> 1 -> 2 -> 3 (before call to rest)
    ListHead   --> 2 -> 3      (after call to rest)

The item on the stack in the above example has type TypeList.
Item 1, 2 and 3 are in managed memory and have type TypeIntNum.
After rest is called, item 1 can be garbage collected if no
other list is pointing to it. The list item on the stack is
not in managed memory.

## Pointers and addresses

Vimana can run on systems with 16 bit, 32 bit or 64 bit pointers.
8 bit processors like the one on Arduino Uno, use 16 bit pointers.

This version of Vimana is 64 bit. Direct pointers are used for
references to data items. This produces the best performance.

On 16 bit and 32 bit versions, index references into memory
have to be used, as the type info in the "next" field won't fit
with full pointers. In a cross-platform implementation, depending
on the pointer size, pointers or addresses are used to refer to
the next item in a list.

The reference to the first item in a list can always be a full
pointer stored in the value field of a list item, on all platforms,
but indexes can be used for this as well, if desired.

The next item field has type info and a garbage collection
mark bit in the high 4 bits. On 64 bit systems it is possible
to use full pointers since a pointer in reality is less that
60 bits (commonly a 64 bit pointer is 48 bits). However, on
16 bit and 32 bit systems, a full pointer does not fit into the
next field, and on these systems an address that is an array
index can be used.

To clarify further, items have two fields (see definition of
struct VItem below). The first field is the value field that
contains the value of the item. This can be for example a number
or a pointer. In the case of a list item, the value is a
full pointer to the first item in the list. The second field
holds type info and a pointer to the next item in the list.
If there is no next item, the "next" field is zero, indicating
that the list ends there.

List memory (listmem.h) is essentially just an array, and indexes
can therefore be used to access items in this array on systems
with 16 bit or 32 bit pointers. This scheme enables type info to
be kept in the high (or low) bits of the next field.

See listmem.h for details on how managed memory is allocated.

References to info about using pointers to store extra data
on 64 bit systems:

* https://stackoverflow.com/questions/16198700/using-the-extra-16-bits-in-64-bit-pointers
* https://craftinginterpreters.com/optimization.html
* https://www.npopov.com/2012/02/02/Pointer-magic-for-efficient-dynamic-value-representations.html
* https://stackoverflow.com/questions/6326338/why-when-to-use-intptr-t-for-type-casting-in-c

## Handles, strings and memory buffers

A handle is an item that points to a memory buffer item. The buffer
item in turn points to a memory block allocated with malloc.
Handles are used for strings.

The buffer item pointing to the string buffer must exist in
garbage-collected memory cleand up, but it must be the single item
that refers to the allocated memory buffer. Thus the use of handles.
There can be multiple handles that point to the same buffer item.

This is what it looks like for strings:

Data Stack     Managed Mem     Malloc Mem
----------     -----------     ----------
HandleItem --> BufferItem      --> MemoryBuffer

For example:

Data Stack     Managed Mem    Malloc Mem
----------     -----------    ----------
HandleItem --> BufferItem     --> 'Hi World'

String items are of TypeHandle with a extra type info stored
in the next field of the buffer item of TypeBuffer.

The buffer type info in the next filed can be used to refer to
other types of allocated memory buffers, like images or data.

## Primitive functions and global variables

Pointers to primitive functions are stored as full pointers in
the item value field. This results in the best performance.

Alternatively, the global reference to the function could be
stored in primfun items. This would allow for primitive
functions to be changed during runtime, but would result in
an extra lookup.

References to global variables (symbols) are stored as pointers
in the value field of items of type symbol.

See code in item.h (this file) and file parser.h for details.

Much more can be said about the interpreter architecture, but
the above gives an introduction to the memory model used, which
is a very important part of the system.
*/

// -------------------------------------------------------------
// Item struct
// -------------------------------------------------------------

typedef struct __VItem
{
  // Value of the item
  union
  {
    VInt          intNum;     // Integer value (symbols)
    //TODO: VNum  num;        // Number value (long or double)
    VPrimFunPtr   primFunPtr; // Pointer to a primitive function
    void*         ptr;        // Pointer to a memory block or
                              // first child in a list
  };

  // Type info, gc mark bit, and address of the next item in a list
  //
  // Layout of field "next":
  // Topmost bit:     GC mark bit
  // Next top 3 bits: Type info
  // Low bits:        Address of next item
  //
  //  1 bit  3 bits  n - 4 low bits
  // | mark | type |                           pointer/index |
  //
  VIndex          next;
}
VItem;

// -------------------------------------------------------------
// Item types
// -------------------------------------------------------------

#if defined(VIMANA_64)

  // In 64 bit pointer space we use 48 bit pointers to
  // reference the "next" item. Type info and mark bit
  // are in a tag in the 4 high bits.

                         // 64-bit hex      // high 4 bits
  #define TypeNone       0x0000000000000000 // 0000
  #define TypeIntNum     0x1000000000000000 // 0001
  #define TypeList       0x2000000000000000 // 0010
  #define TypeHandle     0x3000000000000000 // 0011
  #define TypeSymbol     0x4000000000000000 // 0100
  #define TypePrimFun    0x5000000000000000 // 0101
  #define TypeFun        0x6000000000000000 // 0110
  #define TypeBuffer     0x7000000000000000 // 0111

  #define MarkBit        0x8000000000000000 // 1000
  #define TypeMask       0x7000000000000000 // 0111
  #define TagMask        0xF000000000000000 // 1111

#elif defined(VIMANA_32)

  // In 32 bit pointer space we use 28 bit indexes to
  // reference the "next" item. Type info and mark bit
  // are in a tag in the 4 high bits.

                         // 32-bit hex      // high 4 bits
  #define TypeNone       0x00000000         // 0000
  #define TypeIntNum     0x10000000         // 0001
  #define TypeList       0x20000000         // 0010
  #define TypeHandle     0x30000000         // 0011
  #define TypeSymbol     0x40000000         // 0100
  #define TypePrimFun    0x50000000         // 0101
  #define TypeFun        0x60000000         // 0110
  #define TypeBuffer     0x70000000         // 0111

  #define MarkBit        0x80000000         // 1000
  #define TypeMask       0x70000000         // 0111
  #define TagMask        0xF0000000         // 1111

#elif defined(VIMANA_16)

  // In 16 bit pointer space we use 12 bit indexes to
  // reference the "next" item. Type info and mark bit
  // are in a tag in the 4 high bits.

                         // 16-bit hex      // high 4 bits
  #define TypeNone       0x0000             // 0000
  #define TypeIntNum     0x1000             // 0001
  #define TypeList       0x2000             // 0010
  #define TypeHandle     0x3000             // 0011
  #define TypeSymbol     0x4000             // 0100
  #define TypePrimFun    0x5000             // 0101
  #define TypeFun        0x6000             // 0110
  #define TypeBuffer     0x7000             // 0111

  #define MarkBit        0x8000             // 1000
  #define TypeMask       0x7000             // 0111
  #define TagMask        0xF000             // 1111

#else

  #error Pointer size must be 64 bits, 32 bits, or 16 bits

#endif

// -------------------------------------------------------------
// Buffer types (used by TypeBuffer items, stored in "next" field)
// -------------------------------------------------------------

#define BufferTypeString 1
#define BufferTypeBlock  2

// -------------------------------------------------------------
// Access to data in item "next" field
// -------------------------------------------------------------

#define ItemGetType(item)      ( ((item)->next) & TypeMask )
#define ItemGetGCMark(item)    ( ((item)->next) & MarkBit )
#define ItemGetNext(item)      ( ((item)->next) & ~TagMask )

// Get pointer to interpreter list memory
#define InterpListMem(interp)  (& ((interp)->listMem))

#if defined(VIMANA_32) || defined(VIMANA_16)

// In 32 bit and 16 bit pointer space the "next" field
// contains an index, starting at 1
// Index 1 is the index of the first item
// Index 0 means NULL (end of list)

// On 32 bit systems 28 bits are available for the index
// (which can address 256M 8 byte items = 2GB of memory)
// On 16 bit systems 12 bits are available for the index
// (which can address 4K 4 byte items = 16 KB of memory)

// An item index is multiplied by the item size to get the
// pointer address of an item
// An item pointer offset is divided by the item size to get
// the index of an item
// Right shift is used for division
// Left shift is used for multiplication

#if defined(VIMANA_64)
  #define AddrShift 4  // 16 byte item size
#elif defined(VIMANA_32)
  #define AddrShift 3  // 8 byte item size
#elif defined(VIMANA_16)
  #define AddrShift 2  // 4 byte item size
#endif

// Cast to byte pointer
#define VBytePtr(ptr) ((VByte*)(ptr))

// Subtract one from the index to get the item pointer
#define IndexToPtr(index, memStart) \
  ((VItem*) (VBytePtr(memStart) + (((index) - 1) << AddrShift)))

// Add one to get the item index (first item has index 1)
#define PtrToIndex(ptr, memStart) \
  ((VIndex) (((VBytePtr(ptr) - (memStart)) >> AddrShift) + 1))

// Get pointer to item from index (index 0 means NULL)
#define ListMemGetItemPointer(mem, itemIndex) \
  ((0 == itemIndex) ? NULL : IndexToPtr(itemIndex, (mem)->start))

// Get index of item from pointer
#define ListMemGetItemIndex(mem, itemPointer) \
  ((NULL == itemPointer) ? 0 : PtrToIndex(itemPointer, (mem)->start))

#endif

#if defined(VIMANA_64)

// Get pointer to next item
#define ItemGetNextItem(item) ((VItem*) ItemGetNext(item))
#define ListMemGetNextItem ItemGetNextItem

// Set pointer to next item
#define ItemSetNextItem(item, next) ItemSetNext(item, (VIndex)(next));
#define ListMemSetNextItem ItemSetNextItem

#else // VIMANA_32 or VIMANA_16

// Get pointer to next item
#define ItemGetNextItem(item) \
  (ListMemGetItemPointer(InterpListMem(interp), ItemGetNext(item)))
#define ListMemGetNextItem(item) \
  (ListMemGetItemPointer(mem, ItemGetNext(item)))

// Set index of next item based on "next" pointer
#define ItemSetNextItem(item, next) \
  (ItemSetNext(item, ListMemGetItemIndex(InterpListMem(interp), next)))
#define ListMemSetNextItem(item, next) \
  (ItemSetNext(item, ListMemGetItemIndex(mem, next)))

#endif

void ItemGCMarkSet(VItem* item)
{
  item->next = (item->next | MarkBit);
}

void ItemGCMarkUnset(VItem* item)
{
  item->next = (item->next & ~MarkBit);
}

void ItemSetType(VItem* item, VType type)
{
  item->next = (type | (item->next & ~TypeMask));
}

void ItemSetNext(VItem* item, VIndex next)
{
  item->next = (next | (item->next & TagMask));
}

void ItemSetBufferType(VItem* item, VType bufferType)
{
  ItemSetNext(item, bufferType);
}

VType ItemGetBufferType(VItem* item)
{
  return (VType) ItemGetNext(item);
}

// -------------------------------------------------------------
// Access to data in item value field
// -------------------------------------------------------------

#define ItemGetIntNum(item)      ((item)->intNum)
#define ItemGetFirstItem(item)   ((VItem*)((item)->ptr))
#define ItemGetPtr(item)         ((item)->ptr)
#define ItemGetPrimFun(item)     ((item)->primFunPtr)

void ItemSetIntNum(VItem* item, VInt number)
{
  item->intNum = number;
  ItemSetType(item, TypeIntNum);
}

// Set pointer to SymbolEntry
void ItemSetSymbol(VItem* item, void* ptr)
{
  item->ptr = ptr;
  ItemSetType(item, TypeSymbol);
}

// Set pointer to function
void ItemSetPrimFun(VItem* item, VPrimFunPtr primFunPtr)
{
  item->primFunPtr = primFunPtr;
  ItemSetType(item, TypePrimFun);
}

// Set first item of child list
void ItemSetFirst(VItem* item, VItem* first)
{
  item->ptr = first;
  // WE MUST NOT ALTER THE ITEM TYPE
}

// Set pointer
void ItemSetPtr(VItem* item, void* ptr)
{
  item->ptr = ptr;
  // WE MUST NOT ALTER THE ITEM TYPE
}

// -------------------------------------------------------------
// Item type access
// -------------------------------------------------------------

#define IsTypeNone(item)         (TypeNone == ItemGetType(item))
#define IsTypeIntNum(item)       (TypeIntNum == ItemGetType(item))
#define IsTypeList(item)         (TypeList == ItemGetType(item))
#define IsTypeSymbol(item)       (TypeSymbol == ItemGetType(item))
#define IsTypePrimFun(item)      (TypePrimFun == ItemGetType(item))
#define IsTypeFun(item)          (TypeFun == ItemGetType(item))
#define IsTypeHandle(item)       (TypeHandle == ItemGetType(item))
#define IsTypeBuffer(item)       (TypeBuffer == ItemGetType(item))

// List types
#define IsList(item) (IsTypeList(item) || IsTypeFun(item))

// Types reference a child in the value field (non-atomic items)
#define IsTypeWithChild(item) (IsList(item) || IsTypeHandle(item))

// Check if list is empty
#define IsEmpty(list) (0 == ItemGetFirstItem(list))

// -------------------------------------------------------------
// Init item
// -------------------------------------------------------------

void ItemInit(VItem* item)
{
  item->intNum = 0;
  item->next = 0;
  ItemSetType(item, TypeIntNum);
}

void ItemSetNone(VItem* item)
{
  item->intNum = 0;
  item->next   = 0; // Sets type to TypeNone
}

// -------------------------------------------------------------
// Various item functions
// -------------------------------------------------------------

// TODO: Write function with type checking in place of this macro
#define ItemEquals(item1, item2) \
  ((ItemGetType(item1) == ItemGetType(item2)) && \
   ((item1)->intNum == (item2)->intNum))

VBool IsTypeString(VItem* handle)
{
  if (!IsTypeHandle(handle)) { return FALSE; }

  VItem* buffer = ItemGetFirstItem(handle);

  if (NULL == buffer) { return FALSE; }
  if (!IsTypeBuffer(buffer)) { return FALSE; }

  return (BufferTypeString == ItemGetBufferType(buffer));
}

// Returns the pointer of the buffer the item refers to.
// TODO: GURU_MEDITATION instead of returning NULL?
char* ItemGetString(VItem* handle)
{
  if (!IsTypeString(handle)) { return NULL; }

  VItem* buffer = ItemGetFirstItem(handle);

  return (char*) (buffer->ptr);
}

VBool ItemStringEquals(VItem* item1, VItem* item2)
{
  return (
    (IsTypeString(item1) && IsTypeString(item2))
    && StrEquals(ItemGetString(item1), ItemGetString(item2))
  );
}
