/*
File: datastack.h
Author: Mikael Kindborg (mikael@kindborg.com)

Data stack functions.
*/

// -------------------------------------------------------------
// Data stack struct
// -------------------------------------------------------------

typedef struct __VDataStack
{
  VByte* start;        // Start of data stack
  VByte* end;          // End of data stack
  VByte* top;          // Top of data stack
}
VDataStack;

// -------------------------------------------------------------
// Data stack macros
// -------------------------------------------------------------

#define DataStackCheckNotEmpty(dataStack) \
  if (dataStack->top < dataStack->start) { \
    GURU_MEDITATION(GURU_DATASTACK_IS_EMPTY); }

#define DataStackCheckUnderflow(dataStack, offset) \
  if ((dataStack->top - (offset * sizeof(VItem))) < dataStack->start) { \
    GURU_MEDITATION(GURU_DATASTACK_OFFSET_UNDERFFLOW); }

#define DataStackCheckOverflow(dataStack) \
  if (dataStack->top >= dataStack->end) { \
    GURU_MEDITATION(GURU_DATASTACK_OVERFLOW); }

// -------------------------------------------------------------
// Data stack functions
// -------------------------------------------------------------

void DataStackInit(VDataStack* dataStack, VByte* start, int byteSize)
{
  dataStack->start = start;
  dataStack->end = start + byteSize;
  // Offset start to work with DataStackPush
  dataStack->top = start - sizeof(VItem);
}

// Copy the given item onto the data stack
void DataStackPush(VDataStack* dataStack, VItem* item)
{
  dataStack->top += sizeof(VItem);
  DataStackCheckOverflow(dataStack);
  // Copy item to stack
  *((VItem*)(dataStack->top)) = *item;
}

// Remove the topmost item from the data stack and return it
VItem* DataStackPop(VDataStack* dataStack)
{
  DataStackCheckNotEmpty(dataStack);
  VItem* topItem = (VItem*)(dataStack->top);
  dataStack->top -= sizeof(VItem);
  return topItem;
}

VItem* DataStackTop(VDataStack* dataStack)
{
  DataStackCheckNotEmpty(dataStack);
  return ((VItem*)((dataStack)->top));
}

VItem* DataStackAt(VDataStack* dataStack, int offset)
{
  DataStackCheckUnderflow(dataStack, offset);
  return ((VItem*)(((dataStack)->top) - (sizeof(VItem) * (offset))));
}
