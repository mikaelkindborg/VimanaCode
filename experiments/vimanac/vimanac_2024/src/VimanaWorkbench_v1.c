//#define TRACK_MEMORY_USAGE
//#define DEBUG

// Interactive programming based on the clipboard COPY command key
// A new take on dynamic programming environments
// A new take on the REPL - the editor is the REPL
// Commodore 64 inspired
// Interactive programming with the clipboard
// Unconventional retro style programming in a DIY language
// Make primfun or Vimana fun watchclipboard - you can watch the
// clipboard from any part of the program!

/*
0 (counter) setglobal
(loop) (
  counter print
  counter 1 + (counter) setglobal
  1000 sleep
  pasteboard {STOP} eq not (loop) iftrue
) def
loop
*/

#include "vimana.h"
#include "unix/primfuns_unix.h"
#include "unix/terminal.h"
#include <SDL2/SDL.h>
#include "sdl/primfuns_sdl.h"

#define SYMBOL_TABLE_SIZE 200    // Number of symbols in the symbol table
#define DATA_STACK_SIZE   100    // Number of items on the data stack
#define CALL_STACK_SIZE   100    // Number of stackframes
#define LIST_MEMORY_SIZE  1000   // Number of items in garbage collected list memory

void PrintWelcomeMessage()
{
  TermWrite("Welcome to the Vimana Workbench!\n");
  TermWrite("Here you can evaluate Vimana code interactively.\n");
  TermWrite("Select some code with the mouse and press COMMAND-C.\n");
  TermWrite("Then select the word DOIT and press COMMAND-C.\n");
  TermWrite("The resulting data stack is printed to the screen.\n");
  TermWrite("Note that DOIT is a workbench command, it is not part\n");
  TermWrite("of the Vimana language. Code can be copied in any editor.\n");
  TermWrite("Copy QUIT to exit, or press CTRL-Q or CTRL-C.\n");
  TermWrite("Now copy a code snippet, then copy the word DOIT.\n");
  TermWrite("DOIT QUIT\n");
  TermWrite("1 2 + printstack \n");
  TermWrite("drop\n");
  TermWrite("(hi) ({Hi World} print) def\n");
  TermWrite("hi\n");
  TermWrite("0 (counter) setglobal\n");
  TermWrite("(clipboard) ({pbpaste} systemcommand) def\n");
  TermWrite("(loop) (\n");
  TermWrite("  counter print\n");
  TermWrite("  counter 1 + (counter) setglobal\n");
  TermWrite("  2000 sleep\n");
  TermWrite("  clipboard {STOP} stringequals (loop) iffalse\n");
  TermWrite(") def\n");
  TermWrite("loop\n");
}

//{STOP} print (pasteboard) setglobal

void HandleKey(int c)
{
  if (!TermMoveCursor(c))
  {
    unsigned char ch = (unsigned char)c;
    TermWriteChar(&ch);
  }
}

static char* ClipBoardPrev = NULL;
static char* ClipboardCurrent = NULL;

VBool HandleClipboardEval(VInterp* interp)
{
  VBool quit = FALSE;

  ClipboardCurrent = SystemCommand("pbpaste");

  if (ClipboardCurrent && ClipBoardPrev)
  {
    if (StrEquals(ClipboardCurrent, ClipBoardPrev))
    {
      // Do nothing
    }
    else
    if (StrEquals("QUIT", ClipboardCurrent))
    {
      printf("QUIT\n");
      quit = TRUE;
    }
    else
    if (StrEquals("DOIT", ClipboardCurrent))
    {
      //printf("DOIT\n");
      // Parse source code
      VItem* list = InterpParse(interp, ClipBoardPrev);

      // Evaluate list
      InterpEval(interp, list);

      // Cleanup
      InterpGC(interp);
    }
  }

  // Free previous state
  if (ClipBoardPrev)
  {
    SysFree(ClipBoardPrev);
  }

  // Save current state
  ClipBoardPrev = ClipboardCurrent;

  return quit;
}

unsigned long GetMillis()
{
  struct timeval timestamp;

  gettimeofday(&timestamp, NULL);

  // Make timestamp smaller (for 32 bit systems)
  unsigned long secondsoneyear = (60 * 24 * 365);
  unsigned long millis =
    ((timestamp.tv_sec % secondsoneyear) * 1000) +
    (timestamp.tv_usec / 1000);

  return millis;
}

int main()
{
  // Create the Vimana machine
  VInterp* interp = InterpAlloc(
    SYMBOL_TABLE_SIZE, DATA_STACK_SIZE,
    CALL_STACK_SIZE,   LIST_MEMORY_SIZE);

  InterpAddCorePrimFuns(interp);
  InterpAddUnixPrimFuns(interp);
  InterpAddSDLPrimFuns(interp);

  TermScreenSetCommodore64EditMode();
  atexit(TermScreenSetStandardMode);

  TermScreenClear();
  TermCursorHome();

  PrintWelcomeMessage();

  SystemCommandSend("pbcopy", "");

  int c = TermReadChar();

  unsigned long msClipboardWatchInterval = 200;
  unsigned long msClipboardWatchNext =
    GetMillis() + msClipboardWatchInterval;

  while (KEY_CTRL_C != c && KEY_CTRL_Q != c)
  {
    // Be nice to the processor
    usleep(1000);

    // Handle key
    if (c > 0)
    {
      HandleKey(c);
      //printf("%i\n", c);
    }

    // Read next key
    c = TermReadChar();

    // Clipboard watcher
    unsigned long msNow = GetMillis();
    if (msNow > msClipboardWatchNext)
    {
      msClipboardWatchNext += msClipboardWatchInterval;

      VBool quit = HandleClipboardEval(interp);
      if (quit)
      {
        c = KEY_CTRL_C;
      }
    }
  }

  TermScreenSetStandardMode();

  printf("\n");

  // Free saved clipboard state
  if (ClipBoardPrev)
  {
    SysFree(ClipBoardPrev);
  }

  InterpFree(interp);

  return 0;
}
