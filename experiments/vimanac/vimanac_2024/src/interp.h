/*
File: interp.h
Author: Mikael Kindborg (mikael@kindborg.com)

Interpreter data structures and functions.

Example use:

    #define SYMBOL_TABLE_SIZE 200  // Number of symbols (including primfuns)
    #define DATA_STACK_SIZE   100  // Number of items on the data stack
    #define CALL_STACK_SIZE   100  // Number of stack frames
    #define LIST_MEMORY_SIZE  1000 // Number of items in garbage collected memory

    VInterp* interp = InterpAlloc(
      SYMBOL_TABLE_SIZE, DATA_STACK_SIZE,
      CALL_STACK_SIZE,   LIST_MEMORY_SIZE);
    InterpAddCorePrimFuns(interp);
    InterpEvalString(interp, "(Hi World) print 1 2 + print");
    InterpFree(interp);
*/

// -------------------------------------------------------------
// Data types and structs
// -------------------------------------------------------------

typedef struct __VInterp
{
  VUInt           run;             // Run flag
  VSymbolTable    symbolTable;     // Global variables
  VDataStack      dataStack;       // Data stack items
  VCallStack      callStack;       // Stack frames
  VListMemory     listMem;         // Lisp-style list memory
  VStringMem      stringMem;       // String memory for symbols
}
VInterp;

#define InterpSymbolTable(interp)  (& ((interp)->symbolTable))
#define InterpDataStack(interp)    (& ((interp)->dataStack))
#define InterpCallStack(interp)    (& ((interp)->callStack))
#define InterpStringMem(interp)    (& ((interp)->stringMem))
// Defined in item.h
//#define InterpListMem(interp)    (& ((interp)->listMem))

// -------------------------------------------------------------
// Forward declarations
// -------------------------------------------------------------

VItem* InterpParse(VInterp* interp, char* code);
void   InterpPrint(VInterp* interp, VItem* item);

// -------------------------------------------------------------
// Interpreter alloc/free
// -------------------------------------------------------------

// Print memory usage (helps to tweak memory allocation)
void InterpPrintMemoryUse(VInterp* interp, int bytesInterpTotal,
  int sizeSymbolTable, int sizeDataStack, int sizeCallStack,
  int sizeListMem, int bytesStringMem)
{
/*
  int bytesSymbolTable  = sizeSymbolTable * sizeof(VSymbolEntry);
  int bytesDataStack    = sizeDataStack * sizeof(VItem);
  int bytesCallStack    = sizeCallStack * sizeof(VStackFrame);
  int bytesListMem      = sizeListMem * sizeof(VItem);
  int bytesInterpStruct = sizeof(VInterp);
  int bytesTotal        = bytesInterpStruct + bytesListMem +
                          bytesCallStack + bytesDataStack +
                          bytesSymbolTable + bytesStringMem;
*/
  printf("------------------------------------------------------------\n");
  printf("Vimana Interpreter Memory\n");
  printf("------------------------------------------------------------\n");
  printf("Total allocated:    %i bytes\n", bytesInterpTotal);
  printf("String memory:      %i bytes\n", bytesStringMem);
  printf("Symbol table size:  %i items\n", sizeSymbolTable);
  printf("Data stack size:    %i items\n", sizeDataStack);
  printf("Call stack size:    %i items\n", sizeCallStack);
  printf("List memory size:   %i items\n", sizeListMem);
  printf("------------------------------------------------------------\n");
  //printf("Bytes VInterp:        %i\n", bytesInterpStruct);
  //printf("Bytes SymbolTable:    %i\n", bytesSymbolTable);
  //printf("Bytes DataStack:      %i\n", bytesDataStack);
  //printf("Bytes CallStack:      %i\n", bytesCallStack);
  //printf("Bytes ListMemory:     %i\n", bytesListMem);
  //printf("Bytes total:          %i\n", bytesTotal);
}

void InterpPrintNumberOfPrimFuns(VInterp* interp)
{
  #ifdef TRACK_MEMORY_USAGE
  int numPrimFuns = SymbolTableNumPrimFuns(InterpSymbolTable(interp));
  printf("Number of primfuns:    %i\n", numPrimFuns);
  printf("------------------------------------------------------------\n");
  #endif
}

// Allocate interpreter memory
VInterp* InterpAlloc(int sizeSymbolTable, int sizeDataStack,
                     int sizeCallStack,   int sizeListMem)
{
  // Calculate sizes
  int bytesInterpStruct = sizeof(VInterp);
  int bytesSymbolTable  = sizeSymbolTable * sizeof(VSymbolEntry);
  int bytesDataStack    = sizeDataStack * sizeof(VItem);
  int bytesCallStack    = sizeCallStack * sizeof(VStackFrame);
  int bytesListMem      = sizeListMem * sizeof(VItem);
  // Estimate 10 chars per symbol plus extra space for print/stringify
  int bytesStringMem    = sizeSymbolTable * 10 * 2;

  int bytesInterpTotal = bytesInterpStruct + bytesSymbolTable +
    bytesDataStack + bytesCallStack + bytesListMem + bytesStringMem;

  // Allocate interpreter memory block
  VInterp* interp = SysAlloc(bytesInterpTotal);
  if (NULL == interp)
  {
    GURU_MEDITATION(GURU_INTERPRETER_CANNOT_ALLOC_MEMORY);
  }

  // Set memory regions
  VByte* pInterp      = (VByte*)interp;
  VByte* pSymbolTable = pInterp      + bytesInterpStruct;
  VByte* pDataStack   = pSymbolTable + bytesSymbolTable;
  VByte* pCallStack   = pDataStack   + bytesDataStack;
  VByte* pListMem     = pCallStack   + bytesCallStack;
  VByte* pStringMem   = pListMem     + bytesListMem;

  SymbolTableInit (InterpSymbolTable(interp), pSymbolTable, bytesSymbolTable);
  DataStackInit   (InterpDataStack(interp),   pDataStack,   bytesDataStack);
  CallStackInit   (InterpCallStack(interp),   pCallStack,   bytesCallStack);
  ListMemInit     (InterpListMem(interp),     pListMem,     bytesListMem);
  StringMemInit   (InterpStringMem(interp),   pStringMem,   bytesStringMem);

  // Associate list memory with this interpreter.
  InterpListMem(interp)->interp = interp;

  #ifdef TRACK_MEMORY_USAGE
    InterpPrintMemoryUse(interp, bytesInterpTotal, sizeSymbolTable,
      sizeDataStack, sizeCallStack, sizeListMem, bytesStringMem);
  #endif

  return interp;
}

void InterpFree(VInterp* interp)
{
  // This will dealloc memory blocks pointer to by TypeBuffer items
  ListMemSweep(InterpListMem(interp));

  #ifdef TRACK_MEMORY_USAGE
    ListMemPrintAllocCounter(InterpListMem(interp));
  #endif

  SysFree(interp);
}

// -------------------------------------------------------------
// Primfuns
// -------------------------------------------------------------

void InterpAddPrimFun(VInterp* interp, char* name, VPrimFunPtr fun)
{
  VSymbolEntry* entry = SymbolTableAddEntry(InterpSymbolTable(interp), name);
  ItemSetPrimFun(&(entry->value), fun);
}

// -------------------------------------------------------------
// Item functions
// -------------------------------------------------------------

#define InterpAllocItem(interp) \
  ListMemAlloc(InterpListMem(interp))

#define InterpAllocHandle(interp, data, bufferType) \
  ListMemAllocHandle(InterpListMem(interp), data, bufferType)

// -------------------------------------------------------------
// Garbage collection
// -------------------------------------------------------------

// Mark items referenced by global symbols
void InterpMarkGlobalVars(VInterp* interp)
{
  VListMemory* mem = InterpListMem(interp);
  VSymbolTable* table = InterpSymbolTable(interp);
  VByte* p = table->start;
  VByte* last = table->top;
  while (p <= last)
  {
    VSymbolEntry* entry = (VSymbolEntry*)(p);
    VItem* item = &(entry->value);

    // The items in the table should not be marked since they are
    // in non-gc memory, but any child elements need to be marked
    if (IsTypeWithChild(item))
    {
      ListMemMark(mem, ItemGetFirstItem(item));
    }

    p += sizeof(VSymbolEntry);
  }
}

// Mark items referenced by the data stack
void InterpMarkDataStack(VInterp* interp)
{
  VListMemory* mem = InterpListMem(interp);
  VDataStack* dataStack = InterpDataStack(interp);
  VByte* p = dataStack->start;
  VByte* last = dataStack->top;
  while (p <= last)
  {
    VItem* item = (VItem*)(p);

    // The items in the array should not be marked since they are
    // in non-gc memory, but any child elements need to be marked
    if (IsTypeWithChild(item))
    {
      ListMemMark(mem, ItemGetFirstItem(item));
    }

    p += sizeof(VItem);
  }
}

// Mark items referenced from the callstack
void InterpMarkCallStack(VInterp* interp)
{
  VListMemory* mem = InterpListMem(interp);
  VCallStack* callStack = InterpCallStack(interp);
  VByte* p = callStack->start;
  VByte* last = callStack->top;
  while (p <= last)
  {
    VStackFrame* frame = (VStackFrame*)(p);
    VItem* item = frame->instruction;
    ListMemMark(mem, item);
    p += sizeof(VStackFrame);
  }
}

// GC is not yet fully tested and integrated
void InterpGC(VInterp* interp)
{
  // Mark data stack
  InterpMarkDataStack(interp);

  // Mark global vars
  InterpMarkGlobalVars(interp);

  // Mark callstack
  InterpMarkCallStack(interp);

  // Sweep memory
  ListMemSweep(InterpListMem(interp));

  #ifdef TRACK_MEMORY_USAGE
  //ListMemPrintAllocCounter(InterpListMem(interp));
  #endif
}

// -------------------------------------------------------------
// Data stack
// -------------------------------------------------------------

#define InterpStackPush(interp, item) \
  DataStackPush(InterpDataStack(interp), item)

#define InterpStackPop(interp) \
  DataStackPop(InterpDataStack(interp))

#define InterpStackTop(interp) \
  DataStackTop(InterpDataStack(interp))

#define InterpStackAt(interp, offset) \
  DataStackAt(InterpDataStack(interp), offset)

// -------------------------------------------------------------
// Call stack
// -------------------------------------------------------------

#define InterpPushStackFrame(interp, list) \
  CallStackPush(InterpCallStack(interp), list)

#define InterpPopStackFrame(interp) \
  CallStackPop(InterpCallStack(interp))

#define InterpCallStackIsEmpty(interp) \
  CallStackIsEmpty(InterpCallStack(interp))

// -------------------------------------------------------------
// Global vars
// -------------------------------------------------------------

// Items of TypeSymbol point to entries in the global symbol table

#define SymbolEntryValue(entryPtr) \
  ((VItem*)(&(((VSymbolEntry*)(entryPtr))->value)))

#define SymbolEntryName(entryPtr) \
  ((char*)(((VSymbolEntry*)(entryPtr))->symbol))

#define GlobalVarGet(item) \
  SymbolEntryValue(ItemGetPtr(item))

// Copy value
#define GlobalVarSet(item, value) \
  (*SymbolEntryValue(ItemGetPtr(symbol)) = *(value))

#define GlobalVarGetName(item) \
  SymbolEntryName(ItemGetPtr(item))

// -------------------------------------------------------------
// Eval
// -------------------------------------------------------------

void InterpEval(VInterp* interp, VItem* list)
{
  VStackFrame* stackFrame;
  VItem*       instruction;

  CallStackPushNewStackFrame(InterpCallStack(interp), list);

  interp->run = TRUE;

  while (interp->run)
  {
    // Get current instruction
    stackFrame = CallStackTop(InterpCallStack(interp));
    instruction = stackFrame->instruction;

    // Slow down interpreter sleep for 100 ms
    //int micros = 100 * 1000;
    //usleep(micros);
    // Debug print
    //PrintIntNum(instruction);
    //PrintNewLine();

    // Evaluate current instruction.
    if (instruction)
    {
      // Advance instruction for the NEXT loop
      stackFrame->instruction = ItemGetNextItem(instruction);

      if (IsTypeSymbol(instruction))
      {
        VItem* value = GlobalVarGet(instruction);
        if (IsTypePrimFun(value))
        {
          // Call primfun
          (value->primFunPtr)(interp);
        }
        else
        if (IsTypeFun(value))
        {
          // Call function
          InterpPushStackFrame(interp, value);
        }
        else
        if (!IsTypeNone(value))
        {
          // Push global variable value
          InterpStackPush(interp, value);
        }
        else
        {
          // If type is None it is an error
          Print("[ERROR] SYMBOL IS UNBOUND: ");
          PrintLine(GlobalVarGetName(instruction));
          GURU_MEDITATION(GURU_INTERPRETER_SYMBOL_IS_NONE);
        }
      }
      else
      {
        InterpStackPush(interp, instruction);
      }
    }
    else // (NULL == instruction -> end of instruction list)
    {
      // Pop stackframe
      InterpPopStackFrame(interp);

      // Exit interpreter loop if this was the last stackframe
      if (InterpCallStackIsEmpty(interp))
      {
        interp->run = FALSE;
      }
    }
  }
  // while
}

// Evaluate a string
void InterpEvalString(VInterp* interp, char* code)
{
  VItem* list = InterpParse(interp, code);
  InterpEval(interp, list);
  InterpGC(interp);
}
