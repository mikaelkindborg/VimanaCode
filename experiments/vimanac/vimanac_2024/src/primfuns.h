/*
File: primfuns.h
Author: Mikael Kindborg (mikael@kindborg.com)

This file contains core primfuns and some extra ones
that could have been defined in Vimana itself.

The naming convention uses all lower case for primfuns
in this file.
*/

// -------------------------------------------------------------
// Error checking
// -------------------------------------------------------------

#define ItemMustBeList(list, gurucode) \
  if (!IsList(list)) { GURU_MEDITATION(gurucode); }

// -------------------------------------------------------------
// Print functions
// -------------------------------------------------------------

void PrimFun_sayhi(VInterp* interp)
{
  PrintLine("Hi World!");
}

void PrimFun_saymantra(VInterp* interp)
{
  PrintLine("I follow my breath");
}

void PrimFun_print(VInterp* interp)
{
  VItem* item = InterpStackPop(interp);
  InterpPrint(interp, item);
}

void PrimFun_printstack(VInterp* interp)
{
  VDataStack* stack = InterpDataStack(interp);

  VItem* item = (VItem*)(stack->start);
  VItem* top  = (VItem*)(stack->top);

  VBool isNotEmpty = item <= top;
  VBool isFirstItem = TRUE;

  if (isNotEmpty)
  {
    Print("[");
  }

  while (item <= top)
  {
    if (!isFirstItem)
    {
      Print(" ");
    }
    isFirstItem = FALSE;

    char* s = InterpStringify(interp, item);
    Print(s);
    StringMemReset(InterpStringMem(interp));

    ++ item;
  }

  if (isNotEmpty)
  {
    Print("]");
    PrintNewLine();
  }
}

void PrimFun_printglobals(VInterp* interp)
{
  SymbolTablePrintGlobals(InterpSymbolTable(interp));
}

void PrimFun_printprimfuns(VInterp* interp)
{
  SymbolTablePrintPrimFuns(InterpSymbolTable(interp));
}

// -------------------------------------------------------------
// Eval and conditionals
// -------------------------------------------------------------

void PrimFun_eval(VInterp* interp)
{
  VItem* list = InterpStackPop(interp);
  ItemMustBeList(list, GURU_EVAL_ITEM_IS_NOT_LIST);
  InterpPushStackFrame(interp, list);
}

/*
void PrimFun_popStackFrame(VInterp* interp)
{
  PrintLine("PrimFun_popStackFrame");
  //VStackFrame* stackFrame = CallStackTop(InterpCallStack(interp));
  //stackFrame->instruction = NULL;
  InterpPopStackFrame(interp);
}
*/

void PrimFun_iftrue(VInterp* interp)
{
  VItem* trueBlock = InterpStackPop(interp);
  VItem* trueOrFalse = InterpStackPop(interp);
  ItemMustBeList(trueBlock, GURU_IFTRUE_ITEM_IS_NOT_LIST);
  if (trueOrFalse->intNum)
  {
    InterpPushStackFrame(interp, trueBlock);
  }
}

void PrimFun_iffalse(VInterp* interp)
{
  VItem* falseBlock = InterpStackPop(interp);
  VItem* trueOrFalse = InterpStackPop(interp);
  ItemMustBeList(falseBlock, GURU_IFFALSE_ITEM_IS_NOT_LIST);
  if (!(trueOrFalse->intNum))
  {
    InterpPushStackFrame(interp, falseBlock);
  }
}

void PrimFun_ifelse(VInterp* interp)
{
  VItem* falseBlock = InterpStackPop(interp);
  VItem* trueBlock = InterpStackPop(interp);
  VItem* trueOrFalse = InterpStackPop(interp);
  ItemMustBeList(trueBlock, GURU_IFELSE_ITEM_IS_NOT_LIST);
  ItemMustBeList(falseBlock, GURU_IFELSE_ITEM_IS_NOT_LIST);
  if (trueOrFalse->intNum)
  {
    InterpPushStackFrame(interp, trueBlock);
  }
  else
  {
    InterpPushStackFrame(interp, falseBlock);
  }
}

// -------------------------------------------------------------
// Parse string
// -------------------------------------------------------------

void PrimFun_parse(VInterp* interp)
{
  VItem* handle = InterpStackTop(interp);
  if (!IsTypeString(handle))
  {
    GURU_MEDITATION(GURU_PARSE_ARG_NOT_STRING);
  }
  char* string = ItemGetString(handle);
  VItem* list = InterpParse(interp, string);
  *handle = *list;
}

// -------------------------------------------------------------
// Global variables
// -------------------------------------------------------------

// Set value of global variable
// 42 (foo) setglobal ->
// 42 (foo) setvar ->  // synonym
void PrimFun_setglobal(VInterp* interp)
{
  VItem* list = InterpStackPop(interp);   // list
  VItem* value = InterpStackPop(interp);  // value
  ItemMustBeList(list, GURU_SETGLOBAL_ITEM_IS_NOT_LIST);
  VItem* symbol = ItemGetFirstItem(list); // symbol in list
  GlobalVarSet(symbol, value);
}

// Get value of global variable
// (foo) getglobal -> value of foo
// (foo) getvar -> value of foo  // synonym
void PrimFun_getglobal(VInterp* interp)
{
  VItem* item = InterpStackTop(interp);   // list
  ItemMustBeList(item, GURU_GETGLOBAL_ITEM_IS_NOT_LIST);
  VItem* symbol = ItemGetFirstItem(item); // symbol in list
  *item = *GlobalVarGet(symbol);
}

// -------------------------------------------------------------
// Funify a list - calls it as a function
// -------------------------------------------------------------

// list funify -> funified list
void PrimFun_funify(VInterp* interp)
{
  VItem* list = InterpStackTop(interp);
  ItemMustBeList(list, GURU_FUNIFY_ITEM_IS_NOT_LIST);
  ItemSetType(list, TypeFun);
}

// -------------------------------------------------------------
// Arithmentic
// -------------------------------------------------------------

void PrimFun_plus(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum += b->intNum;
}

void PrimFun_minus(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum -= b->intNum;
}

void PrimFun_times(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum *= b->intNum;
}

void PrimFun_div(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum /= b->intNum;
}

void PrimFun_1plus(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  a->intNum += 1;
}

void PrimFun_1minus(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  a->intNum -= 1;
}

void PrimFun_2plus(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  a->intNum += 2;
}

void PrimFun_2minus(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  a->intNum -= 2;
}

void PrimFun_lessthan(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum = a->intNum < b->intNum;
}

void PrimFun_greaterthan(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum = a->intNum > b->intNum;
}

void PrimFun_eq(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum = ItemEquals(a, b);
  ItemSetType(a, TypeIntNum);
}

void PrimFun_stringequals(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum = ItemStringEquals(a, b);
  ItemSetType(a, TypeIntNum);
}

void PrimFun_iszero(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  a->intNum = (0 == a->intNum);
}

void PrimFun_isempty(VInterp* interp)
{
  VItem* item = InterpStackTop(interp);
  // TODO: Check list type?
  item->intNum = IsEmpty(item);
}

void PrimFun_not(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  a->intNum = !(a->intNum);
}

void PrimFun_and(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum = a && b;
  ItemSetType(a, TypeIntNum);
}

void PrimFun_or(VInterp* interp)
{
  VItem* b = InterpStackPop(interp);
  VItem* a = InterpStackTop(interp);
  a->intNum = a || b;
  ItemSetType(a, TypeIntNum);
}

// -------------------------------------------------------------
// Stack functions
// -------------------------------------------------------------

// a drop ->
void PrimFun_drop(VInterp* interp)
{
  InterpStackPop(interp);
}

// a dup -> a a
void PrimFun_dup(VInterp* interp)
{
  InterpStackPush(interp, InterpStackTop(interp));
}

// a b swap -> b a
void PrimFun_swap(VInterp* interp)
{
  DataStackCheckUnderflow(InterpDataStack(interp), 1);
  VItem* a = (VItem*)interp->dataStack.top;
  VItem  b = *(a - 1);
  *(a - 1) = *a;
  *a = b;
/*
  VItem a = *InterpStackPop(interp);
  VItem b = *InterpStackPop(interp);
  InterpStackPush(interp, &a);
  InterpStackPush(interp, &b);
*/
}

// a b c rot -> b c a
void PrimFun_rot(VInterp* interp)
{
  VItem* c = InterpStackAt(interp, 0);
  VItem* b = InterpStackAt(interp, 1);
  VItem* a = InterpStackAt(interp, 2);
  VItem temp = *b;
  *b = *c;
  *c = *a;
  *a = temp;
}

// a b over -> a b a
void PrimFun_over(VInterp* interp)
{
  InterpStackPush(interp, InterpStackAt(interp, 1));
}

// -------------------------------------------------------------
// List functions
// -------------------------------------------------------------

//
// How other languages do it:
//
// (cons 1 2) => (1 2)   // NewLisp
// (cons 1 2) => (1 . 2) // Lisp
// (cons 1 nil) => (1)   // Lisp
// (car '()) => nil      // Lisp
//
// Specification of Vimana list functions:
//
// 1 2 cons => error
// 1 () cons => (1)
// 1 (2) cons => (1 2)
// () () cons => (())
// () (1) cons => (() 1)
// () first => ()
// () rest => ()
// () first () cons => (())
// () () eq => 1
// (1) (1) eq => 0
// () isempty => 1
// setfirst and setrest do NOT push the modified list to the data stack
// (1 2 3) 4 setfirst => (4 2 3)
// () 4 setfirst => (4)
// (1 2 3) (4) setfirst => ((4) 1 2 3)
// (1 2 3) () setfirst => (() 1 2 3)
// TODO: setrest is not implemented
// (1 2 3) (4) setrest => (1 4)
// (1) () setrest => (1)
// () (1) setrest => (1)
// () () setrest => ()
//
// Define isempty in vimana:
//
// (isempty) (() eq) def
//

// list first -> item
void PrimFun_first(VInterp* interp)
{
  VItem* list = InterpStackTop(interp);
  ItemMustBeList(list, GURU_FIRST_ITEM_IS_NOT_LIST);

  // Get first of non-empty list
  if (!IsEmpty(list))
  {
    // Get first item
    VItem* item = ItemGetFirstItem(list);

    // Copy first item to data stack
    *list = *item;
  }
  // else:
  //   Leave empty list on the stack
  //   () first => ()
}

// list rest -> list
void PrimFun_rest(VInterp* interp)
{
  VItem* list = InterpStackTop(interp);
  ItemMustBeList(list, GURU_REST_ITEM_IS_NOT_LIST);

  // Leave empty list on the stack
  // () rest => ()

  // Get rest of non-empty list
  if (!IsEmpty(list))
  {
    // Get first item
    VItem* first = ItemGetFirstItem(list);

    // If empty tail, make empty list item on the stack
    // (1) rest => ()
    if (!ItemGetNextItem(first))
    {
      list->ptr = NULL; // Make list item empty
    }
    else
    {
      // Get second item in the list
      VItem* next = ItemGetNextItem(first);

      // Set second item as first element of the list item on the stack
      ItemSetFirst(list, next);
    }
  }
  // else:
  //   Leave empty list item on the stack
  //   () rest => ()
}

// item list cons -> list
void PrimFun_cons(VInterp* interp)
{
  // Get list and item to cons
  VItem* list = InterpStackPop(interp);
  VItem* item = InterpStackTop(interp);
  ItemMustBeList(list, GURU_CONS_ITEM_IS_NOT_LIST);

  // InterpAllocItem will result in a GC if the
  // list memory has no free items. Therefore, we
  // need to mark items that are no longer on the
  // data stack, as they are unreachable and cannot
  // be accessed by the GC marker.
  if (ListMemIsFull(InterpListMem(interp)))
  {
    ListMemMark(InterpListMem(interp), list);
  }

  // This will be the new list head of the cons
  VItem newList;
  ItemInit(&newList);
  ItemSetType(&newList, ItemGetType(list));

  // Allocate new element - may cause GC
  VItem* newFirst = InterpAllocItem(interp);
  if (NULL == newFirst)
  {
    GURU_MEDITATION(GURU_CONS_OUT_OF_MEMORY);
  }

  // Copy item to new element
  *newFirst = *item;

  if (IsEmpty(list))
  {
    // If empty list, the new item is the last and only element
    ItemSetNext(newFirst, 0);
  }
  else
  {
    // Link new item to the first element of the list
    VItem* first = ItemGetFirstItem(list);
    ItemSetNextItem(newFirst, first);
  }

  // Set first of the new list head to refer to the new element
  ItemSetFirst(&newList, newFirst);

  // Copy new list item to data stack
  *item = newList;
}

// list item setfirst -->
void PrimFun_setfirst(VInterp* interp)
{
  VItem* item = InterpStackPop(interp);
  VItem* list = InterpStackPop(interp);
  ItemMustBeList(list, GURU_SETFIRST_ITEM_IS_NOT_LIST);

  // InterpAllocItem will result in a GC if the
  // list memory has no free items. Therefore, we
  // need to mark items that are no longer on the
  // data stack, as they are unreachable and cannot
  // be accessed by the GC marker.
  if (ListMemIsFull(InterpListMem(interp)))
  {
    ListMemMark(InterpListMem(interp), item);
    ListMemMark(InterpListMem(interp), list);
  }

  // Get first item
  VItem* first = ItemGetFirstItem(list);

  // Set first of empty list
  if (NULL == first)
  {
    // Allocate new element - may cause GC
    first = InterpAllocItem(interp);
    if (NULL == first)
    {
      GURU_MEDITATION(GURU_SETFIRST_OUT_OF_MEMORY);
    }
    ItemSetFirst(list, first);
  }

  // Save address to next item
  VIndex next = ItemGetNext(first);

  // Copy item to first
  *first = *item;

  // Restore address to next
  ItemSetNext(first, next);
}

// list n nth -> item
void PrimFun_nth(VInterp* interp)
{
  VItem* nth  = InterpStackPop(interp);
  VItem* list = InterpStackPop(interp);
  ItemMustBeList(list, GURU_NTH_ITEM_IS_NOT_LIST);
  VItem* node = ItemGetFirstItem(list);
  int n = ItemGetIntNum(nth);

  while (node && n > 0)
  {
    node = ItemGetNextItem(node);
    -- n;
  }

  if (NULL == node)
  {
    GURU_MEDITATION(GURU_NTH_ITEM_IS_NULL);
  }

  InterpStackPush(interp, node);
}

// list n item setnth ->
void PrimFun_setnth(VInterp* interp)
{
  VItem* item = InterpStackPop(interp);
  VItem* nth  = InterpStackPop(interp);
  VItem* list = InterpStackPop(interp);
  ItemMustBeList(list, GURU_SETNTH_ITEM_IS_NOT_LIST);
  VItem* node = ItemGetFirstItem(list);
  int n = ItemGetIntNum(nth);

  while (node && n > 0)
  {
    node = ItemGetNextItem(node);
    -- n;
  }

  if (NULL == node)
  {
    GURU_MEDITATION(GURU_SETNTH_ITEM_IS_NULL);
  }

  // Save address to next item
  VIndex next = ItemGetNext(node);

  // Copy item
  *node = *item;

  // Restore address to next
  ItemSetNext(node, next);
}

// -------------------------------------------------------------
// GC functions
// -------------------------------------------------------------

// gc ->
void PrimFun_gc(VInterp* interp)
{
  InterpGC(interp);
}

// gcverbose ->
void PrimFun_gcverbose(VInterp* interp)
{
  ListMemVerboseGC = 1;
  InterpGC(interp);
}

// -------------------------------------------------------------
// Define function and set variable (name is first)
// -------------------------------------------------------------

// Define function
// (symbol) body def ->
void PrimFun_def(VInterp* interp)
{
  PrimFun_funify(interp);
  PrimFun_swap(interp);
  PrimFun_setglobal(interp);
}

// -------------------------------------------------------------
// Performance optimisation experiments
// -------------------------------------------------------------

// Cheating a little
// (globalvar) incr ->
void PrimFun_incr(VInterp* interp)
{
  VItem* list = InterpStackPop(interp);
  //ItemMustBeList(list, GURU_INCR_ITEM_IS_NOT_LIST);
  VItem* symbol = ItemGetFirstItem(list); // symbol in list
  ++ (GlobalVarGet(symbol)->intNum);
}

// Cheating a bit
void PrimFun_gt1(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  a->intNum = a->intNum > 1;
}

// Cheating a bit more
void PrimFun_dup_gt1(VInterp* interp)
{
  PrimFun_dup(interp);
  PrimFun_gt1(interp);
}

// Cheating again
void PrimFun_dup_1minus(VInterp* interp)
{
  PrimFun_dup(interp);
  PrimFun_1minus(interp);
}

// -------------------------------------------------------------
// Call this function to add the core primfuns
// -------------------------------------------------------------

void InterpAddCorePrimFuns(VInterp* interp)
{
  InterpAddPrimFun(interp, "sayhi", PrimFun_sayhi);
  InterpAddPrimFun(interp, "saymantra", PrimFun_saymantra);
  InterpAddPrimFun(interp, "print", PrimFun_print);
  InterpAddPrimFun(interp, "printstack", PrimFun_printstack);
  InterpAddPrimFun(interp, "printglobals", PrimFun_printglobals);
  InterpAddPrimFun(interp, "printprimfuns", PrimFun_printprimfuns);
  InterpAddPrimFun(interp, "eval", PrimFun_eval);
  //InterpAddPrimFun(interp, "popStackFrame", PrimFun_popStackFrame);
  InterpAddPrimFun(interp, "iftrue", PrimFun_iftrue);
  InterpAddPrimFun(interp, "iffalse", PrimFun_iffalse);
  InterpAddPrimFun(interp, "ifelse", PrimFun_ifelse);
  InterpAddPrimFun(interp, "setglobal", PrimFun_setglobal);
  InterpAddPrimFun(interp, "getglobal", PrimFun_getglobal);
  InterpAddPrimFun(interp, "setvar", PrimFun_setglobal);        // synonym
  InterpAddPrimFun(interp, "getvar", PrimFun_getglobal);        // synonym
  InterpAddPrimFun(interp, "funify", PrimFun_funify);
  InterpAddPrimFun(interp, "parse", PrimFun_parse);
  InterpAddPrimFun(interp, "add", PrimFun_plus);                // synonym
  InterpAddPrimFun(interp, "sub", PrimFun_minus);               // synonym
  InterpAddPrimFun(interp, "mult", PrimFun_times);              // synonym
  InterpAddPrimFun(interp, "div", PrimFun_div);                 // synonym
  InterpAddPrimFun(interp, "+", PrimFun_plus);
  InterpAddPrimFun(interp, "-", PrimFun_minus);
  InterpAddPrimFun(interp, "*", PrimFun_times);
  InterpAddPrimFun(interp, "/", PrimFun_div);
  InterpAddPrimFun(interp, "1+", PrimFun_1plus);
  InterpAddPrimFun(interp, "1-", PrimFun_1minus);
  InterpAddPrimFun(interp, "2+", PrimFun_2plus);
  InterpAddPrimFun(interp, "2-", PrimFun_2minus);
  InterpAddPrimFun(interp, "<", PrimFun_lessthan);
  InterpAddPrimFun(interp, ">", PrimFun_greaterthan);
  InterpAddPrimFun(interp, "isbigger", PrimFun_lessthan);       // synonym
  InterpAddPrimFun(interp, "issmaller", PrimFun_greaterthan);   // synonym
  InterpAddPrimFun(interp, "eq", PrimFun_eq);
  InterpAddPrimFun(interp, "stringequals", PrimFun_stringequals);
  InterpAddPrimFun(interp, "iszero", PrimFun_iszero);
  InterpAddPrimFun(interp, "isempty", PrimFun_isempty);
  InterpAddPrimFun(interp, "not", PrimFun_not);
  InterpAddPrimFun(interp, "and", PrimFun_and);
  InterpAddPrimFun(interp, "or", PrimFun_or);
  InterpAddPrimFun(interp, "drop", PrimFun_drop);
  InterpAddPrimFun(interp, "dup", PrimFun_dup);
  InterpAddPrimFun(interp, "swap", PrimFun_swap);
  InterpAddPrimFun(interp, "rot", PrimFun_rot);
  InterpAddPrimFun(interp, "over", PrimFun_over);
  InterpAddPrimFun(interp, "first", PrimFun_first);
  InterpAddPrimFun(interp, "rest", PrimFun_rest);
  InterpAddPrimFun(interp, "cons", PrimFun_cons);
  InterpAddPrimFun(interp, "setfirst", PrimFun_setfirst);
  InterpAddPrimFun(interp, "nth", PrimFun_nth);
  InterpAddPrimFun(interp, "setnth", PrimFun_setnth);
  InterpAddPrimFun(interp, "def", PrimFun_def);
  InterpAddPrimFun(interp, "gc", PrimFun_gc);
  InterpAddPrimFun(interp, "gcverbose", PrimFun_gcverbose);
  InterpAddPrimFun(interp, "incr", PrimFun_incr);
  InterpAddPrimFun(interp, "gt1", PrimFun_gt1);
  InterpAddPrimFun(interp, "dup_gt1", PrimFun_dup_gt1);
  InterpAddPrimFun(interp, "dup_1minus", PrimFun_dup_1minus);
}
