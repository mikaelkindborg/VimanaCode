#define TRACK_MEMORY_USAGE
//#define DEBUG

// Interactive programming based on the clipboard COPY command key
// A new take on dynamic programming environments
// A new take on the REPL - the editor is the REPL
// Commodore 64 inspired
// Interactive programming with the clipboard
// Unconventional retro style programming in a DIY language
// Make primfun or Vimana fun watchclipboard - you can watch the
// clipboard from any part of the program!

/*
0 (counter) setglobal
(loop) (
  counter print
  counter 1 + (counter) setglobal
  1000 sleep
  pasteboard {STOP} eq not (loop) iftrue
) def
loop
*/

#include "vimana.h"
#include "unix/primfuns_unix.h"
#include "unix/terminal.h"
#include <SDL.h>
#include "sdl/primfuns_sdl.h"

#define SYMBOL_TABLE_SIZE 200    // Number of symbols in the symbol table
#define DATA_STACK_SIZE   100    // Number of items on the data stack
#define CALL_STACK_SIZE   100    // Number of stackframes
#define LIST_MEMORY_SIZE  1000   // Number of items in garbage collected list memory

void PrintWelcomeMessage()
{
  TermWrite("Welcome to the Vimana Workbench!\n");
  TermWrite("Here you can evaluate Vimana code interactively.\n");
  TermWrite("Select some code with the mouse and press COMMAND-C.\n");
  TermWrite("Note that the selcted code must start with a colon :\n");
  //TermWrite("The resulting data stack is printed to the screen.\n");
  TermWrite("Note that he colon is a workbench command, it is not part\n");
  TermWrite("of the Vimana language. Code can be copied in any editor.\n");
  TermWrite("To exit press CTRL-Q or CTRL-C.\n");
  TermWrite(": 1 2 + printstack\n");
  TermWrite(": drop\n");
  TermWrite(": (hi) ({Hi World} print) def\n");
  TermWrite(": hi\n");
  TermWrite(": 0 (counter) setglobal\n");
  TermWrite("(clipboard) ({pbpaste} systemcommand) def\n");
  TermWrite("(loop) (\n");
  TermWrite("  counter print\n");
  TermWrite("  counter 1 + (counter) setglobal\n");
  TermWrite("  5000 sleep\n");
  TermWrite("  clipboard {STOP} stringequals (loop) iffalse\n");
  TermWrite(") def\n");
  TermWrite(": loop\n");
}

//{STOP} print (pasteboard) setglobal

void HandleKey(int c)
{
  if (!TermMoveCursor(c))
  {
    unsigned char ch = (unsigned char)c;
    TermWriteChar(&ch);
  }
}

/*
Item:
  ptr -> char* string

stringFind(start, substr)
string stringIndexOfFirstNonBlank -> index

String functions:
string index stringAtIndex -> char*
string index stringCharAtIndex -> char
string index char stringSetCharAtIndex

handle handleToPointer -> bufferPtr (free buffer item but not buffer)
handle unhandle -> bufferPtr (free buffer item but not buffer)
handle handleUnpack  -> bufferPtr (free buffer item but not buffer)
handle handleFree (free buffer item and buffer)
bufferPtr handleWithBuffer -> handle (create buffer item with bufferPtr)
sysFree

[3 3 3]
: 1 2 + printstack
: drop[3 3 3]
*/

// Return string (dealloc with SysFree)
char* InterpStringifyStack(VInterp* interp, char* prefix, char* postfix)
{
  VDataStack* stack = InterpDataStack(interp);

  VItem* item = (VItem*)(stack->start);
  VItem* top  = (VItem*)(stack->top);

  char*  buffer;
  size_t size;

  VPrintStream* stream = open_memstream(&buffer, &size);
  fprintf(stream, "%s", prefix);

  VBool isFirstItem = TRUE;
  while (item <= top)
  {
    if (!isFirstItem)
    {
      fprintf(stream, " ");
    }
    isFirstItem = FALSE;

    char* s = InterpStringify(interp, item);
    fprintf(stream, "%s", s);
    StringMemReset(InterpStringMem(interp));

    ++ item;
  }

  fprintf(stream, "%s", postfix);

  // Zero-terminate string
  //fputc(0, stream);

  // Close stream
  fclose(stream);

  // Increment SysAllocCounter
  SysAllocCounterIncr();

  return buffer;
}

static char* ClipBoardPrev = NULL;
static char* ClipboardCurrent = NULL;

VBool HandleClipboardEval(VInterp* interp)
{
  VBool quit = FALSE;

  // Get clipboard content
  ClipboardCurrent = SystemCommand("pbpaste");

  if (ClipboardCurrent && ClipBoardPrev)
  {
    if (StrEquals(ClipboardCurrent, ClipBoardPrev))
    {
      // If user has clicked too fast, current and prev
      // will be equal. In this case set clipboard to stack.
      if (StrStartsWith(ClipboardCurrent, ":"))
      {
        // Copy stack state to clipboard
        char* str = InterpStringifyStack(interp, " [", "]");
        SystemCommandSend("pbcopy", str);
        SysFree(str);
      }
    }
    else
    if (StrStartsWith(ClipboardCurrent, ":"))
    {
      // Evaluate clipboard code

      // Parse source code
      VItem* list = InterpParse(interp, ClipboardCurrent + 1);

      // Evaluate list
      InterpEval(interp, list);

      // Copy stack state to clipboard
      char* str = InterpStringifyStack(interp, "[", "]");
      // Print stack to console
      printf("%s\nfoo\n", str); // str begins with a space

      SystemCommandSend("pbcopy", str);
      SysFree(str);

      // Cleanup
      InterpGC(interp);

      printf("bar\n");
    }
  }

  // Free previous state
  if (ClipBoardPrev)
  {
    SysFree(ClipBoardPrev);
  }

  // Save current state
  ClipBoardPrev = ClipboardCurrent;

  return quit;
}

int main()
{
  // Create the Vimana machine
  VInterp* interp = InterpAlloc(
    SYMBOL_TABLE_SIZE, DATA_STACK_SIZE,
    CALL_STACK_SIZE,   LIST_MEMORY_SIZE);

  InterpAddCorePrimFuns(interp);
  InterpAddUnixPrimFuns(interp);
  InterpAddSDLPrimFuns(interp);

  TermScreenSetCommodore64EditMode();
  atexit(TermScreenSetStandardMode);

  TermScreenClear();
  TermCursorHome();

  PrintWelcomeMessage();

  SystemCommandSend("pbcopy", "");

  int c = TermReadChar();

  unsigned long msClipboardWatchInterval = 250;
  unsigned long msClipboardWatchNext =
    UnixMillis() + msClipboardWatchInterval;

  while (KEY_CTRL_C != c && KEY_CTRL_Q != c)
  {
    // Be nice to the processor
    usleep(1000);

    // Handle key
    if (c > 0)
    {
      HandleKey(c);
      //printf("%i\n", c);
    }

    // Read next key
    c = TermReadChar();

    // Clipboard watcher
    unsigned long msNow = UnixMillis();
    if (msNow > msClipboardWatchNext)
    {
      msClipboardWatchNext += msClipboardWatchInterval;

      VBool quit = HandleClipboardEval(interp);
      if (quit)
      {
        c = KEY_CTRL_C;
      }
    }
  }

  TermScreenSetStandardMode();

  printf("\n");

  // Free saved clipboard state
  if (ClipBoardPrev)
  {
    SysFree(ClipBoardPrev);
  }

  InterpFree(interp);

  SysPrintMemStat();

  return 0;
}
