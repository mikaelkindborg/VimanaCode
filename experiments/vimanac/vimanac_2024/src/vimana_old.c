#define TRACK_MEMORY_USAGE
//#define DEBUG

#include <limits.h>
#include "vimana.h"
#include "unix/primfuns_unix.h"
#if defined(VIMANA_SDL)
#include <SDL2/SDL.h>
#include "sdl/primfuns_sdl.h"
#endif

#define SYMBOL_TABLE_SIZE 200    // Number of symbols in the symbol table
#define DATA_STACK_SIZE   100    // Number of items on the data stack
#define CALL_STACK_SIZE   100    // Number of stackframes
#define LIST_MEMORY_SIZE  1000   // Number of items in garbage collected list memory

// TODO: Provide command for exiting interactive mode from the text editor
static VBool GlobalInteractiveMode = FALSE;

int main(int numargs, char* args[])
{
  char*  fileName = NULL;
  char   dirName[PATH_MAX];
  VBool  interativeMode = FALSE;
  time_t lastUpdate = 0;

  if (numargs < 2)
  {
    PrintLine("------------------------------------------------------------");
    PrintLine("Welcome to the wonderful world of Vimana");
    PrintNewLine();
    PrintLine("Specify the filename to evaluate a Vimana source file:");
    PrintLine("./vimana filename");
    PrintNewLine();
    PrintLine("You can also run Vimana in interactive mode with live reload.");
    PrintLine("In this mode, the source file is reloaded when it is updated");
    PrintLine("(when saving the file in a text editor).");
    PrintNewLine();
    PrintLine("Use the following flag to specify interactive mode");
    PrintLine("./vimana --interactive filename");
    PrintLine("");
    PrintLine("Exit interactive mode with CTRL-C");
    PrintLine("------------------------------------------------------------");

    return 0; // Exit
  }
  else
  if (numargs > 3)
  {
    PrintLine("Too many parameters given");

    return 0; // Exit
  }

  for (int i = 1; i < numargs; ++ i)
  {
    if (StrEquals(args[i], "--interactive"))
    {
      GlobalInteractiveMode = TRUE;
    }
    else
    {
      fileName = args[i];
    }
  }

  // Create the Vimana machine
  VInterp* interp = InterpAlloc(
    SYMBOL_TABLE_SIZE, DATA_STACK_SIZE,
    CALL_STACK_SIZE,   LIST_MEMORY_SIZE);
  InterpAddCorePrimFuns(interp);
  InterpAddUnixPrimFuns(interp);
#if defined(VIMANA_SDL)
  InterpAddSDLPrimFuns(interp);
#endif
  InterpPrintNumberOfPrimFuns(interp);

  do
  {
    // Change working directory to that of the source file
    FileDirName(fileName, dirName);
    SetWorkingDir(dirName);

    // Read source file
    char* baseName = FileBaseName(fileName);
    char* code = FileRead(baseName);
    if (NULL == code)
    {
      PrintLine("Cannot read source code file");
      break;
    }

    // Parse source code
    VItem* list = InterpParse(interp, code);
    SysFree(code);

    // Evaluate list
    InterpEval(interp, list);

    // Cleanup
    InterpGC(interp);

    // Handle interactive mode
    if (GlobalInteractiveMode)
    {
      printf("Interactive mode (exit with CTRL-C)\n");
      while (TRUE)
      {
        int result = FileIsModified(baseName, &lastUpdate);

        if (FILE_STAT_ERROR == result)
        {
          GlobalInteractiveMode = FALSE;
          break;
        }

        if (FILE_STAT_MODIFIED == result)
        {
          break;
        }

        usleep(100000);
      }
    }
  }
  while (GlobalInteractiveMode);

  #ifdef TRACK_MEMORY_USAGE
    PrintLine("------------------------------------------------------------");
  #endif

  InterpFree(interp);

  #ifdef TRACK_MEMORY_USAGE
    SysPrintMemStat();
    PrintLine("------------------------------------------------------------");
  #endif

  return 0;
}
