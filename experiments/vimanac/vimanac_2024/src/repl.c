// cc repl.c -lreadline

// https://twobithistory.org/2019/08/22/readline.html

#define TRACK_MEMORY_USAGE
//#define DEBUG

#include <readline/readline.h>
#include <readline/history.h>
#include "vimana.h"

#define SYMBOL_TABLE_SIZE 200    // Number of symbols in the symbol table
#define DATA_STACK_SIZE   100    // Number of items on the data stack
#define CALL_STACK_SIZE   100    // Number of stackframes
#define LIST_MEMORY_SIZE  1000   // Number of items in garbage collected list memory

int main()
{
  int run = 1;

  // Create the Vimana machine
  VInterp* interp = InterpAlloc(
    SYMBOL_TABLE_SIZE, DATA_STACK_SIZE,
    CALL_STACK_SIZE,   LIST_MEMORY_SIZE);
  InterpAddCorePrimFuns(interp);
  InterpPrintNumberOfPrimFuns(interp);

  //PrintLine("------------------------------------------------------------");
  PrintLine("Welcome to the wonderful world of Vimana");
  PrintLine("To quit type: exit");

  while (run)
  {
    char* line = readline(": ");
    if (0 == strcmp(line, "exit"))
    {
      run = 0;
    }
    else
    {
      add_history(line);

      VItem* list = InterpParse(interp, line);
      InterpEval(interp, list);
      InterpGC(interp);

      PrimFun_printstack(interp);

      //usleep(100000);
    }

    free(line);
  }

  #ifdef TRACK_MEMORY_USAGE
    PrintLine("------------------------------------------------------------");
  #endif

  InterpFree(interp);

  #ifdef TRACK_MEMORY_USAGE
    SysPrintMemStat();
    PrintLine("------------------------------------------------------------");
  #endif

  return 0;
}
