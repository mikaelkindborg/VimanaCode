/*
File: base.h
Author: Mikael Kindborg (mikael@kindborg.com)

Basic data types and functions.
*/

// -------------------------------------------------------------
// Pointer size
// -------------------------------------------------------------

// Determine pointer size
#if   UINTPTR_MAX == 0xFFFF
  #define VIMANA_16
#elif UINTPTR_MAX == 0xFFFFFFFF
  #define VIMANA_32
#elif UINTPTR_MAX == 0xFFFFFFFFFFFFFFFF
  #define VIMANA_64
#else
  #error Unknow pointer size
#endif

// TEST 32 bit functions
//#undef  VIMANA_64
//#define VIMANA_32

// -------------------------------------------------------------
// Basic types - Vimana types are prefixed with V
// -------------------------------------------------------------

typedef uint8_t    VByte;  // unsigned char
typedef intptr_t   VInt;   // signed integer
typedef uintptr_t  VUInt;  // unsigned integer
typedef uintptr_t  VType;  // Item type
typedef uint8_t    VBool;  // bool type
typedef uintptr_t  VIndex; // index type

/*
// TODO: Make use of integers or floats configurable
#if defined(USE_DOUBLE)
typedef double     VNum;  // use decimal numbers
#else
typedef long       VNum;  // use signed integers
#endif
*/

// -------------------------------------------------------------
// Boolean values
// -------------------------------------------------------------

#define TRUE  1
#define FALSE 0

// -------------------------------------------------------------
// Interpreter forward declarations
// -------------------------------------------------------------

typedef struct __VInterp VInterp;
//typedef struct __VListMemory VListMemory;
typedef void (*VPrimFunPtr) (VInterp*);

// -------------------------------------------------------------
// Print functions
// -------------------------------------------------------------

#define Print(str)        fputs(str, stdout)
#define PrintIntNum(num)  printf("%ld", (long)(num))
#define PrintChar(c)      printf("%c",  (char)(c))
#define PrintNewLine()    printf("\n")
#define PrintLine(str)    printf("%s\n", str)
