#define TRACK_MEMORY_USAGE
//#define DEBUG

#include <limits.h>
#include "vimana.h"
#include "unix/primfuns_unix.h"
#if defined(VIMANA_SDL)
#include <SDL.h>
#include "sdl/primfuns_sdl.h"
#endif

#define SYMBOL_TABLE_SIZE 200    // Number of symbols in the symbol table
#define DATA_STACK_SIZE   100    // Number of items on the data stack
#define CALL_STACK_SIZE   100    // Number of stackframes
#define LIST_MEMORY_SIZE  1000   // Number of items in garbage collected list memory

int main(int numargs, char* args[])
{
  char*  fileName = NULL;
  char   dirName[PATH_MAX];

  if (2 != numargs)
  {
    PrintLine("------------------------------------------------------------");
    PrintLine("Welcome to the wonderful world of Vimana");
    PrintNewLine();
    PrintLine("Specify a filename to evaluate a Vimana source file:");
    PrintLine("./vimana filename");
    PrintLine("Example:");
    PrintLine("./vimana hi.vimana");
    PrintLine("------------------------------------------------------------");

    return 0; // Exit
  }

  // Set filename
  fileName = args[1];

  // Create the Vimana machine
  VInterp* interp = InterpAlloc(
    SYMBOL_TABLE_SIZE, DATA_STACK_SIZE,
    CALL_STACK_SIZE,   LIST_MEMORY_SIZE);
  InterpAddCorePrimFuns(interp);
  InterpAddUnixPrimFuns(interp);
#if defined(VIMANA_SDL)
  InterpAddSDLPrimFuns(interp);
#endif
  InterpPrintNumberOfPrimFuns(interp);

  // Change working directory to that of the source file
  FileDirName(fileName, dirName);
  SetWorkingDir(dirName);

  // Read source file
  char* baseName = FileBaseName(fileName);
  char* code = FileRead(baseName);
  if (NULL == code)
  {
    PrintLine("Cannot read source code file");
  }
  else
  {
    // Parse source code
    VItem* list = InterpParse(interp, code);
    SysFree(code);

    // Evaluate list
    InterpEval(interp, list);

    // Cleanup
    InterpGC(interp);
  }

  #ifdef TRACK_MEMORY_USAGE
    PrintLine("------------------------------------------------------------");
  #endif

  InterpFree(interp);

  #ifdef TRACK_MEMORY_USAGE
    SysPrintMemStat();
    PrintLine("------------------------------------------------------------");
  #endif

  return 0;
}
