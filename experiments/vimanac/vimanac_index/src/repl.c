// cc repl.c -lreadline

// https://twobithistory.org/2019/08/22/readline.html

#define TRACK_MEMORY_USAGE
//#define DEBUG
#define OPTIMIZE

#include "vimana.h"

#define MACHINE_BYTE_SIZE 30744  // Total memory size in bytes
#define SYMBOL_TABLE_SIZE 100    // Number of symbols in the symbol table
#define DATA_STACK_SIZE   100    // Number of items on the data stack
#define CALL_STACK_SIZE   100    // Number of stackframes
#define LIST_MEMORY_SIZE  1000   // Number of items in garbage collected list memory

#include <readline/readline.h>
#include <readline/history.h>

int main()
{
  int run = 1;

  // Create the Vimana machine
  MachineAllocate(MACHINE_BYTE_SIZE);
  MachineAddCorePrimFuns();
  MachineCreate(
    SYMBOL_TABLE_SIZE,
    DATA_STACK_SIZE,
    CALL_STACK_SIZE,
    LIST_MEMORY_SIZE);

  PrintLine("------------------------------------------------------------");
  PrintLine("Welcome to the wonderful world of Vimana");
  PrintLine("To quit type: exit");

#ifdef TRACK_MEMORY_USAGE
  MachinePrintMemoryUse();
#else
  PrintLine("------------------------------------------------------------");
#endif

  VInterp* interp = MachineInterp();

  while (run)
  {
    char* line = readline(": ");
    if (0 == strcmp(line, "exit"))
    {
      run = 0;
    }
    else
    {
      add_history(line);

      VItem* list = InterpParse(MachineInterp(), line);
      InterpEval(MachineInterp(), list);

      // Don't print empty stack
      int numItems = 1 + ( VItemPtr(interp->dataStackTop) - VItemPtr(interp->dataStack) );
      if (numItems > 0)
      {
        PrintItemArray(VItemPtr(interp->dataStack), numItems, interp);
        PrintNewLine();
      }

      usleep(100000);
    }

    free(line);
  }

#ifdef TRACK_MEMORY_USAGE
  PrintLine("------------------------------------------------------------");
#endif

  MachineFree();

#ifdef TRACK_MEMORY_USAGE
  SysPrintMemStat();
  PrintLine("------------------------------------------------------------");
#endif

  return 0;
}
