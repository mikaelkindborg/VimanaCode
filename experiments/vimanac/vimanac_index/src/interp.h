/*
File: interp.h
Author: Mikael Kindborg (mikael@kindborg.com)

Interpreter data structures and functions.
*/

// -------------------------------------------------------------
// Data types and structs
// -------------------------------------------------------------

typedef struct __VStackFrame VStackFrame;

struct __VStackFrame
{
  //VItem*         code;             // List being evaluated
  VItem*         instruction;      // Current instruction (a list item)
};

#define VStackFramePtr(ptr) ((VStackFrame*)(ptr))

typedef struct __VInterp
{
  int             run;                 // Run flag

  VItem*          globalVarTable;      // Global items
  int             globalVarTableSize;  // Max number of global vars

  VByte*          dataStack;           // Data stack items
  VByte*          dataStackEnd;        // End of data stack
  VByte*          dataStackTop;        // Top of data stack

  VByte*          callStack;           // Callstack frames
  VByte*          callStackEnd;        // End of callstack
  VByte*          callStackTop;        // Current stackframe

  VListMemory*    listMemory;          // Lisp-style list memory
}
VInterp;

#define InterpListMem(interp) ((interp)->listMemory)

// -------------------------------------------------------------
// Interp
// -------------------------------------------------------------

// Return the number of bytes needed for the given configuration
int InterpByteSize(
  int sizeGlobalVarTable, int sizeDataStack,
  int sizeCallStack,      int sizeListMemory)
{
  int byteSizeInterpStruct = sizeof(VInterp);
  int byteSizeGlobalVarTable = sizeGlobalVarTable * ItemSize();
  int byteSizeDataStack = sizeDataStack * ItemSize();
  int byteSizeCallStack = sizeCallStack * sizeof(VStackFrame);
  int byteSizeListMemory = ListMemByteSize(sizeListMemory);

  int byteSizeInterp =
    byteSizeInterpStruct +
    byteSizeGlobalVarTable +
    byteSizeDataStack +
    byteSizeCallStack +
    byteSizeListMemory;

  return byteSizeInterp;
}

// Take care to specify aligned memory sizes
void InterpInit(
  VInterp* interp,
  int sizeGlobalVarTable, int sizeDataStack,
  int sizeCallStack,      int sizeListMemory)
{
  int byteSizeInterpStruct = sizeof(VInterp);
  int byteSizeGlobalVarTable = sizeGlobalVarTable * ItemSize();
  int byteSizeDataStack = sizeDataStack * ItemSize();
  int byteSizeCallStack = sizeCallStack * sizeof(VStackFrame);
  int byteSizeListMemory = ListMemByteSize(sizeListMemory);

  interp->globalVarTable = PtrOffset(interp, byteSizeInterpStruct);
  interp->globalVarTableSize = sizeGlobalVarTable;

  interp->dataStack = PtrOffset(interp->globalVarTable, byteSizeGlobalVarTable);
  interp->dataStackEnd = interp->dataStack + byteSizeDataStack;
  interp->dataStackTop = interp->dataStack - ItemSize();

  interp->callStack = PtrOffset(interp->dataStack, byteSizeDataStack);
  interp->callStackEnd = interp->callStack + byteSizeCallStack;
  interp->callStackTop = NULL;

  interp->listMemory = PtrOffset(interp->callStack, byteSizeListMemory);
  ListMemInit(interp->listMemory, sizeListMemory);
}

void InterpFree(VInterp* interp)
{
  ListMemSweep(InterpListMem(interp));

  #ifdef TRACK_MEMORY_USAGE
    ListMemPrintAllocCounter(InterpListMem(interp));
  #endif
}

// -------------------------------------------------------------
// Item access
// -------------------------------------------------------------

// These are high-level macros to access item pointers
// Note that the interpreter instance is the last parameter

#define GetFirst(item, interp) ListMemGetFirst(InterpListMem(interp), item)
#define GetNext(item, interp) ListMemGetNext(InterpListMem(interp), item)

#define SetFirst(item, first, interp) ListMemSetFirst(InterpListMem(interp), item, first)
#define SetNext(item, next, interp) ListMemSetNext(InterpListMem(interp), item, next)

#define AllocItem(interp) ListMemAlloc(InterpListMem(interp))
#define AllocHandle(data, type, interp) ListMemAllocHandle(InterpListMem(interp), data, type)
#define GetHandlePtr(item, interp) ListMemGetHandlePtr(InterpListMem(interp), item)

#define DeallocItem(item, interp) ListMemDeallocItem(InterpListMem(interp), item)

// -------------------------------------------------------------
// Garbage collection
// -------------------------------------------------------------

// GC is not yet fully tested and integrated
void InterpGC(VInterp* interp)
{
  // Mark data stack
  VByte* p = interp->dataStack;
  while (p <= interp->dataStackTop)
  {
    VItem* item = VItemPtr(p);
    if (!IsTypeAtomic(item))
    {
      ListMemMark(InterpListMem(interp), GetFirst(item, interp));
    }
    p += ItemSize();
  }

  // Mark global vars
  VItem* table = interp->globalVarTable;
  for (int i = 0; i < interp->globalVarTableSize; ++ i)
  {
    VItem* item = & (table[i]);
    if (!IsTypeAtomic(item))
    {
      ListMemMark(InterpListMem(interp), GetFirst(item, interp));
    }
  }

  ListMemSweep(InterpListMem(interp));

  #ifdef TRACK_MEMORY_USAGE
  //  ListMemPrintAllocCounter(InterpListMem(interp));
  #endif
}

// -------------------------------------------------------------
// Data stack
// -------------------------------------------------------------

// Copy the given item onto the data stack
void InterpStackPush(VInterp* interp, VItem* item)
{
  interp->dataStackTop += ItemSize();

  if (! (interp->dataStackTop < interp->dataStackEnd) )
  {
    GURU_MEDITATION(DATA_STACK_OVERFLOW);
  }

  // Copy item
  *VItemPtr((interp->dataStackTop)) = *item;
}

// Remove the topmost item from the data stack and return it
VItem* InterpStackPop(VInterp* interp)
{
  if (interp->dataStackTop < interp->dataStack)
  {
    GURU_MEDITATION(DATA_STACK_IS_EMPTY);
  }

  interp->dataStackTop -= ItemSize();
  return VItemPtr(interp->dataStackTop + ItemSize());
}

#define InterpStackTop(interp) VItemPtr(((interp)->dataStackTop))

#define InterpStackAt(interp, offsetFromTop) \
  VItemPtr(((interp)->dataStackTop) - (offsetFromTop * ItemSize()))

// -------------------------------------------------------------
// Call stack
// -------------------------------------------------------------

// Push the inital stackframe
void InterpPushFirstStackFrame(VInterp* interp, VItem* list)
{
  // Set first stackframe
  interp->callStackTop = interp->callStack;

  VStackFrame* current = VStackFramePtr(interp->callStackTop);

  // Set code list
  //current->code = list;

  // Set first instruction in the frame
  current->instruction = GetFirst(list, interp);
}

// Push a new stackframe if needed, reuse top stackframe on tailcall
void InterpPushStackFrame(VInterp* interp, VItem* list)
{
  // The current stackframe is the parent for the new stackframe
  VStackFrame* parent = VStackFramePtr(interp->callStackTop);

  // Assume tailcall (resuse parent stackframe)
  VStackFrame* current = parent;

  // Check tailcall (are there any instructions left?)
  if (parent->instruction)
  {
    // NOT A TAILCALL - PUSH NEW STACK FRAME

    interp->callStackTop += sizeof(VStackFrame);

    if (! (interp->callStackTop < interp->callStackEnd) )
    {
      GURU_MEDITATION(CALL_STACK_OVERFLOW);
    }

    // Set new stackframe
    current = VStackFramePtr(interp->callStackTop);
  }

  // Set current instruction and code list (stackframe is reused on tailcall)
  current->instruction = GetFirst(list, interp);
  //current->code = list;
}

// Pop stackframe
void InterpPopStackFrame(VInterp* interp)
{
  if (interp->callStackTop < interp->callStack)
  {
    GURU_MEDITATION(CALL_STACK_IS_EMPTY);
  }

  interp->callStackTop -= sizeof(VStackFrame);
}

// -------------------------------------------------------------
// Global vars
// -------------------------------------------------------------

// Copies item
void InterpSetGlobalVar(VInterp* interp, int index, VItem* item)
{
  if (index < interp->globalVarTableSize)
  {
    // Copy item
    (interp->globalVarTable)[index] = *item;
  }
  else
  {
    GURU_MEDITATION(GLOBAL_VAR_TABLE_OVERFLOW);
  }
}

#define InterpGetGlobalVar(interp, index) \
  (& (((interp)->globalVarTable)[index]) )

// -------------------------------------------------------------
// Eval
// -------------------------------------------------------------

int InterpEvalSlice(VInterp* interp, int sliceSize);

void InterpEval(VInterp* interp, VItem* list)
{
  InterpPushFirstStackFrame(interp, list);
  InterpEvalSlice(interp, 0);
  InterpGC(interp);
}

// Evaluate a slice of the code.
//
// The idea is to avoid hogging the CPU if needed, for example
// in GUIs, and also make it possible to run multiple interpreters
// concurrently in a single thread.
//
// sliceSize specifies the number of instructions to execute.
// sliceSize 0 means eval as one slice until program ends.
// Returns done flag (TRUE = done, FALSE = not done).
int InterpEvalSlice(VInterp* interp, int sliceSize)
{
  VStackFrame* current;
  VItem*       instruction;
  VItem*       symbolValue;
  int          primFun;
  int          sliceCounter = 0;

  #ifdef DEBUG
    VByte* callStackTopMax = 0;
    long   interpLoops = 0;
  #endif

  interp->run = TRUE;

  while (interp->run)
  {
    #ifdef DEBUG
      if (interp->callStackTop > callStackTopMax) callStackTopMax = interp->callStackTop;
      ++ interpLoops;
    #endif

    // Count slices if a sliceSize is specified.
    if (sliceSize)
    {
      if (sliceSize > sliceCounter)
        ++ sliceCounter;
      else
        goto Exit; // Exit loop
    }

    // Get current instruction
    current = VStackFramePtr(interp->callStackTop);
    instruction = current->instruction;

    // Evaluate current instruction.
    if (instruction)
    {
      // Advance instruction for the NEXT loop
      current->instruction = GetNext(instruction, interp);

      if (IsTypePrimFun(instruction))
      {
        // Call primfun
        VPrimFunPtr fun = ItemGetPrimFun(instruction);
        fun(interp);
      }
      else
      if (IsTypePushable(instruction))
      {
        // Push "pushable" items (lists, numbers, strings)
        InterpStackPush(interp, instruction);
      }
      else
      if (IsTypeSymbol(instruction))
      {
        VItem* value = InterpGetGlobalVar(interp, ItemGetSymbol(instruction));
        if (IsTypeFun(value))
        {
          // Call function
          InterpPushStackFrame(interp, value);
        }
        else
        if (!IsTypeNone(value))
        {
          // Push global variable value
          InterpStackPush(interp, value);
        }
        // else nothing is done (unbound symbols are not pushed onto the stack)
      }
    }
    else // (NULL == instruction - end of instruction list)
    {
      // Pop stackframe
      InterpPopStackFrame(interp);

      // Exit interpreter loop if this was the last stackframe
      if (interp->callStackTop < interp->callStack)
      {
        interp->run = FALSE;
        goto Exit; // Exit loop
      }
    }
  } // while

Exit:

  #ifdef DEBUG
    PrintLine("EXIT INTERP LOOP");
    int callstackMax = 1 + (VStackFramePtr(callStackTopMax) - VStackFramePtr(interp->callStack));
    Print("CALLSTACK DEPTH: "); PrintIntNum(callstackMax); PrintNewLine();
    Print("INTERP LOOPS:    "); PrintIntNum(interpLoops);  PrintNewLine();
  #endif

  return ! interp->run;
}
