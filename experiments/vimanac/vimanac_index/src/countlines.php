<?php

$NumLines = 0;
$NumLinesCode = 0;
$NumLinesComments = 0;
$NumLinesBlank = 0;
$NumLinesOpenCurly = 0;
$NumLinesSemiColons = 0;

/*
// Unused line count function (count all lines in a file)
function CountLines($file)
{
  global $NumLines;

  $NumLines += count(file($file));
}
*/

function StringContains($string, $substring)
{
  return false !== strpos($string, $substring);
}

function CountLines($file)
{
  global $NumLines;
  global $NumLinesCode;
  global $NumLinesComment;
  global $NumLinesBlank;
  global $NumLinesOpenCurly;
  global $NumSemiColons;

  $insideMultiLineComment = false;

  foreach (file($file) as $line):
    ++ $NumLines;
    // if inside multi-line comment
    if ($insideMultiLineComment):
      ++ $NumLinesComment;
      if (StringContains($line, "*/")):
        $insideMultiLineComment = false;
      endif;
    elseif (StringContains($line, "/*")):
      ++ $NumLinesComment;
      $insideMultiLineComment = true;
    elseif (preg_match('/^\s*\/\/./', $line)):
      ++ $NumLinesComment;
    elseif (0 == strlen(trim($line))):
      ++ $NumLinesBlank;
    elseif (preg_match('/^\s*\{/', $line)):
      ++ $NumLinesOpenCurly;
    else:
      ++ $NumLinesCode;
    endif;
    $NumSemiColons += substr_count($line, ";");
  endforeach;
}

$files = [
  "base.h",
  "gurumeditation.h",
  "print.h",
  "alloc.h",
  "string.h",
  "file.h",
  "primfuntable.h",
  "symbolmem.h",
  "symboltable.h",
  "item.h",
  "listmem.h",
  "interp.h",
  "parser.h",
  "printlist.h",
  "primfuns.h",
  "machine.h",
];

foreach ($files as $file):
  CountLines($file);
endforeach;

echo "NumLines:          " . ($NumLines) . "\n";
echo "NumLinesCode:      " . ($NumLinesCode) . "\n";
echo "NumLinesOpenCurly: " . ($NumLinesOpenCurly) . "\n";
echo "NumLinesComment:   " . ($NumLinesComment) . "\n";
echo "NumLinesBlank:     " . ($NumLinesBlank) . "\n";
echo "NumSemiColons:     " . ($NumSemiColons) . "\n";

/*
Code size 2023-02-08:

NumLines:          2850
NumLinesCode:      1487
NumLinesOpenCurly: 260
NumLinesComment:   667
NumLinesBlank:     436
NumSemiColons:     746
*/
