/*
File: machine.h
Author: Mikael Kindborg (mikael@kindborg.com)

Functions for creating and using the machine that runs VimanaCode.

Creates a single interpreter (multiple interpreters would be possible).

The primfun table and the symbol table are global and shared by
interpreters (using multiple interpreters has not been tested).

Memory is preallocated in a single block.

Strings are allocated as individual blocks using malloc.

How to use:

  // You have to manually tweak these settings (primfun table must also fit)
  #define MACHINE_BYTE_SIZE (128 * 1024)  // Total allocated memory size in bytes
  #define SYMBOL_TABLE_SIZE 100           // Number of symbols in symbol table
  #define DATA_STACK_SIZE   100           // Number of items on the data stack
  #define CALL_STACK_SIZE   100           // Number of stack frames
  #define LIST_MEMORY_SIZE  1000          // Number of items in garbage collected memory

  MachineAllocate(MACHINE_BYTE_SIZE);
  MachineAddCorePrimFuns();
  //AddCustomPrimFuns();
  MachineCreate(
    SYMBOL_TABLE_SIZE,
    DATA_STACK_SIZE,
    CALL_STACK_SIZE,
    LIST_MEMORY_SIZE);
  MachineEval("(Hi World) print");
*/

// -------------------------------------------------------------
// Globals
// -------------------------------------------------------------

// System memory (single memory block)
static VByte*  GlobalMemory;
static VByte*  GlobalMemoryEnd;
static int     GlobalMemoryByteSize;

// Single global interpreter instance (it would be
// possible to have multiple interperter instances)
static VByte*  GlobalInterp;
static int     GlobalInterpByteSize;

// -------------------------------------------------------------
// System memory
// -------------------------------------------------------------

// Allocate memory block
void MachineAllocate(int numBytes)
{
  GlobalMemory = SysAlloc(numBytes);
  GlobalMemoryByteSize = numBytes;
}

// Deallocate memory block
void MachineDeallocate()
{
  SysFree(GlobalMemory);
}

// Print memory usage (helps to tweak memory allocation)
void MachinePrintMemoryUse()
{
  int memoryUsed = GlobalMemoryEnd - GlobalMemory;

  printf("------------------------------------------------------------\n");
  printf("Vimana Machine Memory\n");
  printf("------------------------------------------------------------\n");
  printf("Allocated:      %i\n", GlobalMemoryByteSize);
  printf("Used:           %i\n", memoryUsed);
  printf("Primfun table:  %i\n", PrimFunTableByteSize());
  printf("Num primfuns:   %i\n", GlobalPrimFunTableSize);
  printf("Symbol table:   %i\n", SymbolTableByteSize(SymbolTableMaxSize()));
  printf("Symbol memory:  %i\n", SymbolMemByteSize(SymbolMemGlobal()->size));
  printf("Interpreter:    %i\n", GlobalInterpByteSize);
  printf("------------------------------------------------------------\n");
}

// -------------------------------------------------------------
// Interpreter instance
// -------------------------------------------------------------

// Get the single interpreter instance
VInterp* MachineInterp()
{
  return (VInterp*) GlobalInterp;
}

// -------------------------------------------------------------
// Create the Vimana machine
// -------------------------------------------------------------

// Add core primfuns
void MachineAddCorePrimFuns()
{
  PrimFunTableInit(GlobalMemory);
  AddCorePrimFuns();
}

// Create symbol table
void MachineCreateSymbolTable(int symbolTableSize)
{
  VByte* symbolTableStart = GlobalMemory + PrimFunTableByteSize();
  SymbolTableInit(symbolTableStart, symbolTableSize);

  VByte* symbolMemoryStart = symbolTableStart + SymbolTableByteSize(symbolTableSize);
  int numChars = symbolTableSize * 10; // Estimate 10 chars per symbol
  SymbolMemInitGlobal(symbolMemoryStart, numChars);

  GlobalInterp = symbolMemoryStart + SymbolMemByteSize(numChars);
}

// Create interpreter instance
void MachineCreateInterp(int dataStackSize, int callStackSize, int listMemorySize)
{
  int numSymbols = SymbolTableMaxSize();

  GlobalInterpByteSize = InterpByteSize(numSymbols, dataStackSize, callStackSize, listMemorySize);

  GlobalMemoryEnd = GlobalInterp + GlobalInterpByteSize;

  // Check that interpreter will fit in allocated memory
  if (GlobalMemoryEnd > GlobalMemory + GlobalMemoryByteSize)
  {
    PrintLine("OUT OF MEMORY - ALLOCATE MORE MEMORY FOR THE VIMANA MACHINE");
    MachinePrintMemoryUse();
    GURU_MEDITATION(MACHINE_OUT_OF_MEMORY);
  }

  InterpInit(
    (VInterp*) GlobalInterp,
    numSymbols, dataStackSize, callStackSize, listMemorySize);
}

// Create the Vimana machine
void MachineCreate(
  int symbolTableSize, int dataStackSize,
  int callStackSize,   int listMemorySize)
{
  srand(time(0));

  MachineCreateSymbolTable(symbolTableSize);
  MachineCreateInterp(dataStackSize, callStackSize, listMemorySize);
}

// Deallocate the machine
void MachineFree()
{
  InterpFree(MachineInterp());
  MachineDeallocate();
}

// Parsing a string of code (returns a list)
VItem* MachineParse(char* code)
{
  return InterpParse(MachineInterp(), code);
}

// Evaluate a list
void MachineEval(VItem* list)
{
  InterpEval(MachineInterp(), list);
}

// Evaluate a string (parse + eval)
void MachineEvalString(char* code)
{
  MachineEval(MachineParse(code));
}

// Print an item (list or individual item)
void MachinePrint(VItem* item)
{
  InterpPrint(MachineInterp(), item);
}
