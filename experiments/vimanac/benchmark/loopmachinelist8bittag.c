#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine with linked list for program memory.
*/

#define OP_SUB1      1
#define OP_ISZERO    2
#define OP_IFFALSE   3
#define OP_JUMP      4
#define OP_PUSH      5
#define OP_PRINT     6
#define OP_EXIT      7
#define OP_DROP      8

typedef int VIntNum;
typedef unsigned int VUIntNum;

typedef struct __VItem
{
  VIntNum  data;
  VUIntNum next; // next + type/gcmark
}
VItem;

#define VItemData(item) ((item).data)
#define VItemOp(item)   (((item).next) & 0xFFF)
#define VItemNext(item) (((item).next) >> 12)

VItem ProgMem[100];
VIntNum DataStack[10];
//VUIntNum CallStack[10];

int InstrPtr = 0;
int StackTop = -1;
int RunFlag  = 1;

int ProgMemWriteAddress = 0;

int ProgMemAddr()
{
  return ProgMemWriteAddress;
}

void ProgMemWrite(VIntNum data, VUIntNum op)
{
  VItem item;
  item.data = data;
  item.next = ((1 + ProgMemWriteAddress) << 12) | op;
  ProgMem[ProgMemWriteAddress] = item;
  ++ ProgMemWriteAddress;
}

void ProgRun()
{
  while (RunFlag)
  {
    VIntNum n;
    VUIntNum addr;

    VItem item = ProgMem[InstrPtr];
    VIntNum op = VItemOp(item);

    //printf("Op: %i\n", op);

    switch (op)
    {
      case OP_SUB1:
        n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        InstrPtr = VItemNext(item);
        break;

      case OP_ISZERO:
        n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        InstrPtr = VItemNext(item);
        break;

      case OP_IFFALSE:
        addr = DataStack[StackTop];
        -- StackTop;
        n = DataStack[StackTop];
        -- StackTop;
        if (!n)
          { InstrPtr = addr; }
        else
          { InstrPtr = VItemNext(item); }
        break;

      case OP_PRINT:
        n = DataStack[StackTop];
        -- StackTop;
        printf("%i\n", n);
        InstrPtr = VItemNext(item);
        break;

      case OP_EXIT:
        RunFlag = 0;
        break;

      default:
        // TODO: Handle GVAR/FUN/PRIM
        switch (op)
        {
          case OP_PUSH:
            ++ StackTop;
            DataStack[StackTop] = VItemData(item);
            InstrPtr = VItemNext(item);
            break;

          case OP_DROP:
            -- StackTop;
            InstrPtr = VItemNext(item);
            break;

          case OP_JUMP:
            addr = DataStack[StackTop];
            -- StackTop;
            InstrPtr = addr;
            break;
        }
        break;
    }
  }
  printf("End of Program\n");
}

int main()
{
  int size = sizeof(VItem);
  printf("sizeof(VItem): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  printf("loopCount: %i\n", loopCount);

  // PUSH NUMBER
  ProgMemWrite(loopCount, OP_PUSH);

  // LOOP
  VIntNum loop = ProgMemAddr();
  ProgMemWrite(0, OP_ISZERO);
  VIntNum countdown = 4 + ProgMemAddr();
  ProgMemWrite(countdown, OP_PUSH);
  ProgMemWrite(0, OP_IFFALSE);
  ProgMemWrite(0, OP_PRINT);
  ProgMemWrite(0, OP_EXIT);

  // COUNTDOWN
  //ProgMemWrite(42, OP_PUSH);  // 1.67s
  //ProgMemWrite(0, OP_DROP);
  //ProgMemWrite(42, OP_PUSH);  // 2.14s
  //ProgMemWrite(0, OP_DROP);
  //ProgMemWrite(42, OP_PUSH);  // 2.61s
  //ProgMemWrite(0, OP_DROP);
  // 0.48s per stack push

  ProgMemWrite(0, OP_SUB1);
  ProgMemWrite(loop, OP_PUSH);
  ProgMemWrite(0, OP_JUMP);

  ProgRun(); // 1.19s

  return 0;
}

/*
(loop) (
  iszero (countdown) iffalse
) def

(loop) (
  iszero (sub1 loop) iffalse
) def

(countdown) (
  sub1 loop
) def

100000 loop print
*/