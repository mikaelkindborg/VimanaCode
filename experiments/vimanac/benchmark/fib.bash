#!/bin/bash

# Time to compute fib 20:
# miki@mikis-Air v12_c_cons % time bash fib.bash
# 6765
# bash fib.bash  4.38s user 6.92s system 93% cpu 12.088 total

fib()
{
  if [ $1 -le 0 ]
  then
    echo 0
    return 0
  fi
  if [ $1 -le 2 ]
  then
    echo 1
  else
    a=$(fib $[$1-1])
    b=$(fib $[$1-2])
    echo $(($a+$b))
  fi
}

fib 20
