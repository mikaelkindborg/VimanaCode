#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

/*
Compile without optimization
cc loop.c -O0 -fno-inline
*/

/*
void RandomInit()
{
  srand(time(0));
}

int RandomGenerate(int n)
{
  return rand() % n;
}
*/

//long n = 100000000;
long n = 2000000000;

long ComputeNext1()
{
  return 1;
}

long ComputeNext2(long n)
{
  return n - 1;
}

long ComputeNext3()
{
  return n - 1;
}

void ComputeNext4()
{
  n = n - 1;
}

void loop(long n)
{
  while (n > 0)
  {
    n = n - 1;
  }

  printf("Done loop: %ld\n", n);
}

void loopn1(long n)
{
  while (n > 0)
  {
    n = n - ComputeNext1();
  }

  printf("Done loopn1: %ld\n", n);
}

void loopn2(long n)
{
  while (n > 0)
  {
    n = ComputeNext2(n);
  }

  printf("Done loopn2: %ld\n", n);
}

void loopn3()
{
  while (n > 0)
  {
    n = ComputeNext3();
  }

  printf("Done loopn3: %ld\n", n);
}

void loopn4()
{
  while (n > 0)
  {
    ComputeNext4();
  }

  printf("Done loopn4: %ld\n", n);
}

void loopg()
{
  while (n > 0)
  {
    n = n - 1;
  }

  printf("Done loopg: %ld\n", n);
}

void loopp1(long* n)
{
  while (*n > 0)
  {
    *n = *n - 1;
  }

  printf("Done loopp1: %ld\n", *n);
}

void loopp2(long* n)
{
  while (*n > 0)
  {
    -- *n;
  }

  printf("Done loopp2: %ld\n", *n);
}

void loopp3(long* n)
{
  long localn = *n;
  long* pn = &localn;

  while (*pn > 0)
  {
    -- *pn;
  }

  printf("Done loopp3: %ld\n", *pn);
}

int main()
{
  //RandomInit();
  //n = n + RandomGenerate(3);

  loop(n);      // 1.29s
  //loopn1(n);  // 2.55s
  //loopn2(n);  // 4.90s
  //loopn3();   // 4.41s
  //loopn4();   // 4.17s
  //loopg();    // 4.41s
  //loopp1(&n); // 4.37s
  //loopp2(&n); // 4.38s
  //loopp3(&n); // 4.38s

  return 0;
}
