#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine with callstact support.
*/

#define OP_SUB1      1
#define OP_ISZERO    2
#define OP_IFFALSE   3
#define OP_JUMP      4
#define OP_PUSH      5
#define OP_PRINT     6
#define OP_EXIT      7
#define OP_DROP      8
#define OP_CALL      9
#define OP_RETURN    10

typedef long DATA;

DATA ProgMem[100];
DATA DataStack[10];
DATA CallStack[10];

int RunFlag = 1;
int InstrPtr = 0;
int StackTop = -1;
int CallStackTop = -1;

int ProgMemWriteAddress = 0;

int ProgMemAddr()
{
  return ProgMemWriteAddress;
}

void ProgMemWrite(DATA data)
{
  ProgMem[ProgMemWriteAddress] = data;
  ++ ProgMemWriteAddress;
}

void ProgRun()
{
  // Fastest
  DATA n;
  DATA addr;
  DATA op;

  while (RunFlag)
  {
    //switch (ProgMem[InstrPtr]) // slower!
    op = ProgMem[InstrPtr];
    //printf("Op: %li\n", op);
    switch (op)
    {
      case OP_SUB1:
        n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_ISZERO:
        n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        ++ InstrPtr;
        break;
      case OP_IFFALSE:
        n = DataStack[StackTop];
        -- StackTop;
        ++ InstrPtr;
        addr = ProgMem[InstrPtr];
        if (!n)
          { InstrPtr = addr; }
        else
          { ++ InstrPtr; }
        break;
      case OP_JUMP:
        ++ InstrPtr;
        addr = ProgMem[InstrPtr];
        InstrPtr = addr;
        break;
      case OP_CALL:
        ++ InstrPtr;
        ++ CallStackTop;
        CallStack[CallStackTop] = 1 + InstrPtr;
        addr = ProgMem[InstrPtr];
        InstrPtr = addr;
        break;
      case OP_RETURN:
        addr = CallStack[CallStackTop];
        -- CallStackTop;
        InstrPtr = addr;
        break;
      case OP_PUSH:
        ++ InstrPtr;
        n = ProgMem[InstrPtr];
        ++ StackTop;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_DROP:
        -- StackTop;
        ++ InstrPtr;
        break;
      case OP_PRINT:
        n = DataStack[StackTop];
        -- StackTop;
        printf("%li\n", n);
        ++ InstrPtr;
        break;
      case OP_EXIT:
        RunFlag = 0;
        break;
    }
  }
  printf("End of Program\n");
}

int main()
{
  int size = sizeof(DATA);
  //printf("sizeof(DATA): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  //printf("loopCount: %i\n", loopCount);
  //printf("\n");
  // PUSH NUMBER
  ProgMemWrite(OP_PUSH);
  ProgMemWrite(loopCount);

  // LOOP
  DATA loop = ProgMemAddr();
  DATA countdown = 10 + ProgMemAddr();
  ProgMemWrite(OP_CALL);
  ProgMemWrite(countdown);
  ProgMemWrite(OP_ISZERO);
  ProgMemWrite(OP_IFFALSE);
  ProgMemWrite(loop);
  ProgMemWrite(OP_PRINT);
  ProgMemWrite(OP_EXIT);

  // COUNTDOWN FUNCTION
  ProgMemWriteAddress = countdown;
  //ProgMemWrite(OP_PUSH); // 0.73s
  //ProgMemWrite(42);
  //ProgMemWrite(OP_DROP);
  // 0.16s per stack push
  ProgMemWrite(OP_SUB1);
  ProgMemWrite(OP_RETURN);

  ProgRun(); // 0.57s

  return 0;
}
