#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine that uses function calls instead of pointers.
*/

#define OP_SUB1      1
#define OP_ISZERO    2
#define OP_IFFALSE   3
#define OP_JUMP      4
#define OP_PUSH      5
#define OP_PRINT     6
#define OP_EXIT      7

typedef long DATA;
typedef void (*FUN)(void);

DATA ProgMem[100];
DATA DataStack[10];
FUN  OpTable[20];

int InstrPtr = 0;
int StackTop = -1;
int RunFlag  = 1;

int ProgMemWriteAddress = 0;

int ProgMemAddr()
{
  return ProgMemWriteAddress;
}

void ProgMemWrite(DATA data)
{
  ProgMem[ProgMemWriteAddress] = data;
  ++ ProgMemWriteAddress;
}

void OpSub1()
{
  DATA n = DataStack[StackTop];
  -- n;
  DataStack[StackTop] = n;
  ++ InstrPtr;
}

void OpIsZero()
{
  DATA n = DataStack[StackTop];
  ++ StackTop;
  DataStack[StackTop] = (n == 0);
  ++ InstrPtr;
}

void OpIfFalse()
{
  DATA n = DataStack[StackTop];
  -- StackTop;
  ++ InstrPtr;
  DATA addr = ProgMem[InstrPtr];
  if (!n)
    { InstrPtr = addr; }
  else
    { ++ InstrPtr; }
}

void OpJump()
{
  ++ InstrPtr;
  DATA addr = ProgMem[InstrPtr];
  InstrPtr = addr;
}

void OpPush()
{
  ++ InstrPtr;
  DATA n = ProgMem[InstrPtr];
  ++ StackTop;
  DataStack[StackTop] = n;
  ++ InstrPtr;
}

void OpPrint()
{
  DATA n = DataStack[StackTop];
  -- StackTop;
  printf("%li\n", n);
  ++ InstrPtr;
}

void OpExit()
{
  RunFlag = 0;
}

void OpTableInit()
{
  OpTable[OP_SUB1]    = OpSub1;
  OpTable[OP_ISZERO]  = OpIsZero;
  OpTable[OP_IFFALSE] = OpIfFalse;
  OpTable[OP_JUMP]    = OpJump;
  OpTable[OP_PUSH]    = OpPush;
  OpTable[OP_PRINT]   = OpPrint;
  OpTable[OP_EXIT]    = OpExit;
}

void xProgRun()
{
  while (RunFlag)
  {
    FUN fun = (FUN) ProgMem[InstrPtr];
    fun();
  }
  printf("End of Program\n");
}

void ProgRun()
{
  while (RunFlag)
  {
    DATA op = ProgMem[InstrPtr];
    FUN fun = OpTable[op];
    fun();
  }
  printf("End of Program\n");
}

int xmain()
{
  int size = sizeof(DATA);
  //printf("sizeof(DATA): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  printf("loopCount:    %i\n", loopCount);

  // PUSH NUMBER
  ProgMemWrite((DATA)OpPush);
  ProgMemWrite(loopCount);

  // LOOP
  DATA loop = ProgMemAddr();
  ProgMemWrite((DATA)OpIsZero);
  ProgMemWrite((DATA)OpIfFalse);
  DATA countdown = 3 + ProgMemAddr();
  ProgMemWrite(countdown);
  ProgMemWrite((DATA)OpPrint);
  ProgMemWrite((DATA)OpExit);

  // COUNTDOWN
  ProgMemWrite((DATA)OpSub1);
  ProgMemWrite((DATA)OpJump);
  ProgMemWrite(loop);

  ProgRun();

  return 0;
}

int main()
{
  int size = sizeof(DATA);
  printf("sizeof(DATA): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  printf("loopCount:    %i\n", loopCount);

  OpTableInit();

  // PUSH NUMBER
  ProgMemWrite(OP_PUSH);
  ProgMemWrite(loopCount);

  // LOOP
  DATA loop = ProgMemAddr();
  ProgMemWrite(OP_ISZERO);
  ProgMemWrite(OP_IFFALSE);
  DATA countdown = 3 + ProgMemAddr();
  ProgMemWrite(countdown);
  ProgMemWrite(OP_PRINT);
  ProgMemWrite(OP_EXIT);

  // COUNTDOWN
  ProgMemWrite(OP_SUB1);
  ProgMemWrite(OP_JUMP);
  ProgMemWrite(loop);

  ProgRun();

  return 0;
}
