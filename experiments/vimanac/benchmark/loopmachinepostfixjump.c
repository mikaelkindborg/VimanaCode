#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine with postfix jump op.
*/

#define OP_SUB1      1
#define OP_ISZERO    2
#define OP_IFFALSE   3
#define OP_JUMP      4
#define OP_PUSH      5
#define OP_PRINT     6
#define OP_EXIT      7

typedef long DATA;

DATA ProgMem[100];
DATA DataStack[10];

int InstrPtr = 0;
int StackTop = -1;
int RunFlag  = 1;

int ProgMemWriteAddress = 0;

int ProgMemAddr()
{
  return ProgMemWriteAddress;
}

void ProgMemWrite(DATA data)
{
  ProgMem[ProgMemWriteAddress] = data;
  ++ ProgMemWriteAddress;
}

void ProgRun()
{
  DATA n;
  DATA addr;
  DATA op;

  while (RunFlag)
  {
    //switch (ProgMem[InstrPtr]) // slower!
    op = ProgMem[InstrPtr];
    switch (op)
    {
      case OP_SUB1:
        n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_ISZERO:
        n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        ++ InstrPtr;
        break;
      case OP_IFFALSE:
        n = DataStack[StackTop];
        -- StackTop;
        ++ InstrPtr;
        addr = ProgMem[InstrPtr];
        if (!n)
          { InstrPtr = addr; }
        else
          { ++ InstrPtr; }
        break;
      case OP_JUMP:
        addr = DataStack[StackTop];
        -- StackTop;
        InstrPtr = addr;
        break;
      case OP_PUSH:
        ++ InstrPtr;
        n = ProgMem[InstrPtr];
        ++ StackTop;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_PRINT:
        n = DataStack[StackTop];
        -- StackTop;
        printf("%li\n", n);
        ++ InstrPtr;
        break;
      case OP_EXIT:
        RunFlag = 0;
        break;
    }
  }
  printf("End of Program\n");
}

int main()
{
  int size = sizeof(DATA);
  printf("sizeof(DATA): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  printf("loopCount:    %i\n", loopCount);

  // PUSH NUMBER
  ProgMemWrite(OP_PUSH);
  ProgMemWrite(loopCount);

  // LOOP
  DATA loop = ProgMemAddr();
  ProgMemWrite(OP_ISZERO);
  ProgMemWrite(OP_IFFALSE);
  DATA countdown = 3 + ProgMemAddr();
  ProgMemWrite(countdown);
  ProgMemWrite(OP_PRINT);
  ProgMemWrite(OP_EXIT);

  // COUNTDOWN
  ProgMemWrite(OP_SUB1);
  ProgMemWrite(OP_PUSH);
  ProgMemWrite(loop);
  ProgMemWrite(OP_JUMP);

  ProgRun(); // 0.92s

  return 0;
}
