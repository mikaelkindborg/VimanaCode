#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine with linked list for program memory.
*/

#define OP_SUB1      1
#define OP_ISZERO    2
#define OP_IFFALSE   3
#define OP_JUMP      4
#define OP_PUSH      5
#define OP_PRINT     6
#define OP_EXIT      7

typedef int VIntNum;
typedef unsigned int VUIntNum;

typedef struct __VItem
{
  VIntNum  data;
  VUIntNum next; // next + type/gcmark
}
VItem;

#define VItemData(item) ((item).data)
#define VItemOp(item)   (((item).next) & 0xFFF)
#define VItemNext(item) (((item).next) >> 12)

VItem ProgMem[100];
VIntNum DataStack[10];
//VUIntNum CallStack[10];

int InstrPtr = 0;
int StackTop = -1;
int RunFlag  = 1;

int ProgMemWriteAddress = 0;

int ProgMemAddr()
{
  return ProgMemWriteAddress;
}

void ProgMemWrite(VIntNum data, VUIntNum op)
{
  VItem item;
  item.data = data;
  item.next = ((1 + ProgMemWriteAddress) << 12) | op;
  ProgMem[ProgMemWriteAddress] = item;
  ++ ProgMemWriteAddress;
}

void ProgRun()
{
  while (RunFlag)
  {
    VIntNum n;
    VUIntNum addr;

    VItem item = ProgMem[InstrPtr];
    VIntNum op = VItemOp(item);

    //printf("Op: %i\n", op);

    switch (op)
    {
      case OP_SUB1:
        n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        InstrPtr = VItemNext(item);
        break;

      case OP_ISZERO:
        n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        InstrPtr = VItemNext(item);
        break;

      case OP_IFFALSE:
        n = DataStack[StackTop];
        -- StackTop;
        if (!n)
          { InstrPtr = VItemData(item); }
        else
          { InstrPtr = VItemNext(item); }
        break;

      case OP_PRINT:
        n = DataStack[StackTop];
        -- StackTop;
        printf("%i\n", n);
        InstrPtr = VItemNext(item);
        break;

      case OP_EXIT:
        RunFlag = 0;
        break;

      default:
        // TODO: Handle GVAR/FUN/PRIM
        switch (op)
        {
          case OP_PUSH:
            ++ StackTop;
            DataStack[StackTop] = VItemData(item);
            InstrPtr = VItemNext(item);
            break;

          case OP_JUMP:
            InstrPtr = VItemData(item);
            break;
        }
        break;
    }
  }
  printf("End of Program\n");
}

int main()
{
  int size = sizeof(VItem);
  printf("sizeof(VItem): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  printf("loopCount: %i\n", loopCount);

  // PUSH NUMBER
  ProgMemWrite(loopCount, OP_PUSH);

  // LOOP
  VIntNum loop = ProgMemAddr();
  ProgMemWrite(0, OP_ISZERO);
  VIntNum countdown = 3 + ProgMemAddr();
  ProgMemWrite(countdown, OP_IFFALSE);
  ProgMemWrite(0, OP_PRINT);
  ProgMemWrite(0, OP_EXIT);

  // COUNTDOWN
  ProgMemWrite(0, OP_SUB1);
  ProgMemWrite(loop, OP_JUMP);

  ProgRun();

  return 0;
}

/*
Various ideas

(loop) (iszero (countdown) iffalse) def
(loop) (iszero iffalse countdown) def
(countdown) (sub1 loop) def

(loop) (iszero (sub1 loop) iffalse) def
(loop) (iszero iffalse (sub1 loop)) def

100000 loop print

(def foo 1 +)
2 foo print

(foo) (1 +) def
(1 +) :: foo
42 : bar

(countdown) (iszero ifFalseJump countdown2 print) def
(countdown2) (1- jump countdown) def

(fact) (iszero ifelse factend factcount) def
(factend) (drop 1) def
(factcount) (1- fact *) def

(fact) (iszero ifelse (drop 1) (1- fact *)) def

(times) (iszero ifelse timesend timescount) def
(timesend) (drop drop) def
(timescount) (over eval 1- times) def

(fun fact iszero ifelse factend factcount)
(fun factend drop 1)
(fun factcount 1- fact *)

(20 fact print) 10 times
*/