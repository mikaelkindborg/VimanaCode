#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine with tag bit for data and operations
*/

#define OP_SUB1      1
#define OP_ISZERO    2
#define OP_IFFALSE   3
#define OP_JUMP      4
#define OP_PUSH      5
#define OP_PRINT     6
#define OP_EXIT      7
#define OP_DROP      8

typedef long DATA;

#define DataValue(data) ((data) >> 8)
#define DataOp(data)    ((data) & 0xFF)

DATA ProgMem[100];
DATA DataStack[10];

int InstrPtr = 0;
int StackTop = -1;
int RunFlag  = 1;

int ProgMemWriteAddress = 0;

int ProgMemAddr()
{
  return ProgMemWriteAddress;
}

void ProgMemWrite(DATA data, DATA op)
{
  ProgMem[ProgMemWriteAddress] = (data << 8) | op;
  ++ ProgMemWriteAddress;
}

void ProgRun()
{
  // A little slower when these are global
  DATA n;
  DATA addr;
  DATA op;
  DATA data;

  while (RunFlag)
  {
    data = ProgMem[InstrPtr];
    op = DataOp(data);

    switch (op)
    {
      case OP_PUSH:
        ++ StackTop;
        DataStack[StackTop] = DataValue(data);
        ++ InstrPtr;
        break;
      case OP_DROP:
        -- StackTop;
        ++ InstrPtr;
        break;
      case OP_SUB1:
        n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_ISZERO:
        n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        ++ InstrPtr;
        break;
      case OP_IFFALSE:
        n = DataStack[StackTop];
        -- StackTop;
        if (!n)
          { InstrPtr = DataValue(data); }
        else
          { ++ InstrPtr; }
        break;
      case OP_JUMP:
        InstrPtr = DataValue(data);
        break;
      case OP_PRINT:
        n = DataStack[StackTop];
        -- StackTop;
        printf("%li\n", n);
        ++ InstrPtr;
        break;
      case OP_EXIT:
        RunFlag = 0;
        break;
    }
  }
  printf("End of Program\n");
}

int main()
{
  int size = sizeof(DATA);
  printf("sizeof(DATA): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  printf("loopCount:    %i\n", loopCount);

  // PUSH NUMBER
  ProgMemWrite(loopCount, OP_PUSH);

  // LOOP
  DATA loop = ProgMemAddr();
  ProgMemWrite(0, OP_ISZERO);
  DATA countdown = 3 + ProgMemAddr();
  ProgMemWrite(countdown, OP_IFFALSE);
  ProgMemWrite(0, OP_PRINT);
  ProgMemWrite(0, OP_EXIT);

  // COUNTDOWN
  //ProgMemWrite(42, OP_PUSH);  // 1.10s
  //ProgMemWrite(0, OP_DROP);
  // 0.34s per stack push

  ProgMemWrite(0, OP_SUB1);
  ProgMemWrite(loop, OP_JUMP);

  ProgRun(); // 0.76s

  return 0;
}
