variable loopcount
variable counter
10000 loopcount !
0 counter !

: myloop
  loopcount @ 0 DO
    loopcount @ 0 DO
\     i . cr
      counter @ 1+ counter !
    LOOP
  LOOP ;

myloop
counter @ . cr
bye