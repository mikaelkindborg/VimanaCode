#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine with some versions.
*/

#define OP_SUB1      1
#define OP_ISZERO    2
#define OP_IFFALSE   3
#define OP_JUMP      4
#define OP_PUSH      5
#define OP_PRINT     6
#define OP_EXIT      7
#define OP_DROP      8

typedef long DATA;

DATA ProgMem[100];
DATA DataStack[10];

int InstrPtr = 0;
int StackTop = -1;
int RunFlag  = 1;

int ProgMemWriteAddress = 0;

int ProgMemAddr()
{
  return ProgMemWriteAddress;
}

void ProgMemWrite(DATA data)
{
  ProgMem[ProgMemWriteAddress] = data;
  ++ ProgMemWriteAddress;
}

// Fast - no goto
void ProgRun()
{
  while (RunFlag)
  {
    DATA n;
    DATA addr;
    DATA op = ProgMem[InstrPtr];

    switch (op)
    {
      case OP_SUB1:
        n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_ISZERO:
        n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        ++ InstrPtr;
        break;
      case OP_IFFALSE:
        n = DataStack[StackTop];
        -- StackTop;
        ++ InstrPtr;
        addr = ProgMem[InstrPtr];
        if (!n)
          { InstrPtr = addr; }
        else
          { ++ InstrPtr; }
        break;
      case OP_JUMP:
        ++ InstrPtr;
        addr = ProgMem[InstrPtr];
        InstrPtr = addr;
        break;
      case OP_PUSH:
        ++ InstrPtr;
        n = ProgMem[InstrPtr];
        ++ StackTop;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_DROP:
        -- StackTop;
        ++ InstrPtr;
        break;
      case OP_PRINT:
        n = DataStack[StackTop];
        -- StackTop;
        printf("%li\n", n);
        ++ InstrPtr;
        break;
      case OP_EXIT:
        RunFlag = 0;
        break;
    }
  }
  printf("End of Program\n");
}

// Fast - local variables in case statements
void ProgRunWithLocalCaseVariables()
{
  while (RunFlag)
  {
    DATA op = ProgMem[InstrPtr];

    switch (op)
    {
      case OP_SUB1:
      {
        DATA n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      }
      case OP_ISZERO:
      {
        DATA n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        ++ InstrPtr;
        break;
      }
      case OP_IFFALSE:
      {
        DATA n = DataStack[StackTop];
        -- StackTop;
        ++ InstrPtr;
        DATA addr = ProgMem[InstrPtr];
        if (!n)
          { InstrPtr = addr; }
        else
          { ++ InstrPtr; }
        break;
      }
      case OP_JUMP:
      {
        ++ InstrPtr;
        DATA addr = ProgMem[InstrPtr];
        InstrPtr = addr;
        break;
      }
      case OP_PUSH:
      {
        ++ InstrPtr;
        DATA n = ProgMem[InstrPtr];
        ++ StackTop;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      }
      case OP_DROP:
        -- StackTop;
        ++ InstrPtr;
        break;
      case OP_PRINT:
      {
        DATA n = DataStack[StackTop];
        -- StackTop;
        printf("%li\n", n);
        ++ InstrPtr;
        break;
      }
      case OP_EXIT:
        RunFlag = 0;
        break;
    }
  }
  printf("End of Program\n");
}

// Little slower with goto
void ProgRunWithGotoExit()
{
  while (1)
  {
    DATA n;
    DATA addr;
    DATA op = ProgMem[InstrPtr];

    switch (op)
    {
      case OP_SUB1:
        n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_ISZERO:
        n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        ++ InstrPtr;
        break;
      case OP_IFFALSE:
        n = DataStack[StackTop];
        -- StackTop;
        ++ InstrPtr;
        addr = ProgMem[InstrPtr];
        if (!n)
          { InstrPtr = addr; }
        else
          { ++ InstrPtr; }
        break;
      case OP_JUMP:
        ++ InstrPtr;
        addr = ProgMem[InstrPtr];
        InstrPtr = addr;
        break;
      case OP_PUSH:
        ++ InstrPtr;
        n = ProgMem[InstrPtr];
        ++ StackTop;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_DROP:
        -- StackTop;
        ++ InstrPtr;
        break;
      case OP_PRINT:
        n = DataStack[StackTop];
        -- StackTop;
        printf("%li\n", n);
        ++ InstrPtr;
        break;
      case OP_EXIT:
        goto Exit;
        break;
    }
  }
Exit:;
  printf("End of Program\n");
}

int main()
{
  int size = sizeof(DATA);
  //printf("sizeof(DATA): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  printf("loopCount:    %i\n", loopCount);

  // PUSH NUMBER
  ProgMemWrite(OP_PUSH);
  ProgMemWrite(loopCount);

  // LOOP
  DATA loop = ProgMemAddr();
  ProgMemWrite(OP_ISZERO);
  ProgMemWrite(OP_IFFALSE);
  DATA countdown = 3 + ProgMemAddr();
  ProgMemWrite(countdown);
  ProgMemWrite(OP_PRINT);
  ProgMemWrite(OP_EXIT);

  // COUNTDOWN
  //ProgMemWrite(OP_PUSH); // 0.76s
  //ProgMemWrite(42);
  //ProgMemWrite(OP_DROP);
  // 0.14s per stack push

  ProgMemWrite(OP_SUB1);
  ProgMemWrite(OP_JUMP);
  ProgMemWrite(loop);

  ProgRun();                         // 0.62s
  //ProgRunWithLocalCaseVariables(); // 0.63s
  //ProgRunWithGotoExit();           // 0.68s

  return 0;
}
