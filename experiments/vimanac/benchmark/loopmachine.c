#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine used for performance tests.
This file has the fastest basic version.

Apple M1:

// gforth 0.7.3
time gforth loop.fs
0.73s

------------------------------------------------------------
// Plain c (not optimized)
cc loop.c
0.10s

// loopmachine (not optimized)
cc loopmachine.c
time ./a.out
1.83s

// loopmachine (optimized)
cc loopmachine.c -O3
time ./a.out
0.60s
------------------------------------------------------------

// loopmachine with globals
cc loopmachine.c -O3
time ./a.out
0.57s <- switch with global variables is faster (!)

// with locals in ProgRun
cc loopmachinetest.c -O3
time ./a.out
0.62s <- switch without goto exit is a little faster

// with goto exit
cc loopmachinetest.c -O3
time ./a.out
0.68s <-- goto exit is slower

// with postfix jump op
cc loopmachinepostfixjump.c -O3
time ./a.out
0.92s <- a lot slower compared to prefix jump

// direct op function pointers
cc loopmachinefun.c -O3
time ./a.out
1.12s <- function calls are slow

// index to op function pointer table
cc loopmachinefun.c -O3
time ./a.out
1.13s <- not slower than direct funcion call, table lookup is fast

// no local variables in op functions
cc loopmachinefunnolocals.c -O3
time ./a.out
1.20s <- op functions with globals is slower

// tagged data items require more processing (extra pushes)
cc loopmachine1bittag.c -O3
time ./a.out
1.11s

// tagged data items (8 bit tag)
cc loopmachine8bittag.c -O3
time ./a.out
1.13s <- little slower than 1 bit tag

// tagged data items (8 bit tag)
cc loopmachine8bittagprefix.c -O3
time ./a.out
0.76s <- little slower than 1 bit tag

// kind of a linked list instead of array
cc loopmachinelist.c -O3
time ./a.out
1.33s

// linked list with tagged items
cc loopmachinelist8bittag.c -O3
time ./a.out
1.19s <- faster than loopmachinelist.c

// Vimana benchmark
time ./vimana ../benchmark/loop.vimana
3.30s

TODO:
try tagged words, linked list, ...
*/

#define OP_SUB1      1
#define OP_ISZERO    2
#define OP_IFFALSE   3
#define OP_JUMP      4
#define OP_PUSH      5
#define OP_PRINT     6
#define OP_EXIT      7
#define OP_DROP      8

typedef long DATA;

DATA ProgMem[100];
DATA DataStack[10];

int InstrPtr = 0;
int StackTop = -1;
int RunFlag  = 1;

int ProgMemWriteAddress = 0;

int ProgMemAddr()
{
  return ProgMemWriteAddress;
}

void ProgMemWrite(DATA data)
{
  ProgMem[ProgMemWriteAddress] = (data << 8);
  ++ ProgMemWriteAddress;
}

#define PROGMEM() ((ProgMem[InstrPtr]) >> 8)

void ProgRun()
{
  // Fastest
  DATA n;
  DATA addr;
  DATA op;

  while (RunFlag)
  {
    //switch (ProgMem[InstrPtr]) // slower!
    op = PROGMEM();
    //printf("Op: %li\n", op);
    switch (op)
    {
      case OP_SUB1:
        n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_ISZERO:
        n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        ++ InstrPtr;
        break;
      case OP_IFFALSE:
        n = DataStack[StackTop];
        -- StackTop;
        ++ InstrPtr;
        addr = PROGMEM();
        if (!n)
          { InstrPtr = addr; }
        else
          { ++ InstrPtr; }
        break;
      case OP_JUMP:
        ++ InstrPtr;
        addr = PROGMEM();
        InstrPtr = addr;
        break;
      case OP_PUSH:
        ++ InstrPtr;
        n = PROGMEM();
        ++ StackTop;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_DROP:
        -- StackTop;
        ++ InstrPtr;
        break;
      case OP_PRINT:
        n = DataStack[StackTop];
        -- StackTop;
        printf("%li\n", n);
        ++ InstrPtr;
        break;
      case OP_EXIT:
        RunFlag = 0;
        break;
    }
  }
  printf("End of Program\n");
}

int main()
{
  int size = sizeof(DATA);
  //printf("sizeof(DATA): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  printf("loopCount:    %i\n", loopCount);

  // PUSH NUMBER
  ProgMemWrite(OP_PUSH);
  ProgMemWrite(loopCount);

  // LOOP
  DATA loop = ProgMemAddr();
  ProgMemWrite(OP_ISZERO);  // OP_IFZEROJUMP, OP_IFNOTZEROJUMP
  ProgMemWrite(OP_IFFALSE); // Shound be named OP_IFFALSEJUMP
  DATA countdown = 3 + ProgMemAddr();
  ProgMemWrite(countdown);
  ProgMemWrite(OP_PRINT);
  ProgMemWrite(OP_EXIT);

  // COUNTDOWN
  //ProgMemWrite(OP_PUSH); // 0.73s
  //ProgMemWrite(42);
  //ProgMemWrite(OP_DROP);
  // 0.16s per stack push

  ProgMemWrite(OP_SUB1);
  ProgMemWrite(OP_JUMP);
  ProgMemWrite(loop);

  ProgRun(); // 0.57s

  return 0;
}
