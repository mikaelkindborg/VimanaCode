#include <stdio.h>
#include <stdlib.h>

/*
Loop Machine with tag bit for data and operations
*/

#define OP_SUB1      1
#define OP_ISZERO    2
#define OP_IFFALSE   3
#define OP_JUMP      4
#define OP_PRINT     6
#define OP_EXIT      7
#define OP_DROP      8

typedef long DATA;

DATA ProgMem[100];
DATA DataStack[10];

int InstrPtr = 0;
int StackTop = -1;
int RunFlag  = 1;

int ProgMemWriteAddress = 0;

int ProgMemAddr()
{
  return ProgMemWriteAddress;
}

// Operations are tagged with 1
void ProgMemWriteO(DATA data)
{
  ProgMem[ProgMemWriteAddress] = (data << 1) | 1;
  ++ ProgMemWriteAddress;
}

// Data has tag bit 0
void ProgMemWriteD(DATA data)
{
  ProgMem[ProgMemWriteAddress] = (data << 1);
  ++ ProgMemWriteAddress;
}

#define ProgMemData(data) ((data) >> 1)
#define ProgMemDataIsOp(data) ((data) & 1)

// 1.11s with local variables
// 1.14s with global variables
void ProgRun()
{
  // A little slower when these are global
  DATA n;
  DATA addr;
  DATA op;
  DATA data;

  while (RunFlag)
  {
    data = ProgMem[InstrPtr];
    if (!ProgMemDataIsOp(data))
    {
      ++ StackTop;
      DataStack[StackTop] = ProgMemData(data);
      ++ InstrPtr;
      continue;
    }

    op = ProgMemData(data);
    switch (op)
    {
      case OP_DROP:
        -- StackTop;
        ++ InstrPtr;
        break;
      case OP_SUB1:
        n = DataStack[StackTop];
        -- n;
        DataStack[StackTop] = n;
        ++ InstrPtr;
        break;
      case OP_ISZERO:
        n = DataStack[StackTop];
        ++ StackTop;
        DataStack[StackTop] = (n == 0);
        ++ InstrPtr;
        break;
      case OP_IFFALSE:
        addr = DataStack[StackTop];
        -- StackTop;
        n = DataStack[StackTop];
        -- StackTop;
        if (!n)
          { InstrPtr = addr; }
        else
          { ++ InstrPtr; }
        break;
      case OP_JUMP:
        addr = DataStack[StackTop];
        -- StackTop;
        InstrPtr = addr;
        break;
      case OP_PRINT:
        n = DataStack[StackTop];
        -- StackTop;
        printf("%li\n", n);
        ++ InstrPtr;
        break;
      case OP_EXIT:
        RunFlag = 0;
        break;
    }
  }
  printf("End of Program\n");
}

int main()
{
  int size = sizeof(DATA);
  printf("sizeof(DATA): %i\n", size);

  int loopCount = 100000000;
  //int loopCount = 2000000000;
  printf("loopCount:    %i\n", loopCount);

  // PUSH NUMBER
  ProgMemWriteD(loopCount);

  // LOOP
  DATA loop = ProgMemAddr();
  ProgMemWriteO(OP_ISZERO);
  DATA countdown = 4 + ProgMemAddr();
  ProgMemWriteD(countdown);
  ProgMemWriteO(OP_IFFALSE);
  ProgMemWriteO(OP_PRINT);
  ProgMemWriteO(OP_EXIT);

  // COUNTDOWN
  //ProgMemWriteD(42);       // 1.40s
  //ProgMemWriteO(OP_DROP);
  // 0.28s per stack push

  ProgMemWriteO(OP_SUB1);
  ProgMemWriteD(loop);
  ProgMemWriteO(OP_JUMP);

  ProgRun();  // 1.12s

  return 0;
}
