#
# Python 2.7.16
# python fib.py  9.09s user 0.07s system 99% cpu 9.253 total
#
# Python 3.9.5
# python3 fib.py  9.08s user 0.05s system 99% cpu 9.190 total
#
# Update 2022-09-20:
#
# miki@mikis-Air v12_c_cons % time python3 fib.py
# 24157817
# python3 fib.py  8.22s user 0.03s system 99% cpu 8.300 total
#
# miki@mikis-Air v12_c_cons % time python fib.py
# 24157817
# python fib.py  6.50s user 0.09s system 98% cpu 6.706 total
#
# miki@mikis-Air v12_c_cons % time /Library/Frameworks/Python.framework/Versions/3.9/Resources/Python.app/Contents/MacOS/Python ./fib.py
# 24157817
#  ./fib.py  8.31s user 0.02s system 99% cpu 8.342 total
#
# miki@mikis-Air v12_c_cons % time /Library/Frameworks/Python.framework/Versions/3.11/Resources/Python.app/Contents/MacOS/Python ./fib.py
# 24157817
#  ./fib.py  3.75s user 0.01s system 99% cpu 3.774 total

def fib(n):
  if n < 2: return n
  return fib(n - 1) + fib(n - 2)

print(fib(37))
