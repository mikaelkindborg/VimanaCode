# Vimana C

There are currently the following implementations written in C:

* [vimanac_fast](vimanac_fast/) is the fastest implementation.

* [vimanac_index](vimanac_index/) is a somewhat simplified implementation.

* [vimanac_core](vimanac_core/) is an experimental implementation that uses the list memory for "everything" (data stack, call stack, and global symbols). This implementation became more complex than expected (it was meant to be simple).

How to build the interpreter (on Linux and MacOS):

    cd vimanac_fast
    make

Alternatively (use this version on 32 bit platforms):

    cd vimanac_index
    make

Two executable files are created in the directory of the Makefile:

* vimana (the interpreter)
* repl (REPL command line tool)

How to use the interpreter to run the Vimana example programs:

    ./vimana ../examples/hi.vimana
    ./vimana ../examples/introduction.vimana
    ./vimana ../examples/fact.vimana
    ./vimana ../examples/fib.vimana
    ./vimana ../examples/closure.vimana

How to run the benchmark:

    time ./vimana ../benchmark/fib.vimana

How to use the Read Eval Print Loop:

    ./repl
    ------------------------------------------------------------
    Welcome to the wonderful world of Vimana
    To quit type: exit
    ------------------------------------------------------------
    : 1 2 +
    3
    : drop
    : 8
    8
    : dup
    8 8
    : *
    64
    : drop
    : (square) (dup *) def
    : 8 square
    64
    : exit
