/*
File: file.h
Author: Mikael Kindborg (mikael@kindborg.com)
*/

typedef FILE VPrintStream;

#define FILE_STAT_MODIFIED 1
#define FILE_STAT_NOT_MODIFIED 2
#define FILE_STAT_ERROR -1

// Read file - returns allocated buffer
char* FileRead(char* fileName)
{
  char*  buffer;
  size_t size;
  int    c;

  VPrintStream* stream = open_memstream(&buffer, &size);
  FILE* file = fopen(fileName, "r");

  if (file)
  {
    while ((c = getc(file)) != EOF)
    {
      fputc(c, stream);
    }

    fputc(0, stream);
    fclose(file);
    fclose(stream);

    // Increment SysAllocCounter
    SysAllocCounterIncr();

    return buffer;
  }
  else
    return NULL;
}

// https://www.gnu.org/software/libc/manual/html_node/index.html#SEC_Contents

// Return pointer to the basename part of the filename
char* FileBaseName(char* fileName)
{
  char* p = strrchr(fileName, '/');
  if (NULL == p)
  {
    return fileName;
  }
  else
  {
    return p + 1;
  }
}

// Copy dirname part of the filename to resultDirName
// Return FALSE if the filename does not include a directory,
// TRUE if successful
int FileDirName(char* fileName, char* resultDirName)
{
  char* dirNameEnd = strrchr(fileName, '/');
  if (NULL == dirNameEnd)
  {
    return 0; // No directory
  }
  else
  {
    char* pSrc = fileName;
    char* pDest = resultDirName;
    while (pSrc != dirNameEnd)
    {
      *pDest = *pSrc;
      ++ pSrc;
      ++ pDest;
    }
    *pDest = '\0';

    return 1; // Success
  }
}

// Set the working directory.
// Return TRUE on success, FALSE on error
int SetWorkingDir(char* dirName)
{
  return 0 == chdir(dirName);
}

// Check if file is modified, pass lastUpdate = 0
// on first call, like this:
// time_t lastUpdate = 0;
// int result = FileIsModified(fileName, &lastUpdate);
int FileIsModified(char* fileName, time_t* lastUpdate)
{
  struct stat statbuf;

  int result = stat(fileName, &statbuf);
  if (result) return FILE_STAT_ERROR;

  int status = FILE_STAT_NOT_MODIFIED;

  if ((0 != *lastUpdate) && (statbuf.st_mtime > *lastUpdate))
  {
    status = FILE_STAT_MODIFIED;
  }

  *lastUpdate = statbuf.st_mtime;

  return status;
}
