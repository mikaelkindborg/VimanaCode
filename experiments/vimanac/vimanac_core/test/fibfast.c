#define TRACK_MEMORY_USAGE
//#define DEBUG
#define OPTIMIZE

#include "../src/vimana.h"

#define MACHINE_BYTE_SIZE 30744  // Size in bytes
#define NUM_ITEMS         1000   // Number of items in garbage collected memory
#define NUM_SYMBOLS       100    // Number of symbols in symbol table
#define SYMBOL_SIZE       10     // Average symbol size

void fib(VInterp* interp);

int main(int numargs, char* args[])
{
  // Create the Vimana machine
  MachineAllocate(MACHINE_BYTE_SIZE);
  MachineAddCorePrimFuns();
  MachineCreate(NUM_ITEMS, NUM_SYMBOLS, SYMBOL_SIZE);

  VInterp* interp = MachineInterp();

  VItem item;

  ItemSetIntNum(& item, 37);
  InterpStackPush(interp, & item);

  fib(interp);

  PrimFun_print(interp);
}

void fib(VInterp* interp)
{
  //PrimFun_printstack(interp);
  PrimFun_dup(interp);
  VItem item;
  ItemSetIntNum(& item, 1);
  InterpStackPush(interp, & item);
  PrimFun_greaterthan(interp);
  VItem trueOrFalse = InterpStackPop(interp);
  if (trueOrFalse.intNum)
  {
    PrimFun_dup(interp);
    PrimFun_1minus(interp);
    fib(interp);
    PrimFun_swap(interp);
    PrimFun_2minus(interp);
    fib(interp);
    PrimFun_plus(interp);
  }
}
