#define TRACK_MEMORY_USAGE
#define DEBUG

#include "../src/vimana.h"

// -------------------------------------------------------------
// Test helpers
// -------------------------------------------------------------

static int GlobalNumFailedTests = 0;

void ShouldHold(char* description, int condition)
{
  if (!condition)
  {
    ++ GlobalNumFailedTests;
    printf("[FAIL] %s\n", description);
  }
}

void PrintTestResult()
{
  if (GlobalNumFailedTests > 0)
    printf("FAILED TESTS: %i\n", GlobalNumFailedTests);
  else
    printf("ALL TESTS PASS\n");
}

void LogTest(char* testName)
{
  PrintNewLine();
  PrintLine(testName);
  PrintLine("------------------------------------------------------------");
}

// -------------------------------------------------------------
// Tests
// -------------------------------------------------------------

void TestItemAttributes()
{
  LogTest("TestItemAttributes");

  VItem theItem;
  VItem* item = &theItem;

  ItemInit(item);
  PrintBinaryULong(item->next);

  ItemGCMarkSet(item);
  PrintBinaryULong(item->next);

  ItemSetType(item, TypeList);
  PrintBinaryULong(item->next);

  ShouldHold("TestItemAttributes: Item type should equal TypeList", TypeList == ItemGetType(item));

  ItemSetNextAddr(item, 3);
  PrintBinaryULong(item->next);

  ShouldHold("TestItemAttributes: Item next should equal", 3 == ItemGetNextAddr(item));

  ShouldHold("TestItemAttributes: Mark bit should be set", 0 != ItemGetGCMark(item));

  ItemSetIntNum(item, 15);

  ItemGCMarkUnset(item);
  PrintBinaryULong(item->next);

  ShouldHold("TestItemAttributes: Mark bit should be unset", 0 == ItemGetGCMark(item));

  ShouldHold("TestItemAttributes: Item type should equal TypeIntNum", TypeIntNum == ItemGetType(item));

  ShouldHold("TestItemAttributes: Item value should equal 15", 15 == ItemGetIntNum(item));

  printf("Type:  %lu\n", ItemGetType(item));
  printf("Mark:  %lu\n", ItemGetGCMark(item));
  printf("Next:  %lu\n", (unsigned long) ItemGetNextAddr(item));
  printf("First: %lu\n", (unsigned long) ItemGetFirst(item));
}

void TestMemAlloc()
{
  LogTest("TestMemAlloc");

  VListMemory* mem = SysAlloc(ListMemByteSize(10));
  ListMemInit(mem, 10);

  VItem* item;

  item = ListMemAlloc(mem);
  ItemGCMarkSet(item);
  ItemSetType(item, TypeIntNum);
  ItemSetNextAddr(item, 0);

  item = ListMemAlloc(mem);
  ItemGCMarkSet(item);
  ItemSetType(item, TypeIntNum);
  ItemSetNextAddr(item, 0);

  ListMemPrintAllocCounter(mem);

  ListMemSweep(mem);

  ListMemPrintAllocCounter(mem);

  ShouldHold("TestMemAlloc: allocCounter should equal 2", 2 == mem->allocCounter);

  ListMemSweep(mem);
  ListMemPrintAllocCounter(mem);
  ShouldHold("TestMemAlloc: allocCounter should equal 0", 0 == mem->allocCounter);

  SysFree(mem);
}

// Helper function
void PrintItems(VItem* first, VListMemory* mem)
{
  VAddr addr = ListMemGetAddr(mem, first);
  while (addr)
  {
    VItem* item = ListMemGet(mem, addr);
    printf("%li ", ItemGetIntNum(item));
    addr = ItemGetNextAddr(item);
  }
  PrintNewLine();
}

// Helper function
VItem* AllocMaxItems(VListMemory* mem)
{
  // Alloc items until out of memory

  PrintLine("Alloc max items");

  VItem* first = NULL;
  VItem* item;
  VItem* next;

  while (1)
  {
    next = ListMemAlloc(mem);

    if (NULL == next) break;

    if (NULL == first)
    {
      first = next;
      ItemSetIntNum(first, 1);
    }
    else
    {
      ItemSetIntNum(next, ItemGetIntNum(item) + 1);
      ListMemSetNext(mem, item, next);
    }

    item = next;
  }

  return first;
}

// Helper function
int CountItems(VItem* first, VListMemory* mem)
{
  int counter = 0;
  VAddr itemAddr = ListMemGetAddr(mem, first);
  while (itemAddr)
  {
    VItem* item = ListMemGet(mem, itemAddr);
    itemAddr = ItemGetNextAddr(item);
    ++ counter;
  }
  return counter;
}

void TestAllocDealloc()
{
  LogTest("TestAllocDealloc");

  VListMemory* mem = SysAlloc(ListMemByteSize(10));
  ListMemInit(mem, 10);

  PrintLine("Alloc max");
  VItem* first = AllocMaxItems(mem);
  //PrintItems(first, mem);
  int numItems = CountItems(first, mem);
  printf("Num items: %i\n", numItems);

  ListMemSweep(mem);

  PrintLine("Alloc max again");
  first = AllocMaxItems(mem);
  PrintItems(first, mem);
  int numItems2 = CountItems(first, mem);
  printf("Num items: %i\n", numItems2);

  ListMemSweep(mem);

  PrintLine("Alloc max yet again");
  first = AllocMaxItems(mem);
  PrintItems(first, mem);
  int numItems3 = CountItems(first, mem);
  printf("Num items: %i\n", numItems3);

  ListMemSweep(mem);

  ShouldHold("TestAllocDealloc: Allocated items should be equal", numItems == numItems3);

  ListMemPrintAllocCounter(mem);

  SysFree(mem);
  SysPrintMemStat();
}

void TestSetFirst()
{
  LogTest("TestSetFirst");

  VItem* first;
  VItem* item;
  VItem* next;
  VAddr addr;

  VListMemory* mem = SysAlloc(ListMemByteSize(10));
  ListMemInit(mem, 10);

  // Link items by first pointer

  first = ListMemAlloc(mem);
  item = first;
  ItemSetNextAddr(item, 1); // Using next for the item value in this test

  next = ListMemAlloc(mem);
  ItemSetNextAddr(next, 2); // Using next for the item value in this test
  ItemSetFirst(item, next);
  item = next;

  next = ListMemAlloc(mem);
  ItemSetNextAddr(next, 3); // Using next for the item value in this test
  addr = ListMemGetAddr(mem, next);
  ItemSetFirst(item, next);
  item = next;

  // Last item
  ItemSetFirst(item, NULL);

  item = first;
  while (1)
  {
    printf("item: %lu\n", (unsigned long) ItemGetNextAddr(item));
    if (NULL == ItemGetFirst(item)) break;
    item = ListMemGetFirst(mem, item);
  }

  ListMemSweep(mem);

  ShouldHold("TestSetFirst: 0 == mem->allocCounter", 0 == mem->allocCounter);

  ListMemPrintAllocCounter(mem);

  SysFree(mem);
}

void TestMemGetHandlePtr()
{
  LogTest("TestMemGetHandlePtr");

  VItem* item;

  VListMemory* mem = SysAlloc(ListMemByteSize(10));
  ListMemInit(mem, 10);

  char* str1 = "Hi there";
  item = ListMemAllocHandle(mem, StrCopy(str1), TypeString);
  ItemSetType(item, TypeString);

  char* str2 = ListMemGetHandlePtr(mem, item);
  printf("String: %s\n", str2);

  ShouldHold("TestMemGetHandlePtr: Strings should equal", StrEquals(str1, str2));

  ListMemSweep(mem);

  ListMemPrintAllocCounter(mem);

  SysFree(mem);
}

void TestStringItem()
{
  LogTest("TestStringItem");

  VItem* item;

  VListMemory* mem = SysAlloc(ListMemByteSize(10));
  ListMemInit(mem, 10);

  char* str1 = "Hi there";
  item = ListMemAllocHandle(mem, StrCopy(str1), TypeString);
  ItemSetType(item, TypeString);

  VItem* bufferPtrItem = ListMemGetFirst(mem, item);
  char* str2 = (char*) ItemGetPtr(bufferPtrItem);
  printf("String: %s\n", str2);

  ShouldHold("TestStringItem: Strings should equal", StrEquals(str1, str2));

  ListMemSweep(mem);

  ListMemPrintAllocCounter(mem);

  SysFree(mem);
}

void TestArrayWithStringItems()
{
  LogTest("TestArrayWithStringItems");

  // Make room for 5 string items and 5 buffer items
  VListMemory* mem = SysAlloc(ListMemByteSize(10));
  ListMemInit(mem, 10);

  VItem* array[5];
  VItem* item;

  char* str1 = "Hi there";

  for (int i = 0; i < 5; ++ i)
  {
    item = ListMemAllocHandle(mem, StrCopy(str1), TypeString);
    ItemSetType(item, TypeString);
    array[i] = item;
  }

  for (int i = 0; i < 5; ++ i)
  {
    VItem* item = array[i];
    VItem* buffer = ListMemGetFirst(mem, item);
    char* str2 = (char*) ItemGetPtr(buffer);
    ShouldHold("TestArrayWithStringItems: StrEquals(str1, str2)", StrEquals(str1, str2));
    //printf("String: %s\n", str2);
  }

  ShouldHold("TestArrayWithStringItems: 10 == mem->allocCounter", 10 == mem->allocCounter);

  ListMemSweep(mem);

  ListMemPrintAllocCounter(mem);

  ShouldHold("TestArrayWithStringItems: 0 == mem->allocCounter", 0 == mem->allocCounter);

  SysFree(mem);
}

void TestSymbolMemory()
{
  LogTest("TestSymbolMemory");

  void* mem = SysAlloc(SymbolMemByteSize(10, 10));
  SymbolMemInit(mem, 10, 10);

  char* s1 = SymbolMemGetNextFree(mem);
  SymbolMemWriteChar(mem, 'H');
  SymbolMemWriteChar(mem, 'I');
  SymbolMemWriteFinish(mem);

  char* s2 = SymbolMemGetNextFree(mem);
  SymbolMemWriteChar(mem, 'H');
  SymbolMemWriteChar(mem, 'I');
  SymbolMemResetPos(mem);
  SymbolMemWriteChar(mem, ' ');
  SymbolMemWriteChar(mem, 'W');
  SymbolMemWriteChar(mem, 'O');
  SymbolMemWriteChar(mem, 'R');
  SymbolMemWriteChar(mem, 'L');
  SymbolMemWriteChar(mem, 'D');
  SymbolMemWriteFinish(mem);

  ShouldHold("TestSymbolMemory: s1 shold equal", StrEquals(s1, "HI"));
  ShouldHold("TestSymbolMemory: s2 shold equal", StrEquals(s2, " WORLD"));

  printf("s1: %s%s\n", s1 , s2);

  SysFree(mem);
}

// Helper function
void CreateMachine()
{
  MachineAllocate(4000);
  MachineAddCorePrimFuns();
  MachineCreate(
    100, // num items
    10,  // num symbols
    10   // average symbol size
    );
}

void TestMachineCreate()
{
  LogTest("TestMachineCreate");

  CreateMachine();

  //MachineEval("42 print");

  MachineFree();
}

void TestGlobalVars()
{
  LogTest("TestGlobalVars");

  VItem* var;
  VItem* symbol;
  char*  name;
  VItem  ref;
  VItem  value;

  char*  foo = "foo";
  char*  bar = "bar";

  CreateMachine();

  VInterp* interp = MachineInterp();

  // Global variable should not exist
  var = InterpLookupGlobalVarByName(interp, foo);
  ShouldHold("TestGlobalVars: var should be NULL", NULL == var);

  // Add global variable
  var = InterpAddGlobalVar(interp, foo);
  ShouldHold("TestGlobalVars: var should NOT be NULL", NULL != var);

  // Symbol should exist
  symbol = InterpGetNext(interp, var);
  name = ItemGetSymbolString(symbol);
  ShouldHold("TestGlobalVars: name of var should be foo", StrEquals(name, foo));

  // Create variable reference
  ItemSetGlobalVar(& ref, var);
  var = ItemGetGlobalVar(& ref);
  symbol = InterpGetNext(interp, var);
  name = ItemGetSymbolString(symbol);
  ShouldHold("TestGlobalVars: name of var should be foo", StrEquals(name, foo));

  // Lookup name of foo
  name = InterpLookupGlobalVarName(interp, & ref);
  ShouldHold("TestGlobalVars: name of var should be foo", StrEquals(name, foo));

  // Lookup name that does not exist
  ItemSetGlobalVar(& ref, VItemPtr(16));
  name = InterpLookupGlobalVarName(interp, & ref);
  ShouldHold("TestGlobalVars: name of var should be NULL", NULL == name);

  // Add another global variable
  var = InterpAddGlobalVar(interp, bar);
  ShouldHold("TestGlobalVars: var should NOT be NULL", NULL != var);

  // Lookup name of bar
  ItemSetGlobalVar(& ref, var);
  name = InterpLookupGlobalVarName(interp, & ref);
  ShouldHold("TestGlobalVars: name of var should be bar", StrEquals(name, bar));

  // Set global variable value
  ItemSetIntNum(& value, 42);
  InterpSetGlobalVar(interp, & ref, & value);
  VItem* val = InterpGetGlobalVar(interp, & ref);
  ShouldHold("TestGlobalVars: value of var should be 42", 42 == ItemGetIntNum(val));

  MachineFree();
}

void TestParse()
{
  LogTest("TestParse");

  CreateMachine();

  VInterp* interp = MachineInterp();
  VItem* list;
  char*  code;

  code = "foo bar 8888";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();

  code = "1 2 (((3 4) 5))";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();

  code = "";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();
  ShouldHold("TestParse: List should be empty", IsEmpty(list));
  ShouldHold("TestParse: First should be 0", 0 == ItemGetFirst(list));

  code = "()";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();
  ShouldHold("TestParse: Child should be empty", IsEmpty(InterpGetFirst(interp, list)));

  code = "   (( ( ) ))   ";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();
  ShouldHold(
    "TestParse: Innermost should be empty",
    IsEmpty(
      InterpGetFirst(interp,
        InterpGetFirst(interp,
          InterpGetFirst(interp, list)))));

  code = "{Hi World}";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();
  ShouldHold("TestParse: First should be string", IsTypeString(InterpGetFirst(interp, list)));

  code = "{hi {bar}} foo";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();
  ShouldHold("TestParse: First should be string", IsTypeString(InterpGetFirst(interp, list)));

  code = "42";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();
  ShouldHold("TestParse: First should be intnum", IsTypeIntNum(InterpGetFirst(interp, list)));

  code = "42.2";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();
  ShouldHold("TestParse: First should be decnum", IsTypeDecNum(InterpGetFirst(interp, list)));

  code = "42.2.3";
  list = MachineParse(code);
  MachinePrint(list);
  PrintNewLine();
  ShouldHold("TestParse: First should be global var ref", IsTypeGlobalVarRef(InterpGetFirst(interp, list)));

  MachineFree();
}

void TestInterp()
{
  LogTest("TestInterp");

  CreateMachine();

  VInterp* interp = MachineInterp();
  VItem* list;
  char*  code;

  code = "1 2 3 1 2 3 + + + + + (SUM) setglobal SUM dup print";
  list = MachineParse(code);

  MachinePrint(list);
  PrintNewLine();

  MachineEval(list);

  PrimFun_printstack(interp);

  ShouldHold(
    "TestInterp: top of stack should be 12",
    12 == ItemGetIntNum(InterpStackTop(interp)));

  ShouldHold(
    "TestInterp: callStack should be NULL",
    NULL == interp->callStack);

  MachineFree();
}

void TestMachine()
{
  LogTest("TestMachine");

  CreateMachine();

  VInterp* interp = MachineInterp();
  char*  code;

  code = "(foo) (bar) def (foo) ({foo} print) def foo";
  MachineEvalString(code);

  PrimFun_printstack(interp);

  MachineFree();
}

void TestMachineX()
{
  LogTest("TestMachineX");

  CreateMachine();

  VInterp* interp = MachineInterp();
  char*  code;

  code = "(fib) (dup 1 > (dup 1- fib swap 2- fib +) iftrue) def 5 fib";
  MachineEvalString(code);

  ShouldHold(
    "TestMachineX: callStack should be NULL",
    NULL == interp->callStack);

  PrimFun_printstack(interp);

  ShouldHold(
    "TestMachineX: top of stack should be 5",
    5 == ItemGetIntNum(InterpStackTop(interp)));

  MachineFree();
}

// -------------------------------------------------------------
// Main
// -------------------------------------------------------------

int main()
{
  PrintLine("WELCOME TO VIMANACODE TESTS");

  TestItemAttributes();
  TestMemAlloc();
  TestAllocDealloc();
  TestSetFirst();
  TestMemGetHandlePtr();
  TestStringItem();
  TestArrayWithStringItems();
  TestSymbolMemory();
  TestMachineCreate();
  TestGlobalVars();
  TestParse();
  TestInterp();
  TestMachine();
  TestMachineX();

  PrintNewLine();
  SysPrintMemStat();

  PrintNewLine();
  PrintTestResult();

  return 0;
}
