// cc repl.c -lreadline

// https://twobithistory.org/2019/08/22/readline.html

#define TRACK_MEMORY_USAGE
//#define DEBUG
#define OPTIMIZE

#include "vimana.h"

#define MACHINE_BYTE_SIZE 30744  // Total memory size in bytes
#define NUM_ITEMS         1000   // Number of items in garbage collected memory
#define NUM_SYMBOLS       100    // Number of symbols in symbol table
#define SYMBOL_SIZE       10     // Average symbol size

#include <readline/readline.h>
#include <readline/history.h>

int main()
{
  int run = 1;

  // Create the Vimana machine
  MachineAllocate(MACHINE_BYTE_SIZE);
  MachineAddCorePrimFuns();
  MachineCreate(NUM_ITEMS, NUM_SYMBOLS, SYMBOL_SIZE);

  PrintLine("------------------------------------------------------------");
  PrintLine("Welcome to the wonderful world of Vimana");
  PrintLine("To quit type: exit");
  PrintLine("------------------------------------------------------------");

  VInterp* interp = MachineInterp();

  while (run)
  {
    char* line = readline(": ");
    if (0 == strcmp(line, "exit"))
    {
      run = 0;
    }
    else
    {
      add_history(line);

      VItem* list = InterpParse(MachineInterp(), line);
      InterpEval(MachineInterp(), list);

      // Don't print empty stack
      if (NULL != InterpStackTop(interp))
      {
        PrintSequence(InterpStackTop(interp), interp);
        PrintNewLine();
      }

      usleep(100000);
    }

    free(line);
  }

#ifdef TRACK_MEMORY_USAGE
  PrintLine("------------------------------------------------------------");
#endif

  MachineFree();

#ifdef TRACK_MEMORY_USAGE
  SysPrintMemStat();
  PrintLine("------------------------------------------------------------");
#endif

  return 0;
}
