/*
File: machine.h
Author: Mikael Kindborg (mikael@kindborg.com)

Functions for creating and using the machine that runs VimanaCode.

Creates a single interpreter (multiple interpreters would be possible).

The primfun table and the symbol table are global and shared by
interpreters (using multiple interpreters has not been tested).

Memory is preallocated in a single block.

Strings are allocated as individual blocks using malloc.

How to use:

  // You have to manually tweak these settings (primfun table must also fit)
  #define MACHINE_BYTE_SIZE (128 * 1024)  // Total allocated memory size in bytes
  #define NUM_ITEMS         1000          // Number of items in garbage collected memory
  #define NUM_SYMBOLS       100           // Number of symbols in symbol table
  #define SYMBOL_SIZE       10            // Average symbol size

  MachineAllocate(MACHINE_BYTE_SIZE);
  MachineAddCorePrimFuns();
  //AddCustomPrimFuns();
  MachineCreate(NUM_ITEMS, NUM_SYMBOLS, SYMBOL_SIZE);
  MachineEval("(Hi World) print");
*/

// -------------------------------------------------------------
// Globals
// -------------------------------------------------------------

// System memory (single memory block)
static VByte*  GlobalMemoryStart;
static VByte*  GlobalMemoryEnd;
static int     GlobalMemoryByteSize;

// Single global interpreter instance (it would be
// possible to have multiple interperters)
static VByte*  GlobalInterp;
static int     GlobalInterpByteSize;

// -------------------------------------------------------------
// System memory
// -------------------------------------------------------------

// Allocate memory block
void MachineAllocate(int numBytes)
{
  GlobalMemoryStart = SysAlloc(numBytes);
  GlobalMemoryByteSize = numBytes;
}

// Deallocate memory block
void MachineDeallocate()
{
  SysFree(GlobalMemoryStart);
}

// Print memory usage (help to tweak memory allocation)
void MachinePrintMemoryUse(int maxNumItems, int maxNumSymbols, int symbolAverageSize)
{
  int memoryUsed = GlobalMemoryEnd - GlobalMemoryStart;

  printf("------------------------------------------------------------\n");
  printf("Vimana Machine Memory\n");
  printf("------------------------------------------------------------\n");
  printf("Allocated:          %i\n", GlobalMemoryByteSize);
  //printf("GlobalMemoryStart:  %lu\n", (unsigned long) GlobalMemoryStart);
  //printf("GlobalMemoryEnd:    %lu\n", (unsigned long) GlobalMemoryEnd);
  printf("Used:               %i\n", memoryUsed);
  printf("Num primfuns:       %i\n", GlobalPrimFunTableSize);
  printf("Primfun table size: %i\n", PrimFunTableByteSize());
  printf("Interpreter size:   %i\n", GlobalInterpByteSize);
  printf("List mem size:      %i\n", ListMemByteSize(maxNumItems));
  printf("Symbol mem size:    %i\n", SymbolMemByteSize(maxNumSymbols, symbolAverageSize));
  printf("Max num items:      %i\n", maxNumItems);
  printf("Max num symbols:    %i\n", maxNumSymbols);
  printf("------------------------------------------------------------\n");
}

// -------------------------------------------------------------
// Interpreter instance
// -------------------------------------------------------------

// Get the single interpreter instance
VInterp* MachineInterp()
{
  return (VInterp*) GlobalInterp;
}

// -------------------------------------------------------------
// Create the Vimana machine
// -------------------------------------------------------------

// Add core primfuns
void MachineAddCorePrimFuns()
{
  PrimFunTableInit(GlobalMemoryStart);
  AddCorePrimFuns();
}

// Create interpreter instance
void MachineCreateInterp(int maxNumItems, int maxNumSymbols, int symbolAverageSize)
{
  GlobalInterpByteSize = InterpByteSize(maxNumItems, maxNumSymbols, symbolAverageSize);

  GlobalMemoryEnd = GlobalInterp + GlobalInterpByteSize;

  #ifdef TRACK_MEMORY_USAGE
    MachinePrintMemoryUse(maxNumItems, maxNumSymbols, symbolAverageSize);
  #endif

  // Check that interpreter will fit in allocated memory
  if (GlobalMemoryEnd > GlobalMemoryStart + GlobalMemoryByteSize)
  {
    #ifndef TRACK_MEMORY_USAGE
      MachinePrintMemoryUse(maxNumItems, maxNumSymbols, symbolAverageSize);
    #endif
    PrintLine("OUT OF MEMORY - ALLOCATE MORE MEMORY FOR THE VIMANA MACHINE");
    GURU_MEDITATION(MACHINE_OUT_OF_MEMORY);
  }

  InterpInit((VInterp*) GlobalInterp, maxNumItems, maxNumSymbols, symbolAverageSize);
}

// Create the Vimana machine
void MachineCreate(int maxNumItems, int maxNumSymbols, int symbolAverageSize)
{
  // Initialize random number generator
  srand(time(0));

  // Interpreter goes right after primfun table
  GlobalInterp = GlobalMemoryStart + PrimFunTableByteSize();

  // Create interpreter
  MachineCreateInterp(maxNumItems, maxNumSymbols, symbolAverageSize);
}

// Deallocate the machine
void MachineFree()
{
  InterpFree(MachineInterp());
  MachineDeallocate();
}

// Parsing a string of code (returns a list)
VItem* MachineParse(char* code)
{
  return InterpParse(MachineInterp(), code);
}

// Evaluate a list
void MachineEval(VItem* list)
{
  InterpEval(MachineInterp(), list);
}

// Evaluate a string (parse + eval)
void MachineEvalString(char* code)
{
  MachineEval(MachineParse(code));
}

// Print an item (list or individual item)
void MachinePrint(VItem* item)
{
  InterpPrint(MachineInterp(), item);
}
