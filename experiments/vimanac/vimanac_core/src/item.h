/*
File: item.h
Author: Mikael Kindborg (mikael@kindborg.com)

# Items

Items are similar to conses in Lisp. They hold a value and
an address to the next item.

Items have a fixed size and are allocated from a larger block
by the memory manager in listmem.h.

## Item memory, lists, and garbage collection

Items are copied when pushed onto the data stack and
assigned to variables. The data stack and global variables
have separate memory areas (arrays), which are not handled
by the memory manager.

Items allocated by the memory manager are garbage collected.
Items in the data stack and in variables are used as the roots
for garbage collection.

Importantly, items on the stack and in variables are non-garbage
collected items. These items are not garbage collected themselves,
since they are not allocated by the memory manager. However, they
may point to items in the garbage collected space, for example
lists.

Here is an example:

Data Stack     Managed Mem
----------     ------------------------
ListHead   --> FirstItem --> SecondItem

Example of a list that contains a list:

Data Stack     Managed Mem
----------     ------------------------
ListHead   --> ListHead   --> SecondItem
                 !
               FirstItem  --> SecondItem

Note that a list item always is of TypeList or TypeFun.
The list item is the head of the list. The elements in the
list have types that correspond to the data they hold.

For example, the rest function will put a list item on the
data stack that points to the first item in the rest of the list.

Example program:

   (1 2 3) REST

Stack sequence:

    (1 2 3) // List pushed onto the stack
            // rest is called (note that the function
            // itself is NOT pushed onto the stack)
    (2 3)   // Resulting list on the stack

Data Stack     Managed Mem
----------     ------------------------
ListHead   --> 1 -> 2 -> 3 (before call to rest)
ListHead   --> 2 -> 3      (after call to rest)

The item on the stack in the above example has type TypeList.
Item 1, 2 and 3 are in managed memory and have type TypeIntNum.
After rest is called, item 1 can be garbage collected if no
other list is pointing to it. The list item on the stack is
not in managed memory.

## Pointers and addresses

The reference to the first item in a list is a full
pointer stored in the value field of a list item.

The next item field has type info and a garbage collection
mark bit in the low 5 bits. The high bits contain an address
that is an array index which refers to a location in item memory.

To clarify further, items have two fields (see definition of
struct VItem below). The first field is the value field that
contains the value of the item. This can be for example a number
or a pointer. In the case of a list item, the value is a
full pointer to the first item in the list. The second field
holds type info and an address (index) to the next item
in the list. If there is no next item, the address is zero,
indicating the end of the list.

List memory (listmem.h) is essentially just an array, and indexes
can therefore be used to access items. This scheme enables type info
to be kept in the low bits of the next field.

See listmem.h for details on how managed memory is allocated and
how addresses are used to reference items.

## Strings and memory buffers

An item can point to a memory buffer (allocated with malloc).
This is the case with strings, for example. The item pointing
to the string buffer must exist in garbage-collected memory
to be collected, and there may be only one item that refers to
a malloc allocated memory buffer.

For this reason, an additional item is used to refer to the
allocated menory buffer.

This is what it looks like for strings:

Data Stack     Managed Mem     Malloc Mem
----------     -----------     ----------
StringItem --> BufferItem      --> MemoryBuffer

For example:

Data Stack     Managed Mem    Malloc Mem
----------     -----------    ----------
StringItem --> BufferItem     --> 'Hi World'

String items are of TypeString, and there is also a TypeHandle
which is used to refer to other types of allocated memory
buffers, like images or data.

## Primitive functions

Pointers to primitive functions are stored as full pointers in
the item value field in optimized mode. This results in the
best performance, and the function pointers are inserted by
the parser. In non-optimized mode, the id of the function is used.
See code in item.h (this file) and file parser.h for details.

Much more can be said about the interpreter architecture, but
the above gives an introduction to the memory model used, which
is a very important part of the system.
*/

// -------------------------------------------------------------
// Item struct
// -------------------------------------------------------------

typedef struct __VItem
{
  // Value of the item
  union
  {
    VIntNum         intNum;     // Integer value (symbols and integers)
    VDecNum         decNum;     // Floating point value
    VPrimFunPtr     primFunPtr; // Pointer to a primitive function
    struct __VItem* itemPtr;    // Pointer to an item (first child, global var)
    void*           ptr;        // Pointer to a memory block
  };

  // Layout of field "next":
  // Low 4 bits:   Type info
  // Low 5th bit:  GC mark bit
  // High bits:    Address (index) of next item
  // | adress/index of next | mark bit | type info |
  VAddr         next;
}
VItem;

// Cast to VItem*
#define VItemPtr(ptr) ((VItem*)(ptr))

// This looks better
#define ItemSize() sizeof(VItem)

// -------------------------------------------------------------
// Item types
// -------------------------------------------------------------

// Type info and mark bit go in the 5 low bits

#define TypeNone          0x00
#define TypeIntNum        0x01
#define TypeDecNum        0x02
#define TypeList          0x03
#define TypeString        0x04
#define TypeHandle        0x05
#define TypeGlobalVarRef  0x06 // Pushable types are listed before TypeGlobalVarRef
#define TypePrimFun       0x07
#define TypeFun           0x08
#define TypeSymbol        0x09 // References string in StringMem
#define TypeBuffer        0x0A // References buffer allocated with malloc

#define MarkBit           0x10
#define TypeMask          0x0F
#define TagMask           0x1F

// -------------------------------------------------------------
// Access to data in item next field
// -------------------------------------------------------------

#define ItemGetType(item)     ( ((item)->next) & TypeMask )
#define ItemGetGCMark(item)   ( ((item)->next) & MarkBit )
#define ItemGetNextAddr(item) ( ((item)->next) >> 5 )

void ItemGCMarkSet(VItem* item)
{
  item->next = item->next | MarkBit;
}

void ItemGCMarkUnset(VItem* item)
{
  item->next = item->next & ~MarkBit;
}

void ItemSetType(VItem* item, VType type)
{
  item->next = type | (item->next & ~TypeMask);
}

void ItemSetNextAddr(VItem* item, VAddr next)
{
  item->next = (next << 5) | (item->next & TagMask);
}

// -------------------------------------------------------------
// Item type access
// -------------------------------------------------------------

#define IsTypeNone(item)            (TypeNone == ItemGetType(item))
#define IsTypeList(item)            (TypeList == ItemGetType(item))
#define IsTypeIntNum(item)          (TypeIntNum == ItemGetType(item))
#define IsTypeDecNum(item)          (TypeDecNum == ItemGetType(item))
#define IsTypeString(item)          (TypeString == ItemGetType(item))
#define IsTypeHandle(item)          (TypeHandle == ItemGetType(item))
#define IsTypeGlobalVarRef(item)    (TypeGlobalVarRef == ItemGetType(item))
#define IsTypePrimFun(item)         (TypePrimFun == ItemGetType(item))
#define IsTypeFun(item)             (TypeFun == ItemGetType(item))
#define IsTypeSymbol(item)          (TypeSymbol == ItemGetType(item))
#define IsTypeBuffer(item)          (TypeBuffer == ItemGetType(item))

// Pushable types are pushed to the data stack without being evaluated
#define IsTypePushable(item) (ItemGetType(item) < TypeGlobalVarRef)

// Types that do not reference other items
#define IsTypeAtomic(item) \
  (IsTypeNone(item)   || IsTypeIntNum(item)  || IsTypeDecNum(item) || \
   IsTypeSymbol(item) || IsTypePrimFun(item) || IsTypeBuffer(item))

// List types
#define IsList(item) (IsTypeList(item) || IsTypeFun(item))

// Check if list is empty
#define IsEmpty(list) (NULL == ItemGetFirst(list))

// -------------------------------------------------------------
// Access to data in item value field
// -------------------------------------------------------------

#define ItemGetGlobalVar(item)     ((item)->itemPtr)
#define ItemGetSymbolString(item)  ((char*)((item)->ptr))
#define ItemGetPrimFun(item)       ((item)->primFunPtr)
#define ItemGetIntNum(item)        ((item)->intNum)
#define ItemGetDecNum(item)        ((item)->decNum)
#define ItemGetFirst(item)         ((item)->itemPtr)
#define ItemGetPtr(item)           ((item)->ptr)

// Set first of child list
void ItemSetFirst(VItem* item, VItem* first)
{
  item->itemPtr = first;
}

void ItemSetIntNum(VItem* item, VIntNum number)
{
  item->intNum = number;
  ItemSetType(item, TypeIntNum);
}

void ItemSetDecNum(VItem* item, VDecNum number)
{
  item->decNum = number;
  ItemSetType(item, TypeDecNum);
}

// Set pointer that refers to a global variable
void ItemSetGlobalVar(VItem* item, VItem* globalVar)
{
  item->itemPtr = globalVar;
  ItemSetType(item, TypeGlobalVarRef);
}

void ItemSetSymbolString(VItem* item, char* symbolString)
{
  item->ptr = symbolString;
  ItemSetType(item, TypeSymbol);
}

// Set pointer to primfun
void ItemSetPrimFun(VItem* item, VIntNum primFunId)
{
  VPrimFunPtr primFun = PrimFunTableGet(primFunId)->fun;
  item->primFunPtr = primFun;
  ItemSetType(item, TypePrimFun);
}

char* ItemGetPrimFunName(VItem* item)
{
  int index = PrimFunTableLookupByFunPtr(ItemGetPtr(item));
  return PrimFunTableGet(index)->name;
}

// -------------------------------------------------------------
// Init item
// -------------------------------------------------------------

void ItemInit(VItem* item)
{
  item->ptr   = NULL;
  item->next  = 0;
}

// -------------------------------------------------------------
// Copy item
// -------------------------------------------------------------

// Copy item data and type without overwriting the address to next
void ItemCopyPreserveNext(VItem* copyTo, VItem* copyFrom)
{
  VAddr nextAddr = ItemGetNextAddr(copyTo);  // Save address
  *copyTo = *copyFrom;                       // Overwrites address
  ItemSetNextAddr(copyTo, nextAddr);         // Restore address
}

// -------------------------------------------------------------
// Item functions
// -------------------------------------------------------------

// TODO: Needs to be fixed for doubles
// Use function with type checking instead of macro
#define ItemEquals(item1, item2) \
  ( (ItemGetType(item1) == ItemGetType(item2)) && \
    ((item1)->intNum == (item2)->intNum) )
