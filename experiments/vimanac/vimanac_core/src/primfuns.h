/*
File: primfuns.h
Author: Mikael Kindborg (mikael@kindborg.com)
*/

void PrimFun_listglobals(VInterp* interp)
{
  // TODO
}

void PrimFun_listprimfuns(VInterp* interp)
{
  // TODO
}

void PrimFun_sayhi(VInterp* interp)
{
  PrintLine("Hi World!");
}

void PrimFun_saymantra(VInterp* interp)
{
  PrintLine("I follow my breath");
}

void PrimFun_print(VInterp* interp)
{
  VItem item = InterpStackPop(interp);
  InterpPrint(interp, & item);
  PrintNewLine();
}

void PrimFun_printstack(VInterp* interp)
{
  Print("STACK: ");
  PrintSequence(InterpStackTop(interp), interp);
  PrintNewLine();
}

void PrimFun_eval(VInterp* interp)
{
  VItem codeBlock = InterpStackPop(interp);
  InterpPushStackFrame(interp, & codeBlock);
}

void PrimFun_iftrue(VInterp* interp)
{
  VItem trueBlock   = InterpStackPop(interp);
  VItem trueOrFalse = InterpStackPop(interp);
  if (trueOrFalse.intNum)
    InterpPushStackFrame(interp, & trueBlock);
}

void PrimFun_iffalse(VInterp* interp)
{
  VItem falseBlock  = InterpStackPop(interp);
  VItem trueOrFalse = InterpStackPop(interp);
  if (! trueOrFalse.intNum)
    InterpPushStackFrame(interp, & falseBlock);
}

void PrimFun_ifelse(VInterp* interp)
{
  VItem falseBlock  = InterpStackPop(interp);
  VItem trueBlock   = InterpStackPop(interp);
  VItem trueOrFalse = InterpStackPop(interp);
  if (trueOrFalse.intNum)
    InterpPushStackFrame(interp, & trueBlock);
  else
    InterpPushStackFrame(interp, & falseBlock);
}

void PrimFun_setglobal(VInterp* interp)
{
  VItem  list = InterpStackPop(interp);
  VItem  value = InterpStackPop(interp);
  VItem* globalVarRef = InterpGetFirst(interp, & list);
  InterpSetGlobalVar(interp, globalVarRef, & value);
}

void PrimFun_getglobal(VInterp* interp)
{
  VItem  list = InterpStackPop(interp);
  VItem* globalVarRef = InterpGetFirst(interp, & list);
  VItem* value  = InterpGetGlobalVar(interp, globalVarRef);
  InterpStackPush(interp, value);
}

void PrimFun_funify(VInterp* interp)
{
  VItem* list = InterpStackTop(interp);
  ItemSetType(list, TypeFun);
}

void PrimFun_parse(VInterp* interp)
{
  VItem* item = InterpStackTop(interp);
  if (!IsTypeString(item))
    GURU_MEDITATION(PARSE_ARG_NOT_STRING);
  char* string = InterpGetHandlePtr(interp, item);
  VItem* list = InterpParse(interp, string);
  ItemCopyPreserveNext(item, list);
}

void PrimFun_readfile(VInterp* interp)
{
  VItem item = InterpStackPop(interp);

  if (!IsTypeString(& item))
    GURU_MEDITATION(READFILE_ARG_NOT_STRING);

  char* fileName = InterpGetHandlePtr(interp, & item);

  char* data = FileRead(fileName);

  if (NULL == data)
  {
    GURU_MEDITATION(READFILE_FILE_NOT_FOUND);
  }

  VItem* handle = InterpAllocHandle(interp, data, TypeString);
  InterpStackPush(interp, handle);
}

void PrimFun_setworkingdir(VInterp* interp)
{
  VItem item = InterpStackPop(interp);

  if (!IsTypeString(& item))
    GURU_MEDITATION(SETWORKINGDIR_ARG_NOT_STRING);

  char* dirName = InterpGetHandlePtr(interp, & item);
  int result = SetWorkingDir(dirName);

  if (FALSE == result)
  {
    GURU_MEDITATION(SETWORKINGDIR_DIRECTORY_DOES_NOT_EXIST);
  }
}

void PrimFun_plus(VInterp* interp)
{
  VItem  item = InterpStackPop(interp);
  VItem* b = & item;
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a) && IsTypeIntNum(b))
    a->intNum += b->intNum;
  else
  if (IsTypeDecNum(a) && IsTypeDecNum(b))
    a->decNum += b->decNum;
  else
  if (IsTypeIntNum(a) && IsTypeDecNum(b))
    a->intNum += b->decNum;
  else
  if (IsTypeDecNum(a) && IsTypeIntNum(b))
    a->decNum += b->intNum;
  else
    GURU_MEDITATION(PLUS_NOT_A_NUMBER);
}

void PrimFun_minus(VInterp* interp)
{
  VItem  item = InterpStackPop(interp);
  VItem* b = & item;
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a) && IsTypeIntNum(b))
    a->intNum -= b->intNum;
  else
  if (IsTypeDecNum(a) && IsTypeDecNum(b))
    a->decNum -= b->decNum;
  else
  if (IsTypeIntNum(a) && IsTypeDecNum(b))
    a->intNum -= b->decNum;
  else
  if (IsTypeDecNum(a) && IsTypeIntNum(b))
    a->decNum -= b->intNum;
  else
    GURU_MEDITATION(MINUS_NOT_A_NUMBER);
}

void PrimFun_times(VInterp* interp)
{
  VItem  item = InterpStackPop(interp);
  VItem* b = & item;
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a) && IsTypeIntNum(b))
    a->intNum *= b->intNum;
  else
  if (IsTypeDecNum(a) && IsTypeDecNum(b))
    a->decNum *= b->decNum;
  else
  if (IsTypeIntNum(a) && IsTypeDecNum(b))
    a->intNum *= b->decNum;
  else
  if (IsTypeDecNum(a) && IsTypeIntNum(b))
    a->decNum *= b->intNum;
  else
    GURU_MEDITATION(TIMES_NOT_A_NUMBER);
}

void PrimFun_div(VInterp* interp)
{
  VItem  item = InterpStackPop(interp);
  VItem* b = & item;
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a) && IsTypeIntNum(b))
    a->intNum /= b->intNum;
  else
  if (IsTypeDecNum(a) && IsTypeDecNum(b))
    a->decNum /= b->decNum;
  else
  if (IsTypeIntNum(a) && IsTypeDecNum(b))
    a->intNum /= b->decNum;
  else
  if (IsTypeDecNum(a) && IsTypeIntNum(b))
    a->decNum /= b->intNum;
  else
    GURU_MEDITATION(DIV_NOT_A_NUMBER);
}

void PrimFun_1plus(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a))
    a->intNum += 1;
  else
  if (IsTypeDecNum(a))
    a->decNum += 1;
  else
    GURU_MEDITATION(PLUS_1_NOT_A_NUMBER);
}

void PrimFun_2plus(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a))
    a->intNum += 2;
  else
  if (IsTypeDecNum(a))
    a->decNum += 2;
  else
    GURU_MEDITATION(PLUS_2_NOT_A_NUMBER);
}

void PrimFun_1minus(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a))
    a->intNum -= 1;
  else
  if (IsTypeDecNum(a))
    a->decNum -= 1;
  else
    GURU_MEDITATION(MINUS_1_NOT_A_NUMBER);
}

void PrimFun_2minus(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a))
    a->intNum -= 2;
  else
  if (IsTypeDecNum(a))
    a->decNum -= 2;
  else
    GURU_MEDITATION(MINUS_2_NOT_A_NUMBER);
}

void PrimFun_lessthan(VInterp* interp)
{
  VItem  item = InterpStackPop(interp);
  VItem* b = & item;
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a) && IsTypeIntNum(b))
    a->intNum = a->intNum < b->intNum;
  else
  if (IsTypeDecNum(a) && IsTypeDecNum(b))
    a->intNum = a->decNum < b->decNum;
  else
  if (IsTypeIntNum(a) && IsTypeDecNum(b))
    a->intNum = a->intNum < b->decNum;
  else
  if (IsTypeDecNum(a) && IsTypeIntNum(b))
    a->intNum = a->decNum < b->intNum;
  else
    GURU_MEDITATION(LESS_THAN_NOT_A_NUMBER);
}

void PrimFun_greaterthan(VInterp* interp)
{
  VItem  item = InterpStackPop(interp);
  VItem* b = & item;
  VItem* a = InterpStackTop(interp);

  if (IsTypeIntNum(a) && IsTypeIntNum(b))
    a->intNum = a->intNum > b->intNum;
  else
  if (IsTypeDecNum(a) && IsTypeDecNum(b))
    a->intNum = a->decNum > b->decNum;
  else
  if (IsTypeIntNum(a) && IsTypeDecNum(b))
    a->intNum = a->intNum > b->decNum;
  else
  if (IsTypeDecNum(a) && IsTypeIntNum(b))
    a->intNum = a->decNum > b->intNum;
  else
    GURU_MEDITATION(GREATER_THAN_NOT_A_NUMBER);
}

// TODO CORE Support decnum
void PrimFun_eq(VInterp* interp)
{
  VItem  item = InterpStackPop(interp);
  VItem* b = & item;
  VItem* a = InterpStackTop(interp);

  a->intNum = ItemEquals(a, b);
  ItemSetType(a, TypeIntNum);
}

// TODO CORE Support decnum
void PrimFun_iszero(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  a->intNum = 0 == a->intNum;
}

void PrimFun_not(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  a->intNum = ! a->intNum;
}

void PrimFun_drop(VInterp* interp)
{
  InterpStackPop(interp);
}

void PrimFun_dup(VInterp* interp)
{
  VItem* a = InterpStackTop(interp);
  InterpStackPush(interp, a);
}

void PrimFun_swap(VInterp* interp)
{
  VItem b = InterpStackPop(interp);
  VItem a = InterpStackPop(interp);
  InterpStackPush(interp, & b);
  InterpStackPush(interp, & a);
}

void PrimFun_rot(VInterp* interp)
{
  VItem c = InterpStackPop(interp);
  VItem b = InterpStackPop(interp);
  VItem a = InterpStackPop(interp);
  InterpStackPush(interp, & b);
  InterpStackPush(interp, & c);
  InterpStackPush(interp, & a);
}

void PrimFun_over(VInterp* interp)
{
  VItem b = InterpStackPop(interp);
  VItem a = InterpStackPop(interp);
  InterpStackPush(interp, & a);
  InterpStackPush(interp, & b);
  InterpStackPush(interp, & a);
}

// How other languages do it:
// (cons 1 2) => (1 2)   // NewLisp
// (cons 1 2) => (1 . 2) // Lisp
// (cons 1 nil) => (1)   // Lisp
// (car '()) => nil      // Lisp

//
// Specification of Vimana list functions:
//
// 1 2 cons => error
// 1 () cons => (1)
// 1 (2) cons => (1 2)
// () () cons => (())
// () (1) cons => (() 1)
// () first => ()
// () rest => ()
// () first () cons => (())
// () () eq => 1
// (1) (1) eq => 0
// () isempty => 1
// (isempty) (() eq) def
// setfirst and setrest do NOT push the modified list to the data stack
// (1 2 3) 4 setfirst => (4 2 3)
// () 4 setfirst => (4)
// (1 2 3) (4) setfirst => ((4) 1 2 3)
// (1 2 3) () setfirst => (() 1 2 3)
// TODO: setrest is not implemented
// (1 2 3) (4) setrest => (1 4)
// (1) () setrest => (1)
// () (1) setrest => (1)
// () () setrest => ()

// list first -> item
void PrimFun_first(VInterp* interp)
{
  VItem* list = InterpStackTop(interp);

  // Must be a list type
  if (!IsList(list))
    GURU_MEDITATION(FIRST_OBJECT_IS_NOT_A_LIST);

  // Get first of non-empty list
  if (!IsEmpty(list))
  {
    // Get first item
    VItem* item = InterpGetFirst(interp, list);

    // Copy first item to data stack
    ItemCopyPreserveNext(list, item);
  }
  // else:
  //   Leave empty list on the stack
  //   () first => ()
}

// list rest -> list
void PrimFun_rest(VInterp* interp)
{
  VItem* list = InterpStackTop(interp);

  // Must be a list type
  if (!IsList(list))
    GURU_MEDITATION(REST_OBJECT_IS_NOT_A_LIST);

  // Leave empty list on the stack
  // () rest => ()

  // Get rest of non-empty list
  if (!IsEmpty(list))
  {
    // Get first item
    VItem* first = InterpGetFirst(interp, list);

    // If empty tail, make empty list item on the stack
    // (1) rest => ()
    if (!ItemGetNextAddr(first))
    {
      list->itemPtr = NULL; // Make list item empty
    }
    else
    {
      // Get second item in the list
      VItem* next = InterpGetNext(interp, first);

      // Set second item as first element of the list on the stack
      InterpSetFirst(interp, list, next);
    }
  }
  // else:
  //   Leave empty list item on the stack
  //   () rest => ()
}

// item list cons -> list
void PrimFun_cons(VInterp* interp)
{
  // Get list and item to cons
  VItem  list = InterpStackPop(interp);
  VItem* item = InterpStackTop(interp);

  // List must be a list type
  if (!IsList(& list))
    GURU_MEDITATION(CONS_OBJECT_IS_NOT_A_LIST);

  // This will be the new list head of the cons
  VItem newList;
  ItemInit(& newList);
  ItemSetType(& newList, ItemGetType(& list));

  // Allocate new element
  VItem* newFirst = InterpAllocItem(interp);
  if (NULL == newFirst)
    GURU_MEDITATION(CONS_OUT_OF_MEMORY);

  // Copy item to the new element
  *newFirst = *item;

  if (IsEmpty(& list))
  {
    // If empty list, the new item is the last and only element
    ItemSetNextAddr(newFirst, 0);
  }
  else
  {
    // Link new item to the first element of the list
    VItem* first = InterpGetFirst(interp, & list);
    InterpSetNext(interp, newFirst, first);
  }

  // Set first of the new list head to refer to the new element
  InterpSetFirst(interp, & newList, newFirst);

  // Copy new list to data stack
  ItemCopyPreserveNext(item, & newList);
}

// list item setfirst -->
void PrimFun_setfirst(VInterp* interp)
{
  VItem item = InterpStackPop(interp);
  VItem list = InterpStackPop(interp);

  // Must be a list type
  if (!IsList(& list))
    GURU_MEDITATION(SETFIRST_OBJECT_IS_NOT_A_LIST);

  // Get first item
  VItem* first = InterpGetFirst(interp, & list);

  // Set first of empty list
  if (NULL == first)
  {
    first = InterpAllocItem(interp);
    if (NULL == first)
    {
      GURU_MEDITATION(SETFIRST_OUT_OF_MEMORY);
    }
    InterpSetFirst(interp, & list, first);
  }

  // Copy item and preserve address to next
  ItemCopyPreserveNext(first, & item);
}

// Deallocate the first item of the list and return the rest of the list
// This places the first item in the list on the freelist - use with care
// list pop -> list
void PrimFun_pop(VInterp* interp)
{
  VItem* list = InterpStackTop(interp);

  // Must be a list type
  if (!IsList(list))
    GURU_MEDITATION(REST_OBJECT_IS_NOT_A_LIST);

  // Leave empty list on the stack
  // () rest => ()

  // Get rest of non-empty list
  if (!IsEmpty(list))
  {
    // Get first item
    VItem* first = InterpGetFirst(interp, list);

    // If empty tail, make empty list item on the stack
    // (1) rest => ()
    if (!ItemGetNextAddr(first))
    {
      list->itemPtr = NULL; // Make list item empty
    }
    else
    {
      // Get second item in the list
      VItem* next = InterpGetNext(interp, first);

      // Set second item as first element of the list on the stack
      InterpSetFirst(interp, list, next);

      // Dealloc the first item (does not free any item that
      // this item points to, like a string or a list)
      InterpDeallocItem(interp, first);
    }
  }
  // else:
  //   Leave empty list item on the stack
  //   () rest => ()
}

// gc ->
void PrimFun_gc(VInterp* interp)
{
  InterpGC(interp);
}

// gcverbose ->
void PrimFun_gcverbose(VInterp* interp)
{
  ListMemVerboseGC = 1;
  InterpGC(interp);
}

// millis --> millisecond time stamp
void PrimFun_millis(VInterp* interp)
{
  struct timeval timestamp;
  VItem item;

  gettimeofday(&timestamp, NULL);
  long days = (1000 * 60 * 24 * 1000);
  long millis =
    ((timestamp.tv_sec % days) * 1000) +
    (timestamp.tv_usec / 1000);
  //printf("sec: %li millis: %li\n", (long)timestamp.tv_sec % days, (long)timestamp.tv_usec / 1000);
  ItemSetIntNum(& item, millis);

  InterpStackPush(interp, & item);
}

// millis sleep -->
void PrimFun_sleep(VInterp* interp)
{
  VItem item = InterpStackPop(interp);
  if (!IsTypeIntNum(& item))
  {
    GURU_MEDITATION(SLEEP_NOT_INTNUM);
  }

  int millis = item.intNum;
  int seconds = millis / 1000;
  int micros = (millis % 1000) * 1000;

  sleep(seconds);
  usleep(micros);
}

// n random --> random integer between 0 and n (exclusive)
void PrimFun_random(VInterp* interp)
{
  VItem* item = InterpStackTop(interp);
  if (!IsTypeIntNum(item))
  {
    GURU_MEDITATION(RANDOM_NOT_INTNUM);
  }

  int r = rand();
  int n = item->intNum;
  item->intNum = r % n;
}

void PrimFun_def(VInterp* interp)
{
  PrimFun_funify(interp);
  PrimFun_swap(interp);
  PrimFun_setglobal(interp);
}

void PrimFun_evalfile(VInterp* interp)
{
  PrimFun_readfile(interp);
  PrimFun_parse(interp);
  PrimFun_eval(interp);
}

void AddCorePrimFuns()
{
  PrimFunTableAdd("sayhi", PrimFun_sayhi);
  PrimFunTableAdd("saymantra", PrimFun_saymantra);
  PrimFunTableAdd("print", PrimFun_print);
  PrimFunTableAdd("printstack", PrimFun_printstack);
  PrimFunTableAdd("eval", PrimFun_eval);
  PrimFunTableAdd("iftrue", PrimFun_iftrue);
  PrimFunTableAdd("iffalse", PrimFun_iffalse);
  PrimFunTableAdd("ifelse", PrimFun_ifelse);
  PrimFunTableAdd("setglobal", PrimFun_setglobal);
  PrimFunTableAdd("getglobal", PrimFun_getglobal);
  PrimFunTableAdd("funify", PrimFun_funify);
  PrimFunTableAdd("parse", PrimFun_parse);
  PrimFunTableAdd("readfile", PrimFun_readfile);
  PrimFunTableAdd("setworkingdir", PrimFun_setworkingdir);
  PrimFunTableAdd("+", PrimFun_plus);
  PrimFunTableAdd("-", PrimFun_minus);
  PrimFunTableAdd("*", PrimFun_times);
  PrimFunTableAdd("/", PrimFun_div);
  PrimFunTableAdd("1+", PrimFun_1plus);
  PrimFunTableAdd("1-", PrimFun_1minus);
  PrimFunTableAdd("2+", PrimFun_2plus);
  PrimFunTableAdd("2-", PrimFun_2minus);
  PrimFunTableAdd("<", PrimFun_lessthan);
  PrimFunTableAdd(">", PrimFun_greaterthan);
  PrimFunTableAdd("eq", PrimFun_eq);
  PrimFunTableAdd("iszero", PrimFun_iszero);
  PrimFunTableAdd("not", PrimFun_not);
  PrimFunTableAdd("drop", PrimFun_drop);
  PrimFunTableAdd("dup", PrimFun_dup);
  PrimFunTableAdd("swap", PrimFun_swap);
  PrimFunTableAdd("rot", PrimFun_rot);
  PrimFunTableAdd("over", PrimFun_over);
  PrimFunTableAdd("first", PrimFun_first);
  PrimFunTableAdd("rest", PrimFun_rest);
  PrimFunTableAdd("cons", PrimFun_cons);
  PrimFunTableAdd("setfirst", PrimFun_setfirst);
  PrimFunTableAdd("pop", PrimFun_pop);
  PrimFunTableAdd("gc", PrimFun_gc);
  PrimFunTableAdd("gcverbose", PrimFun_gcverbose);
  PrimFunTableAdd("millis", PrimFun_millis);
  PrimFunTableAdd("sleep", PrimFun_sleep);
  PrimFunTableAdd("random", PrimFun_random);
  PrimFunTableAdd("def", PrimFun_def);
  PrimFunTableAdd("evalfile", PrimFun_evalfile);
}
