Unfinished notes
----------------

callstackpush(list);

instr = first(list);

while (callstack > 0)
{
  while (instr)
  {
    nextInstr = next(instr);

    if (primfun(instr))
      callprimfun(instr);
    else
    if (symbol(instr))
    {
      value = lookup(instr);
      if (fun(value))
        callstackpush(value)
      else
        push(value);
    }
    else
      push(instr);

    instr = nextInstr;
  }

  callstackpop();
}


// Primfuns in static table
typedef struct __VInterp
{
  char   symbolMemory[SymbolMemorySize];  // Symbol strings
  char*  symbolTable[NumGlobals];         // Symbol table
  VItem  globalVars[NumGlobals];          // Global variables

  VItem  dataStack[DataStackSize];        // Data stack items
  VItem  callStack[CallStackSize];        // Call stack frames
  int    dataStackIndex;
  int    callStackIndex;

  VItem  listMemory[ListMemorySize];      // Lisp-style list memory
  int    freeIndex;
  VItem* freeList;
}
VInterp;


dup iszero :
  [ 2 + ]
  [ 1 + ] ?

dup iszero ?
  2 + :
  1 + ;
