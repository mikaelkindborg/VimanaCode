/*
File: listmem.h
Author: Mikael Kindborg (mikael@kindborg.com)

Memory manager for Lisp-style linked lists.

See item.h for an explanation of memory layout.
*/

// -------------------------------------------------------------
// Global flags
// -------------------------------------------------------------

int ListMemVerboseGC = 0;

// -------------------------------------------------------------
// VListMemory struct
// -------------------------------------------------------------

// Addresses in list memory are pointer offsets (VAddr)

typedef struct __VListMemory
{
  VByte* start;         // Start of memory block
  VAddr  addrNextFree;  // Next free item in memory block
  VAddr  addrFirstFree; // First item in freelist
  VAddr  addrEnd;       // End of memory
  #ifdef TRACK_MEMORY_USAGE
  int    allocCounter;
  #endif
}
VListMemory;

// -------------------------------------------------------------
// Pointer size and address shift
// -------------------------------------------------------------

// Determine pointer size
#if UINTPTR_MAX == 0xFFFF
  #define VIMANA_16
#elif UINTPTR_MAX == 0xFFFFFFFF
  #define VIMANA_32
#elif UINTPTR_MAX == 0xFFFFFFFFFFFFFFFF
  #define VIMANA_64
#else
  #error Unknow pointer size
#endif

// The the number of bits to shift (multiply) an item index to
// get the pointer offset in item memory.
// See further details below.
#if defined(VIMANA_64)
  #define AddrShift 4  // 16 byte item size
#elif defined(VIMANA_32)
  #define AddrShift 3  // 8 byte item size
#elif defined(VIMANA_16)
  #define AddrShift 2  // 4 byte item size
#endif

// -------------------------------------------------------------
// Item access
// -------------------------------------------------------------

// The item address is an index that starts at 1.

// On 64 bit systems 49 bits are available for the index,
// which can address an incredible amount of memory.

// On 32 bit systems 27 bits are available for the index,
// which can address 1GB of memory, or 134M 8 byte items.

// On 16 bit systems 11 bits are available for the index,
// which can address 8 KB of memory, or 2K 4 byte items.

// The index is multiplied (left shifted) by the item size to
// get the index address offset, and is divided (right shifted)
// by the item size to get the index address from a pointer offset.

#define ListMemStart(mem)        ((mem)->start)
#define AddrToPtr(addr, start)   VItemPtr((start) + ((addr) << AddrShift))
#define PtrToAddr(ptr, start)    ((VAddr) ((VBytePtr(ptr) - (start)) >> AddrShift))
#define ListMemGet(mem, addr)    AddrToPtr(addr, ListMemStart(mem))
#define ListMemGetAddr(mem, ptr) PtrToAddr(ptr, ListMemStart(mem))

// The value of the first item field is a full pointer

// Return NULL if empty list
#define ListMemGetFirst(mem, item) ItemGetFirst(item)

// Accepts first NULL
void ListMemSetFirst(VListMemory* mem, VItem* item, VItem* first)
{
  ItemSetFirst(item, first);
}

// The value of the next item field is an index

// Return NULL if end of list
#define ListMemGetNext(mem, item) \
  (ItemGetNextAddr(item) ? ListMemGet(mem, ItemGetNextAddr(item)) : NULL)

// Accepts next NULL
void ListMemSetNext(VListMemory* mem, VItem* item, VItem* next)
{
  (next) ?
    ItemSetNextAddr(item, ListMemGetAddr(mem, next)) :
    ItemSetNextAddr(item, 0);
}

// -------------------------------------------------------------
// Initialize
// -------------------------------------------------------------

// Return size in bytes of VListMemory header plus item memory size
int ListMemByteSize(int numItems)
{
  return sizeof(VListMemory) + (numItems * ItemSize());
}

void ListMemInit(VListMemory* mem, int numItems)
{
  // Addresses are indexes
  mem->start = (VBytePtr(mem) + sizeof(VListMemory)) - ItemSize();
  mem->addrNextFree = 1;                  // Index of next free item
  mem->addrFirstFree = 0;                 // Freelist is empty
  mem->addrEnd = numItems + 1;            // End of address space

  #ifdef TRACK_MEMORY_USAGE
    mem->allocCounter = 0;
  #endif
}

#ifdef TRACK_MEMORY_USAGE
  void ListMemPrintAllocCounter(VListMemory* mem)
  {
    Print("MemAllocCounter: ");
    PrintIntNum(mem->allocCounter);
    PrintNewLine();
  }
#endif

// -------------------------------------------------------------
// Alloc and dealloc
// -------------------------------------------------------------

// Allocate an item
VItem* ListMemAlloc(VListMemory* mem)
{
  VItem* item;
  VAddr  addr;
/*
  // TODO: Garbage collect if all memory is allocated
  if ((0 == mem->addrFirstFree) && (! (mem->addrNextFree < mem->addrEnd)))
  {
    // TODO: Garbage collect
  }
*/
  if (0 < mem->addrFirstFree)
  {
    // ALLOCATE FROM FREELIST

    //PrintLine("Alloc from freelist");

    item = ListMemGet(mem, mem->addrFirstFree);
    mem->addrFirstFree = ItemGetNextAddr(item);
  }
  else
  {
    // ALLOCATE FROM UNUSED MEMORY

    if (mem->addrNextFree < mem->addrEnd)
    {
      //PrintLine("Alloc from memory");

      item = ListMemGet(mem, mem->addrNextFree);
      ++ mem->addrNextFree;
    }
    else
    {
      #ifdef DEBUG
        PrintLine("[GURU_MEDITATION] ALLOC_ITEM_OUT_OF_MEMORY");
        return NULL;
      #else
        GURU_MEDITATION(ALLOC_ITEM_OUT_OF_MEMORY);
      #endif
    }
  }

  #ifdef TRACK_MEMORY_USAGE
    ++ mem->allocCounter;
  #endif

  // Init and set default type
  ItemInit(item);
  ItemSetType(item, TypeIntNum);

  return item;
}

void ListMemDeallocItem(VListMemory* mem, VItem* item)
{
  if (IsTypeNone(item))
  {
    return;
  }

  if (ListMemVerboseGC)
  {
    printf("dealloc item of type: 0x%lX\n", (unsigned long) ItemGetType(item));
  }

  #ifdef TRACK_MEMORY_USAGE
    -- mem->allocCounter;
  #endif

  if (IsTypeBuffer(item))
  {
    // Free allocated buffer used by items of TypeString and TypeHandle
    if (ItemGetPtr(item))
    {
      SysFree(ItemGetPtr(item));
    }
  }

  // Set type None and add deallocated item to freelist
  ItemSetType(item, TypeNone);
  ItemSetNextAddr(item, mem->addrFirstFree);
  mem->addrFirstFree = ListMemGetAddr(mem, item);

  ListMemVerboseGC = 0;

  //printf("dealloc mem->addrFirstFree: %lu\n", (unsigned long) mem->addrFirstFree);
}

// -------------------------------------------------------------
// Buffer items
// -------------------------------------------------------------

// Allocate two new items: one that can be copied and one that
// points to the memory buffer. The memory buffer must be allocated
// with malloc() or a similar function. The caller must set
// the type of the returned item.
//
// There must be only one instance of the item that points to
// allocated memory, and this item must NOT be shared; that is,
// it must NOT be present on the data stack or used for variables.
// If the raw pointer would be refereced from many items, the
// garbage collector would not be able to tell if it is deallocated.
//
// Ownership of bufferPtr goes to the memory manager
// bufferPtr must be allocated with SysAlloc (malloc)
//
VItem* ListMemAllocHandle(VListMemory* mem, void* bufferPtr, VType type)
{
  //printf("DEBUG: Alloc handle\n");

  VItem* item = ListMemAlloc(mem);
  ItemSetType(item, type);

  VItem* buffer = ListMemAlloc(mem);
  ItemSetType(buffer, TypeBuffer);

  buffer->ptr = bufferPtr;

  ListMemSetFirst(mem, item, buffer);

  return item;
}

// Returns the pointer of the buffer the item refers to.
// TODO: GURU_MEDITATION instead of returning NULL?
void* ListMemGetHandlePtr(VListMemory* mem, VItem* item)
{
  if (! (IsTypeString(item) || IsTypeHandle(item)) ) return NULL;

  VItem* buffer = ListMemGetFirst(mem, item);

  if (NULL == buffer) return NULL;
  if (!IsTypeBuffer(buffer)) return NULL;

  return buffer->ptr;
}

// -------------------------------------------------------------
// Garbage collection
// -------------------------------------------------------------

void ListMemMark(VListMemory* mem, VItem* item)
{
  while (item)
  {
    if (ItemGetGCMark(item))
    {
      //PrintLine("ALREADY MARKED");
      return;
    }

    //Print("MARK ITEM: "); ListMemPrintItem(mem, item); PrintNewLine();
    ItemGCMarkSet(item);

    // Mark children
    if (!IsTypeAtomic(item))
    {
      VItem* child = ListMemGetFirst(mem, item);
      ListMemMark(mem, child);
    }

    item = ListMemGetNext(mem, item);
  }
}

void ListMemSweep(VListMemory* mem)
{
  VByte* p = mem->start + ItemSize();
  VByte* end = VBytePtr(ListMemGet(mem, mem->addrNextFree));

  while (p < end)
  {
    VItem* item = VItemPtr(p);

    if (ItemGetGCMark(item))
    {
      //PrintLine("MemSweep unmark");
      ItemGCMarkUnset(item);
    }
    else
    {
      //PrintLine("MemSweep dealloc");
      ListMemDeallocItem(mem, item);
    }

    p += ItemSize();
  }
}
