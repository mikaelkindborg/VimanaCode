/*
File: interp.h
Author: Mikael Kindborg (mikael@kindborg.com)

Interpreter data structures and functions.
*/

// -------------------------------------------------------------
// Interp struct
// -------------------------------------------------------------

typedef struct __VInterp
{
  int             run;                 // Run flag

  VItem*          globalVars;          // Global variables (symbol table)
  VItem*          dataStack;           // Data stack items
  VItem*          callStack;           // Callstack frames

  VListMemory*    listMemory;          // Lisp-style list memory
  VSymbolMemory*  symbolMemory;        // Symbol strings
}
VInterp;

#define InterpListMem(interp)   ((interp)->listMemory)
#define InterpSymbolMem(interp) ((interp)->symbolMemory)
#define InterpStackTop(interp)  ((interp)->dataStack)

// -------------------------------------------------------------
// Interp
// -------------------------------------------------------------

// Estimate 10 chars per symbol on average

// Return the number of bytes needed for the given configuration
int InterpByteSize(int maxNumItems, int maxNumSymbols, int symbolAverageSize)
{
  int byteSizeInterpStruct = sizeof(VInterp);
  int byteSizeListMemory = ListMemByteSize(maxNumItems);
  int byteSizeSymbolMemory = SymbolMemByteSize(maxNumSymbols, symbolAverageSize);

  int byteSizeInterp =
    byteSizeInterpStruct +
    byteSizeListMemory +
    byteSizeSymbolMemory;

  return byteSizeInterp;
}

// Take care to assure aligned memory sizes
void InterpInit(VInterp* interp, int maxNumItems, int maxNumSymbols, int symbolAverageSize)
{
  // Initialize interpreter globals and stacks

  interp->globalVars = NULL;
  interp->dataStack = NULL;
  interp->callStack = NULL;

  // Set pointers within the memory block pointed to by interp

  int byteSizeInterpStruct = sizeof(VInterp);
  int byteSizeListMemory = ListMemByteSize(maxNumItems);

  InterpListMem(interp) = PtrOffset(interp, byteSizeInterpStruct);
  ListMemInit(InterpListMem(interp), maxNumItems);

  InterpSymbolMem(interp) = PtrOffset(InterpListMem(interp), byteSizeListMemory);
  SymbolMemInit(InterpSymbolMem(interp), maxNumSymbols, symbolAverageSize);
}

void InterpFree(VInterp* interp)
{
  ListMemSweep(InterpListMem(interp));

  #ifdef TRACK_MEMORY_USAGE
    ListMemPrintAllocCounter(InterpListMem(interp));
  #endif
}

// -------------------------------------------------------------
// Item access
// -------------------------------------------------------------

// These are high-level macros to access item pointers

#define InterpGetFirst(interp, item) ListMemGetFirst(InterpListMem(interp), item)
#define InterpGetNext(interp, item) ListMemGetNext(InterpListMem(interp), item)

#define InterpSetFirst(interp, item, first) ListMemSetFirst(InterpListMem(interp), item, first)
#define InterpSetNext(interp, item, next) ListMemSetNext(InterpListMem(interp), item, next)

#define InterpAllocItem(interp) ListMemAlloc(InterpListMem(interp))
#define InterpAllocHandle(interp, data, type) ListMemAllocHandle(InterpListMem(interp), data, type)
#define InterpDeallocItem(interp, item) ListMemDeallocItem(InterpListMem(interp), item)

#define InterpGetHandlePtr(interp, item) ListMemGetHandlePtr(InterpListMem(interp), item)

//void CopyItemRestoreNext(VItem* copyTo, VItem* copyFrom)

// -------------------------------------------------------------
// Garbage collection
// -------------------------------------------------------------

// GC is not yet fully tested and integrated
void InterpGC(VInterp* interp)
{
  // Mark data stack
  if (InterpStackTop(interp))
    ListMemMark(InterpListMem(interp), InterpStackTop(interp));

  // Mark call stack
  if (interp->callStack)
    ListMemMark(InterpListMem(interp), interp->callStack);

  // Mark global vars
  if (interp->globalVars)
    ListMemMark(InterpListMem(interp), interp->globalVars);

  // Sweep list memory and free unused items
  ListMemSweep(InterpListMem(interp));

  #ifdef TRACK_MEMORY_USAGE
    ListMemPrintAllocCounter(InterpListMem(interp));
  #endif
}

// -------------------------------------------------------------
// Symbols - global vars
// -------------------------------------------------------------

VItem* InterpLookupGlobalVarByName(VInterp* interp, char* name)
{
  VItem* item = interp->globalVars;

  // Find symbol
  while (NULL != item)
  {
    VItem* symbol = InterpGetNext(interp, item);
    if (StrEquals(ItemGetSymbolString(symbol), name))
    {
      return item; // Found it
    }

    item = InterpGetNext(interp, symbol);
  }

  // Symbol does not exist
  return NULL;
}

char* InterpLookupGlobalVarName(VInterp* interp, VItem* globalVarRef)
{
  VItem* item = interp->globalVars;
  VItem* globalVar = ItemGetGlobalVar(globalVarRef);

  // Find symbol
  while (NULL != item)
  {
    if (globalVar == item)
    {
      // Symbol found
      VItem* symbol = InterpGetNext(interp, item);
      return ItemGetSymbolString(symbol);
    }

    item = InterpGetNext(interp, item);
  }

  // Symbol not found
  return NULL;
}

VItem* InterpAddGlobalVar(VInterp* interp, char* name)
{
  VItem* first = interp->globalVars;

  VItem* symbol = InterpAllocItem(interp);
  ItemSetSymbolString(symbol, name);
  InterpSetNext(interp, symbol, first);

  VItem* globalVar = InterpAllocItem(interp);
  ItemSetFirst(globalVar, NULL);
  InterpSetNext(interp, globalVar, symbol);

  interp->globalVars = globalVar;

  return globalVar;
}

#define InterpGetGlobalVar(interp, globalVarRef) ItemGetGlobalVar(globalVarRef)

void InterpSetGlobalVar(VInterp* interp, VItem* globalVarRef, VItem* value)
{
  // Copy value to global variable
  VItem* globalVar = ItemGetGlobalVar(globalVarRef);
  ItemCopyPreserveNext(globalVar, value);
}

// -------------------------------------------------------------
// Data stack
// -------------------------------------------------------------

// Copy the given item onto the data stack
void InterpStackPush(VInterp* interp, VItem* item)
{
  VItem* top = InterpStackTop(interp);
  VItem* newTop = InterpAllocItem(interp);

  if (NULL == newTop)
  {
    GURU_MEDITATION(DATA_STACK_OVERFLOW);
  }

  *newTop = *item;

  InterpSetNext(interp, newTop, top); // Accepts NULL as second arg

  InterpStackTop(interp) = newTop;
}

// Remove the topmost item from the data stack and return a copy
// --> Returns a copy, does not return a pointer!
VItem InterpStackPop(VInterp* interp)
{
  if (NULL == InterpStackTop(interp))
  {
    GURU_MEDITATION(DATA_STACK_IS_EMPTY);
  }

  VItem* top = InterpStackTop(interp);
  VItem* next = InterpGetNext(interp, top); // Returns NULL if end of list

  VItem item = *top; // Make copy

  InterpDeallocItem(interp, top);

  InterpStackTop(interp) = next;

  return item;
}

// -------------------------------------------------------------
// Call stack
// -------------------------------------------------------------

// Push a new stackframe if needed, reuse top (last) stackframe on tailcall
void InterpPushStackFrame(VInterp* interp, VItem* list)
{
  // Handle tailcall
  VItem* top = interp->callStack;
  int tailCall = (NULL != top) && (NULL == InterpGetFirst(interp, top));

  if (tailCall)
  {
    // Reuse stackframe
    VItem* first = InterpGetFirst(interp, list); // First instruction in list
    ItemSetFirst(top, first);
  }
  else // Not a tailcall, top may be NULL
  {
    VItem* newTop = InterpAllocItem(interp);

    if (NULL == newTop)
    {
      GURU_MEDITATION(CALL_STACK_OVERFLOW);
    }

    VItem* first = InterpGetFirst(interp, list); // First instruction in list
    ItemSetFirst(newTop, first);
    InterpSetNext(interp, newTop, top); // Accepts NULL as second arg

    interp->callStack = newTop;
  }
}

// Pop stackframe
void InterpPopStackFrame(VInterp* interp)
{
  if (NULL == interp->callStack)
  {
    GURU_MEDITATION(CALL_STACK_IS_EMPTY);
  }

  VItem* top = interp->callStack;
  VItem* next = InterpGetNext(interp, top); // Returns NULL if end of list

  InterpDeallocItem(interp, top);

  interp->callStack = next;
}

// -------------------------------------------------------------
// Eval
// -------------------------------------------------------------

int InterpEvalSlice(VInterp* interp, int sliceSize);

void InterpEval(VInterp* interp, VItem* list)
{
  InterpPushStackFrame(interp, list);
  InterpEvalSlice(interp, 0);
  InterpGC(interp);
}

// Evaluate a slice of the code.
//
// The idea is to avoid hogging the CPU if needed, for example
// in GUIs, and also make it possible to run multiple interpreters
// concurrently in a single thread.
//
// sliceSize specifies the number of instructions to execute.
// sliceSize 0 means eval as one slice until program ends.
// Returns done flag (TRUE = done, FALSE = not done).
int InterpEvalSlice(VInterp* interp, int sliceSize)
{
  int sliceCounter = 0;

  interp->run = TRUE;

  while (interp->run)
  {
    // Count slices if a sliceSize is specified.
    if (sliceSize)
    {
      if (sliceSize > sliceCounter)
        ++ sliceCounter;
      else
        goto Exit; // Exit loop
    }

    // Get current instruction
    VItem* instruction = ItemGetFirst(interp->callStack);

    // Evaluate current instruction
    if (instruction)
    {
      // Advance instruction for the NEXT loop
      ItemSetFirst(interp->callStack, InterpGetNext(interp, instruction));

      // Dispatch on instruction
      if (IsTypePrimFun(instruction))
      {
        // Call primfun
        VPrimFunPtr fun = ItemGetPrimFun(instruction);
        fun(interp);
      }
      else
      if (IsTypePushable(instruction))
      {
        // Push "pushable" items (lists, numbers, strings)
        InterpStackPush(interp, instruction);
      }
      else
      if (IsTypeGlobalVarRef(instruction))
      {
        VItem* value = InterpGetGlobalVar(interp, instruction);
        if (IsTypeFun(value))
        {
          // Call function
          InterpPushStackFrame(interp, value);
        }
        else
        if (!IsTypeNone(value))
        {
          // Push global variable value
          InterpStackPush(interp, value);
        }
        // else nothing is done (unbound globals are not pushed onto the stack)
      }
    }
    else // (NULL == instruction - end of instruction list)
    {
      // Pop stackframe
      InterpPopStackFrame(interp);

      // Exit interpreter loop if this was the last stackframe
      if (NULL == interp->callStack)
      {
        interp->run = FALSE;
        goto Exit; // Exit loop
      }
    }
  } // while

Exit:

  return ! interp->run;
}
