// SDL2 Hi World

// https://wiki.libsdl.org/SDL2/README/macos
// https://gist.github.com/cicanci/b54715298ab55dff2fbcd0ca3829d13b
// https://discourse.libsdl.org/t/building-sdl2-on-os-x-the-unix-way/27136

// cc sdlhi.c -o sdlhi `sdl2-config --cflags --libs`

#include <SDL2/SDL.h>
#include <stdio.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

int main(int argc, char* args[]) 
{
  SDL_Window* window = NULL;
  SDL_Surface* screenSurface = NULL;
  
  if (SDL_Init(SDL_INIT_VIDEO) < 0) 
  {
    fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
    return 1;
  }
  
  window = SDL_CreateWindow
  (
    "Hi SDL",
    SDL_WINDOWPOS_UNDEFINED, 
    SDL_WINDOWPOS_UNDEFINED,
    SCREEN_WIDTH, 
    SCREEN_HEIGHT,
    SDL_WINDOW_SHOWN
  );
  
  if (window == NULL) 
  {
    fprintf(stderr, "could not create window: %s\n", SDL_GetError());
    return 1;
  }
  
  printf("Display window\n");
  screenSurface = SDL_GetWindowSurface(window);
  SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0xFF, 0xAA, 0xFF));
  SDL_UpdateWindowSurface(window);
  
  // Keep the main loop until the window is closed (SDL_QUIT event)
  int exit = 0;
  SDL_Event eventData;
  while (!exit)
  {
    while (SDL_PollEvent(&eventData))
    {
      switch (eventData.type)
      {
        case SDL_QUIT:
          exit = 1;
          break;
      }
    }
  }
    
  //SDL_Delay(2000);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
