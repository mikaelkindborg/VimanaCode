/*
File: vimanasteam.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Vimana Steam Interpreter
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// --------------------------------------------------------------
// Interpreter data types
// --------------------------------------------------------------

#include "optable.h"

typedef long TypeData;
typedef int  TypeIndex;
typedef int  TypeBool;

#define TRUE  1
#define FALSE 0

// Program memory contains linked items, much like Lisp lists.
// Items are addressed by an index into the program memory.
// The data field contains an index to the next item and an op code.
typedef struct __TypeItem
{
  TypeData value; // Holds item value (e.g. an integer)
  TypeData next;  // Holds index to next item and an op code (and GC mark bit)
}
TypeItem;

// --------------------------------------------------------------
// Program memory areas
// --------------------------------------------------------------

#define PROGMEM_MAXSIZE     100
#define DATSTACK_MAXSIZE    50
#define CALLTACK_MAXSIZE    50
#define SYMBOLTABLE_MAXSIZE 20
#define STRINGMEM_MAXSIZE   500
#define MAX_TOKEN_LENGTH    32   // Max length for symbol names is
                                 // 31 chars + 1 zero-terminator char
#define ITEM_TAG_BITS       8
#define ITEM_TAG_MASK       0xFF

TypeIndex ProgMemFirstFree = 1; // First free index must not be zero
TypeItem  ProgMem[PROGMEM_MAXSIZE]; // Data items

// Data stack and call stack
TypeItem  DataStack[DATSTACK_MAXSIZE]; // Data stack items
TypeIndex CallStack[CALLTACK_MAXSIZE]; // Return address/index stack

// Global symbols
TypeIndex SymbolTableFirstFree = 0; // First free index
TypeItem  SymbolTable[SYMBOLTABLE_MAXSIZE];
char*     SymbolNameTable[SYMBOLTABLE_MAXSIZE];

// String memory
TypeIndex StringMemFirstFree = 0; // First free index
TypeIndex StringMemStringStart = 0; // Current string start index
char      StringMem[STRINGMEM_MAXSIZE];

// Item access
#define ItemNext(item)    (((item).next) >> ITEM_TAG_BITS)
#define ItemOp(item)      (((item).next) & ITEM_TAG_MASK)
#define ItemValue(item)   ((item).value)

// Compose value of next field
#define ItemNextWith(index, op) \
  (((index) << ITEM_TAG_BITS) | (op))

// Get index (not full item) of first item in list
#define ListFirst(head)   (ItemValue(head))

// Get value (full item) of global variable
#define GlobalGet(item) (SymbolTable[ItemValue(item)])

// --------------------------------------------------------------
// Debug functions
// --------------------------------------------------------------

#include "debug.h"

// --------------------------------------------------------------
// Library functions
// --------------------------------------------------------------

#include "lib.h"

// --------------------------------------------------------------
// Parser
// --------------------------------------------------------------

#include "parser.h"

// --------------------------------------------------------------
// Stringify
// --------------------------------------------------------------

#include "stringify.h"

// --------------------------------------------------------------
// Interpreter loop
// --------------------------------------------------------------

void ProgEval(TypeItem head)
{
  // Persistent variables begin with an uppercase letter
  TypeIndex InstrIndex = ListFirst(head); // first of list
  TypeIndex DataStackTop = -1; // data stack is empty
  TypeIndex CallStackTop = 1;  // call stack starts at index 1

  // Temporary variables for use by opcodes
  TypeItem item;  // current instruction
  TypeData op;    // current operation
  TypeData A;     // data value
  TypeData B;     // data value
  TypeItem ItemA; // item value
  TypeItem ItemB; // item value

  // When both callstack index is zero and instruction
  // index is zero the interpreter exits (this is why
  // callstack index starts at 1)
  CallStack[CallStackTop] = 0;

  while (CallStackTop)
  {
    while (InstrIndex)
    {
      item = ProgMem[InstrIndex];
      op = ItemOp(item);
      //DebugPrintItem("ProgEval", item);

      switch (op)
      {
        #include "opcodes.h"
      }
    }

    //DebugPrint("ProgEval exit while");

    // Return to instruction on call stack
    InstrIndex = CallStack[CallStackTop];
    -- CallStackTop;
  }

  DebugPrint("End of Program");
  DebugPrintInt("Data Stack Size", DataStackTop);
}
