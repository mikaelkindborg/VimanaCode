// OP Codes

// --------------------------------------------------------
// Undefined
// --------------------------------------------------------

case OP_UNDEFINED:
  GURU_MEDITATION("Global name is undefined");
  break;

// --------------------------------------------------------
// Set global variables
// --------------------------------------------------------

// def globalname item ->
case OP_DEF: // def
case OP_DEFCHAR: // :
  // Get symbol from next instruction
  InstrIndex = ItemNext(item);
  item = ProgMem[InstrIndex];

  // Set to value at next instruction
  InstrIndex = ItemNext(item);
  GlobalGet(item) = ProgMem[InstrIndex];

  // Go to next instruction
  InstrIndex = ItemNext(ProgMem[InstrIndex]);
  break;

// item set globalname ->
case OP_SET: // set
case OP_SETARROW: // ->
  // Get value from data stack
  ItemA = DataStack[DataStackTop];
  -- DataStackTop;

  // Set value of symbol in next instruction
  InstrIndex = ItemNext(item);
  GlobalGet(ProgMem[InstrIndex]) = ItemA;
  //SymbolTable[ItemValue(ProgMem[InstrIndex])] = ItemA;

  // Go to next instruction
  InstrIndex = ItemNext(ProgMem[InstrIndex]);
  break;

// --------------------------------------------------------
// Get global variable
// --------------------------------------------------------

case OP_GLOBAL:
  // Push global variable value to data stack
  ++ DataStackTop;
  DataStack[DataStackTop] = GlobalGet(item);
  InstrIndex = ItemNext(item);
  break;

// --------------------------------------------------------
// Call functions
// --------------------------------------------------------

case OP_FUNCTION:
  // Save instruction index on return stack
  ++ CallStackTop;
  // ??? TODO Check tailcall if next is zero - dont put zero on the callstack
  CallStack[CallStackTop] = ItemNext(item);
  // Set address of function (first element in list)
  InstrIndex = ListFirst(GlobalGet(item));
  break;

// tail fun ->
case OP_TAIL: // tail
  InstrIndex = ItemNext(item);
  InstrIndex = ListFirst(GlobalGet(ProgMem[InstrIndex]));
  break;

case OP_EVAL: // eval
  // TODO Check if next is zero, then it is the end of the list = tailcall
  // Save instruction index on return stack
  ++ CallStackTop;
  CallStack[CallStackTop] = ItemNext(item);
  // Set instruction index to the first item in the list
  A = ListFirst(DataStack[DataStackTop]);
  -- DataStackTop;
  InstrIndex = A;
  break;

// head taileval ->
case OP_TAILEVAL: // taileval
  // Set instruction index to the first item in the list
  A = ListFirst(DataStack[DataStackTop]);
  -- DataStackTop;
  InstrIndex = A;
  break;

// --------------------------------------------------------
// Exit interpreter loop
// --------------------------------------------------------

case OP_EXIT:
  InstrIndex = 0;
  CallStackTop = 0;
  break;

// --------------------------------------------------------
// Conditional functions
// --------------------------------------------------------

// bool ifTrueTail list ->
case OP_IFTRUETAIL: // ifTrueTail
  // Move to next instruction
  InstrIndex = ItemNext(item);
  // Test top of stack
  if (ItemValue(DataStack[DataStackTop]))
  {
    // Perform the branch
    InstrIndex = ListFirst(ProgMem[InstrIndex]);
    -- DataStackTop;
  }
  else
  {
    // Skip to next
    InstrIndex = ItemNext(ProgMem[InstrIndex]);
    -- DataStackTop;
  }
  break;

// bool ifFalseTail list ->
case OP_IFFALSETAIL: // ifFalseTail
case OP_IFZEROTAIL: // ifZeroTail
  // Move to next instruction
  InstrIndex = ItemNext(item);
  // Test top of stack
  if (! ItemValue(DataStack[DataStackTop]))
  {
    // Perform the branch
    InstrIndex = ListFirst(ProgMem[InstrIndex]);
    -- DataStackTop;
  }
  else
  {
    // Skip to next
    InstrIndex = ItemNext(ProgMem[InstrIndex]);
    -- DataStackTop;
  }
  break;

case OP_IFLESSTAIL: // ifLessTail
  // Move to next instruction
  InstrIndex = ItemNext(item);
  // Test values at top of stack
  if (ItemValue(DataStack[DataStackTop - 1]) >
      ItemValue(DataStack[DataStackTop]))
  {
    // Perform the branch
    InstrIndex = ListFirst(ProgMem[InstrIndex]);
    -- DataStackTop;
    -- DataStackTop;
  }
  else
  {
    // Skip to next
    InstrIndex = ItemNext(ProgMem[InstrIndex]);
    -- DataStackTop;
    -- DataStackTop;
  }
  break;

case OP_IFBIGGERTAIL: // ifBiggerTail
  // Move to next instruction
  InstrIndex = ItemNext(item);
  // Test values at top of stack
  if (ItemValue(DataStack[DataStackTop - 1]) <
      ItemValue(DataStack[DataStackTop]))
  {
    // Perform the branch
    InstrIndex = ListFirst(ProgMem[InstrIndex]);
    -- DataStackTop;
    -- DataStackTop;
  }
  else
  {
    // Skip to next
    InstrIndex = ItemNext(ProgMem[InstrIndex]);
    -- DataStackTop;
    -- DataStackTop;
  }
  break;

// --------------------------------------------------------
// Push values
// --------------------------------------------------------

case OP_INTEGER:
  // Push item to data stack
  ++ DataStackTop;
  DataStack[DataStackTop] = item;
  InstrIndex = ItemNext(item);
  break;

case OP_LIST:
  // Push list head to data stack
  ++ DataStackTop;
  DataStack[DataStackTop] = item;
  InstrIndex = ItemNext(item);
  break;

// -------------------------------------------------------
// Math functions
// -------------------------------------------------------

case OP_ADD: // +
  A = ItemValue(DataStack[DataStackTop]);
  -- DataStackTop;
  B = ItemValue(DataStack[DataStackTop]);
  ItemValue(DataStack[DataStackTop]) = B + A;
  InstrIndex = ItemNext(item);
  break;

case OP_SUB: // -
  A = ItemValue(DataStack[DataStackTop]);
  -- DataStackTop;
  B = ItemValue(DataStack[DataStackTop]);
  ItemValue(DataStack[DataStackTop]) = B - A;
  InstrIndex = ItemNext(item);
  break;

case OP_MULT: // *
  A = ItemValue(DataStack[DataStackTop]);
  -- DataStackTop;
  B = ItemValue(DataStack[DataStackTop]);
  ItemValue(DataStack[DataStackTop]) = B * A;
  InstrIndex = ItemNext(item);
  break;

case OP_DIV: // /
  A = ItemValue(DataStack[DataStackTop]);
  -- DataStackTop;
  B = ItemValue(DataStack[DataStackTop]);
  ItemValue(DataStack[DataStackTop]) = B / A;
  InstrIndex = ItemNext(item);
  break;

case OP_ADD1: // 1+
  A = ItemValue(DataStack[DataStackTop]);
  ItemValue(DataStack[DataStackTop]) = A + 1;
  InstrIndex = ItemNext(item);
  break;

case OP_SUB1: // 1-
  A = ItemValue(DataStack[DataStackTop]);
  ItemValue(DataStack[DataStackTop]) = A - 1;
  InstrIndex = ItemNext(item);
  break;

case OP_SUB2: // 2-
  A = ItemValue(DataStack[DataStackTop]);
  ItemValue(DataStack[DataStackTop]) = A - 2;
  InstrIndex = ItemNext(item);
  break;

// Increment global variable by 1
// incr GlobalVariable ->
case OP_INCR: // incr
  // Get symbol
  InstrIndex = ItemNext(item);
  item = ProgMem[InstrIndex];
  // Set incremented value
  ++ GlobalGet(item).value;
  InstrIndex = ItemNext(item);
  break;

case OP_ISLESS: // isLess
  A = ItemValue(DataStack[DataStackTop]);
  -- DataStackTop;
  B = ItemValue(DataStack[DataStackTop]);
  ItemValue(DataStack[DataStackTop]) = B > A;
  InstrIndex = ItemNext(item);
  break;

case OP_ISBIGGER: // isBigger
  A = ItemValue(DataStack[DataStackTop]);
  -- DataStackTop;
  B = ItemValue(DataStack[DataStackTop]);
  ItemValue(DataStack[DataStackTop]) = B < A;
  InstrIndex = ItemNext(item);
  break;

// --------------------------------------------------------
// Stack operations
// --------------------------------------------------------

// dup
case OP_A: // A
  ++ DataStackTop;
  DataStack[DataStackTop] = DataStack[DataStackTop - 1];
  InstrIndex = ItemNext(item);
  break;

// over
case OP_B: // B
  ++ DataStackTop;
  DataStack[DataStackTop] = DataStack[DataStackTop - 2];
  InstrIndex = ItemNext(item);
  break;

case OP_C: // C
  ++ DataStackTop;
  DataStack[DataStackTop] = DataStack[DataStackTop - 3];
  InstrIndex = ItemNext(item);
  break;

case OP_SWAP: // swap
  ItemA = DataStack[DataStackTop];
  DataStack[DataStackTop] = DataStack[DataStackTop - 1];
  DataStack[DataStackTop - 1] = ItemA;
  InstrIndex = ItemNext(item);
  break;

case OP_ROT: // rot
  ItemA = DataStack[DataStackTop];
  ItemB = DataStack[DataStackTop - 1];
  DataStack[DataStackTop] = DataStack[DataStackTop - 2];
  DataStack[DataStackTop - 2] = ItemB;
  DataStack[DataStackTop - 1] = ItemA;
  InstrIndex = ItemNext(item);
  break;

case OP_DROP: // _
  -- DataStackTop;
  InstrIndex = ItemNext(item);
  break;

// --------------------------------------------------------
// Printing
// --------------------------------------------------------

case OP_PRINT: // print
  //DebugPrintItem("OP_PRINT", DataStack[DataStackTop]);
  printf("%li\n", (long)ItemValue(DataStack[DataStackTop]));
  -- DataStackTop;
  InstrIndex = ItemNext(item);
  break;

case OP_PRINTSTACK: // printstack
  printf("[");
  for (TypeIndex index = 0; index <= DataStackTop; ++ index)
  {
    if (index > 0) { printf(" "); }
    printf("%li", (long)ItemValue(DataStack[index]));
  }
  printf("]\n");
  InstrIndex = ItemNext(item);
  break;

/*
// --------------------------------------------------------
// Other native functions
// --------------------------------------------------------

// Currently unused - all primfuns are opcodes for now.

// Must be last of the opcodes, primfuns follow after this id
case OP_PRIMFUN:
  // Primfuns have the id in the value field
  switch (ItemValue(item))
  {
    #include "primfuns.h"
  }
  break;
*/
