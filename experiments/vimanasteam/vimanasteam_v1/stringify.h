/*
File: stringify.h
Author: Mikael Kindborg (mikael@kindborg.com)
Year: 2024

Stringify lists and items
*/

// --------------------------------------------------------------
// Stringify lists and items
// --------------------------------------------------------------

void StringifyList(TypeItem list);

void StringifyChar(char c)
{
  StringMemWriteChar(c);
}

void StringifyString(char* string)
{
  char* p = string;
  while (0 != *p)
  {
    StringMemWriteChar(*p);
    ++ p;
  }
}

void StringifyInteger(TypeData n)
{
  char buf[32];
  sprintf(buf, "%ld", (long)n);
  StringifyString(buf);
}

void StringifyVimanaString(char* string)
{
  StringMemWriteChar('{');
  StringifyString(string);
  StringMemWriteChar('}');
}

void StringifyItem(TypeItem item)
{
  if (OP_LIST == ItemOp(item))
  {
    StringifyList(item);
  }
  else if (OP_INTEGER == ItemOp(item))
  {
    StringifyInteger(ItemValue(item));
  }
  else if (OP_GLOBAL == ItemOp(item) || OP_FUNCTION == ItemOp(item))
  {
    char* string = SymbolTableLookupNameForIndex(ItemValue(item));
    StringifyString(string);
  }
  /*else if (OP_STRING == ItemOp(item))
  {
    char* string = ??
    StringifyVimanaString(string);
  }*/
  else // op code
  {
    char* string = OpTableLookupNameForCode(ItemOp(item));
    StringifyString(string);
  }
}

void StringifyList(TypeItem head)
{
  StringifyChar('(');

  TypeBool printSpace = FALSE;

  TypeIndex index = ListFirst(head);
  while (index)
  {
    if (printSpace) { StringifyChar(' '); }
    printSpace = TRUE;
    TypeItem item = ProgMem[index];
    StringifyItem(item);
    index = ItemNext(item);
  }

  StringifyChar(')');
}

//
// Stringify returns a pointer to a zero-terminated
// (temporary) string. The string memory area is used as a
// temporary buffer.
//
// The returned string must be kept or dropped before
// next call to Stringify. Keeping strings can
// use up memory quickly. To keep the string it is better
// to make a copy of it, and then drop it.
//
// To keep the string call:
//   StringMemKeep();
//
// To drop the string call:
//   StringMemReset();
//
// Example:
//   char* string = Stringify(list);
//   printf("%s\n", string);
//   StringMemReset();
//
char* Stringify(TypeItem item)
{
  StringifyItem(item);
  return StringMemWriteEnd();
}

// Print an item/list
void ProgPrint(TypeItem item)
{
  char* string = Stringify(item);
  printf("%s\n", string);
  StringMemReset();
  //DebugPrintInt("StringMemFirstFree", StringMemFirstFree);
  //DebugPrintInt("StringMemStringStart", StringMemStringStart);
}
