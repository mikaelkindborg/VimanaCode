//
// File: workbench.js
// Vimana workbench UI
// Copyright (c) 2021-2023 Mikael Kindborg
// mikael@kindborg.com
//

// Object that holds UI functions
window.VimanaUI = {}

// Create interpreter
VimanaUI.interp = new VimanaInterp()

// Evaluate a string
VimanaUI.eval = function(string)
{
  VimanaUI.interp.evalString(string)
}

// Print some object in the UI
VimanaUI.print = function(obj)
{
  const output = document.getElementsByTagName("textarea")[1]
  if (output.value.length > 0)
  {
    output.value = output.value + "\n" + obj.toString()
  }
  else
  {
    output.value = obj.toString()
  }
  output.scrollTop = output.scrollHeight
}

// Here follows VimanaCode definitions of UI functions (using embedded JavaScript)
VimanaUI.bootstrap =
`
/-- Redefine print --/
(print)
{
  const obj = interp.popStack()
  console.log(obj)
  VimanaUI.print(obj)
}
defprim

/-- Redefine printstack --/
(printstack)
{
  VimanaUI.print("STACK: " + interp.prettyPrintStack())
}
defprim

/-- Define eval command --/
(commandEvalAsync) (
  {
    try
    {
      const textarea = document.getElementsByTagName("textarea")[0]

      let code = textarea.value
      if (textarea.selectionStart < textarea.selectionEnd)
      {
        code = code.substring(textarea.selectionStart, textarea.selectionEnd)
      }

      const list = VimanaUI.interp.parse(code)

      VimanaUI.interp.evalAsync(
        list,
        function()
        {
          console.log("FOOBAR")
          VimanaUI.print("[" + VimanaUI.interp.prettyPrintStack().trim() + "]")
        })
    }
    catch (exception)
    {
      console.log("EXCEPTION:")
      console.log(exception)
      VimanaUI.print(exception)
    }
  } evalJS
) def

/-- Define eval command --/
(commandEvalAsync) (
  {
    try
    {
      const textarea = document.getElementsByTagName("textarea")[0]

      let code = textarea.value
      if (textarea.selectionStart < textarea.selectionEnd)
      {
        code = code.substring(textarea.selectionStart, textarea.selectionEnd)
      }

      const list = VimanaUI.interp.parse(code)

      VimanaUI.interp.evalAsync(
        list,
        function()
        {
          console.log("FOOBAR")
          VimanaUI.print("[" + VimanaUI.interp.prettyPrintStack().trim() + "]")
        })
    }
    catch (exception)
    {
      console.log("EXCEPTION:")
      console.log(exception)
      VimanaUI.print(exception)
    }
  } evalJS
) def
`
// End of VimanaUI bootstrap code

// Run UI bootstrap code
console.log('hi1')
VimanaDefinePrimFuns(VimanaUI.interp)
VimanaUI.interp.evalString("{hi2} inspect")
VimanaUI.interp.evalString("{console.log('hi3')} evalJS 42 (foo) setglobal foo print")

VimanaUI.interp.evalString(VimanaUI.bootstrap)
console.log(VimanaUI.interp)
