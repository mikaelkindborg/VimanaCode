//
// File: prettyprint.js
// Pretty printer
// Copyright (c) 2021-2022 Mikael Kindborg
// mikael@kindborg.com
//

class PrettyPrint
{
  prettyPrint(obj)
  {
    this.prettyPrintIndent = 0
    this.prettyNumItemsPrinted = 0
    return this.prettyPrintObj(obj)
  }

  // Returns string
  prettyPrintObj(obj)
  {
    if ("object" === typeof (obj))
    {
      return this.prettyPrintList(obj)
    }
    else if ("function" === typeof (obj))
    {
      return this.getPrimFunName(obj)
    }
    else if ("symbol" === typeof (obj))
    {
      return Symbol.keyFor(obj)
    }
    else if ("string" === typeof (obj))
    {
      return "{" + obj + "}"
    }
    else
    {
      return obj.toString()
    }
  }

  // Returns string
  prettyPrintList(list)
  {
    let string = "("
    let item = list

    if (this.prettyNumItemsPrinted > 2)
    {
      this.prettyNumItemsPrinted = 0
      this.prettyPrintIndent += 2
      let indent = " ".repeat(this.prettyPrintIndent)
      string = "\n" + indent + "("
    }

    while (item != null)
    {
      this.prettyNumItemsPrinted ++
      this.prettyPrintListEnd = false

      string += this.prettyPrintObj(item.car)

      item = item.cdr

      if (item != null)
      {
        string += " "
      }
    }

    string += ")"

    if (this.prettyPrintIndent > 0)
    {
      this.prettyNumItemsPrinted = 0
      this.prettyPrintIndent -= 2
    }
/*
    let indent = " ".repeat(this.prettyPrintIndent)

    if (this.prettyPrintListEnd)
    {
      string += "\n" + indent
    }

    this.prettyPrintListEnd = true
*/
    return string
  }

  // Returns string
  prettyPrintArray(stack)
  {
    let string = ""

    if (stack.length <= 0)
    {
      string = "EMPTY"
    }
    else
    {
      for (let i = 0; i < stack.length; ++ i)
      {
        string += this.prettyPrint(stack[i]) + " "
      }
    }

    return string
  }
}
