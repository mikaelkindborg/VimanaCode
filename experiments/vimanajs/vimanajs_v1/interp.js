//
// File: interp.js
// Vimana interpreter
// Copyright (c) 2021-2022 Mikael Kindborg
// mikael@kindborg.com
//

// TODO: lists, strings, token symbols, symbol table, canvas object, save cards, jsbenchmark

// INTERPRETER --------------------------------------------

class VimanaInterp
{
  // INTERPRETER DATA STRUCTURES --------------------------

  constructor()
  {
    this.primFuns = {} // primitive functions
    this.primFunSymbols = {} // primfun symbol lookup
    this.globalVars = {} // global vars
    this.dataStack = [] // data stack
    this.stackFrame = null // top stackframe of callstack
    this.speed = 0 // millisecond delay in eval loop

    // TODO: Make prettyprint class
    this.prettyPrintIndent = 0
    this.prettyNumItemsPrinted = 0
    this.prettyPrintListEnd = false
  }

  // DEFINE PRIMITIVE FUNCTION ----------------------------

  // Define a primitive function
  // Symbols map to functions
  defPrimFun(name, fun)
  {
    let symbol = Symbol.for(name)
    this.primFuns[symbol] = fun
    this.primFunSymbols[fun] = symbol
  }

  // Return string name for primfun function object
  getPrimFunName(primFun)
  {
    return Symbol.keyFor(this.primFunSymbols[primFun])
  }

  // Get primfun by string name
  getPrimFun(name)
  {
    return this.primFuns[Symbol.for(name)]
  }

  // Check if string name is primfun
  isPrimFun(name)
  {
    return Symbol.for(name) in this.primFuns
  }

  // DATA STACK -------------------------------------------

  // Push item onto the data stack
  pushStack(obj)
  {
    this.dataStack.push(obj)
  }

  // Pop the data stack
  popStack()
  {
    if (0 === this.dataStack.length)
    {
      this.guruMeditation("DATASTACK IS EMPTY")
    }

    return this.dataStack.pop()
  }

  // CALLSTACK --------------------------------------------

  pushStackFrame(list)
  {
    // Check tail call
    if (null === this.stackFrame.car)
    {
      // Tailcall: Reuse current stackframe
      this.stackFrame.car = list
    }
    else
    {
      // Push new stackframe
      this.stackFrame = { car: list, cdr: this.stackFrame }
    }
  }

  popStackFrame()
  {
    if (null === this.stackFrame)
    {
      this.guruMeditation("CALLSTACK IS EMPTY")
    }

    this.stackFrame = this.stackFrame.cdr
  }

  // GLOBAL VARIABLES -------------------------------------

  // Lookup symbol in global environment
  getGlobalVar(symbol)
  {
    if (symbol in this.globalVars)
    {
      return this.globalVars[symbol]
    }
    else
    {
      return null
    }
  }

  // Set global variable
  setGlobalVar(symbol, value)
  {
    this.globalVars[symbol] = value
  }

  // INTERPRETER LOOP (EVAL) ------------------------------

  // Eval a string
  evalString(string)
  {
    this.eval(this.parse(string))
  }

  // Eval a list
  eval(list)
  {
    // Obtain previous stackframe (may be null)
    const stackFrame = this.stackFrame

    // Push new stackframe
    this.stackFrame = { car: list, cdr: this.stackFrame }

    // Eval loop
    while (this.stackFrame && (stackFrame !== this.stackFrame))
    {
      this.evalSlice()
    }
  }

  // Eval driven by a timer, which is slower but more resource friendly
  evalAsync(list, doneFun = null)
  {
    // Obtain previous stackframe (may be null)
    const stackFrame = this.stackFrame

    //console.log(JSON.stringify(stackFrame))

    // Push new stackframe
    this.stackFrame = { car: list, cdr: this.stackFrame }

    // For runTimer
    const interp = this

    // Enter eval loop
    runTimer()

    // TODO: Use one timer, isRunning flag?

    function runTimer()
    {
      if (interp.stackFrame && (stackFrame !== interp.stackFrame))
      {
        //console.log(JSON.stringify(interp.stackFrame))
        interp.evalSlice()
        setTimeout(runTimer, interp.speed)
      }
      else
      {
        if (doneFun) doneFun()
      }
    }
  }

  // Eval next instruction
  evalSlice()
  {
    //console.log(JSON.stringify(this.stackFrame))
    const stackFrame = this.stackFrame
    const instruction = stackFrame.car

    // Evaluate current instruction
    if (null !== instruction)
    {
      // Advance instruction pointer for next loop
      stackFrame.car = instruction.cdr

      /*
      // Optimization of primfuns - instruction value is a Function
      if ("function" === typeof (instruction.car))
      {
        // Call primfun
        instruction.car(this)
      }
      else
      */
      if ("symbol" === typeof (instruction.car))
      {
        // Check if primfun
        const primFun = this.primFuns[instruction.car]
        if (primFun)
        {
          // Call primfun
          primFun(this)
        }
        else
        {
          // Check if globalvar
          const value = this.getGlobalVar(instruction.car)
          if (value != null)
          {
            // TODO? if JavaScript function ... (use primfun table instead)

            // Check if Vimana function
            if ("fun" === value.type)
            {
              // Call function
              this.pushStackFrame(value)
            }
            else
            {
              // Push value onto data stack
              this.pushStack(value)
            }
          }
        }
      }
      else
      {
        // Push literal onto data stack
        this.pushStack(instruction.car)
      }
    }
    else
    {
      // End of instruction list - pop stackframe
      this.popStackFrame()
    }
  }

  // PARSING ----------------------------------------------

  parse(string)
  {
    return new VimanaParser().parse(string, this)
  }

  // ERROR HANDLING ---------------------------------------

  // Error handling is simple - an error aborts execution

  guruMeditation(message)
  {
    console.trace()
    console.log(this)

    const meditation = "Guru Meditation: " + message
    throw meditation
  }

  mustBeList(list, message)
  {
    if ("object" != typeof (list))
    {
      this.guruMeditation(message)
    }
  }

  // PRINTING ---------------------------------------------

  prettyPrint(obj)
  {
    return new PrettyPrint().prettyPrint(obj);
  }

  prettyPrintStack()
  {
    return new PrettyPrint().prettyPrintArray(this.dataStack);
  }
}
