//
// File: workbench.js
// Vimana workbench UI
// Copyright (c) 2021-2022 Mikael Kindborg
// mikael@kindborg.com
//

class VimanaUI
{
  constructor()
  {
    // Create interpeter
    this.interp = new VimanaInterp()

    // Create primitive functions
    VimanaDefinePrimFuns(this.interp)

    // Show editor
    this.showCardFront = true
    this.commandToggleEditor()
  }

  readCards()
  {
    // Create interpeter
    this.interp = new VimanaInterp()

    // Create primitive functions
    VimanaDefinePrimFuns(this.interp)

    // Show editor
    this.showCardFront = true
    this.commandToggleEditor()

    // Set master program
    let codeCards = localStorage.getItem("vimana-code-cards")
    if (!codeCards)
    {
      codeCards = document.querySelector(".vimana-code-cards").textContent
    }
    //this.interp.evalString("0 showCard")
    this.interp.evalString("{Hi World} print")

    // Read cards from local storage
    //this.cards = this.readCards()

    //let cardCode = "(VimanaCards) ( (home (home card) print) (addcard 1 2 + print) ) defval"
    //let list = this.parser.parse(cardCode, this.interp)
    //this.interp.eval(list)

    //this.setCardCode("(VimanaCards) getglobal print")

  }

/*
Det som behövs är att hämta kod till korten
Sätt en global med en lista med kod för korten
Typ (VimanaCards) ((card1 ...) (card2 ...)) defval
Denna källkod sparas i localStorage och kan redigeras.
*/

  XreadCards()
  {
    // Read cards from local storage
    let data = localStorage.getItem("VimanaCards")
    if (data)
    {
      return data
    }
    else
    {
      return ""
    }
  }

  saveCards(data)
  {
    localStorage.setItem("VimanaCards", data)
  }

  eval(code)
  {
    this.interp.evalString(code)
  }

  commandToggleEditor()
  {
    let front = document.querySelector(".vimana-card-front")
    let editor = document.querySelector(".vimana-card-editor")
    let button = document.querySelector(".vimana-toggle-editor")

    this.showCardFront = ! this.showCardFront

    if (this.showCardFront)
    {
      front.style.display = "block"
      editor.style.display = "none"
      button.innerHTML = "Show Editor"
    }
    else
    {
      front.style.display = "none"
      editor.style.display = "block"
      button.innerHTML = "Show Card"
    }
  }

  commandPrint(obj)
  {
    let output = document.getElementsByTagName("textarea")[1]
    if (output.value.length > 0)
    {
      output.value = output.value + "\n" + obj.toString()
    }
    else
    {
      output.value = obj.toString()
    }
    //output.insertAdjacentHTML("beforeend", obj.toString() + "\n")
    output.scrollTop = output.scrollHeight;
  }

  commandPrettyPrint(obj)
  {
    this.commandPrint(this.interp.prettyPrint(obj));
  }

  commandPrintStack()
  {
    this.commandPrint("STACK: " + this.interp.prettyPrintStack())
  }

  commandPrintException(exception)
  {
    this.commandPrint(exception)

    /*
    // TODO: Fix this code!
    let interp = window.VimanaCode
    let context = interp.callstack[interp.callstackIndex]
    if (context && context.codePointer)
    {
      let index = context.codePointer
      let array = Array.from(context.code.items)
      array.splice(index, 0, "ERROR HERE >>>>")
      VimanaUIPrint("CODE: " + JSON.stringify(array))
      VimanaUIPrint("CONTEXT: " + JSON.stringify(context))
    }
    */
  }

  commandEvalAsync()
  {
    try
    {
      let textarea = document.getElementsByTagName("textarea")[0]

      let code = textarea.value
      if (textarea.selectionStart < textarea.selectionEnd)
      {
        code = code.substring(textarea.selectionStart, textarea.selectionEnd)
      }

      let list = this.interp.parse(code)
      let ui = this // for the closure below
      this.interp.evalAsync(
        list,
        function()
        {
          ui.commandPrintStack()
        })
    }
    catch (exception)
    {
      console.log("EXCEPTION:")
      console.log(exception)
      this.commandPrintException(exception)
    }
  }

  commandEval()
  {
    try
    {
      let textarea = document.getElementsByTagName("textarea")[0]

      let code = textarea.value
      if (textarea.selectionStart < textarea.selectionEnd)
      {
        code = code.substring(textarea.selectionStart, textarea.selectionEnd)
      }

      let list = this.interp.parse(code)
      this.interp.eval(list)
      this.commandPrintStack()
    }
    catch (exception)
    {
      console.log("EXCEPTION:")
      console.log(exception)
      this.commandPrintException(exception)
    }
  }

  commandSaveCard()
  {
    let codeArea = document.getElementsByTagName("textarea")[0]
    localStorage.setItem("vimana-master-program", codeArea.value)
    this.commandPrint("Master Program Saved")
  }

  commandResetCard()
  {
    let codeArea = document.getElementsByTagName("textarea")[0]
    let code = document.querySelector(".vimana-master-program").textContent
    codeArea.value = code
    this.commandPrint("Master Program Reset")
  }

  setCardCode(code)
  {
    let codeArea = document.getElementsByTagName("textarea")[0]
    codeArea.value = code
    codeArea.scrollTop = 0
  }

  commandClearStack()
  {
    this.interp.dataStack = []
    this.commandPrintStack()
  }

  commandClearOutput()
  {
    document.getElementsByTagName("textarea")[1].value = ""
  }

  commandListPrimFuns()
  {
    this.commandPrint(`BUILT-IN FUNCTIONS:`)
    const primFuns = this.interp.primFuns
    const symbols = Object.getOwnPropertySymbols(primFuns)
    for (const key in symbols)
    {
      this.commandPrint(Symbol.keyFor(symbols[key]))
    }
  }

  commandOpenGitHub()
  {
    window.location.href = "https://github.com/mikaelkindborg/VimanaCode"
  }

} // class

// Create global UI instance
window.TheVimanaUI = new VimanaUI()
